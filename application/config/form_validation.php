<?php
$config = array (
  'pemagang/upload_test_external' => array(
    array(
      'field' => 'kd_pemagang',
      'label' => 'Kode Pemagang',
      'rules' => 'required'
    ),
    array(
      'field' => 'tgl_test',
      'label' => 'Tanggal',
      'rules' => 'required'
    ),
    array(
      'field' => 'kd_asosiasi',
      'label' => 'Asosiasi',
      'rules' => 'required'
    ),
    array(
      'field' => 'kd_perusahaan',
      'label' => 'Perusahaan',
      'rules' => 'required'
    ),
    array(
      'field' => 'kd_bidang_usaha',
      'label' => 'Bidang Usaha',
      'rules' => 'required'
    ),
    array(
      'field' => 'hasil_test',
      'label' => 'Hasil Test',
      'rules' => 'required'
    ),
  ),
  'pemagang/upload_berkas' => array(
    array(
      'field' => 'nama_berkas',
      'label' => 'Nama berkas',
      'rules' => 'required'
    ),
    array(
      'field' => 'kd_pemagang',
      'label' => 'Pemagang',
      'rules' => 'required'
    )
  ),
  'pengumuman/add' => array(
    array(
      'field' => 'judul',
      'label' => 'judul',
      'rules' => 'required'
    ),
    array(
      'field' => 'text',
      'label' => 'text',
      'rules' => 'required'
    )
  ),
  'pengumuman/edit' => array(
    array(
      'field' => 'judul',
      'label' => 'judul',
      'rules' => 'required'
    ),
    array(
      'field' => 'text',
      'label' => 'text',
      'rules' => 'required'
    )
  ),
  'jurusan/add' => array(
    array(
      'field' => 'nama_jurusan',
      'label' => 'Nama Jurusan',
      'rules' => 'required'
    ),
    array(
      'field' => 'tingkat_pendidikan',
      'label' => 'Tingkat Pendidikan',
      'rules' => 'required'
    ),
  ),
  'jurusan/edit' => array(
    array(
      'field' => 'id_jurusan',
      'label' => 'ID Jurusan',
      'rules' => 'required'
    ),
    array(
      'field' => 'nama_jurusan',
      'label' => 'Nama Jurusan',
      'rules' => 'required'
    ),
    array(
      'field' => 'tingkat_pendidikan',
      'label' => 'Tingkat Pendidikan',
      'rules' => 'required'
    ),
  ),
  'magang/index' => array(
    array (
      'field' => 'ktp',
      'label' => 'No. KTP',
      'rules' => 'required'
    ),
    array (
      'field' => 'email',
      'label' => 'Email',
      'rules' => 'required'
    ),
    array (
      'field' => 'password',
      'label' => 'Password',
      'rules' => 'required'
    ),
  ),
  'magang/register' => array (
      array ( 'field' => 'nama_lengkap',
               'label' => 'Nama Lengkap',
              'rules' => 'required'
      ),
      array ( 'field' => 'tempat_lahir',
               'label' => 'Tempat Lahir',
              'rules' => 'required'
      ),
      array ( 'field' => 'tanggal_lahir',
               'label' => 'Tanggal Lahir',
              'rules' => 'required'
      ),
      array ( 'field' => 'bulan_lahir',
               'label' => 'Bulan Lahir',
              'rules' => 'required'
      ),
      array ( 'field' => 'tahun_lahir',
               'label' => 'Tahun Lahir',
              'rules' => 'required'
      ),
      array ( 'field' => 'jenis_kelamin',
               'label' => 'Jenis Kelamin',
              'rules' => 'required'
      ),
      array ( 'field' => 'status',
               'label' => 'status',
              'rules' => 'required'
      ),
      array ( 'field' => 'golongan_darah',
               'label' => 'Golongan Darah',
              'rules' => 'required'
      ),
      array ( 'field' => 'provinsi_sekarang',
               'label' => 'Provinsi',
              'rules' => 'required'
      ),
      array ( 'field' => 'agama',
               'label' => 'Agama',
              'rules' => 'required'
      ),
      array ( 'field' => 'tempat_asal',
               'label' => 'Tempat asal',
              'rules' => 'required'
      ),
      array ( 'field' => 'kewarganegaraan',
               'label' => 'kewarganegaraan',
              'rules' => 'required'
      ),
      array ( 'field' => 'alamat_ktp',
               'label' => 'Alamat Sesuai KTP',
              'rules' => 'required'
      ),
      array ( 'field' => 'nomor_handphone',
               'label' => 'Nomor HP',
              'rules' => 'required'
      ),
      array ( 'field' => 'ukuran_sepatu',
               'label' => 'Ukuran Sepatu',
              'rules' => 'required'
      ),
      array ( 'field' => 'lingkar_pinggang',
               'label' => 'Lingkar Pinggang',
              'rules' => 'required'
      ),
      array ( 'field' => 'berat_badan',
               'label' => 'Berat Badan',
              'rules' => 'required'
      ),
      array ( 'field' => 'tinggi_badan',
               'label' => 'Tinggi Badan',
              'rules' => 'required'
      ),
      array ( 'field' => 'pelajaran_favorit',
               'label' => 'Pelajaran Favorit',
              'rules' => 'required'
      ),
      array ( 'field' => 'hobi',
               'label' => 'Hobi',
              'rules' => 'required'
      ),
      array ( 'field' => 'keterampilan',
               'label' => 'Keterampilan / Skill',
              'rules' => 'required'
      ),
      array ( 'field' => 'merokok',
               'label' => 'Merokok',
              'rules' => 'required'
      ),
      array ( 'field' => 'minum_alkohol',
               'label' => '',
              'rules' => 'required'
      ),
      array ( 'field' => 'pernah_keluar_negri',
               'label' => 'Pernah Keluar Negri',
              'rules' => 'required'
      ),
      array ( 'field' => 'passport',
               'label' => 'Passport',
              'rules' => 'required'
      )
  ),

  'member/update_biodata' => array(
    array(
      'field' => 'kd_pemagang',
      'label' => 'Pemagang',
      'rules' => 'required'
    ),
    array(
      'field' => 'nama_pem',
      'label' => 'Nama Pemagang',
      'rules' => 'required'
    ),
    array(
      'field' => 'tempat_lahir',
      'label' => 'Tempat Lahir',
      'rules' => 'required'
    ),
    array(
      'field' => 'tempat_tinggal',
      'label' => 'Alamat Sekarang',
      'rules' => 'required'
    ),
    array(
      'field' => 'kd_provinsi',
      'label' => 'Provinsi',
      'rules' => 'required'
    ),
    array(
      'field' => 'no_hp',
      'label' => 'Nomor Handphone',
      'rules' => 'required'
    ),
    array(
      'field' => 'warga_negara',
      'label' => 'Warga Negara',
      'rules' => 'required'
    ),
  ),

  'member/update_passport' => array(
    array(
      'field' => 'kd_pemagang',
      'label' => 'Pemagang',
      'rules' => 'required'
    ),
    array(
      'field' => 'no_pass',
      'label' => 'Nomor Passport',
      'rules' => 'required'
    ),
    array(
      'field' => 'masa_berlaku',
      'label' => 'Masa Berlaku',
      'rules' => 'required'
    )
  ),
);

 ?>
