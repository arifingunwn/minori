<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/*
| -------------------------------------------------------------------------
| URI ROUTING
| -------------------------------------------------------------------------
| This file lets you re-map URI requests to specific controller functions.
|
| Typically there is a one-to-one relationship between a URL string
| and its corresponding controller class/method. The segments in a
| URL normally follow this pattern:
|
|	example.com/class/method/id/
|
| In some instances, however, you may want to remap this relationship
| so that a different class/function is called than the one
| corresponding to the URL.
|
| Please see the user guide for complete details:
|
|	https://codeigniter.com/user_guide/general/routing.html
|
| -------------------------------------------------------------------------
| RESERVED ROUTES
| -------------------------------------------------------------------------
|
| There are three reserved routes:
|
|	$route['default_controller'] = 'welcome';
|
| This route indicates which controller class should be loaded if the
| URI contains no data. In the above example, the "welcome" class
| would be loaded.
|
|	$route['404_override'] = 'errors/page_missing';
|
| This route will tell the Router which controller/method to use if those
| provided in the URL cannot be matched to a valid route.
|
|	$route['translate_uri_dashes'] = FALSE;
|
| This is not exactly a route, but allows you to automatically route
| controller and method names that contain dashes. '-' isn't a valid
| class or method name character, so it requires translation.
| When you set this option to TRUE, it will replace ALL dashes in the
| controller and method URI segments.
|
| Examples:	my-controller/index	-> my_controller/index
|		my-controller/my-method	-> my_controller/my_method
*/
$route['default_controller'] = 'magang';

$route['rcr/auth'] = 'auth';

$route['rcr/pemagang'] = 'pemagang';
$route['rcr/pemagang/index/(:any)'] = 'pemagang/index/$1';
$route['rcr/magang/add'] = 'pemagang/add';
$route['rcr/pemagang/riwayat_pekerjaan'] = 'pemagang/riwayat_pekerjaan';
$route['rcr/pemagang/server_side_ajax'] = 'pemagang/server_side_ajax';
$route['rcr/pemagang/show/(:any)'] = 'pemagang/show/$1';
$route['rcr/pemagang/edit/(:any)'] = 'pemagang/edit/$1';
$route['rcr/pemagang/test_internal/(:any)'] = 'pemagang/test_internal/$1';
$route['rcr/pemagang/import_test_internal'] = 'pemagang/import_test_internal';
$route['rcr/pemagang/save_import_test_internal'] = 'pemagang/save_import_test_internal';
$route['rcr/pemagang/print_biodata_pemagang/(:any)'] = 'pemagang/print_biodata_pemagang/$1';
$route['rcr/pemagang/upload_berkas'] = 'pemagang/upload_berkas';
$route['rcr/pemagang/upload_test_external'] = 'pemagang/upload_test_external';
$route['rcr/pemagang/export_to_excel'] = 'pemagang/export_to_excel';
$route['rcr/pemagang/blacklist'] = 'pemagang/blacklist';
$route['rcr/pemagang/pemagang_lulus'] = 'pemagang/pemagang_lulus';
$route['rcr/pemagang/lulus_pemagang/(:any)'] = 'pemagang/lulus_pemagang/$1';
$route['rcr/pemagang/ajax_form_riwayat_pekerjaan/(:any)'] = 'pemagang/ajax_form_riwayat_pekerjaan/$1';
$route['rcr/pemagang/ajax_form_riwayat_pendidikan/(:any)'] = 'pemagang/ajax_form_riwayat_pendidikan/$1';

$route['rcr/lowongan'] = 'lowongan';
$route['rcr/lowongan/add'] = 'lowongan/add';
$route['rcr/lowongan/recruit/(:any)'] = 'lowongan/recruit/$1';
$route['rcr/lowongan/show/(:any)'] = 'lowongan/show/$1';
$route['rcr/lowongan/nilai/(:any)'] = 'lowongan/nilai/$1';
$route['rcr/lowongan/edit/(:any)'] = 'lowongan/edit/$1';
$route['rcr/lowongan/delete/(:any)'] = 'lowongan/delete/$1';
$route['rcr/lowongan/remove_recruit'] = 'lowongan/remove_recruit';
$route['rcr/lowongan/test/(:any)'] = 'lowongan/test/$1';
$route['rcr/lowongan/setNilai'] = 'lowongan/setNilai';

$route['rcr/user'] = 'user';
$route['rcr/user/edit/(:any)'] = 'user/edit/$1';
$route['rcr/user/delete/(:any)'] = 'user/delete/$1';
$route['rcr/user/add_authorization'] = 'user/add_authorization';
$route['rcr/user/delete_authorization'] = 'user/delete_authorization';
$route['rcr/user/set_authorization'] = 'user/set_authorization';

$route['rcr/pengumuman'] = 'pengumuman';
$route['rcr/pengumuman/index'] = 'pengumuman/index';
$route['rcr/pengumuman/add'] = 'pengumuman/add';
$route['rcr/pengumuman/show/(:any)'] = 'pengumuman/show/$1';
$route['rcr/pengumuman/edit/(:any)'] = 'pengumuman/edit/$1';
$route['rcr/pengumuman/delete/(:any)'] = 'pengumuman/delete/$1';

$route['rcr/pekerjaan'] = 'Jenis_Pekerjaan';

$route['rcr/jurusan'] = 'jurusan';
$route['rcr/jurusan/process'] = 'jurusan/process';

$route['rcr/bahasa'] = 'bahasa';

$route['404_override'] = '';
$route['translate_uri_dashes'] = FALSE;
