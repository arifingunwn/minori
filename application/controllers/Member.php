<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Member extends CI_Controller {

	function __construct()
	{
			parent::__construct();
			$this->load->library('form_validation');
			$this->load->library('session');
			$this->load->helper('my_helper');

			if(!empty($_SESSION['member']))
			{
				redirect('magang');
			}

      $this->load->database();
			$this->load->model('Pemagang_model', 'pemagang');
	}

  public function index()
  {
		var_dump($_SESSION['authorization']);
		$q = $this->db->get_where('tb_lowongan', array(
			'status' => 'T'
		));
		$data['lowongan'] = $q->result_array();
		$q = $this->db->get('tb_pengumuman');
		$data['pengumuman'] = $q->result_array();
    $this->load->view('layout/header');
    $this->load->view('member/index', $data);
    $this->load->view('layout/footer');
  }

	public function apply_lowongan()
	{
		$email = $this->input->post('email');
		$kd_lowongan = $this->input->post('kd_lowongan');

		$kd_pemagang = $this->db->get_where('tb_pemagangan', array(
			'email' => $email
		))->row('kd_pemagang');

		$row = $this->db->get_where('tb_pendaftar', array(
			'id_pemagang' => $kd_pemagang,
			'id_lowongan' => $kd_lowongan
		));

		if($row->num_rows() <= 0)
		{
			$this->db->insert('tb_pendaftar', array(
				'id_pemagang' => $kd_pemagang,
				'id_lowongan' => $kd_lowongan
			));
			echo json_encode('success');
		}
		else {
			echo json_encode('duplicate');
		}
	}

	public function ubah_data_diri()
	{
		$email = $this->session->userdata('member');
		$kd_pemagang = $this->db->get_where('tb_pemagangan', array(
			'email' => $email
		))->row('kd_pemagang');

		$this->db->select('P.*, PROV.provinsi');
		$this->db->from('tb_pemagangan P');
		$this->db->join('tb_provinsi PROV', 'P.kd_provinsi = PROV.kd_provinsi');
		$this->db->where('P.kd_pemagang', $kd_pemagang);
		$data['pemagang'] = $this->db->get()->row();

		$data['hubungan'] = $this->db->get('tb_hubungan')->result_array();

		$data['provinsi'] = $this->db->get('tb_provinsi')->result_array();

		$data['passport'] = $this->db->get_where('tb_pasport', array(
			'kd_pemagang' => $kd_pemagang
		))->row();

		$this->db->select(array('H.hubungan', 'K.nama_kel', 'K.tgl', 'K.pekerjaan', 'K.no_telp'));
		$this->db->from('tb_keluarga K');
		$this->db->join('tb_hubungan H', 'H.kd_hubungan = K.kd_stat_kel', 'left');
		$this->db->where('nik', $kd_pemagang);
		$data['keluarga'] = $this->db->get()->result_array();

		$this->db->select(array('H.hubungan', 'K.nama', 'K.tanggal_lahir', 'K.pekerjaan', 'K.hp'));
		$this->db->from('tb_keluarga_pem K');
		$this->db->join('tb_hubungan H', 'H.kd_hubungan = K.hubungan', 'left');
		$this->db->where('kd_pemagang', $kd_pemagang);
		$data['keluarga_pem'] = $this->db->get()->result_array();

		$this->db->select(array('RP.*', 'PKJ.pekerjaan'));
		$this->db->from('tb_riwayat_pekerjaan RP');
		$this->db->join('tb_pekerjaan PKJ', 'RP.posisi = PKJ.kd_pekerjaan');
		$this->db->where('nik', $kd_pemagang);
		$data['riwayat_pekerjaan'] = $this->db->get()->result_array();

		$this->db->select(array('RP.*', 'J.nama_jurusan'));
		$this->db->from('dt_riwayat_pendidikan RP');
		$this->db->join('ms_jurusan_pendidikan J', 'RP.id_jurusan = J.id_jurusan', 'left');
		$this->db->where('RP.kd_pemagang', $kd_pemagang);
		$data['riwayat_pendidikan'] = $this->db->get()->result_array();

		$this->load->view('layout/header');
    $this->load->view('member/ubah_data_diri', $data);
    $this->load->view('layout/footer');
	}

	public function update_biodata()
	{
		$this->config->load('form_validation', TRUE);
		if($this->form_validation->run() === TRUE)
		{
				$this->pemagang->update_member_biodata();
				redirect('member/ubah_data_diri');
		}

		echo "Terjadi Kesalahan";
	}

	public function update_passport()
	{
		$this->config->load('form_validation', TRUE);
		if($this->form_validation->run() === TRUE)
		{
				$this->pemagang->update_member_passport();
				redirect('member/ubah_data_diri');
		}

		echo "Terjadi Kesalahan";
	}

	public function update_password()
	{
		$this->form_validation->set_rules('kd_pemagang', 'Pemagang', 'required');
		$this->form_validation->set_rules('password', 'Password', 'required');
		$this->form_validation->set_rules('password_confirmation', 'Password', 'required|matches[password]');
		if($this->form_validation->run() === TRUE)
		{
			$this->pemagang->update_member_password();
			redirect('member/ubah_data_diri');
		}
	}
}
