<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Pengumuman extends MY_Controller {
	protected $access = 'admin';
	function __construct()
	{
			parent::__construct();
			$this->load->library('form_validation');
			$this->load->library('session');
			$this->load->helper('my_helper');
			$this->load->model('Pengumuman_model', 'pengumuman');
			$this->load->database();

			if(empty($_SESSION['authorization']['PENGUMUMAN'])){
				http_response_code(403);
				echo "Forbidden";
				die();
			}
	}

	public function index()
	{
    $data['pengumuman'] = $this->pengumuman->get();
		$this->load->view('rcr/layout/header');
		$this->load->view('rcr/pengumuman/index', $data);
		$this->load->view('rcr/layout/footer');
	}

	public function show($kd = NULL)
	{
		if($kd == NULL)
		{
			redirect('rcr/pengumuman');
		}
    $data['pengumuman'] = $this->pengumuman->get_row($kd);
		$this->load->view('rcr/layout/header');
		$this->load->view('rcr/pengumuman/show', $data);
		$this->load->view('rcr/layout/footer');
	}

  public function add()
  {
    $this->config->load('form_validation', TRUE);

    if($this->form_validation->run() === TRUE)
    {
        $q = $this->pengumuman->save();

				if($q)
				{
					$this->session->set_flashdata('flag', 'Sukses Tambah Pengumuman');
				}
				else {
					$this->session->set_flashdata('flag', 'Gagal Tambah Pengumuman');
				}

        redirect('rcr/pengumuman');
    }

		$this->load->view('rcr/layout/header');
		$this->load->view('rcr/pengumuman/add');
		$this->load->view('rcr/layout/footer');
  }

  public function edit($kd = NULL)
  {
		if($kd == NULL)
		{
			redirect('rcr/pengumuman');
		}

    $this->config->load('form_validation', TRUE);

		if($this->form_validation->run() === TRUE)
		{
			$q = $this->pengumuman->update($kd);

			if($q)
			{
				$this->session->set_flashdata('flag', 'Sukses Update Pengumuman');
			}
			else
			{
				$this->session->set_flashdata('flag', 'Gagal update Pengumuman');
			}

			redirect('rcr/pengumuman');
		}

		$data['pengumuman'] = $this->pengumuman->get_row($kd);
		$data['kd_pemagang'] = $kd;
		$this->load->view('rcr/layout/header');
		$this->load->view('rcr/pengumuman/edit', $data);
		$this->load->view('rcr/layout/footer');
  }

  public function delete($kd = NULL)
  {
		if($kd == NULL)
		{
			redirect('rcr/pengumuman');
		}
		$this->config->load('form_validation', TRUE);
		$q = $this->pengumuman->delete($kd);

		if($q)
		{
			$this->session->set_flashdata('flag', 'Sukses Hapus Pengumuman');
		}
		else
		{
			$this->session->set_flashdata('flag', 'Gagal Hapus Pengumuman');
		}

		redirect('rcr/pengumuman');
  }
}
