<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Bahasa extends MY_Controller {
	protected $access = 'admin';

	function __construct()
	{
			parent::__construct();
			$this->load->library('form_validation');
			$this->load->library('session');
			$this->load->helper('my_helper');
      $this->load->model('Rcr_model');
      $this->load->database();

			if(empty($_SESSION['authorization']['TRANSLATE'])){
				http_response_code(403);
				echo "Forbidden";
				die();
			}
	}

  public function index()
  {
		$data['bahasa'] = $this->db->get('translation_JP')->result_array();
    $this->load->view('rcr/layout/header', $this->user_auth);
    $this->load->view('rcr/bahasa/index', $data);
    $this->load->view('rcr/layout/footer');
  }

  public function add()
  {
    $this->form_validation->set_rules('indonesia', 'Kata Indonesia', 'required');
    $this->form_validation->set_rules('jepang', 'Kata Jepang', 'required');

    if($this->form_validation->run() === true)
    {
      $this->db->insert('translation_JP', array(
        'indonesia' => $this->input->post('indonesia'),
        'jepang' => $this->input->post('jepang')
      ));
    }
    redirect('rcr/bahasa');
  }

  public function edit($indonesia)
  {

  }

  public function delete($indonesia)
  {

  }
}

?>
