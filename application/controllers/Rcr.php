<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Rcr extends MY_Controller {

	protected $access = 'admin';
	function __construct()
	{
			parent::__construct();
			$this->load->library('form_validation');
			$this->load->library('session');
			$this->load->helper('my_helper');
			$this->load->model('Rcr_model');
      $this->load->database();

			// if(empty($_SESSION['authorization']['RCR'])){
			// 	http_response_code(403);
			// 	echo "Forbidden";
			// 	die();
			// }
	}

  public function index()
  {
		$data['new_pemagang'] = $this->Rcr_model->get_pemagang_monthly_statistic();
		$data['new_pemagang_per_day'] = $this->Rcr_model->get_pemagang_daily_statistic();
    $this->load->view('rcr/layout/header');
    $this->load->view('rcr/index', $data);
    $this->load->view('rcr/layout/footer');
  }

	// public function user()
	// {
	// 	$user = $this->db->get('ms_user');
	// 	$data['user'] = $user->result_array();
	//
	// 	$this->load->view('rcr/layout/header', $this->user_auth);
  //   $this->load->view('rcr/user/index', $data);
  //   $this->load->view('rcr/layout/footer');
	// }
	//
	// public function add_user()
	// {
	// 	$this->form_validation->set_rules('username', 'Username', 'required|min_length[5]');
	// 	$this->form_validation->set_rules('password', 'Password', 'required');
	// 	$this->form_validation->set_rules('passconf', 'Password Confirmation', 'required|matches[password]');
	//
	// 	if($this->form_validation->run() == TRUE)
	// 	{
	// 		$q = $this->Rcr_model->add_new_user(array(
	// 			'username' => $this->input->post('username'),
	// 			'password' => $this->input->post('password')
	// 		));
	//
	// 		if($q)
	// 		{
	// 			$this->session->set_flashdata('flag', 'Sukses Tambah User');
	// 		}
	// 		else
	// 		{
	// 			$this->session->set_flashdata('flag', 'Gagal Tambah User');
	// 		}
	// 	}
	// 	redirect('rcr/user');
	// }
	//
	// public function edit_user($username = NULL)
	// {
	// 	$this->form_validation->set_rules('username', 'Username', 'required|min_length[5]');
	// 	$this->form_validation->set_rules('password', 'Password', 'required');
	// 	$this->form_validation->set_rules('passconf', 'Password Confirmation', 'required|matches[password]');
	//
	// 	if($this->form_validation->run() == TRUE)
	// 	{
	// 		$q = $this->Rcr_model->update_user(array(
	// 			'username' => $this->input->post('username'),
	// 			'password' => $this->input->post('password')
	// 		));
	//
	// 		if($q)
	// 		{
	// 			$this->session->set_flashdata('flag', 'Sukses Update User');
	// 		}
	// 		else
	// 		{
	// 			$this->session->set_flashdata('flag', 'Gagal Update User');
	// 		}
	//
	// 		redirect('rcr/user');
	// 	}
	//
	// 	$data['user'] = $this->db->get_where('ms_user', array('username' => $username))->row();
	// 	$this->load->view('rcr/layout/header', $this->user_auth);
  //   $this->load->view('rcr/user/edit', $data);
  //   $this->load->view('rcr/layout/footer');
	// }

	public function download_berkas($kd = NULL)
	{
		if($kd == NULL)
		{
			redirect('rcr/');
		}
		$q = $this->db->get_where('tb_berkas', array(
			'kd' => $kd
		));

		$file_name = $q->row('berkas');
		$path = $_SERVER['DOCUMENT_ROOT'].'/minori_beta/data/berkas/'.$file_name;
		header("Content-Disposition: attachment; filename=\"$file_name\"");
		header('Expires: 0');
		header('Cache-Control: must-revalidate, post-check=0, pre-check=0');
		header('Cache-Control: private', false);
		header('Content-Transfer-Encoding: binary');
		header('Content-Length: ' . filesize($path));
		readfile($path);
	}

	public function download_test_external($kd = NULL)
	{
		if($kd == NULL)
		{
			redirect('rcr/');
		}
		$q = $this->db->get_where('test_external', array(
			'kd_test_external' => $kd
		));

		$file_name = $q->row('file');
		$path = $_SERVER['DOCUMENT_ROOT'].'/minori_beta/data/test_external/'.$file_name;
		header("Content-Disposition: attachment; filename=\"$file_name\"");
		header('Expires: 0');
		header('Cache-Control: must-revalidate, post-check=0, pre-check=0');
		header('Cache-Control: private', false);
		header('Content-Transfer-Encoding: binary');
		header('Content-Length: ' . filesize($path));
		readfile($path);
	}

	public function delete_berkas($kd = NULL)
	{
		if($kd == NULL)
		{
			redirect('rcr/');
		}

		$q = $this->db->get_where('tb_berkas', array(
			'kd' => $kd
		));

		$file_name = $q->row('berkas');
		$path = $_SERVER['DOCUMENT_ROOT'].'/minori_beta/data/berkas/'.$file_name;
		$flag = unlink($path);

		if($flag)
		{
			$this->db->delete('tb_berkas', array(
				'kd' => $kd
			));
			$this->session->set_flashdata('flag', 'Sukses Hapus berkas');
		}
		else
		{
			$this->session->set_flashdata('flag', 'Gagal Hapus Berkas');
		}
		header('Location: ' . $_SERVER['HTTP_REFERER']);
	}

	public function delete_test_external($kd = NULL)
	{
		if($kd == NULL)
		{
			redirect('rcr/');
		}

		$q = $this->db->get_where('test_external', array(
			'kd_test_external' => $kd
		));

		$file_name = $q->row('file');
		$path = $_SERVER['DOCUMENT_ROOT'].'/minori_beta/data/test_external/'.$file_name;
		$flag = unlink($path);

		if($flag)
		{
			$this->db->delete('test_external', array(
				'kd_test_external' => $kd
			));
			$this->session->set_flashdata('flag', 'Sukses Hapus berkas');
		}
		else
		{
			$this->session->set_flashdata('flag', 'Gagal Hapus berkas');
		}
		header('Location: ' . $_SERVER['HTTP_REFERER']);
	}
}
?>
