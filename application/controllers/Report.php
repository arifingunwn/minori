<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Report extends MY_Controller {
	protected $access = 'admin';
	function __construct()
	{
			parent::__construct();
			$this->load->library('form_validation');
			$this->load->library('session');
			$this->load->helper('my_helper');
      $this->load->model('Pemagang_model', 'pemagang');
      $this->load->database();

			if(empty($_SESSION['authorization']['USER'])){
				http_response_code(403);
				echo "Forbidden";
				die();
			}
	}

  public function index()
  {
		echo 'There is nothing here.';
  }

  public function recruited_pemagang($id = NULL)
  {
    if($id == NULL)
    {
      redirect('/');
    }

    $q = $this->pemagang->get_recruited_pemagang($id);

    $this->db->select('judul');
    $this->db->from('tb_lowongan');
    $this->db->where('kd', $id);

    $judul = $this->db->get()->result_array();


    $data['pemagang'] = $q;
    $data['title'] = 'Laporan Recruit';
    $data['report_type'] = 'LAPORAN PEMAGANG RECRUIT - ' . $judul[0]['judul'];
    $this->load->view('rcr/report/pemagang', $data);
  }

	public function to_be_tested_pemagang($id = NULL)
  {
    if($id == NULL)
    {
      redirect('/');
    }

    $q = $this->pemagang->get_to_be_tested_pemagang($id);

    $this->db->select('judul');
    $this->db->from('tb_lowongan');
    $this->db->where('kd', $id);

    $judul = $this->db->get()->result_array();


    $data['pemagang'] = $q;
    $data['title'] = 'Laporan Recruit';
    $data['report_type'] = 'LAPORAN PEMAGANG YANG AKAN DI TEST - ' . $judul[0]['judul'];
    $this->load->view('rcr/report/pemagang', $data);
  }

	public function hasil_test_pemagang($id = NULL)
	{
		if($id == NULL)
    {
      redirect('/');
    }

    $q = $this->pemagang->get_hasil_test_pemagang($id);

    $this->db->select('judul');
    $this->db->from('tb_lowongan');
    $this->db->where('kd', $id);

    $judul = $this->db->get()->result_array();


    $data['pemagang'] = $q;
    $data['title'] = 'Laporan Recruit';
    $data['report_type'] = 'HASIL TEST LOWONGAN - ' . $judul[0]['judul'];
    $this->load->view('rcr/report/hasil_test', $data);
	}
}
