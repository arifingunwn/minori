<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Jurusan extends MY_Controller {
	protected $access = 'admin';
	function __construct()
	{
			parent::__construct();
			$this->load->library('form_validation');
			$this->load->library('session');
			$this->load->helper('my_helper');
      $this->load->database();

			if(empty($_SESSION['authorization']['JURUSPEND'])){
				http_response_code(403);
				echo "Forbidden";
				die();
			}
	}

  public function index()
  {
    $tingkat_pendidikan = $this->db->get('ms_tingkatan_pendidikan');

    $this->db->select('*');
    $this->db->from('ms_jurusan_pendidikan J');
    $this->db->join('ms_tingkatan_pendidikan T', 'J.id_tingkatan_pendidikan = T.id');
    $jurusan = $this->db->get();

    $data['tingkat_pendidikan'] = $tingkat_pendidikan->result_array();
    $data['jurusan'] = $jurusan->result_array();
		$this->load->view('rcr/layout/header', $this->user_auth);
    $this->load->view('rcr/jurusan/index', $data);
    $this->load->view('rcr/layout/footer');
  }

  public function process()
  {
    $action = $this->input->post('type');
    $this->form_validation->set_rules('nama_jurusan', 'Nama Jurusan', 'required');
    $this->form_validation->set_rules('tingkat_pendidikan', 'Tingkat Pendidikan', 'required');

    if($action == 'ADD')
    {
      if($this->form_validation->run() == TRUE)
      {
        $array = array(
          'id_tingkatan_pendidikan' => $this->input->post('tingkat_pendidikan'),
          'nama_jurusan'          	=> $this->input->post('nama_jurusan'),
					'nama_jurusan_jp'         => $this->input->post('nama_jurusan_jp')
        );
        $q = $this->db->insert('ms_jurusan_pendidikan', $array);
				if($q)
				{
					$this->session->set_flashdata('flag', 'Sukses tambah jurusan');
				}
				else
				{
					$this->session->set_flashdata('flag', 'Gagal tambah jurusan');
				}
      }
    }

    if($action == 'EDIT')
    {
      $this->form_validation->set_rules('id_jurusan', 'ID Jurusan', 'required');
      if($this->form_validation->run() == TRUE)
      {
        $array = array(
          'id_jurusan'              => $this->input->post('id_jurusan'),
          'id_tingkatan_pendidikan' => $this->input->post('tingkat_pendidikan'),
          'nama_jurusan'            => $this->input->post('nama_jurusan'),
					'nama_jurusan_jp'            => $this->input->post('nama_jurusan_jp')
        );
        $this->db->where('id_jurusan', $array['id_jurusan']);
        $q = $this->db->update('ms_jurusan_pendidikan', array(
          'id_tingkatan_pendidikan' => $array['id_tingkatan_pendidikan'],
          'nama_jurusan'            => $array['nama_jurusan'],
					'nama_jurusan_jp'            => $array['nama_jurusan_jp']
        ));

				if($q)
				{
					$this->session->set_flashdata('flag', 'Sukses update jurusan');
				}
				else
				{
					$this->session->set_flashdata('flag', 'Gagal update jurusan');
				}
      }
    }
		echo validation_errors();
    //redirect('rcr/jurusan');
  }
}
