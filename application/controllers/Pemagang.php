<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Pemagang extends MY_Controller {

	protected $access = 'admin';

	function __construct()
	{
			parent::__construct();
			$this->load->library('form_validation');
			$this->load->library('session');
			$this->load->helper('my_helper');

      $this->load->model('Pemagang_model', 'pemagang');
			$this->load->model('Rcr_model');
      $this->load->database();

			if(empty($_SESSION['authorization']['PEMAGANG'])){
				http_response_code(403);
				echo "Forbidden";
				die();
			}
	}

  public function index($rentang = '')
  {
		$this->db->select('*');
    $this->db->from('ms_tingkatan_pendidikan');
		$this->db->where('tingkat >= ', 3); //Hanya mulai dari SMA
    $this->db->order_by('tingkat', 'desc');
    $q_pendidikan = $this->db->get();
    $tingkat_pendidikan = $q_pendidikan->result_array();

		$this->db->select('tb_pekerjaan.kd_pekerjaan, tb_jenis_pekerjaan.jenis_pekerjaan, tb_pekerjaan.pekerjaan');
		$this->db->from('tb_pekerjaan');
		$this->db->join('tb_jenis_pekerjaan', 'tb_pekerjaan.kd_jenis_pekerjaan = tb_jenis_pekerjaan.kd_jenis_pekerjaan', 'left');
		$this->db->order_by('tb_jenis_pekerjaan.kd_jenis_pekerjaan', 'asc');
    $pekerjaan = $this->db->get();

    $data['tingkat_pendidikan'] = $tingkat_pendidikan;
		$data['pekerjaan'] = $pekerjaan->result_array();
		$data['provinsi'] = $this->db->get('tb_provinsi')->result_array();
		$data['jurusan'] = $this->db->get('ms_jurusan_pendidikan')->result_array();

		$data['rentang'] = htmlspecialchars($rentang);

    $this->load->view('rcr/layout/header');
    $this->load->view('rcr/pemagang/index', $data);
    $this->load->view('rcr/layout/footer');
  }

	public function edit($kd_pemagang = NULL)
	{
		if($kd_pemagang == NULL)
		{
			redirect('rcr/pemagang');
		}
		$this->db->set(array(
			'nama_pem' => $this->input->post('nama_pem'),
			'tanggal_lahir' => $this->input->post('tanggal_lahir'),
			'tempat_lahir' => $this->input->post('tempat_lahir'),
			'alamat_ktp' => $this->input->post('alamat_ktp'),
			'tempat_tinggal' => $this->input->post('tempat_tinggal'),
			'tempat_asal' => $this->input->post('tempat_asal'),
			'kd_provinsi' => $this->input->post('kd_provinsi'),
			'no_hp' => $this->input->post('no_hp'),
			'no_hp1' => $this->input->post('no_hp1'),
			'no_hp2' => $this->input->post('no_hp2'),
			'no_tel' => $this->input->post('no_tel'),
			'gol_darah' => $this->input->post('gol_darah'),
			'tb' => $this->input->post('tb'),
			'bb' => $this->input->post('bb'),
			'lingkar_pinggang' => $this->input->post('lingkar_pinggang'),
			'no_sepatu' => $this->input->post('no_sepatu'),
			'suka_pelajaran' => $this->input->post('suka_pelajaran'),
			'tgl_rev' => Date('Y-m-d h:i:sa')
		));
		$this->db->where('kd_pemagang', $kd_pemagang);
		$q = $this->db->update('tb_pemagangan');
		if($q)
		{
			$this->session->set_flashdata('flag', 'Sukses Update Biodata Pemagang');
		}
		else
		{
			$this->session->set_flashdata('flag', 'Gagal Update Biodata Pemagang');
		}
		header('Location: ' . $_SERVER['HTTP_REFERER']);
	}

	public function show($kd_pemagang = NULL)
	{
		if($kd_pemagang == NULL)
		{
			redirect('rcr/pemagang');
		}

		$this->db->select(array('tb_pemagangan.*', 'AGAMA.agama', 'PROV.provinsi'));
		$this->db->from('tb_pemagangan');
		$this->db->where(array('kd_pemagang' => $kd_pemagang));
		$this->db->join('tb_agama AGAMA', 'AGAMA.kd_agama = tb_pemagangan.id_agama');
		$this->db->join('tb_provinsi PROV', 'PROV.kd_provinsi = tb_pemagangan.kd_provinsi');
		$pemagang = $this->db->get();
		$passport = $this->db->get_where('tb_pasport', array('kd_pemagang' => $kd_pemagang));
		$riwayat_keluar_negri = $this->db->get_where('tb_riwayat_keluarnegri', array ('kd_pemagang' => $kd_pemagang));

		$berkas = $this->db->get_where('tb_berkas', array('kd_pemagang' => $kd_pemagang));
		$asosiasi = $this->db->get_where('tb_custemer', array('untuk' => 3, 'stat' => 1));
		$perusahaan = $this->db->get_where('tb_custemer', array('untuk' => 4, 'stat' => 1));

		$this->db->select(array('RP.*', 'TP.nama as tingkat_pendidikan', 'JP.nama_jurusan'));
		$this->db->from('dt_riwayat_pendidikan RP');
		$this->db->join('ms_tingkatan_pendidikan TP', 'RP.id_pendidikan = TP.id');
		$this->db->join('ms_jurusan_pendidikan JP', 'RP.id_jurusan = JP.id_jurusan', 'left');
		$this->db->where('RP.kd_pemagang', $kd_pemagang);
		$riwayat_pendidikan = $this->db->get();

		$this->db->select('*');
		$this->db->from('tb_keluarga K');
		$this->db->join('tb_hubungan H', 'K.kd_stat_kel = H.kd_hubungan');
		$this->db->where(array('K.nik' => $kd_pemagang));
		$keluarga = $this->db->get();

		$this->db->select('*');
		$this->db->from('tb_keluarga_pem KP');
		$this->db->join('tb_hubungan H', 'KP.hubungan = H.kd_hubungan');
		$this->db->where(array('KP.kd_pemagang' => $kd_pemagang));
		$keluarga_pem = $this->db->get();

		$this->db->select('*, YEAR(NOW()) - YEAR(tangal_lahir) as umur');
		$this->db->from('keluarga_pemagang_dijepang KPJ');
		$this->db->join('tb_hubungan H', 'KPJ.kd_hubungan = H.kd_hubungan');
		$this->db->where(array('KPJ.kd_pemagang' => $kd_pemagang));
		$keluarga_pemagang_dijepang = $this->db->get();

		$this->db->select(array('TE.kd_test_external', 'TE.tgl_seleksi', 'ASOSIASI.nama_pt as asosiasi', 'PERUSAHAAN.nama_pt as perusahaan', 'JU.nama as jenis_usaha', 'TE.hasil'));
		$this->db->from('test_external TE');
		$this->db->join('tb_custemer ASOSIASI', 'TE.kd_asosiasi = ASOSIASI.kd_pt');
		$this->db->join('tb_custemer PERUSAHAAN', 'TE.kd_asosiasi = PERUSAHAAN.kd_pt');
		$this->db->join('jenis_usaha JU', 'JU.kd_jenis_usaha = TE.kd_bidang_usaha');
		$this->db->where('TE.kd_pemagang', $kd_pemagang);
		$test_external = $this->db->get();

		$this->db->select('*');
		$this->db->from('tb_riwayat_pekerjaan RP');
		$this->db->join('tb_pekerjaan JP', 'RP.posisi = JP.kd_pekerjaan');
		$this->db->where('RP.nik', $kd_pemagang);
		$riwayat_pekerjaan = $this->db->get();

		$jenis_usaha = $this->db->get('jenis_usaha');

		$riwayat_penyakit = $this->db->get_where('tb_riwayat_penyakit', array(
			'nik' => $kd_pemagang
		));

		$blacklist = $this->db->get_where('blacklist', array(
			'kd_pemagang' => $kd_pemagang
		));

		$this->db->select('L.*, LOW.judul');
		$this->db->from('lulus L');
		$this->db->join('tb_lowongan LOW', 'L.kd_lowongan = LOW.kd', 'left');
		$this->db->where('L.kd_pemagang', $kd_pemagang);
		$q_lulus = $this->db->get();

		$merokok = $this->db->get_where('tb_detail_kebiasaan', array(
			'kd_pemagang' => $kd_pemagang,
			'kd_kebiasaan' => 1
		));

		$minum_alkohol = $this->db->get_where('tb_detail_kebiasaan', array(
			'kd_pemagang' => $kd_pemagang,
			'kd_kebiasaan' => 2
		));

		$data['pemagang'] = $pemagang->row();
		$data['passport'] = $passport->result_array();
		$data['riwayat_keluar_negri'] = $riwayat_keluar_negri->result_array();
		$data['keluarga'] = $keluarga->result_array();
		$data['keluarga_pem'] = $keluarga_pem->result_array();
		$data['keluarga_pemagang_dijepang'] = $keluarga_pemagang_dijepang->result_array();
		$data['berkas'] = $berkas->result_array();
		$data['test_external'] = $test_external->result_array();
		$data['asosiasi'] = $asosiasi->result_array();
		$data['perusahaan'] = $perusahaan->result_array();
		$data['riwayat_pekerjaan'] = $riwayat_pekerjaan->result_array();
		$data['jenis_usaha'] = $jenis_usaha->result_array();
		$data['riwayat_pendidikan'] = $riwayat_pendidikan->result_array();
		$data['riwayat_penyakit'] = $riwayat_penyakit->result_array();
		$data['blacklist'] = $blacklist->row();
		$data['lulus'] = $q_lulus->row();
		$data['merokok'] = $merokok->row();
		$data['minum_alkohol'] = $minum_alkohol->row();
		$data['provinsi'] = $this->db->get('tb_provinsi')->result_array();
		$this->load->view('rcr/layout/header');
    $this->load->view('rcr/pemagang/show', $data);
    $this->load->view('rcr/layout/footer');
	}

  public function riwayat_pekerjaan($filter = 'none')
  {
    $pemagang = $this->db->query('
    SELECT  nama_pem,
            KERJA.nama_perusahaan,
            pekerjaan,
            TIMESTAMPDIFF(MONTH, CONCAT(tahun_mulai, \'-\', bulan_mulai, \'-01\'), \'2015-01-01\') as lama_kerja
    FROM tb_pemagangan P
    INNER JOIN dt_riwayat_pekerjaan KERJA ON KERJA.kd_pemagang = P.kd_pemagang
    INNER JOIN tb_pekerjaan PEKERJAAN ON KERJA.kd_pekerjaan = PEKERJAAN.kd_pekerjaan;
    ');
    $data['pemagang'] = $pemagang->result_array();
    $this->load->view('rcr/layout/header');
    $this->load->view('rcr/pemagang/filter_riwayat_pekerjaan', $data);
    $this->load->view('rcr/layout/footer');
  }

	public function pemagang_lulus()
	{
		if(empty($_SESSION['authorization']['PMGLULUS'])){
			http_response_code(403);
			echo "Forbidden";
			die();
		}

		$this->db->select('L.*, P.nama_pem, TP.kd_lowongan, TP.nilai, TP.keterangan');
		$this->db->from('lulus L');
		$this->db->join('tb_pemagangan P', 'L.kd_pemagang = P.kd_pemagang');
		$this->db->join('tb_test_pendaftar TP', 'L.kd_pemagang = TP.kd_pemagang', 'left');
		$q = $this->db->get();

		$data['lulus'] = $q->result_array();
		$this->load->view('rcr/layout/header');
    $this->load->view('rcr/pemagang/lulus', $data);
    $this->load->view('rcr/layout/footer');
	}

	//set pemagang as lulus
	public function lulus_pemagang($kd_pemagang = NULL)
	{
		if(empty($_SESSION['authorization']['PMGLULUS'])){
			http_response_code(403);
			echo "Forbidden";
			die();
		}

		if($kd_pemagang == NULL)
		{
			rediret('rcr/pemagang');
		}
		$q = $this->db->insert('lulus', array(
			'kd_pemagang' => $this->input->post('kd_pemagang'),
			'alasan' => $this->input->post('alasan_lulus'),
			'tgl' => $this->input->post('tgl'),
		));

		if($q)
		{
			$this->session->set_flashdata('flag', 'Sukses Luluskan Pemagang');
		}
		else
		{
			$this->session->set_flashdata('flag', 'Gagal Luluskan Pemagang');
		}

		redirect('rcr/pemagang/show/'.$kd_pemagang);
	}

	public function test_internal($kd_pemagang)
	{
		if(isset($_POST['submit']))
		{
			$q = $this->Rcr_model->add_test_internal($_POST);
			if($q)
			{
				$this->session->set_flashdata('flag', 'Sukses Update Test Internal');
			}
			else
			{
				$this->session->set_flashdata('flag', 'Gagal Update Test Internal');
			}
		}
		$this->db->select(array('TES.nama_tes', 'NILAI.nilai', 'DETAILNILAI.kd_detail_nilai', 'DETAILNILAI.tgl_test'));
		$this->db->from('tb_detail_nilai DETAILNILAI');
		$this->db->join('tb_nilai NILAI', 'NILAI.kd_detail_nilai = DETAILNILAI.kd_detail_nilai');
		$this->db->join('tb_tes TES', 'TES.kd_tes = NILAI.kd_tes');
		$this->db->where(array(
			'kd_pemagang' => $kd_pemagang
		));
		$q = $this->db->get();
		$pemagang = $this->db->get_where('tb_pemagangan', array('kd_pemagang' => $kd_pemagang));

		$test_column = $this->db->get('tb_tes');

		$data['test'] = $q->result_array();
		$data['pemagang'] = $pemagang->row();
		$data['kd_pemagang'] = $kd_pemagang;
		$data['test_column'] = $test_column->result_array();

		$this->load->view('rcr/layout/header');
    $this->load->view('rcr/pemagang/test_internal', $data);
    $this->load->view('rcr/layout/footer');
	}

	public function import_test_internal()
	{
		$temp_array = array();
		$query = $this->db->get('tb_tes');
		$test = $query->result_array();
		$error = "";
		if (isset($_POST['import'])) {
			$filename = $_FILES['file']['tmp_name'];
			if($_FILES["file"]["size"] > 0)
			{
				$ext = pathinfo($_FILES['file']['name'], PATHINFO_EXTENSION);

				if($ext == 'csv')
				{
					$file = fopen($filename, "r");
					$c = 0;
					while(($fileop = fgetcsv($file, 1000, ',')) !== false)
					{
						$c++;
						if($c > 1) //Start second row
						{
							$kd_pemagang 		= isset($fileop[0]) ? $fileop[0] : '';
							$nama_pemagang 	= isset($fileop[1]) ? $fileop[1] : '';
							$tgl_test 			= isset($fileop[21]) ? $fileop[21] : '';
							$berat_badan 		= isset($fileop[22]) ? $fileop[22] : '';
							$tinggi_badan 	= isset($fileop[23]) ? $fileop[23] : '';
							$lingkar_pinggang = isset($fileop[24]) ? $fileop[24] : '';
							$no_sepatu = isset($fileop[25]) ? $fileop[25] : '';
							if(!empty($kd_pemagang) && !empty($tgl_test))
							{
								$column = 0;
								$kd_tes = 0;

								$temp_nilai_array = array();

								foreach ($fileop as $val) {
									$column++;
									if($column > 2 && $column < 22)
									{
										$kd_tes++;
										$temp_nilai_array[$kd_tes] = $val;
									}
								}
								$temp_nilai_array['nama'] = $nama_pemagang;
								$temp_nilai_array['tanggal'] = $tgl_test;
								$temp_nilai_array['kd_pemagang'] = $kd_pemagang;
								$temp_nilai_array['berat_badan'] = $berat_badan;
								$temp_nilai_array['tinggi_badan'] = $tinggi_badan;
								$temp_nilai_array['lingkar_pinggang'] = $lingkar_pinggang;
								$temp_nilai_array['no_sepatu'] = $no_sepatu;
								$temp_nilai_array['tgl_test'] = $tgl_test;

								array_push($temp_array, $temp_nilai_array);
							}
						}
					}
				}
				else
				{
					$error = "Format file harus CSV!";
				}
			}
		}

		$data['temp_array'] = $temp_array;
		$data['test_column'] = $test;
		$data['error'] = $error;
		$this->load->view('rcr/layout/header');
		$this->load->view('rcr/pemagang/import_test_internal', $data);
		$this->load->view('rcr/layout/footer');
	}

	public function upload_berkas()
	{
		$this->config->load('form_validation', TRUE);
		if($this->form_validation->run() === TRUE)
		{
			$upload = $this->pemagang->upload_berkas();
			if($upload == 'sukses')
			{
				$this->session->set_flashdata('flag', 'Sukses upload berkas');
				header('Location: ' . $_SERVER['HTTP_REFERER']);
			}
			else
			{
				$this->session->set_flashdata('flag', 'Gagal upload berkas');
				header('Location: ' . $_SERVER['HTTP_REFERER']);
			}
		}
	}

	public function upload_test_external()
	{
		$this->config->load('form_validation', TRUE);
		var_dump($_POST);
		if($this->form_validation->run() === TRUE)
		{
			echo 'PASS';
			$upload = $this->pemagang->upload_test_external();
			if($upload == 'sukses')
			{
				$this->session->set_flashdata('flag', 'Sukses upload berkas');
				header('Location: ' . $_SERVER['HTTP_REFERER']);
			}
			else
			{
				$this->session->set_flashdata('flag', 'Gagal upload berkas');
				header('Location: ' . $_SERVER['HTTP_REFERER']);
			}
		}
	}

	public function export_to_excel()
	{
		$page = $this->input->post('page');
		$limit = $this->input->post('limit');

		$offset = ($page - 1)*$limit;
		$kd_pemagang = explode(',', $_POST['kd_pemagang']);
		$arr = $this->pemagang->export_to_excel($kd_pemagang);

		$this->load->library('excel');
		$this->excel->getActiveSheet()->getPageSetup()->setOrientation(PHPExcel_Worksheet_PageSetup::ORIENTATION_LANDSCAPE);
		$this->excel->getActiveSheet()->getPageSetup()->setPaperSize(PHPExcel_Worksheet_PageSetup::PAPERSIZE_LEGAL);
		$this->excel->getActiveSheet()->getPageMargins()->setTop(0.75);
		$this->excel->getActiveSheet()->getPageMargins()->setRight(0.75);
		$this->excel->getActiveSheet()->getPageMargins()->setLeft(0.75);
		$this->excel->getActiveSheet()->getPageMargins()->setBottom(0.75);
		$this->excel->getActiveSheet()->getHeaderFooter()->setOddFooter('&L&B' . $this->excel->getProperties()->getTitle() . '&RPage &P of &N');

		$this->excel->setActiveSheetIndex(0);

		$row_count = 1;

		$this->excel->getActiveSheet()->setCellValue("A{$row_count}", 'kode_pemagang');
		$this->excel->getActiveSheet()->setCellValue("B{$row_count}", 'nama');
		$this->excel->getActiveSheet()->setCellValue("C{$row_count}", 'tinggi_badan');
		$this->excel->getActiveSheet()->setCellValue("D{$row_count}", 'berat_badan');
		$this->excel->getActiveSheet()->setCellValue("E{$row_count}", 'usia');
		$this->excel->getActiveSheet()->setCellValue("F{$row_count}", 'status');
		$this->excel->getActiveSheet()->setCellValue("G{$row_count}", 'alamat_ktp');
		$this->excel->getActiveSheet()->setCellValue("H{$row_count}", 'tempat_tinggal');
		$this->excel->getActiveSheet()->setCellValue("I{$row_count}", 'provinsi');
		$this->excel->getActiveSheet()->setCellValue("J{$row_count}", 'nama_sma');
		$this->excel->getActiveSheet()->setCellValue("K{$row_count}", 'jurusan_sma');
		$this->excel->getActiveSheet()->setCellValue("L{$row_count}", 'nama_sekolah_tinggi');
		$this->excel->getActiveSheet()->setCellValue("M{$row_count}", 'jurusan_sekolah_tinggi');
		$this->excel->getActiveSheet()->setCellValue("N{$row_count}", 'passport');
		$this->excel->getActiveSheet()->setCellValue("O{$row_count}", 'pernah_keluar_negri');
		$this->excel->getActiveSheet()->setCellValue("P{$row_count}", 'pengalaman_kerja');
		$this->excel->getActiveSheet()->setCellValue("Q{$row_count}", 'nomor_handphone');
		$this->excel->getActiveSheet()->setCellValue("R{$row_count}", 'tanggal_daftar');
		$this->excel->getActiveSheet()->setCellValue("S{$row_count}", 'tanggal_lahir');
		$this->excel->getActiveSheet()->setCellValue("T{$row_count}", 'email');
		$this->excel->getActiveSheet()->setCellValue("U{$row_count}", 'agama');
		$this->excel->getActiveSheet()->setCellValue("V{$row_count}", 'test_internal');

		$row_count++;
		$status = '';
		foreach ($arr as $k => $v) {
			if($v['status'] == 'b')
			{
				$status = 'Belum Menikah';
			}
			else if($v['status'] == 'jh'){
        $status="Cerai Hidup";
      }else if($v['status'] == 'jm'){
        $status="Cerai Mati";
      }else if($v['status'] == 'ns'){
        $status="Nikah Sirih";
      }else{
        $status= "Menikah";
      }

			$this->excel->getActiveSheet()->setCellValue("A{$row_count}", $v['kode_pemagang']);
			$this->excel->getActiveSheet()->setCellValue("B{$row_count}", $v['nama']);
			$this->excel->getActiveSheet()->setCellValue("C{$row_count}", $v['tinggi_badan']);
			$this->excel->getActiveSheet()->setCellValue("D{$row_count}", $v['berat_badan']);
			$this->excel->getActiveSheet()->setCellValue("E{$row_count}", $v['usia']);
			$this->excel->getActiveSheet()->setCellValue("F{$row_count}", $status);
			$this->excel->getActiveSheet()->setCellValue("G{$row_count}", $v['alamat_ktp']);
			$this->excel->getActiveSheet()->setCellValue("H{$row_count}", $v['tempat_tinggal']);
			$this->excel->getActiveSheet()->setCellValue("I{$row_count}", $v['provinsi']);
			$this->excel->getActiveSheet()->setCellValue("J{$row_count}", $v['nama_sma']);
			$this->excel->getActiveSheet()->setCellValue("K{$row_count}", $v['jurusan_sma']);
			$this->excel->getActiveSheet()->setCellValue("L{$row_count}", $v['nama_sekolah_tinggi']);
			$this->excel->getActiveSheet()->setCellValue("M{$row_count}", $v['jurusan_sekolah_tinggi']);
			$this->excel->getActiveSheet()->setCellValue("N{$row_count}", $v['passport']);
			$this->excel->getActiveSheet()->setCellValue("O{$row_count}", $v['pernah_keluar_negri']);
			$this->excel->getActiveSheet()->setCellValue("P{$row_count}", $v['pengalaman_kerja']);
			$this->excel->getActiveSheet()->setCellValue("Q{$row_count}", $v['nomor_handphone']);
			$this->excel->getActiveSheet()->setCellValue("R{$row_count}", $v['tanggal_daftar']);
			$this->excel->getActiveSheet()->setCellValue("S{$row_count}", $v['tanggal_lahir']);
			$this->excel->getActiveSheet()->setCellValue("T{$row_count}", $v['email']);
			$this->excel->getActiveSheet()->setCellValue("U{$row_count}", $v['agama']);
			$this->excel->getActiveSheet()->setCellValue("V{$row_count}", $v['test_internal']);
			$row_count++;
		}

		header('Content-Type: application/vnd.ms-excel');
		header('Content-Disposition: attachment; filename="' . 'Pemagang'.'.xls"');
		header('Cache-Control: max-age= 0');
		$objWriter = PHPExcel_IOFactory::createWriter($this->excel, 'Excel2007');
		$objWriter->save('php://output');
	}

	public function print_biodata_pemagang($kd_pemagang)
	{
		$this->db->select(array
											(
												'tb_pemagangan.*',
 												'AGAMA.agama',
												'YEAR(NOW()) - YEAR(tb_pemagangan.tanggal_lahir) as umur'
											));
		$this->db->from('tb_pemagangan');
		$this->db->where(array('kd_pemagang' => $kd_pemagang));
		$this->db->join('tb_agama AGAMA', 'AGAMA.kd_agama = tb_pemagangan.id_agama');
		$q_pemagang = $this->db->get();
		$q_passport = $this->db->get_where('tb_pasport', array('kd_pemagang' => $kd_pemagang));
		$q_riwayat_keluar_negri = $this->db->get_where('tb_riwayat_keluarnegri', array ('kd_pemagang' => $kd_pemagang));
		$q_riwayat_pekerjaan = $this->db->get_where('tb_riwayat_pekerjaan', array('nik' => $kd_pemagang));

		$this->db->select('RP.*, J.nama_jurusan');
		$this->db->from('dt_riwayat_pendidikan RP');
		$this->db->join('ms_jurusan_pendidikan J', 'J.id_jurusan = RP.id_jurusan', 'left');
		$this->db->where('RP.kd_pemagang', $kd_pemagang);
		$q_riwayat_pendidikan = $this->db->get();

		$this->db->select('K.*, H.hubungan');
		$this->db->from('tb_keluarga K');
		$this->db->join('tb_hubungan H', 'H.kd_hubungan = K.kd_stat_kel');
		$this->db->where('K.nik', $kd_pemagang);
		$q_keluarga = $this->db->get();

		$this->db->select('K.*, H.hubungan');
		$this->db->from('tb_keluarga_pem K');
		$this->db->join('tb_hubungan H', 'H.kd_hubungan = K.hubungan');
		$this->db->where('K.kd_pemagang', $kd_pemagang);
		$q_keluarga_pem = $this->db->get();

		$this->db->select('K.*, H.hubungan');
		$this->db->from('keluarga_pemagang_dijepang K');
		$this->db->join('tb_hubungan H', 'H.kd_hubungan = K.kd_hubungan');
		$this->db->where('K.kd_pemagang', $kd_pemagang);
		$q_keluarga_dijepang = $this->db->get();

		$q_pekerjaan = $this->db->get('tb_pekerjaan');

		$pemagang = $q_pemagang->row();
		$passport = $q_passport->row();
		$riwayat_keluar_negri = $q_riwayat_keluar_negri->result_array();
		$riwayat_pekerjaan = $q_riwayat_pekerjaan->result_array();
		$riwayat_pendidikan = $q_riwayat_pendidikan->result_array();
		$keluarga = $q_keluarga->result_array();
		$keluarga_pem = $q_keluarga_pem->result_array();
		$keluarga_dijepang = $q_keluarga_dijepang->result_array();
		$pekerjaan = $q_pekerjaan->result_array();

		$riwayat_penyakit = $this->db->get_where('tb_riwayat_penyakit', array(
			'nik' => $kd_pemagang
		))->row();

		$merokok = $this->db->get_where('tb_detail_kebiasaan', array(
			'kd_pemagang' => $kd_pemagang,
			'kd_kebiasaan' => 1
		))->row();

		$minum_alkohol = $this->db->get_where('tb_detail_kebiasaan', array(
			'kd_pemagang' => $kd_pemagang,
			'kd_kebiasaan' => 2
		))->row();

		$this->load->library('Excel');

		// Set page orientation and size
		$this->excel->getActiveSheet()->getPageSetup()->setOrientation(PHPExcel_Worksheet_PageSetup::ORIENTATION_LANDSCAPE);
		$this->excel->getActiveSheet()->getPageSetup()->setPaperSize(PHPExcel_Worksheet_PageSetup::PAPERSIZE_LEGAL);
		$this->excel->getActiveSheet()->getPageMargins()->setTop(0.75);
		$this->excel->getActiveSheet()->getPageMargins()->setRight(0.75);
		$this->excel->getActiveSheet()->getPageMargins()->setLeft(0.75);
		$this->excel->getActiveSheet()->getPageMargins()->setBottom(0.75);
		$this->excel->getActiveSheet()->getHeaderFooter()->setOddFooter('&L&B' . $this->excel->getProperties()->getTitle() . '&RPage &P of &N');

		///style/////
		$bawah = array(
     	'borders' => array(
				'bottom' => array(
          'style' => PHPExcel_Style_Border ::BORDER_THIN,
            'color' => array('argb' => '0000'),
           ),
       ),
		);

		$atas = array(
		       'borders' => array(
		             'top' => array(
		                    'style' => PHPExcel_Style_Border ::BORDER_THIN,
		                    'color' => array('argb' => '0000'),
		             ),
		       ),
		);

		$kanan = array(
		       'borders' => array(
		             'right' => array(
		                    'style' => PHPExcel_Style_Border ::BORDER_THIN,
		                    'color' => array('argb' => '0000'),
		             ),
		       ),
		);

		$kiri = array(
		       'borders' => array(
		             'left' => array(
		                    'style' => PHPExcel_Style_Border ::BORDER_THIN,
		                    'color' => array('argb' => '0000'),
		             ),
		       ),
		);

		$border = array(
		       'borders' => array(
		             'outline' => array(
		                    'style' => PHPExcel_Style_Border::BORDER_THIN,
		                    'color' => array('argb' => '000'),
		             ),
		       ),
		);

		$this->excel->setActiveSheetIndex(0);

		//untuk auto size colomn
		$a=2;
		while ($a < 100) {
		  $this->excel->getActiveSheet()->getRowDimension($a)->setRowHeight(25);
		  $a++;
		}

		$this->excel->getActiveSheet()->getStyle('A1:AE100')->getAlignment()->setVertical(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
		$this->excel->getActiveSheet()->getColumnDimension('A')->setWidth(4);
		$this->excel->getActiveSheet()->getColumnDimension('B')->setWidth(4);
		$this->excel->getActiveSheet()->getColumnDimension('C')->setWidth(4);
		$this->excel->getActiveSheet()->getColumnDimension('D')->setWidth(4);
		$this->excel->getActiveSheet()->getColumnDimension('E')->setWidth(4);
		$this->excel->getActiveSheet()->getColumnDimension('F')->setWidth(4);
		$this->excel->getActiveSheet()->getColumnDimension('G')->setWidth(4);
		$this->excel->getActiveSheet()->getColumnDimension('H')->setWidth(4);
		$this->excel->getActiveSheet()->getColumnDimension('I')->setWidth(4);
		$this->excel->getActiveSheet()->getColumnDimension('J')->setWidth(4);
		$this->excel->getActiveSheet()->getColumnDimension('K')->setWidth(4);
		$this->excel->getActiveSheet()->getColumnDimension('L')->setWidth(4);
		$this->excel->getActiveSheet()->getColumnDimension('M')->setWidth(4);
		$this->excel->getActiveSheet()->getColumnDimension('N')->setWidth(4);
		$this->excel->getActiveSheet()->getColumnDimension('O')->setWidth(4);
		$this->excel->getActiveSheet()->getColumnDimension('P')->setWidth(4);
		$this->excel->getActiveSheet()->getColumnDimension('Q')->setWidth(4);
		$this->excel->getActiveSheet()->getColumnDimension('R')->setWidth(4);
		$this->excel->getActiveSheet()->getColumnDimension('S')->setWidth(4);
		$this->excel->getActiveSheet()->getColumnDimension('T')->setWidth(4);
		$this->excel->getActiveSheet()->getColumnDimension('U')->setWidth(4);
		$this->excel->getActiveSheet()->getColumnDimension('V')->setWidth(4);
		$this->excel->getActiveSheet()->getColumnDimension('W')->setWidth(4);
		$this->excel->getActiveSheet()->getColumnDimension('X')->setWidth(4);
		$this->excel->getActiveSheet()->getColumnDimension('Y')->setWidth(4);
		$this->excel->getActiveSheet()->getColumnDimension('Z')->setWidth(4);
		$this->excel->getActiveSheet()->getColumnDimension('AA')->setWidth(4);
		$this->excel->getActiveSheet()->getColumnDimension('AB')->setWidth(4);
		$this->excel->getActiveSheet()->getColumnDimension('AC')->setWidth(4);
		$this->excel->getActiveSheet()->getColumnDimension('AD')->setWidth(4);
		$this->excel->getActiveSheet()->getColumnDimension('AE')->setWidth(4);

		if(!empty($pemagang->foto)){
		  $dir= $_SERVER['DOCUMENT_ROOT'] . '/media/' . md5($pemagang->kd_pemagang.'manfikar') .'/'.$pemagang->foto;

			if(file_exists($dir))
			{
				$objDrawing = new PHPExcel_Worksheet_Drawing();
			  $objDrawing->setName('Logo');
			  $objDrawing->setDescription('Logo');
			  $objDrawing->setPath($dir);
			  $objDrawing->setCoordinates('Z3');
			  $objDrawing->setHeight(250);
			  $objDrawing->setWidth(150);
			  $objDrawing->setWorksheet($this->excel->getActiveSheet());
			}
		}

		$this->excel->getActiveSheet()->setTitle($kd_pemagang);

		/////////////////////////////////////////////////
		$this->excel->getActiveSheet()->setCellValue('A3', "Nama Lengkap :");
		$this->excel->getActiveSheet()->mergeCells('A3:M3');
		$this->excel->getActiveSheet()->getStyle('A3:M3')->applyFromArray($atas);

		$this->excel->getActiveSheet()->setCellValue('N3', 'Kewarganegaraan');
		$this->excel->getActiveSheet()->mergeCells('N3:R3');

		$this->excel->getActiveSheet()->setCellValue('S3', ': '.@$t['warga_negara']);
		$this->excel->getActiveSheet()->mergeCells('S3:T3');

		$this->excel->getActiveSheet()->getStyle('N3:T3')->applyFromArray($border);

		$this->excel->getActiveSheet()->setCellValue('U3', 'Umur :');
		$this->excel->getActiveSheet()->mergeCells('U3:V3');

		$this->excel->getActiveSheet()->setCellValue('W3', $pemagang->umur .' Tahun');
		$this->excel->getActiveSheet()->mergeCells('W3:Y3');

		$this->excel->getActiveSheet()->getStyle('U3:Y3')->applyFromArray($border);
		/////////////////////////////////////
		$this->excel->getActiveSheet()->setCellValue('A4', '(Alpabet) ');
		$this->excel->getActiveSheet()->mergeCells('A4:D4');

		$this->excel->getActiveSheet()->setCellValue('E4',ucfirst($pemagang->nama_pem));
		$this->excel->getActiveSheet()->mergeCells('E4:M4');
		$this->excel->getActiveSheet()->getStyle('F4:M4')->applyFromArray($kanan);

		$this->excel->getActiveSheet()->setCellValue('N4', 'Jenis Kelamin ');
		$this->excel->getActiveSheet()->mergeCells('N4:Q4');

		$this->excel->getActiveSheet()->setCellValue('R4', $pemagang->jk);

		$this->excel->getActiveSheet()->getStyle('N4:R4')->applyFromArray($border);

		$this->excel->getActiveSheet()->setCellValue('S4', 'Status Kawin :');
		$this->excel->getActiveSheet()->mergeCells('S4:V4');

		$status_kawin = "";

		switch ($pemagang->status) {
			case 'b':
				$status_kawin = "Belum menikah";
				break;
			case 'n':
				$status_kawin = 'Menikah';
				break;
			case 'ns':
				$status_kawin = 'Menikah Sirih';
				break;
			case 'jh':
				$status_kawin = 'Janda Hidup';
				break;
			case 'jm':
				$status_kawin = 'Janda Meninggal';
				break;
			default:
				$status_kawin = "Belum Menikah";
				break;
		}

		$this->excel->getActiveSheet()->setCellValue('W4', $status_kawin);
		$this->excel->getActiveSheet()->mergeCells('W4:Y4');

		$this->excel->getActiveSheet()->getStyle('S4:Y4')->applyFromArray($border);

		//////////////////////////////////////////
		$this->excel->getActiveSheet()->setCellValue('A5', "(Katakana)");
		$this->excel->getActiveSheet()->mergeCells('A5:M5');

		$this->excel->getActiveSheet()->setCellValue('N5', 'Agama :');
		$this->excel->getActiveSheet()->mergeCells('N5:P5');

		$this->excel->getActiveSheet()->setCellValue('Q5', $pemagang->agama);
		$this->excel->getActiveSheet()->mergeCells('Q5:Y5');

		$this->excel->getActiveSheet()->getStyle('N5:Y5')->applyFromArray($border);

		//////////////////////////////////////////
		$this->excel->getActiveSheet()->setCellValue('A6', 'Tgl Lahir :');
		$this->excel->getActiveSheet()->mergeCells('A6:C6');

		$this->excel->getActiveSheet()->setCellValue('D6', $pemagang->tanggal_lahir);
		$this->excel->getActiveSheet()->mergeCells('D6:M6');

		$this->excel->getActiveSheet()->getStyle('A6:M6')->applyFromArray($border);

		$this->excel->getActiveSheet()->setCellValue('N6', 'Tempat Asal :');
		$this->excel->getActiveSheet()->mergeCells('N6:Q6');

		$this->excel->getActiveSheet()->setCellValue('R6', $pemagang->tempat_asal);
		$this->excel->getActiveSheet()->mergeCells('R6:Y6');

		$this->excel->getActiveSheet()->getStyle('N6:Y6')->applyFromArray($border);

		//////////////////////////////////////////
		$this->excel->getActiveSheet()->setCellValue('A7', 'Alamat KTP :');
		$this->excel->getActiveSheet()->mergeCells('A7:D7');

		$this->excel->getActiveSheet()->setCellValue('E7', $pemagang->alamat_ktp);
		$this->excel->getActiveSheet()->mergeCells('E7:P7');

		$this->excel->getActiveSheet()->getStyle('A7:P7')->applyFromArray($border);

		$this->excel->getActiveSheet()->setCellValue('Q7', 'No HP :');
		$this->excel->getActiveSheet()->mergeCells('Q7:S7');

		$this->excel->getActiveSheet()->setCellValue('T7', $pemagang->no_hp);
		$this->excel->getActiveSheet()->mergeCells('T7:Y7');

		$this->excel->getActiveSheet()->getStyle('Q7:Y7')->applyFromArray($border);

		//////////////////////////////////////////
		$this->excel->getActiveSheet()->setCellValue('A8', 'Alamat sekarang :');
		$this->excel->getActiveSheet()->mergeCells('A8:E8');

		$this->excel->getActiveSheet()->setCellValue('F8', $pemagang->tempat_tinggal);
		$this->excel->getActiveSheet()->mergeCells('F8:P8');

		$this->excel->getActiveSheet()->getStyle('A8:P8')->applyFromArray($border);

		$this->excel->getActiveSheet()->setCellValue('Q8', 'No telp lainnya :');
		$this->excel->getActiveSheet()->mergeCells('Q8:U8');

		$this->excel->getActiveSheet()->setCellValue('V8', $pemagang->no_tel);
		$this->excel->getActiveSheet()->mergeCells('V8:Y8');

		$this->excel->getActiveSheet()->getStyle('Q8:Y8')->applyFromArray($border);

		////////////////////////////////////////////
		$this->excel->getActiveSheet()->setCellValue('A9', "Paspor");
		$this->excel->getActiveSheet()->mergeCells('A9:F9');

		$this->excel->getActiveSheet()->setCellValue('G9', "Nomor Paspor :");
		$this->excel->getActiveSheet()->mergeCells('G9:P9');

		$this->excel->getActiveSheet()->setCellValue('Q9', 'Tgl Penerbitan');
		$this->excel->getActiveSheet()->mergeCells('Q9:W9');

		$this->excel->getActiveSheet()->setCellValue('X9', 'Thn '. @Date('Y', strtotime($passport->terbit)));
		$this->excel->getActiveSheet()->mergeCells('X9:Y9');
		$this->excel->getActiveSheet()->getStyle('X9:Y9')->applyFromArray($atas);

		$this->excel->getActiveSheet()->setCellValue('Z9', 'Bln '. @Date('m', strtotime($passport->terbit)));
		$this->excel->getActiveSheet()->mergeCells('Z9:AA9');
		$this->excel->getActiveSheet()->getStyle('Z9:AA9')->applyFromArray($atas);

		$this->excel->getActiveSheet()->setCellValue('AB9', 'Tgl  '. @Date('d', strtotime($passport->terbit)));
		$this->excel->getActiveSheet()->mergeCells('AB9:AE9');
		$this->excel->getActiveSheet()->getStyle('AB9:AE9')->applyFromArray($atas);

		///////////////////////////////////////////////////////
		$this->excel->getActiveSheet()->setCellValue('B10', '');
		$this->excel->getActiveSheet()->mergeCells('B10:F10');

		$this->excel->getActiveSheet()->setCellValue('G10', @$passport->no_pass);
		$this->excel->getActiveSheet()->mergeCells('G10:P10');

		$this->excel->getActiveSheet()->setCellValue('Q10', 'Berlaku s/d');
		$this->excel->getActiveSheet()->mergeCells('Q10:W10');

		$this->excel->getActiveSheet()->setCellValue('X10', 'Thn '. @isset($passport->masa_berlaku) ? Date('Y', @strtotime($passport->masa_berlaku)) : '');
		$this->excel->getActiveSheet()->mergeCells('X10:Y10');

		$this->excel->getActiveSheet()->setCellValue('Z10', 'Bln '. @isset($passport->masa_berlaku) ? Date('m', @strtotime($passport->masa_berlaku)) : '');
		$this->excel->getActiveSheet()->mergeCells('Z10:AA10');

		$this->excel->getActiveSheet()->setCellValue('AB10', 'Tgl '. @isset($passport->masa_berlaku) ? Date('d', @strtotime($passport->masa_berlaku)) : '');
		$this->excel->getActiveSheet()->mergeCells('AB10:AC10');

		////////////////////////////////////////////
		$this->excel->getActiveSheet()->setCellValue('A11', "Riwayat keluar negeri");
		$this->excel->getActiveSheet()->mergeCells('A11:AE11');
		$this->excel->getActiveSheet()->getStyle('A11:AE11')->applyFromArray($atas);


		$ke=12;
		////////////////////////////////////////////
		if (!empty($riwayat_keluar_negri)) {
			foreach ($riwayat_keluar_negri as $rkn) {
				$this->excel->getActiveSheet()->setCellValue("A{$ke}", !empty($riwayat_keluar_negri) ? 'YA' : '');
				$this->excel->getActiveSheet()->mergeCells("A{$ke}:G{$ke}");
				$this->excel->getActiveSheet()->getStyle("A{$ke}:AE{$ke}")->applyFromArray($bawah);

				$this->excel->getActiveSheet()->setCellValue("H{$ke}", 'Thn ' . $rkn['tahun']);
				$this->excel->getActiveSheet()->mergeCells("H{$ke}:I{$ke}");

				$this->excel->getActiveSheet()->setCellValue("J{$ke}", "Bln ". $rkn['bulan']);
				$this->excel->getActiveSheet()->mergeCells("J{$ke}:K{$ke}");

				$this->excel->getActiveSheet()->setCellValue("L{$ke}", "Negara Tujuan ". $rkn['Negara']);
				$this->excel->getActiveSheet()->mergeCells("L{$ke}:U{$ke}");

				$this->excel->getActiveSheet()->setCellValue("V{$ke}", "Tujuan ". $rkn['tujuan']);
				$this->excel->getActiveSheet()->mergeCells("V{$ke}:AE{$ke}");
				$ke++;
			}
		}
		else
		{
			$this->excel->getActiveSheet()->setCellValue("A{$ke}", 'Tidak Pernah');
			$this->excel->getActiveSheet()->mergeCells("A{$ke}:G{$ke}");
			$this->excel->getActiveSheet()->getStyle("A{$ke}:AE{$ke}")->applyFromArray($bawah);

			$this->excel->getActiveSheet()->setCellValue("H{$ke}", 'Thn ');
			$this->excel->getActiveSheet()->mergeCells("H{$ke}:I{$ke}");

			$this->excel->getActiveSheet()->setCellValue("J{$ke}", "Bln ".'');
			$this->excel->getActiveSheet()->mergeCells("J{$ke}:K{$ke}");

			$this->excel->getActiveSheet()->setCellValue("L{$ke}", "Negara Tujuan ".'');
			$this->excel->getActiveSheet()->mergeCells("L{$ke}:U{$ke}");

			$this->excel->getActiveSheet()->setCellValue("V{$ke}", "Tujuan ".'');
			$this->excel->getActiveSheet()->mergeCells("V{$ke}:AE{$ke}");
			$ke++;
		}

		$marginpend=0;
		$this->excel->getActiveSheet()->setCellValue("A{$ke}",'Riwayat Belajar Bahasa Jepang');
		while(false){

		  $this->excel->getActiveSheet()->setCellValue('G'.$ke,'Selama');
		  $this->excel->getActiveSheet()->mergeCells('G'.$ke.':I'.$ke);

		  $this->excel->getActiveSheet()->setCellValue('J'.$ke,jangka(@$tampil_pend['thn_masuk_pend'].'-'.@$tampil_pend['bulan_masuk'].'-00',@$tampil_pend['thn_keluar_pend'].'-'.@$tampil_pend['bulan_keluar'].'-00',1,0,0));

		  $this->excel->getActiveSheet()->setCellValue('K'.$ke,'Thn ');
		  $this->excel->getActiveSheet()->mergeCells('K'.$ke.':L'.$ke);

		  $this->excel->getActiveSheet()->setCellValue('M'.$ke,jangka(@$tampil_pend['thn_masuk_pend'].'-'.@$tampil_pend['bulan_masuk'].'-00',@$tampil_pend['thn_keluar_pend'].'-'.@$tampil_pend['bulan_keluar'].'-00',0,1,0));

		  $this->excel->getActiveSheet()->setCellValue('N'.$ke,'Bln');
		  $this->excel->getActiveSheet()->mergeCells('N'.$ke.':O'.$ke);

		  $this->excel->getActiveSheet()->setCellValue('Q'.$ke,'Di '.ucfirst(@$tampil_pend[1]));
		  $this->excel->getActiveSheet()->mergeCells('Q'.$ke.':AE'.$ke);

		  $this->excel->getActiveSheet()->mergeCells('A'.$ke.':F'.$ke);
		  $ke++;
		}

		$this->excel->getActiveSheet()->setCellValue('A'.$ke,'Keluarga yg sedang tinggal/bekerja/magang/kuliah di Jepang '. '');
		$this->excel->getActiveSheet()->mergeCells('A'.$ke.':AE'.$ke);
		$this->excel->getActiveSheet()->getStyle('A'.$ke.':AE'.$ke)->applyFromArray($atas);
		$ke++;

		///////////////////////////////////////////////////////////////////////
		$this->excel->getActiveSheet()->setCellValue('A'.$ke,'Hubungan');
		$this->excel->getActiveSheet()->mergeCells('A'.$ke.':E'.$ke);
		$this->excel->getActiveSheet()->getStyle('A'.$ke.':E'.$ke)->applyFromArray($border);

		$this->excel->getActiveSheet()->setCellValue('F'.$ke,'Nama');
		$this->excel->getActiveSheet()->mergeCells('F'.$ke.':L'.$ke);
		$this->excel->getActiveSheet()->getStyle('F'.$ke.':L'.$ke)->applyFromArray($border);

		$this->excel->getActiveSheet()->setCellValue('M'.$ke,'Tgl Lahir');
		$this->excel->getActiveSheet()->mergeCells('M'.$ke.':P'.$ke);
		$this->excel->getActiveSheet()->getStyle('M'.$ke.':P'.$ke)->applyFromArray($border);

		$this->excel->getActiveSheet()->setCellValue('Q'.$ke,'Kewarganegaraan');
		$this->excel->getActiveSheet()->mergeCells('Q'.$ke.':T'.$ke);
		$this->excel->getActiveSheet()->getStyle('Q'.$ke.':T'.$ke)->applyFromArray($border);

		$this->excel->getActiveSheet()->setCellValue('U'.$ke,'Nama Sekolah/Perusahaan');
		$this->excel->getActiveSheet()->mergeCells('U'.$ke.':AB'.$ke);
		$this->excel->getActiveSheet()->getStyle('U'.$ke.':AB'.$ke)->applyFromArray($border);

		$this->excel->getActiveSheet()->setCellValue('AC'.$ke,'Status Visa');
		$this->excel->getActiveSheet()->mergeCells('AC'.$ke.':AE'.$ke);
		$this->excel->getActiveSheet()->getStyle('AC'.$ke.':AE'.$ke)->applyFromArray($border);
		$ke++;

		foreach($keluarga_dijepang as $kdjp){
		  $this->excel->getActiveSheet()->setCellValue('A'.$ke, $kdjp['hubungan']);
		  $this->excel->getActiveSheet()->mergeCells('A'.$ke.':E'.$ke);
		  $this->excel->getActiveSheet()->getStyle('A'.$ke.':E'.$ke)->applyFromArray($kanan);

		  $this->excel->getActiveSheet()->setCellValue('F'.$ke, $kdjp['nama']);
		  $this->excel->getActiveSheet()->mergeCells('F'.$ke.':L'.$ke);
		  $this->excel->getActiveSheet()->getStyle('F'.$ke.':L'.$ke)->applyFromArray($kanan);

		  $this->excel->getActiveSheet()->setCellValue('M'.$ke, $kdjp['tangal_lahir']);
		  $this->excel->getActiveSheet()->mergeCells('M'.$ke.':P'.$ke);
		  $this->excel->getActiveSheet()->getStyle('M'.$ke.':P'.$ke)->applyFromArray($kanan);

		  $this->excel->getActiveSheet()->setCellValue('Q'.$ke, $kdjp['kewarganegaraan']);
		  $this->excel->getActiveSheet()->mergeCells('Q'.$ke.':T'.$ke);
		  $this->excel->getActiveSheet()->getStyle('Q'.$ke.':T'.$ke)->applyFromArray($kanan);

		  $this->excel->getActiveSheet()->setCellValue('U'.$ke, $kdjp['instansi']);
		  $this->excel->getActiveSheet()->mergeCells('U'.$ke.':AB'.$ke);
		  $this->excel->getActiveSheet()->getStyle('U'.$ke.':AB'.$ke)->applyFromArray($kanan);

		  $this->excel->getActiveSheet()->setCellValue('AC'.$ke, $kdjp['visa']);
		  $this->excel->getActiveSheet()->mergeCells('AC'.$ke.':AE'.$ke);
		  $this->excel->getActiveSheet()->getStyle('AC'.$ke.':AE'.$ke)->applyFromArray($kanan);
		  $ke++;
		}

		///////////////////////////////////////////////////////////////////////////////////////

		$this->excel->getActiveSheet()->setCellValue('A'.$ke,'Riwayat Pendidikan');
		$this->excel->getActiveSheet()->mergeCells('A'.$ke.':Y'.$ke);
		$this->excel->getActiveSheet()->getStyle('A'.$ke.':Y'.$ke)->applyFromArray($border);

		$this->excel->getActiveSheet()->setCellValue('Z'.$ke,'Jurusan');
		$this->excel->getActiveSheet()->mergeCells('Z'.$ke.':AE'.$ke);
		$this->excel->getActiveSheet()->getStyle('Z'.$ke.':AE'.$ke)->applyFromArray($border);
		$ke++;

		///////////////////////////////////////////////////////////////////////////////
		//$mysql_pend_f=mysql_query("SELECT * FROM `tb_riwayat_pend` where nik ='$_REQUEST[id]' and status='F' order by thn_masuk_pend desc");
		foreach($riwayat_pendidikan as $pendidikan){

		    $this->excel->getActiveSheet()->setCellValue('A'.$ke,$pendidikan['mulai']);
		    $this->excel->getActiveSheet()->mergeCells('A'.$ke.':B'.$ke);

		    $this->excel->getActiveSheet()->setCellValue('C'.$ke,'thn');

		    //$this->excel->getActiveSheet()->setCellValue('D'.$ke,$pendidikan['bulan_mulai']);

		    $this->excel->getActiveSheet()->setCellValue('E'.$ke,'bln');

		    $this->excel->getActiveSheet()->setCellValue('F'.$ke,'~');

		    $this->excel->getActiveSheet()->setCellValue('G'.$ke,$pendidikan['selesai']);
		    $this->excel->getActiveSheet()->mergeCells('G'.$ke.':H'.$ke);

		    $this->excel->getActiveSheet()->setCellValue('I'.$ke,'thn');

		    //$this->excel->getActiveSheet()->setCellValue('J'.$ke,$pendidikan['bulan_selsai']);

		    $this->excel->getActiveSheet()->setCellValue('K'.$ke,'bln');

		    $this->excel->getActiveSheet()->setCellValue('L'.$ke,$pendidikan['instansi']);
		    $this->excel->getActiveSheet()->mergeCells('L'.$ke.':Y'.$ke);
		    $this->excel->getActiveSheet()->getStyle('L'.$ke.':Y'.$ke)->applyFromArray($kanan);

		    $this->excel->getActiveSheet()->setCellValue('Z'.$ke,$pendidikan['nama_jurusan']);
		    $this->excel->getActiveSheet()->mergeCells('Z'.$ke.':AE'.$ke);
		    $this->excel->getActiveSheet()->getStyle('Z'.$ke.':AE'.$ke)->applyFromArray($kanan);
		    $ke++;
		}

		///////////////////////////////////////////////////////////////////////////////////////

		$this->excel->getActiveSheet()->setCellValue('A'.$ke,'Riwayat Pekerjaan');
		$this->excel->getActiveSheet()->mergeCells('A'.$ke.':Y'.$ke);
		$this->excel->getActiveSheet()->getStyle('A'.$ke.':Y'.$ke)->applyFromArray($border);

		$this->excel->getActiveSheet()->setCellValue('Z'.$ke,'Jenis Kerja');
		$this->excel->getActiveSheet()->mergeCells('Z'.$ke.':AE'.$ke);
		$this->excel->getActiveSheet()->getStyle('Z'.$ke.':AE'.$ke)->applyFromArray($border);
		$ke++;

		//$mysql_pek=mysql_query("SELECT * FROM `tb_riwayat_pekerjaan` where nik ='$_REQUEST[id]' order by tahun_masuk desc");
		foreach($riwayat_pekerjaan as $rp){
		    $this->excel->getActiveSheet()->setCellValue('A'.$ke, $rp['tahun_masuk']);
		    $this->excel->getActiveSheet()->mergeCells('A'.$ke.':B'.$ke);

		    $this->excel->getActiveSheet()->setCellValue('C'.$ke,'thn');

		    $this->excel->getActiveSheet()->setCellValue('D'.$ke, $rp['bulan_masuk']);

		    $this->excel->getActiveSheet()->setCellValue('E'.$ke,'bln');

		    $this->excel->getActiveSheet()->setCellValue('F'.$ke,'~');
		if($rp['tahun_keluar']=='Sampai Sekarang'){
			$this->excel->getActiveSheet()->setCellValue('G'.$ke, $rp['tahun_keluar']);
		    $this->excel->getActiveSheet()->mergeCells('G'.$ke.':K'.$ke);
		}else{
		    $this->excel->getActiveSheet()->setCellValue('G'.$ke, $rp['tahun_keluar']);
		    $this->excel->getActiveSheet()->mergeCells('G'.$ke.':H'.$ke);

		    $this->excel->getActiveSheet()->setCellValue('I'.$ke,'thn');

		    $this->excel->getActiveSheet()->setCellValue('J'.$ke, $rp['bulan_keluar']);

		    $this->excel->getActiveSheet()->setCellValue('K'.$ke,'bln');
		}
		    $this->excel->getActiveSheet()->setCellValue('L'.$ke,'Di '. $rp['perusahaan']);
		    $this->excel->getActiveSheet()->mergeCells('L'.$ke.':Y'.$ke);
		    $this->excel->getActiveSheet()->getStyle('L'.$ke.':Y'.$ke)->applyFromArray($kanan);

		    $this->excel->getActiveSheet()->setCellValue('Z'.$ke, searchArray($rp['posisi'], $pekerjaan, 'kd_pekerjaan', 'pekerjaan'));
		    $this->excel->getActiveSheet()->mergeCells('Z'.$ke.':AE'.$ke);
		    $this->excel->getActiveSheet()->getStyle('Z'.$ke.':AE'.$ke)->applyFromArray($kanan);
		    $ke++;

		}

		$awal=$ke;
		$this->excel->getActiveSheet()->setCellValue('C'.$ke,'Hubungan');
		$this->excel->getActiveSheet()->mergeCells('C'.$ke.':G'.$ke);
		$this->excel->getActiveSheet()->getStyle('C'.$ke.':G'.$ke)->applyFromArray($border);

		$this->excel->getActiveSheet()->setCellValue('H'.$ke,'Nama');
		$this->excel->getActiveSheet()->mergeCells('H'.$ke.':O'.$ke);
		$this->excel->getActiveSheet()->getStyle('H'.$ke.':O'.$ke)->applyFromArray($border);

		$this->excel->getActiveSheet()->setCellValue('P'.$ke,'Tgl. Lahir');
		$this->excel->getActiveSheet()->mergeCells('P'.$ke.':S'.$ke);
		$this->excel->getActiveSheet()->getStyle('P'.$ke.':S'.$ke)->applyFromArray($border);

		$this->excel->getActiveSheet()->setCellValue('T'.$ke,'Umur');
		$this->excel->getActiveSheet()->mergeCells('T'.$ke.':U'.$ke);
		$this->excel->getActiveSheet()->getStyle('T'.$ke.':U'.$ke)->applyFromArray($border);

		$this->excel->getActiveSheet()->setCellValue('V'.$ke,'Pekerjaan');
		$this->excel->getActiveSheet()->mergeCells('V'.$ke.':AE'.$ke);
		$this->excel->getActiveSheet()->getStyle('V'.$ke.':AE'.$ke)->applyFromArray($border);
		$ke++;

		//$sql_kel=mysql_query("SELECT * FROM `tb_keluarga_pem` where kd_pemagang='$_REQUEST[id]' order by tanggal_lahir ASC ") or die(mysql_error());
		foreach($keluarga_pem as $kp){

		  $this->excel->getActiveSheet()->setCellValue('C'.$ke, $kp['hubungan']);
		  $this->excel->getActiveSheet()->mergeCells('C'.$ke.':G'.$ke);
		  $this->excel->getActiveSheet()->getStyle('C'.$ke.':G'.$ke)->applyFromArray($kanan);

		  $this->excel->getActiveSheet()->setCellValue('H'.$ke, $kp['nama']);
		  $this->excel->getActiveSheet()->mergeCells('H'.$ke.':O'.$ke);
		  $this->excel->getActiveSheet()->getStyle('H'.$ke.':O'.$ke)->applyFromArray($kanan);

		  $this->excel->getActiveSheet()->setCellValue('P'.$ke, $kp['tanggal_lahir']);
		  $this->excel->getActiveSheet()->mergeCells('P'.$ke.':S'.$ke);
		  $this->excel->getActiveSheet()->getStyle('P'.$ke.':S'.$ke)->applyFromArray($kanan);

		  $this->excel->getActiveSheet()->setCellValue('T'.$ke, $kp['tanggal_lahir']);
		  $this->excel->getActiveSheet()->mergeCells('T'.$ke.':U'.$ke);
		  $this->excel->getActiveSheet()->getStyle('T'.$ke.':U'.$ke)->applyFromArray($kanan);

		  $this->excel->getActiveSheet()->setCellValue('V'.$ke, $kp['pekerjaan']);
		  $this->excel->getActiveSheet()->mergeCells('V'.$ke.':AE'.$ke);
		  $this->excel->getActiveSheet()->getStyle('V'.$ke.':AE'.$ke)->applyFromArray($kanan);
		  $ke++;
		}

		//$sql_kel=mysql_query("SELECT * FROM tb_keluarga where nik='$_REQUEST[id]' ORDER BY tgl ASC   ") or die(mysql_error());
		foreach($keluarga as $kel){
		  $this->excel->getActiveSheet()->setCellValue('C'.$ke, $kel['hubungan']);
		  $this->excel->getActiveSheet()->mergeCells('C'.$ke.':G'.$ke);
		  $this->excel->getActiveSheet()->getStyle('C'.$ke.':G'.$ke)->applyFromArray($kanan);

		  $this->excel->getActiveSheet()->setCellValue('H'.$ke, $kel['nama_kel']);
		  $this->excel->getActiveSheet()->mergeCells('H'.$ke.':O'.$ke);
		  $this->excel->getActiveSheet()->getStyle('H'.$ke.':O'.$ke)->applyFromArray($kanan);

		  $this->excel->getActiveSheet()->setCellValue('P'.$ke, $kel['tgl']);
		  $this->excel->getActiveSheet()->mergeCells('P'.$ke.':S'.$ke);
		  $this->excel->getActiveSheet()->getStyle('P'.$ke.':S'.$ke)->applyFromArray($kanan);

		  $this->excel->getActiveSheet()->setCellValue('T'.$ke, $kel['tgl']);
		  $this->excel->getActiveSheet()->mergeCells('T'.$ke.':U'.$ke);
		  $this->excel->getActiveSheet()->getStyle('T'.$ke.':U'.$ke)->applyFromArray($kanan);

		  $this->excel->getActiveSheet()->setCellValue('V'.$ke, $kel['pekerjaan']);
		  $this->excel->getActiveSheet()->mergeCells('V'.$ke.':AE'.$ke);
		  $this->excel->getActiveSheet()->getStyle('V'.$ke.':AE'.$ke)->applyFromArray($kanan);
		  $ke++;
		}


		$akhir=$ke-1;

		$this->excel->getActiveSheet()->getStyle('A'.$awal.':AE'.$awal)->applyFromArray($atas);
		$this->excel->getActiveSheet()->getStyle('B'.$awal.':B'.$akhir)->applyFromArray($kanan);

		$this->excel->getActiveSheet()->setCellValue('A'.$awal,'Susunan');
		$this->excel->getActiveSheet()->mergeCells('A'.$awal.':A'.$akhir);
		$this->excel->getActiveSheet()->getStyle('A'.$awal)->getAlignment()->setTextRotation(90);

		$this->excel->getActiveSheet()->setCellValue('B'.$awal,'Keluarga');
		$this->excel->getActiveSheet()->mergeCells('B'.$awal.':B'.$akhir);
		$this->excel->getActiveSheet()->getStyle('B'.$awal)->getAlignment()->setTextRotation(90);


		///////////////////////////////////////////////////////////////////////////////////////
		$this->excel->getActiveSheet()->setCellValue('A'.$ke,'Tinggi Badan ');
		$this->excel->getActiveSheet()->mergeCells('A'.$ke.':D'.$ke);

		$this->excel->getActiveSheet()->setCellValue('E'.$ke, $pemagang->tb .' C');
		$this->excel->getActiveSheet()->mergeCells('E'.$ke.':G'.$ke);

		$this->excel->getActiveSheet()->getStyle('A'.$ke.':G'.$ke)->applyFromArray($border);

		$this->excel->getActiveSheet()->setCellValue('H'.$ke,'Berat Badan ');
		$this->excel->getActiveSheet()->mergeCells('H'.$ke.':K'.$ke);

		$this->excel->getActiveSheet()->setCellValue('L'.$ke, $pemagang->bb.' K');
		$this->excel->getActiveSheet()->mergeCells('L'.$ke.':N'.$ke);
		$this->excel->getActiveSheet()->getStyle('H'.$ke.':N'.$ke)->applyFromArray($border);

		$this->excel->getActiveSheet()->setCellValue('O'.$ke,'Lingkaran Pinggang ');
		$this->excel->getActiveSheet()->mergeCells('O'.$ke.':T'.$ke);

		$this->excel->getActiveSheet()->setCellValue('U'.$ke, $pemagang->lingkar_pinggang.' C');
		$this->excel->getActiveSheet()->mergeCells('U'.$ke.':V'.$ke);

		$this->excel->getActiveSheet()->getStyle('O'.$ke.':V'.$ke)->applyFromArray($border);

		$this->excel->getActiveSheet()->setCellValue('W'.$ke,'Ukuran Sepatu ');
		$this->excel->getActiveSheet()->mergeCells('W'.$ke.':AA'.$ke);

		$this->excel->getActiveSheet()->setCellValue('AB'.$ke, $pemagang->no_sepatu.' C');
		$this->excel->getActiveSheet()->mergeCells('AB'.$ke.':AE'.$ke);
		$this->excel->getActiveSheet()->getStyle('W'.$ke.':AE'.$ke)->applyFromArray($border);
		$ke++;

		//////////////////////////////////////////////////////////////////////////////////////////
		$this->excel->getActiveSheet()->setCellValue('A'.$ke,'Penglihatan (Kiri/Kanan) ');
		$this->excel->getActiveSheet()->mergeCells('A'.$ke.':G'.$ke);

		$this->excel->getActiveSheet()->setCellValue('H'.$ke,@$cek_mata['nilai']);
		$this->excel->getActiveSheet()->mergeCells('H'.$ke.':K'.$ke);
		$this->excel->getActiveSheet()->getStyle('A'.$ke.':K'.$ke)->applyFromArray($border);

		$this->excel->getActiveSheet()->setCellValue('L'.$ke,'Penglihatan dgn Kacamata (Kiri/Kanan) ');
		$this->excel->getActiveSheet()->mergeCells('L'.$ke.':V'.$ke);

		$this->excel->getActiveSheet()->setCellValue('W'.$ke,@$cek_mata2['nilai']);
		$this->excel->getActiveSheet()->mergeCells('W'.$ke.':Z'.$ke);

		$this->excel->getActiveSheet()->getStyle('L'.$ke.':Z'.$ke)->applyFromArray($border);

		$this->excel->getActiveSheet()->setCellValue('AA'.$ke,'Gol Darah ');
		$this->excel->getActiveSheet()->mergeCells('AA'.$ke.':AC'.$ke);

		$this->excel->getActiveSheet()->setCellValue('AD'.$ke, $pemagang->gol_darah);
		$this->excel->getActiveSheet()->mergeCells('AD'.$ke.':AE'.$ke);
		$this->excel->getActiveSheet()->getStyle('X'.$ke.':AE'.$ke)->applyFromArray($border);
		$ke++;

		/////////////////////////////////////////////////////////////////////////////////////////
		$this->excel->getActiveSheet()->setCellValue('A'.$ke,'Pelajaran yang disukai ');
		$this->excel->getActiveSheet()->mergeCells('A'.$ke.':G'.$ke);

		$this->excel->getActiveSheet()->setCellValue('H'.$ke, $pemagang->suka_pelajaran);
		$this->excel->getActiveSheet()->mergeCells('H'.$ke.':K'.$ke);
		$this->excel->getActiveSheet()->getStyle('A'.$ke.':K'.$ke)->applyFromArray($border);

		$this->excel->getActiveSheet()->setCellValue('L'.$ke,'Hobi ');
		$this->excel->getActiveSheet()->mergeCells('L'.$ke.':M'.$ke);

		$this->excel->getActiveSheet()->setCellValue('N'.$ke, $pemagang->hobi);
		$this->excel->getActiveSheet()->mergeCells('N'.$ke.':U'.$ke);
		$this->excel->getActiveSheet()->getStyle('L'.$ke.':U'.$ke)->applyFromArray($border);

		$this->excel->getActiveSheet()->setCellValue('V'.$ke,'Ketrampilan/Skil ');
		$this->excel->getActiveSheet()->mergeCells('V'.$ke.':Z'.$ke);

		$this->excel->getActiveSheet()->setCellValue('AA'.$ke, $pemagang->skil);
		$this->excel->getActiveSheet()->mergeCells('AA'.$ke.':AE'.$ke);
		$this->excel->getActiveSheet()->getStyle('V'.$ke.':AE'.$ke)->applyFromArray($border);
		$ke++;

		/////////////////////////////////////////////////////////////////////////////////////////
		$this->excel->getActiveSheet()->setCellValue('A'.$ke,'Riwayat Penyakit ');
		$this->excel->getActiveSheet()->mergeCells('A'.$ke.':K'.$ke);

		$this->excel->getActiveSheet()->setCellValue('L'.$ke,'Nama Penyakit '.empty($riwayat_penyakit->nama_penyakit) ? "" : $riwayat_penyakit->nama_penyakit);
		$this->excel->getActiveSheet()->mergeCells('L'.$ke.':T'.$ke);

		$this->excel->getActiveSheet()->setCellValue('U'.$ke,'Kapan? ');
		$this->excel->getActiveSheet()->mergeCells('U'.$ke.':W'.$ke);

		$this->excel->getActiveSheet()->setCellValue('X'.$ke,'Thn '.empty($riwayat_penyakit->tahun) ? "" : $riwayat_penyakit->tahun);
		$this->excel->getActiveSheet()->mergeCells('X'.$ke.':AA'.$ke);

		$this->excel->getActiveSheet()->setCellValue('AB'.$ke,'Bln '. empty($riwayat_penyakit->bulan) ? "" : $riwayat_penyakit->bulan);
		$this->excel->getActiveSheet()->mergeCells('AB'.$ke.':AE'.$ke);
		$this->excel->getActiveSheet()->getStyle('AB'.$ke.':AE'.$ke)->applyFromArray($kanan);
		$ke++;

		$status_merokok = '';
		if(!empty($merokok->status))
		{
			if($merokok->status == 2)
			{
				$status_merokok = 'YA, '.$merokok->hari.' per-hari';
			}
			else if($merokok->status == 1)
			{
				$status_merokok = 'BERHENTI';
			}
			else
			{
				$status_merokok = 'TIDAK';
			}
		}

		$status_minum_alkohol = '';
		if(!empty($minum_alkohol->status))
		{
			if($minum_alkohol->status == 2)
			{
				$status_minum_alkohol = 'YA, '.$minum_alkohol->bulan.' per-hari';
			}
			else if($minum_alkohol->status == 1)
			{
				$status_minum_alkohol = 'BERHENTI';
			}
			else
			{
				$status_minum_alkohol = 'TIDAK';
			}
		}

		/////////////////////////////////////////////////////////////////////////////////////////
		$this->excel->getActiveSheet()->setCellValue('A'.$ke,'Merokok : '.$status_merokok);
		$this->excel->getActiveSheet()->mergeCells('A'.$ke.':AE'.$ke);
		$this->excel->getActiveSheet()->getStyle('A'.$ke.':AE'.$ke)->applyFromArray($border);
		$ke++;

		////////////////////////////////////////////////////////////////////////////////////////
		$this->excel->getActiveSheet()->setCellValue('A'.$ke,'Minum Alkohol : '.$status_minum_alkohol);
		$this->excel->getActiveSheet()->mergeCells('A'.$ke.':AE'.$ke);
		$this->excel->getActiveSheet()->getStyle('A'.$ke.':AE'.$ke)->applyFromArray($border);

		$this->excel->getActiveSheet()->getStyle('AE3:AE'.$ke)->applyFromArray($kanan);
		$this->excel->getActiveSheet()->getStyle('A3:A'.$ke)->applyFromArray($kiri);
		///////////////////////////////////////judul////////////////////////////

		$this->excel->getActiveSheet()->mergeCells('A1:AE1');
		$this->excel->getActiveSheet()->setCellValue('A1', "Biodata");
		$this->excel->getActiveSheet()->getStyle('A1:AE1')->getFont()->setName('Century');
		$this->excel->getActiveSheet()->getStyle('A1:AE1')->getFont()->setSize(24);
		$this->excel->getActiveSheet()->getStyle('A1:AE1')->getFont()->setBold(true);
		$this->excel->getActiveSheet()->getStyle('A1:AE1')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
		////////////////////////////////////judul///////////////////////////

		header('Content-Type: application/vnd.ms-excel');
		header('Content-Disposition: attachment; filename="' . 'Biodata_Pemagang_'.$kd_pemagang.'.xls"');
		header('Cache-Control: max-age= 0');
		$objWriter = PHPExcel_IOFactory::createWriter($this->excel, 'Excel2007');
		ob_end_clean();
		$objWriter->save('php://output');
	}


	// EXPORT BIODATA BAHASA Jepang

	public function print_biodata_pemagang_jepang($kd_pemagang)
	{
		$this->db->select(array
											(
												'tb_pemagangan.*',
 												'AGAMA.agama',
												'YEAR(NOW()) - YEAR(tb_pemagangan.tanggal_lahir) as umur'
											));
		$this->db->from('tb_pemagangan');
		$this->db->where(array('kd_pemagang' => $kd_pemagang));
		$this->db->join('tb_agama AGAMA', 'AGAMA.kd_agama = tb_pemagangan.id_agama');
		$q_pemagang = $this->db->get();
		$q_passport = $this->db->get_where('tb_pasport', array('kd_pemagang' => $kd_pemagang));
		$q_riwayat_keluar_negri = $this->db->get_where('tb_riwayat_keluarnegri', array ('kd_pemagang' => $kd_pemagang));
		$q_riwayat_pekerjaan = $this->db->get_where('tb_riwayat_pekerjaan', array('nik' => $kd_pemagang));

		$this->db->select('RP.*, J.nama_jurusan, J.nama_jurusan_jp');
		$this->db->from('dt_riwayat_pendidikan RP');
		$this->db->join('ms_jurusan_pendidikan J', 'J.id_jurusan = RP.id_jurusan', 'left');
		$this->db->where('RP.kd_pemagang', $kd_pemagang);
		$q_riwayat_pendidikan = $this->db->get();

		$this->db->select('K.*, H.hubungan_jp, PKJ.pekerjaan_jp');
		$this->db->from('tb_keluarga K');
		$this->db->join('tb_hubungan H', 'H.kd_hubungan = K.kd_stat_kel');
		$this->db->join('tb_pekerjaan PKJ', 'K.pekerjaan = PKJ.kd_pekerjaan', 'left');
		$this->db->where('K.nik', $kd_pemagang);
		$q_keluarga = $this->db->get();

		$this->db->select('K.*, H.hubungan_jp, PKJ.pekerjaan_jp');
		$this->db->from('tb_keluarga_pem K');
		$this->db->join('tb_hubungan H', 'H.kd_hubungan = K.hubungan');
		$this->db->join('tb_pekerjaan PKJ', 'K.pekerjaan = PKJ.kd_pekerjaan', 'left');
		$this->db->where('K.kd_pemagang', $kd_pemagang);
		$q_keluarga_pem = $this->db->get();

		$this->db->select('K.*, H.hubungan_jp');
		$this->db->from('keluarga_pemagang_dijepang K');
		$this->db->join('tb_hubungan H', 'H.kd_hubungan = K.kd_hubungan');
		$this->db->where('K.kd_pemagang', $kd_pemagang);
		$q_keluarga_dijepang = $this->db->get();

		$q_pekerjaan = $this->db->get('tb_pekerjaan');

		$pemagang = $q_pemagang->row();
		$passport = $q_passport->row();
		$riwayat_keluar_negri = $q_riwayat_keluar_negri->result_array();
		$riwayat_pekerjaan = $q_riwayat_pekerjaan->result_array();
		$riwayat_pendidikan = $q_riwayat_pendidikan->result_array();
		$keluarga = $q_keluarga->result_array();
		$keluarga_pem = $q_keluarga_pem->result_array();
		$keluarga_dijepang = $q_keluarga_dijepang->result_array();
		$pekerjaan = $q_pekerjaan->result_array();

		$riwayat_penyakit = $this->db->get_where('tb_riwayat_penyakit', array(
			'nik' => $kd_pemagang
		))->row();

		$merokok = $this->db->get_where('tb_detail_kebiasaan', array(
			'kd_pemagang' => $kd_pemagang,
			'kd_kebiasaan' => 1
		))->row();

		$minum_alkohol = $this->db->get_where('tb_detail_kebiasaan', array(
			'kd_pemagang' => $kd_pemagang,
			'kd_kebiasaan' => 2
		))->row();

		$this->load->library('Excel');

		// Set page orientation and size
		$this->excel->getActiveSheet()->getPageSetup()->setOrientation(PHPExcel_Worksheet_PageSetup::ORIENTATION_LANDSCAPE);
		$this->excel->getActiveSheet()->getPageSetup()->setPaperSize(PHPExcel_Worksheet_PageSetup::PAPERSIZE_LEGAL);
		$this->excel->getActiveSheet()->getPageMargins()->setTop(0.75);
		$this->excel->getActiveSheet()->getPageMargins()->setRight(0.75);
		$this->excel->getActiveSheet()->getPageMargins()->setLeft(0.75);
		$this->excel->getActiveSheet()->getPageMargins()->setBottom(0.75);
		$this->excel->getActiveSheet()->getHeaderFooter()->setOddFooter('&L&B' . $this->excel->getProperties()->getTitle() . '&RPage &P of &N');

		///style/////
		$bawah = array(
     	'borders' => array(
				'bottom' => array(
          'style' => PHPExcel_Style_Border ::BORDER_THIN,
            'color' => array('argb' => '0000'),
           ),
       ),
		);

		$atas = array(
		       'borders' => array(
		             'top' => array(
		                    'style' => PHPExcel_Style_Border ::BORDER_THIN,
		                    'color' => array('argb' => '0000'),
		             ),
		       ),
		);

		$kanan = array(
		       'borders' => array(
		             'right' => array(
		                    'style' => PHPExcel_Style_Border ::BORDER_THIN,
		                    'color' => array('argb' => '0000'),
		             ),
		       ),
		);

		$kiri = array(
		       'borders' => array(
		             'left' => array(
		                    'style' => PHPExcel_Style_Border ::BORDER_THIN,
		                    'color' => array('argb' => '0000'),
		             ),
		       ),
		);

		$border = array(
		       'borders' => array(
		             'outline' => array(
		                    'style' => PHPExcel_Style_Border::BORDER_THIN,
		                    'color' => array('argb' => '000'),
		             ),
		       ),
		);

		$this->excel->setActiveSheetIndex(0);

		//untuk auto size colomn
		$a=2;
		while ($a < 100) {
		  $this->excel->getActiveSheet()->getRowDimension($a)->setRowHeight(25);
		  $a++;
		}

		$this->excel->getActiveSheet()->getStyle('A1:AE100')->getAlignment()->setVertical(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
		$this->excel->getActiveSheet()->getColumnDimension('A')->setWidth(4);
		$this->excel->getActiveSheet()->getColumnDimension('B')->setWidth(4);
		$this->excel->getActiveSheet()->getColumnDimension('C')->setWidth(4);
		$this->excel->getActiveSheet()->getColumnDimension('D')->setWidth(4);
		$this->excel->getActiveSheet()->getColumnDimension('E')->setWidth(4);
		$this->excel->getActiveSheet()->getColumnDimension('F')->setWidth(4);
		$this->excel->getActiveSheet()->getColumnDimension('G')->setWidth(4);
		$this->excel->getActiveSheet()->getColumnDimension('H')->setWidth(4);
		$this->excel->getActiveSheet()->getColumnDimension('I')->setWidth(4);
		$this->excel->getActiveSheet()->getColumnDimension('J')->setWidth(4);
		$this->excel->getActiveSheet()->getColumnDimension('K')->setWidth(4);
		$this->excel->getActiveSheet()->getColumnDimension('L')->setWidth(4);
		$this->excel->getActiveSheet()->getColumnDimension('M')->setWidth(4);
		$this->excel->getActiveSheet()->getColumnDimension('N')->setWidth(4);
		$this->excel->getActiveSheet()->getColumnDimension('O')->setWidth(4);
		$this->excel->getActiveSheet()->getColumnDimension('P')->setWidth(4);
		$this->excel->getActiveSheet()->getColumnDimension('Q')->setWidth(4);
		$this->excel->getActiveSheet()->getColumnDimension('R')->setWidth(4);
		$this->excel->getActiveSheet()->getColumnDimension('S')->setWidth(4);
		$this->excel->getActiveSheet()->getColumnDimension('T')->setWidth(4);
		$this->excel->getActiveSheet()->getColumnDimension('U')->setWidth(4);
		$this->excel->getActiveSheet()->getColumnDimension('V')->setWidth(4);
		$this->excel->getActiveSheet()->getColumnDimension('W')->setWidth(4);
		$this->excel->getActiveSheet()->getColumnDimension('X')->setWidth(4);
		$this->excel->getActiveSheet()->getColumnDimension('Y')->setWidth(4);
		$this->excel->getActiveSheet()->getColumnDimension('Z')->setWidth(4);
		$this->excel->getActiveSheet()->getColumnDimension('AA')->setWidth(4);
		$this->excel->getActiveSheet()->getColumnDimension('AB')->setWidth(4);
		$this->excel->getActiveSheet()->getColumnDimension('AC')->setWidth(4);
		$this->excel->getActiveSheet()->getColumnDimension('AD')->setWidth(4);
		$this->excel->getActiveSheet()->getColumnDimension('AE')->setWidth(4);

		if(!empty($pemagang->foto)){
		  $dir= $_SERVER['DOCUMENT_ROOT'] . '/media/' . md5($pemagang->kd_pemagang.'manfikar') .'/'.$pemagang->foto;

			if(file_exists($dir))
			{
				$objDrawing = new PHPExcel_Worksheet_Drawing();
			  $objDrawing->setName('Logo');
			  $objDrawing->setDescription('Logo');
			  $objDrawing->setPath($dir);
			  $objDrawing->setCoordinates('Z3');
			  $objDrawing->setHeight(250);
			  $objDrawing->setWidth(150);
			  $objDrawing->setWorksheet($this->excel->getActiveSheet());
			}
		}

		$this->excel->getActiveSheet()->setTitle($kd_pemagang);

		/////////////////////////////////////////////////
		$this->excel->getActiveSheet()->setCellValue('A3', "氏名 :"); // NAMA LENGKAP
		$this->excel->getActiveSheet()->mergeCells('A3:M3');
		$this->excel->getActiveSheet()->getStyle('A3:M3')->applyFromArray($atas);

		$this->excel->getActiveSheet()->setCellValue('N3', '国籍 :');
		$this->excel->getActiveSheet()->mergeCells('N3:R3');

		$this->excel->getActiveSheet()->setCellValue('S3', ': '. $pemagang->warga_negara);
		$this->excel->getActiveSheet()->mergeCells('S3:T3');

		$this->excel->getActiveSheet()->getStyle('N3:T3')->applyFromArray($border);

		$this->excel->getActiveSheet()->setCellValue('U3', '年齢：'); //UMUR
		$this->excel->getActiveSheet()->mergeCells('U3:V3');

		$this->excel->getActiveSheet()->setCellValue('W3', $pemagang->umur .' Tahun');
		$this->excel->getActiveSheet()->mergeCells('W3:Y3');

		$this->excel->getActiveSheet()->getStyle('U3:Y3')->applyFromArray($border);
		/////////////////////////////////////
		$this->excel->getActiveSheet()->setCellValue('A4', '(英字) '); //ALPHABET
		$this->excel->getActiveSheet()->mergeCells('A4:D4');

		$this->excel->getActiveSheet()->setCellValue('E4',ucfirst($pemagang->nama_pem));
		$this->excel->getActiveSheet()->mergeCells('E4:M4');
		$this->excel->getActiveSheet()->getStyle('F4:M4')->applyFromArray($kanan);

		$this->excel->getActiveSheet()->setCellValue('N4', '性別 : '); //JENIS KELAMIN
		$this->excel->getActiveSheet()->mergeCells('N4:Q4');

		$this->excel->getActiveSheet()->setCellValue('R4', $pemagang->jk == 'P' ? "女" : "男");

		$this->excel->getActiveSheet()->getStyle('N4:R4')->applyFromArray($border);

		$status_kawin = "";

		switch ($pemagang->status) {
			case 'b':
				$status_kawin = "独身";
				break;
			case 'n':
				$status_kawin = '既婚';
				break;
			case 'ns':
				$status_kawin = '未登録の結婚';
				break;
			case 'jh':
				$status_kawin = '離婚';
				break;
			case 'jm':
				$status_kawin = '未亡人';
				break;
			default:
				$status_kawin = "独身";
				break;
		}

		$this->excel->getActiveSheet()->setCellValue('S4', '結婚状況 :');
		$this->excel->getActiveSheet()->mergeCells('S4:V4');

		$this->excel->getActiveSheet()->setCellValue('W4', $pemagang->status == 'n' ? "既婚" : "独身");
		$this->excel->getActiveSheet()->mergeCells('W4:Y4');

		$this->excel->getActiveSheet()->getStyle('S4:Y4')->applyFromArray($border);

		//////////////////////////////////////////
		$this->excel->getActiveSheet()->setCellValue('A5', "(カナ)");
		$this->excel->getActiveSheet()->mergeCells('A5:M5');

		$this->excel->getActiveSheet()->setCellValue('N5', '宗教 :'); //AGAMA
		$this->excel->getActiveSheet()->mergeCells('N5:P5');

		$this->excel->getActiveSheet()->setCellValue('Q5', empty($pemagang->agama_jp) ? "" : $pemagang->agama_jp);
		$this->excel->getActiveSheet()->mergeCells('Q5:Y5');

		$this->excel->getActiveSheet()->getStyle('N5:Y5')->applyFromArray($border);

		//////////////////////////////////////////
		$this->excel->getActiveSheet()->setCellValue('A6', '生年月日 :'); // TANGGAL LAHIR
		$this->excel->getActiveSheet()->mergeCells('A6:C6');

		$this->excel->getActiveSheet()->setCellValue('D6', empty($pemagang->tanggal_lahir) ? "" : $pemagang->tanggal_lahir);
		$this->excel->getActiveSheet()->mergeCells('D6:M6');

		$this->excel->getActiveSheet()->getStyle('A6:M6')->applyFromArray($border);

		$this->excel->getActiveSheet()->setCellValue('N6', '出生地 :'); //TEMPAT ASAL
		$this->excel->getActiveSheet()->mergeCells('N6:Q6');

		$this->excel->getActiveSheet()->setCellValue('R6', empty($pemagang->tempat_asal) ? "" : $pemagang->tempat_asal);
		$this->excel->getActiveSheet()->mergeCells('R6:Y6');

		$this->excel->getActiveSheet()->getStyle('N6:Y6')->applyFromArray($border);

		//////////////////////////////////////////
		$this->excel->getActiveSheet()->setCellValue('A7', ''); //alamat ktp
		$this->excel->getActiveSheet()->mergeCells('A7:D7');

		$this->excel->getActiveSheet()->setCellValue('E7', empty($pemagang->alamat_ktp) ? "" : $pemagang->alamat_ktp);
		$this->excel->getActiveSheet()->mergeCells('E7:P7');

		$this->excel->getActiveSheet()->getStyle('A7:P7')->applyFromArray($border);

		$this->excel->getActiveSheet()->setCellValue('Q7', '電話番号 :'); // NOMOR HANDPHONE
		$this->excel->getActiveSheet()->mergeCells('Q7:S7');

		$this->excel->getActiveSheet()->setCellValue('T7', $pemagang->no_hp);
		$this->excel->getActiveSheet()->mergeCells('T7:Y7');

		$this->excel->getActiveSheet()->getStyle('Q7:Y7')->applyFromArray($border);

		//////////////////////////////////////////
		$this->excel->getActiveSheet()->setCellValue('A8', '現住所 :'); //ALAMAT SEKARANG
		$this->excel->getActiveSheet()->mergeCells('A8:E8');

		$this->excel->getActiveSheet()->setCellValue('F8', $pemagang->tempat_tinggal);
		$this->excel->getActiveSheet()->mergeCells('F8:P8');

		$this->excel->getActiveSheet()->getStyle('A8:P8')->applyFromArray($border);

		$this->excel->getActiveSheet()->setCellValue('Q8', ''); //no telp lainnya
		$this->excel->getActiveSheet()->mergeCells('Q8:U8');

		$this->excel->getActiveSheet()->setCellValue('V8', $pemagang->no_tel);
		$this->excel->getActiveSheet()->mergeCells('V8:Y8');

		$this->excel->getActiveSheet()->getStyle('Q8:Y8')->applyFromArray($border);

		////////////////////////////////////////////
		$this->excel->getActiveSheet()->setCellValue('A9', "パスポート"); // PASSPORT
		$this->excel->getActiveSheet()->mergeCells('A9:F9');

		$this->excel->getActiveSheet()->setCellValue('G9', "パスポートNo. :");
		$this->excel->getActiveSheet()->mergeCells('G9:P9');

		$this->excel->getActiveSheet()->setCellValue('Q9', '有効期限'); //TANGGAL PENERBITAN
		$this->excel->getActiveSheet()->mergeCells('Q9:W9');

		$this->excel->getActiveSheet()->setCellValue('X9', '年 '. @Date('Y', strtotime($passport->terbit))); //tahun
		$this->excel->getActiveSheet()->mergeCells('X9:Y9');
		$this->excel->getActiveSheet()->getStyle('X9:Y9')->applyFromArray($atas);

		$this->excel->getActiveSheet()->setCellValue('Z9', '月 '. @Date('m', strtotime($passport->terbit))); // bulan
		$this->excel->getActiveSheet()->mergeCells('Z9:AA9');
		$this->excel->getActiveSheet()->getStyle('Z9:AA9')->applyFromArray($atas);

		$this->excel->getActiveSheet()->setCellValue('AB9', '日  '. @Date('d', strtotime($passport->terbit))); // tanggal
		$this->excel->getActiveSheet()->mergeCells('AB9:AE9');
		$this->excel->getActiveSheet()->getStyle('AB9:AE9')->applyFromArray($atas);

		///////////////////////////////////////////////////////
		$this->excel->getActiveSheet()->setCellValue('B10', '');
		$this->excel->getActiveSheet()->mergeCells('B10:F10');

		$this->excel->getActiveSheet()->setCellValue('G10', @$passport->no_pass);
		$this->excel->getActiveSheet()->mergeCells('G10:P10');

		$this->excel->getActiveSheet()->setCellValue('Q10', '有効期限 : ');
		$this->excel->getActiveSheet()->mergeCells('Q10:W10');

		$this->excel->getActiveSheet()->setCellValue('X10', '年 '. @isset($passport->masa_berlaku) ? Date('Y', @strtotime($passport->masa_berlaku)) : '');
		$this->excel->getActiveSheet()->mergeCells('X10:Y10');

		$this->excel->getActiveSheet()->setCellValue('Z10', '月 '. @isset($passport->masa_berlaku) ? Date('m', @strtotime($passport->masa_berlaku)) : '');
		$this->excel->getActiveSheet()->mergeCells('Z10:AA10');

		$this->excel->getActiveSheet()->setCellValue('AB10', '日 '. @isset($passport->masa_berlaku) ? Date('d', @strtotime($passport->masa_berlaku)) : '');
		$this->excel->getActiveSheet()->mergeCells('AB10:AC10');

		////////////////////////////////////////////
		$this->excel->getActiveSheet()->setCellValue('A11', "海外渡航歴"); //riwayat keluar negri
		$this->excel->getActiveSheet()->mergeCells('A11:AE11');
		$this->excel->getActiveSheet()->getStyle('A11:AE11')->applyFromArray($atas);


		$ke=12;
		////////////////////////////////////////////
		if (!empty($riwayat_keluar_negri)) {
			foreach ($riwayat_keluar_negri as $rkn) {
				$this->excel->getActiveSheet()->setCellValue("A{$ke}", !empty($riwayat_keluar_negri) ? '有' : '');
				$this->excel->getActiveSheet()->mergeCells("A{$ke}:G{$ke}");
				$this->excel->getActiveSheet()->getStyle("A{$ke}:AE{$ke}")->applyFromArray($bawah);

				$this->excel->getActiveSheet()->setCellValue("H{$ke}", '年 ' . $rkn['tahun']);
				$this->excel->getActiveSheet()->mergeCells("H{$ke}:I{$ke}");

				$this->excel->getActiveSheet()->setCellValue("J{$ke}", "月 ". $rkn['bulan']);
				$this->excel->getActiveSheet()->mergeCells("J{$ke}:K{$ke}");

				$this->excel->getActiveSheet()->setCellValue("L{$ke}", "国名 ". $rkn['Negara']);
				$this->excel->getActiveSheet()->mergeCells("L{$ke}:U{$ke}");

				$this->excel->getActiveSheet()->setCellValue("V{$ke}", "目的 ". $rkn['tujuan']);
				$this->excel->getActiveSheet()->mergeCells("V{$ke}:AE{$ke}");
				$ke++;
			}
		}
		else
		{
			$this->excel->getActiveSheet()->setCellValue("A{$ke}", '無');
			$this->excel->getActiveSheet()->mergeCells("A{$ke}:G{$ke}");
			$this->excel->getActiveSheet()->getStyle("A{$ke}:AE{$ke}")->applyFromArray($bawah);

			$this->excel->getActiveSheet()->setCellValue("H{$ke}", '年 ');
			$this->excel->getActiveSheet()->mergeCells("H{$ke}:I{$ke}");

			$this->excel->getActiveSheet()->setCellValue("J{$ke}", "月 ".'');
			$this->excel->getActiveSheet()->mergeCells("J{$ke}:K{$ke}");

			$this->excel->getActiveSheet()->setCellValue("L{$ke}", "発行日 ".''); //negara
			$this->excel->getActiveSheet()->mergeCells("L{$ke}:U{$ke}");

			$this->excel->getActiveSheet()->setCellValue("V{$ke}", "有効期限 ".''); //tujuan
			$this->excel->getActiveSheet()->mergeCells("V{$ke}:AE{$ke}");
			$ke++;
		}

		$marginpend=0;
		$this->excel->getActiveSheet()->setCellValue("A{$ke}",'外国語学習歴'); //riwayat belajar bahasa jepang

		while(false){

		  $this->excel->getActiveSheet()->setCellValue('G'.$ke,''); //selama
		  $this->excel->getActiveSheet()->mergeCells('G'.$ke.':I'.$ke);

		  $this->excel->getActiveSheet()->setCellValue('J'.$ke,jangka(@$tampil_pend['thn_masuk_pend'].'-'.@$tampil_pend['bulan_masuk'].'-00',@$tampil_pend['thn_keluar_pend'].'-'.@$tampil_pend['bulan_keluar'].'-00',1,0,0));

		  $this->excel->getActiveSheet()->setCellValue('K'.$ke,'Thn ');
		  $this->excel->getActiveSheet()->mergeCells('K'.$ke.':L'.$ke);

		  $this->excel->getActiveSheet()->setCellValue('M'.$ke,jangka(@$tampil_pend['thn_masuk_pend'].'-'.@$tampil_pend['bulan_masuk'].'-00',@$tampil_pend['thn_keluar_pend'].'-'.@$tampil_pend['bulan_keluar'].'-00',0,1,0));

		  $this->excel->getActiveSheet()->setCellValue('N'.$ke,'月');
		  $this->excel->getActiveSheet()->mergeCells('N'.$ke.':O'.$ke);

		  $this->excel->getActiveSheet()->setCellValue('Q'.$ke,'Di '.ucfirst(@$tampil_pend[1]));
		  $this->excel->getActiveSheet()->mergeCells('Q'.$ke.':AE'.$ke);

		  $this->excel->getActiveSheet()->mergeCells('A'.$ke.':F'.$ke);
		  $ke++;
		}

		$this->excel->getActiveSheet()->setCellValue('A'.$ke,'日本にいる親族（父・母・配偶者・子・兄弟姉妹） '. '');
		$this->excel->getActiveSheet()->mergeCells('A'.$ke.':AE'.$ke);
		$this->excel->getActiveSheet()->getStyle('A'.$ke.':AE'.$ke)->applyFromArray($atas);
		$ke++;

		///////////////////////////////////////////////////////////////////////
		$this->excel->getActiveSheet()->setCellValue('A'.$ke,'続柄'); //hubungan
		$this->excel->getActiveSheet()->mergeCells('A'.$ke.':E'.$ke);
		$this->excel->getActiveSheet()->getStyle('A'.$ke.':E'.$ke)->applyFromArray($border);

		$this->excel->getActiveSheet()->setCellValue('F'.$ke,'氏名'); //nama
		$this->excel->getActiveSheet()->mergeCells('F'.$ke.':L'.$ke);
		$this->excel->getActiveSheet()->getStyle('F'.$ke.':L'.$ke)->applyFromArray($border);

		$this->excel->getActiveSheet()->setCellValue('M'.$ke,'生年月日'); //tanggal lahir
		$this->excel->getActiveSheet()->mergeCells('M'.$ke.':P'.$ke);
		$this->excel->getActiveSheet()->getStyle('M'.$ke.':P'.$ke)->applyFromArray($border);

		$this->excel->getActiveSheet()->setCellValue('Q'.$ke,'国籍'); //kewarganegaraan
		$this->excel->getActiveSheet()->mergeCells('Q'.$ke.':T'.$ke);
		$this->excel->getActiveSheet()->getStyle('Q'.$ke.':T'.$ke)->applyFromArray($border);

		$this->excel->getActiveSheet()->setCellValue('U'.$ke,'通勤・通学先'); //perusaahaan / instansi
		$this->excel->getActiveSheet()->mergeCells('U'.$ke.':AB'.$ke);
		$this->excel->getActiveSheet()->getStyle('U'.$ke.':AB'.$ke)->applyFromArray($border);

		$this->excel->getActiveSheet()->setCellValue('AC'.$ke,'在留資格'); //status visa
		$this->excel->getActiveSheet()->mergeCells('AC'.$ke.':AE'.$ke);
		$this->excel->getActiveSheet()->getStyle('AC'.$ke.':AE'.$ke)->applyFromArray($border);
		$ke++;

		foreach($keluarga_dijepang as $kdjp){
		  $this->excel->getActiveSheet()->setCellValue('A'.$ke, $kdjp['hubungan_jp']);
		  $this->excel->getActiveSheet()->mergeCells('A'.$ke.':E'.$ke);
		  $this->excel->getActiveSheet()->getStyle('A'.$ke.':E'.$ke)->applyFromArray($kanan);

		  $this->excel->getActiveSheet()->setCellValue('F'.$ke, $kdjp['nama']);
		  $this->excel->getActiveSheet()->mergeCells('F'.$ke.':L'.$ke);
		  $this->excel->getActiveSheet()->getStyle('F'.$ke.':L'.$ke)->applyFromArray($kanan);

		  $this->excel->getActiveSheet()->setCellValue('M'.$ke, $kdjp['tangal_lahir']);
		  $this->excel->getActiveSheet()->mergeCells('M'.$ke.':P'.$ke);
		  $this->excel->getActiveSheet()->getStyle('M'.$ke.':P'.$ke)->applyFromArray($kanan);

		  $this->excel->getActiveSheet()->setCellValue('Q'.$ke, $kdjp['kewarganegaraan']);
		  $this->excel->getActiveSheet()->mergeCells('Q'.$ke.':T'.$ke);
		  $this->excel->getActiveSheet()->getStyle('Q'.$ke.':T'.$ke)->applyFromArray($kanan);

		  $this->excel->getActiveSheet()->setCellValue('U'.$ke, $kdjp['instansi']);
		  $this->excel->getActiveSheet()->mergeCells('U'.$ke.':AB'.$ke);
		  $this->excel->getActiveSheet()->getStyle('U'.$ke.':AB'.$ke)->applyFromArray($kanan);

		  $this->excel->getActiveSheet()->setCellValue('AC'.$ke, $kdjp['visa']);
		  $this->excel->getActiveSheet()->mergeCells('AC'.$ke.':AE'.$ke);
		  $this->excel->getActiveSheet()->getStyle('AC'.$ke.':AE'.$ke)->applyFromArray($kanan);
		  $ke++;
		}

		///////////////////////////////////////////////////////////////////////////////////////

		$this->excel->getActiveSheet()->setCellValue('A'.$ke,'学歴'); //Riwayat Pendidikan
		$this->excel->getActiveSheet()->mergeCells('A'.$ke.':Y'.$ke);
		$this->excel->getActiveSheet()->getStyle('A'.$ke.':Y'.$ke)->applyFromArray($border);

		$this->excel->getActiveSheet()->setCellValue('Z'.$ke,'専攻'); //jurusan
		$this->excel->getActiveSheet()->mergeCells('Z'.$ke.':AE'.$ke);
		$this->excel->getActiveSheet()->getStyle('Z'.$ke.':AE'.$ke)->applyFromArray($border);
		$ke++;

		///////////////////////////////////////////////////////////////////////////////
		//$mysql_pend_f=mysql_query("SELECT * FROM `tb_riwayat_pend` where nik ='$_REQUEST[id]' and status='F' order by thn_masuk_pend desc");
		foreach($riwayat_pendidikan as $pendidikan){

		    $this->excel->getActiveSheet()->setCellValue('A'.$ke,$pendidikan['mulai']);
		    $this->excel->getActiveSheet()->mergeCells('A'.$ke.':B'.$ke);

		    $this->excel->getActiveSheet()->setCellValue('C'.$ke,'年');

		    //$this->excel->getActiveSheet()->setCellValue('D'.$ke,$pendidikan['bulan_mulai']);

		    $this->excel->getActiveSheet()->setCellValue('E'.$ke,'月');

		    $this->excel->getActiveSheet()->setCellValue('F'.$ke,'~');

		    $this->excel->getActiveSheet()->setCellValue('G'.$ke,$pendidikan['selesai']);
		    $this->excel->getActiveSheet()->mergeCells('G'.$ke.':H'.$ke);

		    $this->excel->getActiveSheet()->setCellValue('I'.$ke,'年');

		    //$this->excel->getActiveSheet()->setCellValue('J'.$ke,$pendidikan['bulan_selsai']);

		    $this->excel->getActiveSheet()->setCellValue('K'.$ke,'月');

		    $this->excel->getActiveSheet()->setCellValue('L'.$ke,$pendidikan['instansi']);
		    $this->excel->getActiveSheet()->mergeCells('L'.$ke.':Y'.$ke);
		    $this->excel->getActiveSheet()->getStyle('L'.$ke.':Y'.$ke)->applyFromArray($kanan);

		    $this->excel->getActiveSheet()->setCellValue('Z'.$ke,$pendidikan['nama_jurusan_jp']);
		    $this->excel->getActiveSheet()->mergeCells('Z'.$ke.':AE'.$ke);
		    $this->excel->getActiveSheet()->getStyle('Z'.$ke.':AE'.$ke)->applyFromArray($kanan);
		    $ke++;
		}

		///////////////////////////////////////////////////////////////////////////////////////

		$this->excel->getActiveSheet()->setCellValue('A'.$ke,'職歴'); //riwayat pekerjaan
		$this->excel->getActiveSheet()->mergeCells('A'.$ke.':Y'.$ke);
		$this->excel->getActiveSheet()->getStyle('A'.$ke.':Y'.$ke)->applyFromArray($border);

		$this->excel->getActiveSheet()->setCellValue('Z'.$ke,'職種'); //jenis kepekerjaan
		$this->excel->getActiveSheet()->mergeCells('Z'.$ke.':AE'.$ke);
		$this->excel->getActiveSheet()->getStyle('Z'.$ke.':AE'.$ke)->applyFromArray($border);
		$ke++;

		//$mysql_pek=mysql_query("SELECT * FROM `tb_riwayat_pekerjaan` where nik ='$_REQUEST[id]' order by tahun_masuk desc");
		foreach($riwayat_pekerjaan as $rp){
		    $this->excel->getActiveSheet()->setCellValue('A'.$ke, $rp['tahun_masuk']);
		    $this->excel->getActiveSheet()->mergeCells('A'.$ke.':B'.$ke);

		    $this->excel->getActiveSheet()->setCellValue('C'.$ke,'年');

		    $this->excel->getActiveSheet()->setCellValue('D'.$ke, $rp['bulan_masuk']);

		    $this->excel->getActiveSheet()->setCellValue('E'.$ke,'月');

		    $this->excel->getActiveSheet()->setCellValue('F'.$ke,'~');
		if($rp['tahun_keluar']=='Sampai Sekarang'){
			$this->excel->getActiveSheet()->setCellValue('G'.$ke, $rp['tahun_keluar']);
		    $this->excel->getActiveSheet()->mergeCells('G'.$ke.':K'.$ke);
		}else{
		    $this->excel->getActiveSheet()->setCellValue('G'.$ke, $rp['tahun_keluar']);
		    $this->excel->getActiveSheet()->mergeCells('G'.$ke.':H'.$ke);

		    $this->excel->getActiveSheet()->setCellValue('I'.$ke,'年');

		    $this->excel->getActiveSheet()->setCellValue('J'.$ke, $rp['bulan_keluar']);

		    $this->excel->getActiveSheet()->setCellValue('K'.$ke,'月');
		}
		    $this->excel->getActiveSheet()->setCellValue('L'.$ke,'Di '. $rp['perusahaan']);
		    $this->excel->getActiveSheet()->mergeCells('L'.$ke.':Y'.$ke);
		    $this->excel->getActiveSheet()->getStyle('L'.$ke.':Y'.$ke)->applyFromArray($kanan);

		    $this->excel->getActiveSheet()->setCellValue('Z'.$ke, searchArray($rp['posisi'], $pekerjaan, 'kd_pekerjaan', 'pekerjaan_jp'));
		    $this->excel->getActiveSheet()->mergeCells('Z'.$ke.':AE'.$ke);
		    $this->excel->getActiveSheet()->getStyle('Z'.$ke.':AE'.$ke)->applyFromArray($kanan);
		    $ke++;

		}

		$awal=$ke;
		$this->excel->getActiveSheet()->setCellValue('C'.$ke,'続柄'); //hubungan
		$this->excel->getActiveSheet()->mergeCells('C'.$ke.':G'.$ke);
		$this->excel->getActiveSheet()->getStyle('C'.$ke.':G'.$ke)->applyFromArray($border);

		$this->excel->getActiveSheet()->setCellValue('H'.$ke,'名前'); //nama
		$this->excel->getActiveSheet()->mergeCells('H'.$ke.':O'.$ke);
		$this->excel->getActiveSheet()->getStyle('H'.$ke.':O'.$ke)->applyFromArray($border);

		$this->excel->getActiveSheet()->setCellValue('P'.$ke,'生年月日'); //tgl lahir
		$this->excel->getActiveSheet()->mergeCells('P'.$ke.':S'.$ke);
		$this->excel->getActiveSheet()->getStyle('P'.$ke.':S'.$ke)->applyFromArray($border);

		$this->excel->getActiveSheet()->setCellValue('T'.$ke,'年齢'); //umur
		$this->excel->getActiveSheet()->mergeCells('T'.$ke.':U'.$ke);
		$this->excel->getActiveSheet()->getStyle('T'.$ke.':U'.$ke)->applyFromArray($border);

		$this->excel->getActiveSheet()->setCellValue('V'.$ke,'職業'); //pekerjaan
		$this->excel->getActiveSheet()->mergeCells('V'.$ke.':AE'.$ke);
		$this->excel->getActiveSheet()->getStyle('V'.$ke.':AE'.$ke)->applyFromArray($border);
		$ke++;

		//$sql_kel=mysql_query("SELECT * FROM `tb_keluarga_pem` where kd_pemagang='$_REQUEST[id]' order by tanggal_lahir ASC ") or die(mysql_error());
		foreach($keluarga_pem as $kp){

		  $this->excel->getActiveSheet()->setCellValue('C'.$ke, $kp['hubungan_jp']);
		  $this->excel->getActiveSheet()->mergeCells('C'.$ke.':G'.$ke);
		  $this->excel->getActiveSheet()->getStyle('C'.$ke.':G'.$ke)->applyFromArray($kanan);

		  $this->excel->getActiveSheet()->setCellValue('H'.$ke, $kp['nama']);
		  $this->excel->getActiveSheet()->mergeCells('H'.$ke.':O'.$ke);
		  $this->excel->getActiveSheet()->getStyle('H'.$ke.':O'.$ke)->applyFromArray($kanan);

		  $this->excel->getActiveSheet()->setCellValue('P'.$ke, $kp['tanggal_lahir']);
		  $this->excel->getActiveSheet()->mergeCells('P'.$ke.':S'.$ke);
		  $this->excel->getActiveSheet()->getStyle('P'.$ke.':S'.$ke)->applyFromArray($kanan);

		  $this->excel->getActiveSheet()->setCellValue('T'.$ke, $kp['tanggal_lahir']);
		  $this->excel->getActiveSheet()->mergeCells('T'.$ke.':U'.$ke);
		  $this->excel->getActiveSheet()->getStyle('T'.$ke.':U'.$ke)->applyFromArray($kanan);

		  $this->excel->getActiveSheet()->setCellValue('V'.$ke, $kp['pekerjaan_jp']);
		  $this->excel->getActiveSheet()->mergeCells('V'.$ke.':AE'.$ke);
		  $this->excel->getActiveSheet()->getStyle('V'.$ke.':AE'.$ke)->applyFromArray($kanan);
		  $ke++;
		}

		//$sql_kel=mysql_query("SELECT * FROM tb_keluarga where nik='$_REQUEST[id]' ORDER BY tgl ASC   ") or die(mysql_error());
		foreach($keluarga as $kel){
		  $this->excel->getActiveSheet()->setCellValue('C'.$ke, $kel['hubungan_jp']);
		  $this->excel->getActiveSheet()->mergeCells('C'.$ke.':G'.$ke);
		  $this->excel->getActiveSheet()->getStyle('C'.$ke.':G'.$ke)->applyFromArray($kanan);

		  $this->excel->getActiveSheet()->setCellValue('H'.$ke, $kel['nama_kel']);
		  $this->excel->getActiveSheet()->mergeCells('H'.$ke.':O'.$ke);
		  $this->excel->getActiveSheet()->getStyle('H'.$ke.':O'.$ke)->applyFromArray($kanan);

		  $this->excel->getActiveSheet()->setCellValue('P'.$ke, $kel['tgl']);
		  $this->excel->getActiveSheet()->mergeCells('P'.$ke.':S'.$ke);
		  $this->excel->getActiveSheet()->getStyle('P'.$ke.':S'.$ke)->applyFromArray($kanan);

		  $this->excel->getActiveSheet()->setCellValue('T'.$ke, $kel['tgl']);
		  $this->excel->getActiveSheet()->mergeCells('T'.$ke.':U'.$ke);
		  $this->excel->getActiveSheet()->getStyle('T'.$ke.':U'.$ke)->applyFromArray($kanan);

		  $this->excel->getActiveSheet()->setCellValue('V'.$ke, $kel['pekerjaan_jp']);
		  $this->excel->getActiveSheet()->mergeCells('V'.$ke.':AE'.$ke);
		  $this->excel->getActiveSheet()->getStyle('V'.$ke.':AE'.$ke)->applyFromArray($kanan);
		  $ke++;
		}


		$akhir=$ke-1;

		$this->excel->getActiveSheet()->getStyle('A'.$awal.':AE'.$awal)->applyFromArray($atas);
		$this->excel->getActiveSheet()->getStyle('B'.$awal.':B'.$akhir)->applyFromArray($kanan);

		$this->excel->getActiveSheet()->setCellValue('A'.$awal,'家族構成'); //susunan
		$this->excel->getActiveSheet()->mergeCells('A'.$awal.':A'.$akhir);
		$this->excel->getActiveSheet()->getStyle('A'.$awal)->getAlignment()->setTextRotation(90);

		$this->excel->getActiveSheet()->setCellValue('B'.$awal,''); //keluarga
		$this->excel->getActiveSheet()->mergeCells('B'.$awal.':B'.$akhir);
		$this->excel->getActiveSheet()->getStyle('B'.$awal)->getAlignment()->setTextRotation(90);


		///////////////////////////////////////////////////////////////////////////////////////
		$this->excel->getActiveSheet()->setCellValue('A'.$ke,'身長 '); //tinggi badan
		$this->excel->getActiveSheet()->mergeCells('A'.$ke.':D'.$ke);

		$this->excel->getActiveSheet()->setCellValue('E'.$ke, $pemagang->tb .' cm');
		$this->excel->getActiveSheet()->mergeCells('E'.$ke.':G'.$ke);

		$this->excel->getActiveSheet()->getStyle('A'.$ke.':G'.$ke)->applyFromArray($border);

		$this->excel->getActiveSheet()->setCellValue('H'.$ke,'体重 '); //berat badan
		$this->excel->getActiveSheet()->mergeCells('H'.$ke.':K'.$ke);

		$this->excel->getActiveSheet()->setCellValue('L'.$ke, $pemagang->bb.' kg');
		$this->excel->getActiveSheet()->mergeCells('L'.$ke.':N'.$ke);
		$this->excel->getActiveSheet()->getStyle('H'.$ke.':N'.$ke)->applyFromArray($border);

		$this->excel->getActiveSheet()->setCellValue('O'.$ke,'ウエスト '); //lingkar pinggang
		$this->excel->getActiveSheet()->mergeCells('O'.$ke.':T'.$ke);

		$this->excel->getActiveSheet()->setCellValue('U'.$ke, $pemagang->lingkar_pinggang.' cm');
		$this->excel->getActiveSheet()->mergeCells('U'.$ke.':V'.$ke);

		$this->excel->getActiveSheet()->getStyle('O'.$ke.':V'.$ke)->applyFromArray($border);

		$this->excel->getActiveSheet()->setCellValue('W'.$ke,'靴のサイズ '); //ukuran sepatu
		$this->excel->getActiveSheet()->mergeCells('W'.$ke.':AA'.$ke);

		$this->excel->getActiveSheet()->setCellValue('AB'.$ke, $pemagang->no_sepatu.' C');
		$this->excel->getActiveSheet()->mergeCells('AB'.$ke.':AE'.$ke);
		$this->excel->getActiveSheet()->getStyle('W'.$ke.':AE'.$ke)->applyFromArray($border);
		$ke++;

		//////////////////////////////////////////////////////////////////////////////////////////
		$this->excel->getActiveSheet()->setCellValue('A'.$ke,'視力 '); //penglihatan
		$this->excel->getActiveSheet()->mergeCells('A'.$ke.':G'.$ke);

		$this->excel->getActiveSheet()->setCellValue('H'.$ke,@$cek_mata['nilai']); //TODO
		$this->excel->getActiveSheet()->mergeCells('H'.$ke.':K'.$ke);
		$this->excel->getActiveSheet()->getStyle('A'.$ke.':K'.$ke)->applyFromArray($border);

		$this->excel->getActiveSheet()->setCellValue('L'.$ke,'矯正視力 '); //penglihatan kacamata
		$this->excel->getActiveSheet()->mergeCells('L'.$ke.':V'.$ke);

		$this->excel->getActiveSheet()->setCellValue('W'.$ke,@$cek_mata2['nilai']); //TODO
		$this->excel->getActiveSheet()->mergeCells('W'.$ke.':Z'.$ke);

		$this->excel->getActiveSheet()->getStyle('L'.$ke.':Z'.$ke)->applyFromArray($border);

		$this->excel->getActiveSheet()->setCellValue('AA'.$ke,'血液型 '); //golongan darah
		$this->excel->getActiveSheet()->mergeCells('AA'.$ke.':AC'.$ke);

		$this->excel->getActiveSheet()->setCellValue('AD'.$ke, $pemagang->gol_darah);
		$this->excel->getActiveSheet()->mergeCells('AD'.$ke.':AE'.$ke);
		$this->excel->getActiveSheet()->getStyle('X'.$ke.':AE'.$ke)->applyFromArray($border);
		$ke++;

		/////////////////////////////////////////////////////////////////////////////////////////
		$this->excel->getActiveSheet()->setCellValue('A'.$ke,'得意学科 '); //pelajara disukai
		$this->excel->getActiveSheet()->mergeCells('A'.$ke.':G'.$ke);

		$this->excel->getActiveSheet()->setCellValue('H'.$ke, $pemagang->suka_pelajaran);
		$this->excel->getActiveSheet()->mergeCells('H'.$ke.':K'.$ke);
		$this->excel->getActiveSheet()->getStyle('A'.$ke.':K'.$ke)->applyFromArray($border);

		$this->excel->getActiveSheet()->setCellValue('L'.$ke,'趣味 '); //hobi
		$this->excel->getActiveSheet()->mergeCells('L'.$ke.':M'.$ke);

		$this->excel->getActiveSheet()->setCellValue('N'.$ke, $pemagang->hobi);
		$this->excel->getActiveSheet()->mergeCells('N'.$ke.':U'.$ke);
		$this->excel->getActiveSheet()->getStyle('L'.$ke.':U'.$ke)->applyFromArray($border);

		$this->excel->getActiveSheet()->setCellValue('V'.$ke,'特技 '); //skill
		$this->excel->getActiveSheet()->mergeCells('V'.$ke.':Z'.$ke);

		$this->excel->getActiveSheet()->setCellValue('AA'.$ke, $pemagang->skil);
		$this->excel->getActiveSheet()->mergeCells('AA'.$ke.':AE'.$ke);
		$this->excel->getActiveSheet()->getStyle('V'.$ke.':AE'.$ke)->applyFromArray($border);
		$ke++;

		/////////////////////////////////////////////////////////////////////////////////////////
		$this->excel->getActiveSheet()->setCellValue('A'.$ke,'病歴 '); //riwayat penyakit
		$this->excel->getActiveSheet()->mergeCells('A'.$ke.':K'.$ke);

		$this->excel->getActiveSheet()->setCellValue('L'.$ke,'病名 '.empty($riwayat_penyakit->nama_penyakit) ? "" : $riwayat_penyakit->nama_penyakit); //nama penyakit
		$this->excel->getActiveSheet()->mergeCells('L'.$ke.':T'.$ke);

		$this->excel->getActiveSheet()->setCellValue('U'.$ke,'時期 ');
		$this->excel->getActiveSheet()->mergeCells('U'.$ke.':W'.$ke);

		$this->excel->getActiveSheet()->setCellValue('X'.$ke,'年 '.empty($riwayat_penyakit->tahun) ? "" : $riwayat_penyakit->tahun);
		$this->excel->getActiveSheet()->mergeCells('X'.$ke.':AA'.$ke);

		$this->excel->getActiveSheet()->setCellValue('AB'.$ke,'月 '.empty($riwayat_penyakit->bulan) ? "" : $riwayat_penyakit->bulan);
		$this->excel->getActiveSheet()->mergeCells('AB'.$ke.':AE'.$ke);
		$this->excel->getActiveSheet()->getStyle('AB'.$ke.':AE'.$ke)->applyFromArray($kanan);
		$ke++;

		$status_merokok = '';
		if(!empty($merokok->status))
		{
			if($merokok->status == 2)
			{
				$status_merokok = '吸う, 1日 '.$merokok->hari.' 本';
			}
			else if($merokok->status == 1)
			{
				$status_merokok = '吸うのをやめた';
			}
			else
			{
				$status_merokok = '以前から吸わない';
			}
		}

		$status_minum_alkohol = '';
		if(!empty($minum_alkohol->status))
		{
			if($minum_alkohol->status == 2)
			{
				$status_minum_alkohol = 'たまに飲む, 週に'.$minum_alkohol->bulan.' 回';
			}
			else if($minum_alkohol->status == 1)
			{
				$status_minum_alkohol = '吸うのをやめた';
			}
			else
			{
				$status_minum_alkohol = '以前から吸わない';
			}
		}

		/////////////////////////////////////////////////////////////////////////////////////////
		$this->excel->getActiveSheet()->setCellValue('A'.$ke,'喫煙 : '.$status_merokok); //merokok
		$this->excel->getActiveSheet()->mergeCells('A'.$ke.':AE'.$ke);
		$this->excel->getActiveSheet()->getStyle('A'.$ke.':AE'.$ke)->applyFromArray($border);
		$ke++;

		////////////////////////////////////////////////////////////////////////////////////////
		$this->excel->getActiveSheet()->setCellValue('A'.$ke,'飲酒 : '.$status_minum_alkohol);
		$this->excel->getActiveSheet()->mergeCells('A'.$ke.':AE'.$ke);
		$this->excel->getActiveSheet()->getStyle('A'.$ke.':AE'.$ke)->applyFromArray($border);

		$this->excel->getActiveSheet()->getStyle('AE3:AE'.$ke)->applyFromArray($kanan);
		$this->excel->getActiveSheet()->getStyle('A3:A'.$ke)->applyFromArray($kiri);
		///////////////////////////////////////judul////////////////////////////

		$this->excel->getActiveSheet()->mergeCells('A1:AE1');
		$this->excel->getActiveSheet()->setCellValue('A1', "履歴書");
		$this->excel->getActiveSheet()->getStyle('A1:AE1')->getFont()->setName('Century');
		$this->excel->getActiveSheet()->getStyle('A1:AE1')->getFont()->setSize(24);
		$this->excel->getActiveSheet()->getStyle('A1:AE1')->getFont()->setBold(true);
		$this->excel->getActiveSheet()->getStyle('A1:AE1')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
		////////////////////////////////////judul///////////////////////////

		header('Content-Type: application/vnd.ms-excel');
		header('Content-Disposition: attachment; filename="' . 'Biodata_jepang_Pemagang_'.$kd_pemagang.'.xls"');
		header('Cache-Control: max-age= 0');
		$objWriter = PHPExcel_IOFactory::createWriter($this->excel, 'Excel2007');
		ob_end_clean();
		$objWriter->save('php://output');
	}

	public function save_import_test_internal()
	{
		$q = $this->Rcr_model->save_import_test_internal($_POST['import']);

		if($q)
		{
			echo
			'<h1>Success Import Test</h1>'.
			'<p>Redirect kehalaman lain dalam 3 detik.</p>'.
			'<script>'.
			'setTimeout(function(){
				window.location=\''.site_url('rcr/pemagang').'\'
			}, 3000)'.
			'</script>';
		}
		else
		{
				echo 'Kesalahan saat import data';
		}
	}

	public function blacklist()
	{
		if(empty($_SESSION['authorization']['PMGBL'])){
			http_response_code(403);
			echo "Forbidden";
			die();
		}

		$this->db->select(array('P.kd_pemagang', 'P.nama_pem', 'B.alasan', 'B.tgl'));
		$this->db->from('blacklist B');
		$this->db->join('tb_pemagangan P', 'P.kd_pemagang = B.kd_pemagang');
		$q = $this->db->get();

		$data['blacklist'] = $q->result_array();
		$this->load->view('rcr/layout/header');
		$this->load->view('rcr/pemagang/blacklist', $data);
		$this->load->view('rcr/layout/footer');
	}

	public function blacklist_pemagang($kd_pemagang = NULL)
	{
		if(empty($_SESSION['authorization']['PMGBL'])){
			http_response_code(403);
			echo "Forbidden";
			die();
		}

		if($kd_pemagang == NULL)
		{
			redirect('rcr/pemagang');
		}
		$this->pemagang->add_blacklist($kd_pemagang);

		redirect('rcr/pemagang/show/'.$kd_pemagang);
	}

	public function hapus_dari_blacklist($kd_pemagang = NULL)
	{
		if($kd_pemagang == NULL)
		{
			redirect('rcr/pemagang');
		}

		$this->db->delete('blacklist', array(
			'kd_pemagang' => $kd_pemagang
		));
		redirect('rcr/pemagang/blacklist');
	}

  public function server_side_ajax($mode = NULL)
  {
  	$list = $this->pemagang->get_datatables();
    $data = array();
		$cols = array();
    $no = $_POST['start'];
		$pernah_ke_jepang = array();

    foreach ($list as $key => $val) {
      $no++;
      $row = array();
      $row[] = $no;
      $row[] = $val['kd_pemagang'];
      $row[] = $val['nama_pem'];
			$row[] = $val['provinsi'];
      $row[] = intval(date('Y')) - intval(date('Y', strtotime($val['tanggal_lahir'])));
			$row[] = $val['tinggi_badan'];
			$row[] = $val['berat_badan'];
			$row[] = $val['email'];
			$row[] = $val['id_pendidikan'] . ' ' . $val['nama_jurusan'];
      $row[] = $val['jk'] == 'L' ? "<i class='ui icon blue male'></i>" : "<i class='ui icon pink female'></i>";
			$row[] = $val['nomor_handphone'];
			$row[] = $val['pengalaman_kerja'];
			$row[] = "<a target='blank' href='" . site_url('rcr/pemagang/show/') . $val['kd_pemagang'] . "'><i class='ui icon search'></i></a>";
      $data[] = $row;

			if($val['pernah_ke_jepang'] != "0")
			{
				array_push($pernah_ke_jepang, $val['kd_pemagang']);
			}
    }

    $output = array (
      'draw' => $_POST['draw'],
      "recordsTotal" => $this->pemagang->count_all(),
      'recordsFiltered' => $this->pemagang->count_filtered(),
      'data' => $data,
			'pernah_ke_jepang' => $pernah_ke_jepang
    );

    echo json_encode($output);
  }

	public function ajax_form_riwayat_pekerjaan($kd_pemagang = NULL)
	{
		if(isset($_POST['riwayat']))
		{
			var_dump($_POST);
			$this->db->trans_start();
			$this->db->delete('tb_riwayat_pekerjaan', array(
				'nik' => $kd_pemagang
			));

			foreach ($_POST['riwayat'] as $v) {
				$this->db->insert('tb_riwayat_pekerjaan', array(
					'nik' => $kd_pemagang,
					'perusahaan' => $v['perusahaan'],
					'posisi'		=> $v['kd_pekerjaan'],
					'bulan_masuk' => $v['bulan_mulai'],
					'bulan_keluar' => $v['bulan_selesai'],
					'tahun_masuk' => $v['tahun_mulai'],
					'tahun_keluar' => $v['tahun_selesai'],
				));
			}
			$this->db->trans_complete();
			redirect('rcr/pemagang/'.$kd_pemagang);
		}

		if($kd_pemagang == NULL)
		{
			echo 'Invalid';
		}
		else
		{
			$data['riwayat_pekerjaan'] = $this->db->get_where('tb_riwayat_pekerjaan', array(
				'nik' => $kd_pemagang
			))->result_array();
			$data['pekerjaan'] = $this->db->get('tb_pekerjaan')->result_array();
			$data['kd_pemagang'] = $kd_pemagang;
			$this->load->view('rcr/pemagang/ajax_form/riwayat_pekerjaan', $data);
		}
	}

	public function ajax_form_riwayat_pendidikan($kd_pemagang = NULL)
	{
		if(isset($_POST['pendidikan']))
		{
			var_dump($_POST);
			// $this->db->trans_start();
			// $this->db->delete('tb_riwayat_pekerjaan', array(
			// 	'nik' => $kd_pemagang
			// ));
			//
			// foreach ($_POST['riwayat'] as $v) {
			// 	$this->db->insert('tb_riwayat_pekerjaan', array(
			// 		'nik' => $kd_pemagang,
			// 		'perusahaan' => $v['perusahaan'],
			// 		'posisi'		=> $v['kd_pekerjaan'],
			// 		'bulan_masuk' => $v['bulan_mulai'],
			// 		'bulan_keluar' => $v['bulan_selesai'],
			// 		'tahun_masuk' => $v['tahun_mulai'],
			// 		'tahun_keluar' => $v['tahun_selesai'],
			// 	));
			// }
			// $this->db->trans_complete();
			// redirect('rcr/pemagang/'.$kd_pemagang);
		}

		if($kd_pemagang == NULL)
		{
			echo 'Invalid';
		}
		else
		{
			$data['riwayat_pendidikan'] = $this->db->get_where('dt_riwayat_pendidikan', array(
				'kd_pemagang' => $kd_pemagang
			))->result_array();
			//$data['pendidikan'] = $this->db->get('tb_pendidikan')->result_array();
			$data['kd_pemagang'] = $kd_pemagang;
			$this->load->view('rcr/pemagang/ajax_form/riwayat_pendidikan', $data);
		}
	}
}

?>
