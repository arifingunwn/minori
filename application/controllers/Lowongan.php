<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Lowongan extends MY_Controller {
	protected $access = 'admin';

	function __construct()
	{
			parent::__construct();
			$this->load->library('form_validation');
			$this->load->library('session');
			$this->load->helper('my_helper');
      $this->load->model('Rcr_model');
			$this->load->model('Lowongan_model', 'lowongan');
      $this->load->database();

			if(empty($_SESSION['authorization']['LOWONGAN'])){
				http_response_code(403);
				echo "Forbidden";
				die();
			}
	}

  public function index()
  {
		$this->db->select('*');
		$this->db->from('tb_lowongan');
		//$this->db->where('status !=', 'nonaktif');
    $lowongan = $this->db->get();
    $data['lowongan'] = $lowongan->result_array();
    $this->load->view('rcr/layout/header', $this->user_auth);
    $this->load->view('rcr/lowongan/index', $data);
    $this->load->view('rcr/layout/footer');
  }

	public function show($kd_lowongan = NULL)
	{
		if($kd_lowongan == NULL)
		{
			redirect('rcr/lowongan');
		}

		$this->db->select(array('L.kd', 'L.judul', 'L.text', 'PM.nama_pem', 'PM.kd_pemagang', 'PK.pekerjaan', 'TP.nilai', 'TP.keterangan'));
		$this->db->from('tb_lowongan L');
		$this->db->join('tb_test_pendaftar TP', 'L.kd = TP.kd_lowongan', 'left');
		$this->db->join('tb_pemagangan PM', 'TP.kd_pemagang = PM.kd_pemagang', 'left');
		$this->db->join('tb_pekerjaan PK', 'L.kd_kategori = PK.kd_pekerjaan', 'left');
		$this->db->where('L.kd', $kd_lowongan);
		$q = $this->db->get();
		$data['data'] = $q->result_array();
		$this->load->view('rcr/layout/header', $this->user_auth);
    $this->load->view('rcr/lowongan/show', $data);
    $this->load->view('rcr/layout/footer');
	}

	public function nilai($kd_lowongan = NULL)
	{
		if($kd_lowongan == NULL)
		{
			redirect('rcr/lowongan');
		}

		$this->form_validation->set_rules('kd_pemagang', 'KODE PEMAGANG', 'required');
		$this->form_validation->set_rules('nilai', 'Nilai', 'required');
		$this->form_validation->set_rules('keterangan', 'Keterangan', 'required');

		if($this->form_validation->run() === TRUE)
		{
			$this->Rcr_model->set_nilai_test_pendaftar(array(
				'kd_lowongan'	=> $kd_lowongan,
				'kd_pemagang' => $this->input->post('kd_pemagang'),
				'nilai' 			=> $this->input->post('nilai'),
				'keterangan' 	=> $this->input->post('keterangan')
			));
			redirect('rcr/lowongan/nilai/'.$kd_lowongan);
		}

		$this->db->select(array('L.kd', 'L.judul', 'L.text', 'PM.nama_pem', 'PM.kd_pemagang', 'PK.pekerjaan', 'TP.nilai', 'TP.keterangan'));
		$this->db->from('tb_lowongan L');
		$this->db->join('tb_test_pendaftar TP', 'L.kd = TP.kd_lowongan', 'left');
		$this->db->join('tb_pemagangan PM', 'TP.kd_pemagang = PM.kd_pemagang', 'left');
		$this->db->join('tb_pekerjaan PK', 'L.kd_kategori = PK.kd_pekerjaan', 'left');
		$this->db->where('L.kd', $kd_lowongan);
		$q = $this->db->get();
		$data['nilai'] = $q->result_array();
		$this->load->view('rcr/layout/header', $this->user_auth);
    $this->load->view('rcr/lowongan/nilai', $data);
    $this->load->view('rcr/layout/footer');
	}

  public function add()
  {
    $this->form_validation->set_rules('judul_lowongan', 'Judul Lowongan', 'required|min_length[5]');
    $this->form_validation->set_rules('descripsi_lowongan', 'Descripsi Lowongan', 'required');
    $this->form_validation->set_rules('kategori_lowongan', 'Kategori Lowongan', 'required');

    if($this->form_validation->run() == TRUE)
    {
      $q = $this->lowongan->add_lowongan(array(
        'judul_lowongan' => $this->input->post('judul_lowongan'),
        'descripsi_lowongan' => $this->input->post('descripsi_lowongan'),
        'kategori_lowongan' => $this->input->post('kategory_lowongan'),
        'awal_lowongan' => $this->input->post('awal_lowongan'),
        'akhir_lowongan' => $this->input->post('akhir_lowongan')
      ));

			if($q)
			{
				$this->session->set_flashdata('flag', 'Sukses Tambah Lowongan');
			}
			else
			{
				$this->session->set_flashdata('flag', 'Gagal Tambah Lowongan');
			}

      redirect('rcr/lowongan');
    }

    $this->db->select('tb_pekerjaan.kd_pekerjaan, tb_jenis_pekerjaan.jenis_pekerjaan, tb_pekerjaan.pekerjaan');
		$this->db->from('tb_pekerjaan');
		$this->db->join('tb_jenis_pekerjaan', 'tb_pekerjaan.kd_jenis_pekerjaan = tb_jenis_pekerjaan.kd_jenis_pekerjaan', 'left');
    $category = $this->db->get();
    $data['category'] = $category->result_array();
    $this->load->view('rcr/layout/header', $this->user_auth);
    $this->load->view('rcr/lowongan/add', $data);
    $this->load->view('rcr/layout/footer');
  }

  public function delete($id)
  {
    if(isset($id))
    {
			$this->db->where('kd', $id);
      $this->db->update('tb_lowongan', array('status' => 'nonaktif'));
    }
    redirect('rcr/lowongan');
  }

  public function edit($id)
  {
    $this->form_validation->set_rules('judul_lowongan', 'Judul Lowongan', 'required|min_length[5]');
    $this->form_validation->set_rules('descripsi_lowongan', 'Descripsi Lowongan', 'required');
    $this->form_validation->set_rules('kd_kategori', 'Kategori Lowongan', 'required');

    if($this->form_validation->run() == TRUE)
    {
      $q = $this->lowongan->update_lowongan(array(
        'kd_lowongan' => $this->input->post('kd_lowongan'),
        'judul_lowongan' => $this->input->post('judul_lowongan'),
        'descripsi_lowongan' => $this->input->post('descripsi_lowongan'),
        'kategori_lowongan' => $this->input->post('kd_kategori'),
        'awal_lowongan' => $this->input->post('awal_lowongan'),
        'akhir_lowongan' => $this->input->post('akhir_lowongan')
      ));

			if($q)
			{
				$this->session->set_flashdata('flag', 'Sukses Update Lowongan');
			}
			else
			{
				$this->session->set_flashdata('flag', 'Gagal Update Lowongan');
			}

      redirect('rcr/lowongan');
    }

    $lowongan = $this->db->get_where('tb_lowongan', array('kd' => $id));
		$this->db->select('tb_pekerjaan.kd_pekerjaan, tb_jenis_pekerjaan.jenis_pekerjaan, tb_pekerjaan.pekerjaan');
		$this->db->from('tb_pekerjaan');
		$this->db->join('tb_jenis_pekerjaan', 'tb_pekerjaan.kd_jenis_pekerjaan = tb_jenis_pekerjaan.kd_jenis_pekerjaan', 'left');
    $category = $this->db->get();
    $data['category'] = $category->result_array();
    $data['lowongan'] = $lowongan->row();
    $this->load->view('rcr/layout/header', $this->user_auth);
    $this->load->view('rcr/lowongan/edit', $data);
    $this->load->view('rcr/layout/footer');
  }

	public function aktif($id)
  {
    if(isset($id))
    {
			$this->db->where('kd', $id);
      $this->db->update('tb_lowongan', array('status' => 'T'));
    }
    redirect('rcr/lowongan');
  }

	public function nonaktif($id)
  {
    if(isset($id))
    {
			$this->db->where('kd', $id);
      $this->db->update('tb_lowongan', array('status' => 'nonaktif'));
    }
    redirect('rcr/lowongan');
  }

  public function recruit($kd_lowongan)
  {
    $lowongan = array();
    if(isset($kd_lowongan))
    {
      $query = $this->db->get_where('tb_lowongan', array('kd' => $kd_lowongan));
      $lowongan = $query->row();
    }

    $this->db->select('*');
    $this->db->from('ms_tingkatan_pendidikan');
		$this->db->where('tingkat >= ', 3);
    $this->db->order_by('tingkat', 'desc');
    $q_pendidikan = $this->db->get();
    $tingkat_pendidikan = $q_pendidikan->result_array();

		$this->db->select(array('PM.nama_pem', 'PM.kd_pemagang'));
		$this->db->from('tb_pendaftar PD');
		$this->db->join('tb_pemagangan PM', 'PD.id_pemagang = PM.kd_pemagang');
		$this->db->where('PD.id_lowongan', $kd_lowongan);
		$list_pemagang_terpilih = $this->db->get();

		$this->db->select('tb_pekerjaan.kd_pekerjaan, tb_jenis_pekerjaan.jenis_pekerjaan, tb_pekerjaan.pekerjaan');
		$this->db->from('tb_pekerjaan');
		$this->db->join('tb_jenis_pekerjaan', 'tb_pekerjaan.kd_jenis_pekerjaan = tb_jenis_pekerjaan.kd_jenis_pekerjaan', 'left');
		$this->db->order_by('tb_jenis_pekerjaan.kd_jenis_pekerjaan', 'asc');
    $pekerjaan = $this->db->get();

    $data['tingkat_pendidikan'] = $tingkat_pendidikan;
    $data['lowongan'] = $lowongan;
		$data['pekerjaan'] = $pekerjaan->result_array();
		$data['list_pemagang_terpilih'] = $list_pemagang_terpilih->result_array();
		$data['provinsi'] = $this->db->get('tb_provinsi')->result_array();
		$data['jurusan'] = $this->db->get('ms_jurusan_pendidikan')->result_array();
    $this->load->view('rcr/layout/header', $this->user_auth);
    $this->load->view('rcr/lowongan/recruit', $data);
    $this->load->view('rcr/layout/footer');
  }

	public function add_recruit()
	{
		$this->form_validation->set_rules('kd_lowongan', 'Lowongan', 'required');
		if($this->form_validation->run() == TRUE)
		{
			$kd_lowongan = $this->input->post('kd_lowongan');
			$q = $this->Rcr_model->add_recruit(array(
				'kd_lowongan' => $kd_lowongan,
				'kd_pemagang' => $this->input->post('kd_pemagang')
			));
		}
		redirect('rcr/lowongan/test/'.$kd_lowongan);
	}

	public function remove_recruit()
	{
		$kd_pemagang = $this->input->post('kd_pemagang');
		$kd_lowongan = $this->input->post('kd_lowongan');

		if($kd_lowongan == NULL || $kd_lowongan == NULL)
		{
			echo 'error';
		}
		$q = $this->db->delete('tb_test_pendaftar', array(
			'kd_pemagang' => $kd_pemagang,
			'kd_lowongan' => $kd_lowongan
		));
		echo json_encode($q);
	}

	public function test($kd_lowongan = NULL)
  {
    $lowongan = array();
    if(isset($kd_lowongan))
    {
      $query = $this->db->get_where('tb_lowongan', array('kd' => $kd_lowongan));
      $lowongan = $query->row();
    }

    $this->db->select('*');
    $this->db->from('ms_tingkatan_pendidikan');
		$this->db->where('tingkat >= ', 3);
    $this->db->order_by('tingkat', 'desc');
    $q_pendidikan = $this->db->get();
    $tingkat_pendidikan = $q_pendidikan->result_array();

		$this->db->select(array('PM.nama_pem', 'PM.kd_pemagang'));
		$this->db->from('tb_test_pendaftar TP');
		$this->db->join('tb_pemagangan PM', 'TP.kd_pemagang = PM.kd_pemagang');
		$this->db->where('TP.kd_lowongan', $kd_lowongan);
		$list_pemagang_terpilih = $this->db->get();

		$this->db->select('tb_pekerjaan.kd_pekerjaan, tb_jenis_pekerjaan.jenis_pekerjaan, tb_pekerjaan.pekerjaan');
		$this->db->from('tb_pekerjaan');
		$this->db->join('tb_jenis_pekerjaan', 'tb_pekerjaan.kd_jenis_pekerjaan = tb_jenis_pekerjaan.kd_jenis_pekerjaan', 'left');
		$this->db->order_by('tb_jenis_pekerjaan.kd_jenis_pekerjaan', 'asc');
    $pekerjaan = $this->db->get();

    $data['tingkat_pendidikan'] = $tingkat_pendidikan;
    $data['lowongan'] = $lowongan;
		$data['pekerjaan'] = $pekerjaan->result_array();
		$data['provinsi'] = $this->db->get('tb_provinsi')->result_array();
		$data['jurusan'] = $this->db->get('ms_jurusan_pendidikan')->result_array();
		$data['list_pemagang_terpilih'] = $list_pemagang_terpilih->result_array();
    $this->load->view('rcr/layout/header', $this->user_auth);
    $this->load->view('rcr/lowongan/test', $data);
    $this->load->view('rcr/layout/footer');
  }

	public function add_test()
	{
		$this->form_validation->set_rules('kd_lowongan', 'Lowongan', 'required');
		if($this->form_validation->run() == TRUE)
		{
			$kd_lowongan = $this->input->post('kd_lowongan');
			$this->Rcr_model->add_test(array(
				'kd_lowongan' => $kd_lowongan,
				'kd_pemagang' => $this->input->post('kd_pemagang')
			));
		}
		redirect('rcr/lowongan/show/'.$kd_lowongan);
	}

	public function remove_test()
	{
		$kd_pemagang = $this->input->post('kd_pemagang');
		$kd_lowongan = $this->input->post('kd_lowongan');

		if($kd_lowongan == NULL || $kd_lowongan == NULL)
		{
			echo 'error';
		}
		$q = $this->db->delete('tb_pendaftar', array(
			'id_pemagang' => $kd_pemagang,
			'id_lowongan' => $kd_lowongan
		));
		echo json_encode($q);
	}

	public function setNilai()
	{
		$kd_pemagang = $this->input->post('kd_pemagang');
		$kd_lowongan = $this->input->post('kd_lowongan');
		$nilai = $this->input->post('nilai');

		$this->db->where(array(
			'id_pemagang' => $kd_pemagang,
			'id_lowongan' => $kd_lowongan
		));
		$q = $this->db->update('tb_pendaftar', array(
			'nilai' => $nilai
		));

		if($q)
		{
			echo json_encode(array(
				'nilai' => $nilai
			));
		}
		else
		{
			echo json_encode('failed');
		}
	}
}

?>
