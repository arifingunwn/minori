<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class User extends MY_Controller {
	protected $access = 'admin';
	function __construct()
	{
			parent::__construct();
			$this->load->library('form_validation');
			$this->load->library('session');
			$this->load->helper('my_helper');
      $this->load->database();
			$this->load->model('User_model', 'user');

			if(empty($_SESSION['authorization']['USER'])){
				http_response_code(403);
				echo "Forbidden";
				die();
			}
	}

  public function index()
  {
		$q = $this->db->get_where('ms_user');
		$data['user'] = $q->result_array();
    $this->load->view('rcr/layout/header', $this->user_auth);
    $this->load->view('rcr/user/index', $data);
    $this->load->view('rcr/layout/footer');
  }

  public function edit($username = NULL)
  {
    if($username == NULL)
    {
      redirect('user/');
    }

    $data['user'] = $this->user->find($username);
    $this->db->select('M.*, A.can_create, A.can_read, A.can_update, A.can_delete, A.special, A.username');
    $this->db->from('ms_module M');
    $this->db->join('ms_authorization A', 'M.module_id = A.module_id AND A.username = \'' . $username . '\'', 'left');
    $q = $this->db->get();

    $data['username'] = $this->security->xss_clean($username);
    $data['roles'] = $q->result_array();
    $this->load->view('rcr/layout/header', $this->user_auth);
    $this->load->view('rcr/user/edit', $data);
    $this->load->view('rcr/layout/footer');
  }

	public function add_user()
	{
		$this->form_validation->set_rules('username', 'Username', 'required|min_length[5]');
		$this->form_validation->set_rules('password', 'Password', 'required');
		$this->form_validation->set_rules('passconf', 'Password Confirmation', 'required|matches[password]');

		if($this->form_validation->run() == TRUE)
		{
			$q = $this->user->add_new_user(array(
				'username' => $this->input->post('username'),
				'password' => $this->input->post('password')
			));

			if($q)
			{
				$this->session->set_flashdata('flag', 'Sukses Tambah User');
			}
			else
			{
				$this->session->set_flashdata('flag', 'Gagal Tambah User');
			}
		}
		redirect('rcr/user');
	}

	public function edit_user($username = NULL)
	{
		$this->form_validation->set_rules('username', 'Username', 'required|min_length[5]');
		$this->form_validation->set_rules('password', 'Password', 'required');
		$this->form_validation->set_rules('passconf', 'Password Confirmation', 'required|matches[password]');

		if($this->form_validation->run() == TRUE)
		{
			$q = $this->user->update_user(array(
				'username' => $this->input->post('username'),
				'password' => $this->input->post('password')
			));

			if($q)
			{
				$this->session->set_flashdata('flag', 'Sukses Update User');
			}
			else
			{
				$this->session->set_flashdata('flag', 'Gagal Update User');
			}

			redirect('rcr/user');
		}

		$data['user'] = $this->db->get_where('ms_user', array('username' => $username))->row();
		$this->load->view('rcr/layout/header', $this->user_auth);
    $this->load->view('rcr/user/edit', $data);
    $this->load->view('rcr/layout/footer');
	}

  public function add_authorization()
  {
    echo $this->user->add_auth();
  }

  public function delete_authorization()
  {
    echo $this->user->delete_auth();
  }

  public function set_authorization()
  {
    echo $this->user->set_auth();
  }
}
