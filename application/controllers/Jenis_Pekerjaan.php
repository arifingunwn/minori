<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Jenis_Pekerjaan extends MY_Controller {
	protected $access = 'admin';
	function __construct()
	{
			parent::__construct();
			$this->load->library('form_validation');
			$this->load->library('session');
      $this->load->model('Jenis_Pekerjaan_model', 'pekerjaan');
			$this->load->helper('my_helper');
			$this->load->database();

			if(empty($_SESSION['authorization']['PEKERJAAN'])){
				http_response_code(403);
				echo "Forbidden";
				die();
			}
	}

	public function index()
	{
    $pekerjaan = $this->pekerjaan->get();

    $data['pekerjaan'] = $pekerjaan;
		$data['jenis_pekerjaan'] = $this->pekerjaan->get_jenis_pekerjaan();
		$this->load->view('rcr/layout/header');
		$this->load->view('rcr/jenis_pekerjaan/index', $data);
		$this->load->view('rcr/layout/footer');
	}

  public function add()
  {
		$this->form_validation->set_rules('kd_jenis_pekerjaan', 'Jenis Pekerjaan', 'required');
		$this->form_validation->set_rules('pekerjaan', 'Nama Pekerjaan', 'required');
		$this->form_validation->set_rules('pekerjaan_jp', 'Nama Pekerjaan Jepang', 'required');

    $this->config->load('form_validation', TRUE);

    if($this->form_validation->run() === TRUE)
    {
        $q = $this->pekerjaan->save();
				if($q)
				{
					$this->session->set_flashdata('flag', 'Sukses Tambah Pekerjaan');
				}
				else
				{
					$this->session->set_flashdata('flag', 'Gagal Tambah Pekerjaan');
				}
        redirect('rcr/pekerjaan');
    }
  }

  public function edit()
  {
		$this->form_validation->set_rules('kd_jenis_pekerjaan', 'Jenis Pekerjaan', 'required');
		$this->form_validation->set_rules('kd_pekerjaan', 'Pekerjaan', 'required');
		$this->form_validation->set_rules('pekerjaan', 'Nama Pekerjaan', 'required');
		$this->form_validation->set_rules('pekerjaan_jp', 'Nama Pekerjaan Jepang', 'required');
    $this->config->load('form_validation', TRUE);

		if($this->form_validation->run() === TRUE)
		{
			$q = $this->pekerjaan->edit();
			if($q)
			{
				$this->session->set_flashdata('flag', 'Sukses Update Pekerjaan');
			}
			else
			{
				$this->session->set_flashdata('flag', 'Gagal Update Pekerjaan');
			}
			redirect('rcr/pekerjaan');
		}
  }
}
