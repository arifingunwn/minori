<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Magang extends CI_Controller {
	//protected $access = '*';
	function __construct()
	{
			parent::__construct();
			$this->load->library('form_validation');
			$this->load->library('session');
			$this->load->helper('my_helper');
			$this->load->model('Magang_model', 'magang');
			$this->load->database();
	}

	public function index()
	{
		if (!empty($_SESSION['member'])) {
			redirect('member');
		}
		$this->config->load('form_validation', TRUE);
		if($this->form_validation->run() == TRUE)
		{
			$_SESSION['register_0'] = array();
			$_SESSION['register_0']['no_ktp'] = $this->input->post('ktp');
			$_SESSION['register_0']['password'] = md5($this->input->post('password'));
			$_SESSION['register_0']['email'] = $this->input->post('email');
			redirect('magang/register');
		}
		$this->db->order_by('kd', 'desc');
		$lowongan = $this->db->get('tb_lowongan');
		$data['lowongan'] = $lowongan->result_array();
		$this->load->view('layout/header');
		$this->load->view('magang/front', $data);
		$this->load->view('layout/footer');
	}

	public function show_lowongan($id = NULL)
	{
		if($id == NULL)
		{
			redirect('magang');
		}
		$email = $this->session->userdata('member');
		$lowongan = $this->db->get_where('tb_lowongan', array('kd' => $id));

		$this->db->select('P.kd_pemagang');
		$this->db->from('tb_pemagangan P');
		$this->db->join('tb_pendaftar PD', 'P.kd_pemagang = PD.id_pemagang');
		$this->db->where(array(
			'P.email' => $email,
			'PD.id_lowongan' => $id
		));
		$q = $this->db->get();

		$data['lowongan'] = $lowongan->result_array();
		$data['sudah_pernah_melamar'] = $q->row();
		$this->load->view('layout/header');
		$this->load->view('magang/show_lowongan', $data);
		$this->load->view('layout/footer');
	}

	public function show_pengumuman($id = NULL)
	{
		if($id == NULL)
		{
			redirect('magang');
		}
		$pengumuman = $this->db->get_where('tb_pengumuman', array('kd' => $id));
		$data['pengumuman'] = $pengumuman->row();
		$this->load->view('layout/header');
		$this->load->view('magang/show_pengumuman', $data);
		$this->load->view('layout/footer');
	}

	public function login()
	{
		$this->form_validation->set_rules('email', 'E-mail', 'required');
		$this->form_validation->set_rules('password', 'Password', 'required');

		if($this->form_validation->run() === TRUE)
		{
			$email = $this->input->post('email');
			$password = $this->input->post('password');

			$this->magang->doLogin($email, $password);
		}
		redirect('magang/');
	}

	public function logout()
	{
		$this->session->unset_userdata('member');
		redirect('magang/');
	}

	public function forget_password()
	{
		$message = '';
		$this->form_validation->set_rules('email', 'Email', 'required');
		if($this->form_validation->run() === TRUE)
		{
			$message = $this->magang->forget_password();
		}
		$data['message'] = $message;
		$this->load->view('layout/header');
		$this->load->view('magang/forget_password', $data);
		$this->load->view('layout/footer');
	}

	public function reset_password($key = NULL, $email = NULL)
	{
		$error = '';
		if($key == NULL || $email == NULL)
		{
			redirect('magang');
		}

		$this->form_validation->set_rules('key1', 'Key 1', 'required');
		$this->form_validation->set_rules('key2', 'Key 2', 'required');
		$this->form_validation->set_rules('new_password', 'New Password', 'required');
		$this->form_validation->set_rules('new_password_confirmation', 'New Password', 'required|matches[new_password]');

		if($this->form_validation->run() === TRUE)
		{
			$q = $this->magang->reset_password();
			if($q == 'sukses')
			{
				redirect('magang');
			}
			else
			{
				$error = $q;
			}
		}
		$data['key1'] = $key;
		$data['key2'] = $email;
		$data['error'] = $error;
		$this->load->view('layout/header');
		$this->load->view('magang/reset_password', $data);
		$this->load->view('layout/footer');
	}

	public function register()
	{
		$this->config->load('form_validation', TRUE);
		if($this->form_validation->run() == TRUE)
		{

			$_SESSION['register_1'] = array(
				'nama_lengkap' 											=> strtoupper($this->input->post('nama_lengkap')),
				'photo'															=> $this->input->post('photo'),
				'tempat_lahir' 											=> strtoupper($this->input->post('tempat_lahir')),
				'tanggal_lahir' 										=> $this->input->post('tahun_lahir') . '-' . $this->input->post('bulan_lahir') . '-' . $this->input->post('tanggal_lahir'),
				'jenis_kelamin'											=> $this->input->post('jenis_kelamin'),
				'status' 														=> $this->input->post('status'),
				'golongan_darah'										=> $this->input->post('golongan_darah'),
				'provinsi_sekarang'									=> $this->input->post('provinsi_sekarang'),
				'agama'															=> $this->input->post('agama'),
				'tempat_asal'												=> strtoupper($this->input->post('tempat_asal')),
				'kewarganegaraan'										=> $this->input->post('kewarganegaraan'),
				'alamat_ktp'												=> $this->input->post('alamat_ktp'),
				'alamat_sekarang'										=> $this->input->post('alamat_sekarang'),
				'nomor_telepon'											=> $this->input->post('nomor_telepon'),
				'nomor_handphone'										=> $this->input->post('nomor_handphone'),
				'nomor_handphone_tambahan' 					=> $this->input->post('nomor_handphone_tambahan'),
				'ukuran_sepatu'											=> $this->input->post('ukuran_sepatu'),
				'lingkar_pinggang'									=> $this->input->post('lingkar_pinggang'),
				'berat_badan'												=> $this->input->post('berat_badan'),
				'tinggi_badan'											=> $this->input->post('tinggi_badan'),
				'pelajaran_favorit'									=> $this->input->post('pelajaran_favorit'),
				'hobi'															=> $this->input->post('hobi'),
				'keterampilan'											=> $this->input->post('keterampilan'),
				'nama_penyakit'											=> $this->input->post('nama_penyakit'),
				'tahun_penyakit'										=> $this->input->post('tahun_penyakit'),
				'bulan_penyakit'										=> $this->input->post('bulan_penyakit'),
				'merokok'														=> $this->input->post('merokok'),
				'batang_rokok_per_hari'							=> $this->input->post('batang_rokok_per_hari'),
				'minum_alkohol'											=> $this->input->post('minum_alkohol'),
				'frekuensi_minum_alkohol_perbulan'	=> $this->input->post('frekuensi_minum_alkohol_perbulan'),
				'pernah_keluar_negri'								=> $this->input->post('pernah_keluar_negri'),
				'passport'													=> $this->input->post('passport'),
				'no_passport'												=> $this->input->post('no_passport'),
				'tanggal_berlaku_passport'					=> $this->input->post('tahun_berlaku_passport') . '-' . $this->input->post('bulan_berlaku_passport') . '-' . $this->input->post('tanggal_berlaku_passport'),
				'tanggal_penerbitan_passport'				=> $this->input->post('tahun_penerbitan_passport') . '-' . $this->input->post('bulan_penerbitan_passport') . '-' . $this->input->post('tanggal_penerbitan_passport'),
			);

			if($_POST['passport'] == 'YA')
			{
				if(!empty($_POST['no_passport']))
				{
					$_SESSION['register_1']['no_passport'] = $this->input->post('no_passport');
				}

				if(!empty($_POST['tanggal_berlaku_passport']) && !empty($_POST['bulan_berlaku_passport']) && !empty($_POST['tahun_berlaku_passport']))
				{
					$_SESSION['register_1']['tanggal_berlaku_passport'] = $this->input->post('tahun_berlaku_passport') . '-' . $this->input->post('bulan_berlaku_passport') . '-' . $this->input->post('tanggal_berlaku_passport');
				}

				if(!empty($_POST['tanggal_penerbitan_passport']) && !empty($_POST['bulan_penerbitan_passport']) && !empty($_POST['tahun_penerbitan_passport']))
				{
					$_SESSION['register_1']['tanggal_penerbitan_passport'] = $this->input->post('tahun_penerbitan_passport') . '-' . $this->input->post('bulan_penerbitan_passport') . '-' . $this->input->post('tanggal_berlaku_passport');
				}
			}

			if($_POST['pernah_keluar_negri'] == 'p')
			{
				if(	!empty($_POST['negara_pernah_dikunjungi'][0]['negara']) &&
						isset($_POST['negara_pernah_dikunjungi'][0]['tahun']) &&
						isset($_POST['negara_pernah_dikunjungi'][0]['bulan'])
				)
				{
					$_SESSION['register_1']['negara_pernah_dikunjungi'] = array();
					foreach ($_POST['negara_pernah_dikunjungi'] as $key => $value) {
						if(!empty($value['negara']) && isset($value['tahun']) && isset($value['bulan']))
						{
							array_push($_SESSION['register_1']['negara_pernah_dikunjungi'], array(
								'negara' 	=> $value['negara'],
								'tahun' 	=> $value['tahun'],
								'bulan' 	=> $value['bulan'],
								'tujuan'	=> $value['tujuan']
							));
						}
					}
				}
			}
			redirect('magang/register2');
		}
		if(empty($_SESSION['register_0']))
		{
			redirect('magang/');
		}

		$provinsi = $this->db->get('tb_provinsi');
		$agama = $this->db->get('tb_agama');

		$data['biodata'] = "active";
		$data['negara'] = $this->getNegara();
		$data['provinsi'] = $provinsi->result_array();
		$data['agama'] = $agama->result_array();

		$this->load->view('layout/header');
		$this->load->view('layout/register_step', $data);
		$this->load->view('magang/register');
		$this->load->view('layout/footer');
	}

	public function register2()
	{
		if (!empty($_POST['bahasa'])) {
			$_SESSION['register_2']['bahasa'] = $this->input->post('bahasa');
			redirect('magang/register3');
		}

		$data['bahasa'] = 'active';
		$this->load->view('layout/header');
		$this->load->view('layout/register_step', $data);
		$this->load->view('magang/register2');
		$this->load->view('layout/footer');
	}

	public function register3()
	{
		if(isset($_POST['riwayat']))
		{
				$_SESSION['register_3'] = array();
				foreach ($_POST['riwayat'] as $key => $val) {
					if(!empty($val['nama_perusahaan']))
					{
						array_push($_SESSION['register_3'], array(
							'nama_perusahaan' => $val['nama_perusahaan'],
							'kd_pekerjaan'		=> $val['kd_pekerjaan'],
							'bulan_mulai'			=> $val['bulan_mulai'],
							'tahun_mulai'			=> $val['tahun_mulai'],
							'bulan_selesai'		=> $val['bulan_selesai'],
							'tahun_selesai'		=> $val['tahun_selesai']
						));
					}
				}

				redirect('magang/register4');
		}

		$this->load->database();
		$this->db->select('tb_pekerjaan.kd_pekerjaan, tb_jenis_pekerjaan.jenis_pekerjaan, tb_pekerjaan.pekerjaan');
		$this->db->from('tb_pekerjaan');
		$this->db->join('tb_jenis_pekerjaan', 'tb_pekerjaan.kd_jenis_pekerjaan = tb_jenis_pekerjaan.kd_jenis_pekerjaan', 'left');
		$this->db->order_by('tb_jenis_pekerjaan.kd_jenis_pekerjaan', 'asc');
		$data['tb_pekerjaan'] = $this->db->get()->result_array();
		$data['active_pekerjaan'] = 'active';
		$this->load->view('layout/header');
		$this->load->view('layout/register_step', $data);
		$this->load->view('magang/register3');
		$this->load->view('layout/footer');
	}

	public function register4()
	{
		if(isset($_POST['pendidikan']))
		{
			$_SESSION['register_4'] = array();
			if(!empty($_POST['nama_sekolah_sd']))
			{
				$_SESSION['register_4']['SD'] = array(
					'nama_sekolah' 	=> $this->input->post('nama_sekolah_sd'),
					'tahun_masuk' 	=> $this->input->post('tahun_masuk_sd'),
					'tahun_selesai' => $this->input->post('tahun_selesai_sd')
				);
			}

			if(!empty($_POST['nama_sekolah_smp']))
			{
				$_SESSION['register_4']['SMP'] = array(
						'nama_sekolah' 	=> $this->input->post('nama_sekolah_smp'),
						'tahun_masuk' 	=> $this->input->post('tahun_masuk_smp'),
						'tahun_selesai' => $this->input->post('tahun_selesai_smp')
				);
			}

			if(!empty($_POST['nama_sekolah_sma']))
			{
				$_SESSION['register_4']['SMA'] = array(
						'nama_sekolah' 	=> $this->input->post('nama_sekolah_sma'),
						'tahun_masuk' 	=> $this->input->post('tahun_masuk_sma'),
						'tahun_selesai' => $this->input->post('tahun_selesai_sma'),
						'jurusan'				=> $this->input->post('jurusan_sma')
				);
			}

			if(!empty($_POST['sekolah_lanjutan'][0]['nama_instansi']))
			{
				$_SESSION['register_4']['sekolah_lanjutan'] = array();

				foreach ($_POST['sekolah_lanjutan'] as $k => $v) {
					if(!empty($v['nama_instansi']))
					{
						array_push($_SESSION['register_4']['sekolah_lanjutan'], array(
							'nama_instansi' => $v['nama_instansi'],
							'jurusan' => $v['jurusan'],
							'tahun_masuk' => $v['tahun_masuk'],
							'tahun_selesai' => $v['tahun_selesai'],
							'tingkatan' => $v['tingkatan']
						));
					}
				}
			}

			if(!empty($_POST['belajar_bahasa_jepang'][0]['nama_instansi']))
			{
				$_SESSION['register_4']['belajar_bahasa_jepang'] = array();
				foreach ($_POST['belajar_bahasa_jepang'] as $k => $v) {
					if(!empty($v['nama_instansi']))
					{
						array_push($_SESSION['register_4']['belajar_bahasa_jepang'], array(
							'nama_instansi' => $v['nama_instansi'],
							'tahun_masuk' => $v['tahun_masuk'],
							'tahun_selesai' => $v['tahun_selesai']
						));
					}
				}
			}

			redirect('magang/register5');
		}

		$data['pendidikan'] = 'active';
		$data['jurusan'] = $this->db->get('ms_jurusan_pendidikan')->result_array();
		$this->load->view('layout/header');
		$this->load->view('layout/register_step', $data);
		$this->load->view('magang/register4');
		$this->load->view('layout/footer');
	}

	public function register5()
	{
		if(isset($_POST['submit']))
		{
			$_SESSION['register_5'] = array();

			if(!empty($_POST['nama_ayah']))
			{
				$_SESSION['register_5']['ayah'] = array(
					'nama' => $this->input->post('nama_ayah'),
					'tempat_lahir' => $this->input->post('tempat_lahir_ayah'),
					'tanggal_lahir' => $this->input->post('tahun_lahir_ayah') . '-' . $this->input->post('bulan_lahir_ayah') . '-' . $this->input->post('tanggal_lahir_ayah'),
					'pekerjaan' => $this->input->post('pekerjaan_ayah'),
					'no_telp' => $this->input->post('no_telp_ayah')
				);
			}

			if(!empty($_POST['nama_ibu']))
			{
				$_SESSION['register_5']['ibu'] = array(
					'nama' => $this->input->post('nama_ibu'),
					'tempat_lahir' => $this->input->post('tempat_lahir_ibu'),
					'tanggal_lahir' => $this->input->post('tahun_lahir_ibu') . '-' . $this->input->post('bulan_lahir_ibu') . '-' . $this->input->post('tanggal_lahir_ibu'),
					'pekerjaan' => $this->input->post('pekerjaan_ibu'),
					'no_telp' => $this->input->post('no_telp_ibu')
				);
			}

			if(!empty($_POST['saudara_kandung'][0]['nama']))
			{
				$_SESSION['register_5']['saudara_kandung'] = array();
				foreach ($_POST['saudara_kandung'] as $k => $v) {
					if(!empty($v['nama']))
					{
						array_push($_SESSION['register_5']['saudara_kandung'], array(
							'nama' => htmlspecialchars($v['nama']),
							'tempat_lahir' => htmlspecialchars($v['tempat_lahir']),
							'tanggal_lahir' => htmlspecialchars($v['tahun_lahir']) . '-' . htmlspecialchars($v['bulan_lahir']) . '-' . htmlspecialchars($v['tanggal_lahir']),
							'hubungan' => htmlspecialchars($v['hubungan']),
							'pekerjaan' => htmlspecialchars($v['pekerjaan']),
							'no_telp' => htmlspecialchars($v['no_telepon'])
						));
					}
				}
			}

			if(!empty($_POST['keluarga_pribadi'][0]['nama']))
			{
				$_SESSION['register_5']['keluarga_pribadi'] = array();
				foreach ($_POST['keluarga_pribadi'] as $k => $v) {
					if(!empty($v['nama']))
					{
						array_push($_SESSION['register_5']['keluarga_pribadi'], array(
							'nama' => htmlspecialchars($v['nama']),
							'tempat_lahir' => htmlspecialchars($v['tempat_lahir']),
							'tanggal_lahir' => htmlspecialchars($v['tahun_lahir']) . '-' . htmlspecialchars($v['bulan_lahir']) . '-' . htmlspecialchars($v['tanggal_lahir']),
							'hubungan' => htmlspecialchars($v['hubungan']),
							'pekerjaan' => htmlspecialchars($v['pekerjaan']),
							'no_telp' => htmlspecialchars($v['no_telepon'])
						));
					}
				}
			}

			if(!empty($_POST['keluarga_dijepang'][0]['nama']))
			{
				$_SESSION['register_5']['keluarga_dijepang'] = array();
				foreach ($_POST['keluarga_dijepang'] as $k => $v) {
					if(!empty($v['nama']))
					{
						array_push($_SESSION['register_5']['keluarga_dijepang'], array(
							'nama' => htmlspecialchars($v['nama']),
							'perusahaan' => htmlspecialchars($v['perusahaan']),
							'tanggal_lahir' => htmlspecialchars($v['tahun_lahir']) . '-' . htmlspecialchars($v['bulan_lahir']) . '-' . htmlspecialchars($v['tanggal_lahir']),
							'hubungan' => htmlspecialchars($v['hubungan']),
							'kewarganegaraan' => htmlspecialchars($v['kewarganegaraan']),
							'status_visa' => htmlspecialchars($v['status_visa'])
						));
					}
				}
			}
			redirect('magang/register_finnish');
		}

		$this->load->model('Jenis_Pekerjaan_model', 'pekerjaan');
		$hubungan = $this->db->get('tb_hubungan');
		$data['hubungan'] = $hubungan->result_array();
		$data['pekerjaan'] = $this->pekerjaan->get();
		$data['kerabat'] = 'active';
		$this->load->view('layout/header');
		$this->load->view('layout/register_step', $data);
		$this->load->view('magang/register5');
		$this->load->view('layout/footer');
	}

	public function register_finnish()
	{
		$q = $this->db->get('tb_hubungan');
		$hubungan = $q->result_array();
		$data['selesai'] = 'active';
		$data['hubungan'] = $hubungan;
		$this->load->view('layout/header');
		$this->load->view('layout/register_step', $data);
		$this->load->view('magang/register_finnish');
		$this->load->view('layout/footer');
	}

	public function register_confirmation()
	{
		if(	empty($_SESSION['register_0']) ||
				empty($_SESSION['register_1']) ||
				empty($_SESSION['register_2']) ||
				empty($_SESSION['register_3']) ||
				empty($_SESSION['register_4']) ||
				empty($_SESSION['register_5'])
			)
		{
			echo "DATA BELUM LENGKAP";
		}

		else
		{
			$q = $this->magang->save_register($_SESSION);
			if($q)
			{
				echo 'Sukses';
				session_destroy();
				redirect('magang');
			}
			else {
				echo 'Gagal';
			}
		}
	}

	public function uploadPhoto()
	{
		$this->upload();
	}

	private function upload()
	{
		$data = $_POST['image'];
		list($type, $data) = explode(';', $data);
		list(, $data)      = explode(',', $data);
		$data = base64_decode($data);
		$imageName = time().'.png';
		file_put_contents($_SERVER['DOCUMENT_ROOT'].'/minori/tmp/foto/'.$imageName, $data);
		//$_SESSION['register_1']['photo'] = $imageName;
		echo $imageName;
	}

	public function check_ktp($no_ktp)
	{

		$q = $this->db->get_where('tb_pemagangan', array(
			'no_ktp' => $no_ktp
		));
		echo json_encode($q->num_rows());
	}

	public function check_email()
	{
		$email = $this->input->post('email');
		$q = $this->db->get_where('tb_pemagangan', array(
			'email' => $email
		));
		echo json_encode($q->num_rows());
	}

	public function getAllPekerjaan()
	{
		$this->db->select('tb_pekerjaan.kd_pekerjaan, tb_jenis_pekerjaan.jenis_pekerjaan, tb_pekerjaan.pekerjaan');
		$this->db->from('tb_pekerjaan');
		$this->db->join('tb_jenis_pekerjaan', 'tb_pekerjaan.kd_jenis_pekerjaan = tb_jenis_pekerjaan.kd_jenis_pekerjaan', 'left');
		echo json_encode($this->db->get()->result_array());
	}

	public function getHubungan()
	{
		$q = $this->db->get('tb_hubungan');
		$data = $q->result_array();
		echo json_encode($data);
	}

	public function checkIfLoggedIn()
	{
		if(!empty($_SESSION['member']))
		{
			echo $_SESSION['member'];
		}
		else
		{
			echo 'not_logged_in';
		}
	}

	public function checkNoKTP($ktp = NULL)
	{
		if(isset($ktp))
		{
			$this->db->select('no_ktp');
			$this->db->from('tb_pemagangan');
			$this->db->where(array('no_ktp' => $ktp));
			$q = $this->db->get();
			$row = $q->row();
			if(!empty($row))
			{
				echo json_encode(true);
			}
			else{
				echo json_encode(false);
			}
		}
	}

	private function getNegara()
	{
		$negara =
		array('Afghanistan',
		      'Åland Islands',
		      'Albania',
		      'Algeria',
		      'American Samoa',
		      'Andorra',
		      'Angola',
		      'Anguilla',
		      'Antarctica',
		      'Antigua and Barbuda',
		      'Argentina',
		      'Armenia',
		      'Aruba',
		      'Australia',
		      'Austria',
		      'Azerbaijan',
		      'Bahamas',
		      'Bahrain',
		      'Bangladesh',
		      'Barbados',
		      'Belarus',
		      'Belgium',
		      'Belize',
		      'Benin',
		      'Bermuda',
		      'Bhutan',
		      'Bolivia',
		      'Bosnia and Herzegovina',
		      'Botswana',
		      'Bouvet Island',
		      'Brazil',
		      'British Indian Ocean Territory',
		      'Brunei Darussalam',
		      'Bulgaria',
		      'Burkina Faso',
		      'Burundi',
		      'Cambodia',
		      'Cameroon',
		      'Canada',
		      'Cape Verde',
		      'Cayman Islands',
		      'Central African Republic',
		      'Chad',
		      'Chile',
		      'China',
		      'Christmas Island',
		      'Cocos (Keeling) Islands',
		      'Colombia',
		      'Comoros',
		      'Congo',
		      'Congo, The Democratic Republic of the',
		      'Cook Islands',
		      'Costa Rica',
		      'Cote D\'Ivoire',
		      'Croatia',
		      'Cuba',
		      'Cyprus',
		      'Czech Republic',
		      'Denmark',
		      'Djibouti',
		      'Dominica',
		      'Dominican Republic',
		      'Ecuador',
		      'Egypt',
		      'El Salvador',
		      'Equatorial Guinea',
		      'Eritrea',
		      'Estonia',
		      'Ethiopia',
		      'Falkland Islands (Malvinas)',
		      'Faroe Islands',
		      'Fiji',
		      'Finland',
		      'France',
		      'French Guiana',
		      'French Polynesia',
		      'French Southern Territories',
		      'Gabon',
		      'Gambia',
		      'Georgia',
		      'Germany',
		      'Ghana',
		      'Gibraltar',
		      'Greece',
		      'Greenland',
		      'Grenada',
		      'Guadeloupe',
		      'Guam',
		      'Guatemala',
		      'Guernsey',
		      'Guinea',
		      'Guinea-Bissau',
		      'Guyana',
		      'Haiti',
		      'Heard Island and Mcdonald Islands',
		      'Holy See (Vatican City State)',
		      'Honduras',
		      'Hong Kong',
		      'Hungary',
		      'Iceland',
		      'India',
		      'Indonesia',
		      'Iran, Islamic Republic Of',
		      'Iraq',
		      'Ireland',
		      'Isle of Man',
		      'Israel',
		      'Italy',
		      'Jamaica',
		      'Japan',
		      'Jersey',
		      'Jordan',
		      'Kazakhstan',
		      'Kenya',
		      'Kiribati',
		      'Democratic People\'s Republic of Korea',
		      'Korea, Republic of',
		      'Kosovo',
		      'Kuwait',
		      'Kyrgyzstan',
		      'Lao People\'s Democratic Republic',
		      'Latvia',
		      'Lebanon',
		      'Lesotho',
		      'Liberia',
		      'Libyan Arab Jamahiriya',
		      'Liechtenstein',
		      'Lithuania',
		      'Luxembourg',
		      'Macao',
		      'Macedonia, The Former Yugoslav Republic of',
		      'Madagascar',
		      'Malawi',
		      'Malaysia',
		      'Maldives',
		      'Mali',
		      'Malta',
		      'Marshall Islands',
		      'Martinique',
		      'Mauritania',
		      'Mauritius',
		      'Mayotte',
		      'Mexico',
		      'Micronesia, Federated States of',
		      'Moldova, Republic of',
		      'Monaco',
		      'Mongolia',
		      'Montenegro',
		      'Montserrat',
		      'Morocco',
		      'Mozambique',
		      'Myanmar',
		      'Namibia',
		      'Nauru',
		      'Nepal',
		      'Netherlands',
		      'Netherlands Antilles',
		      'New Caledonia',
		      'New Zealand',
		      'Nicaragua',
		      'Niger',
		      'Nigeria',
		      'Niue',
		      'Norfolk Island',
		      'Northern Mariana Islands',
		      'Norway',
		      'Oman',
		      'Pakistan',
		      'Palau',
		      'Palestinian Territory, Occupied',
		      'Panama',
		      'Papua New Guinea',
		      'Paraguay',
		      'Peru',
		      'Philippines',
		      'Pitcairn',
		      'Poland',
		      'Portugal',
		      'Puerto Rico',
		      'Qatar',
		      'Reunion',
		      'Romania',
		      'Russian Federation',
		      'Rwanda',
		      'Saint Helena',
		      'Saint Kitts and Nevis',
		      'Saint Lucia',
		      'Saint Pierre and Miquelon',
		      'Saint Vincent and the Grenadines',
		      'Samoa',
		      'San Marino',
		      'Sao Tome and Principe',
		      'Saudi Arabia',
		      'Senegal',
		      'Serbia',
		      'Seychelles',
		      'Sierra Leone',
		      'Singapore',
		      'Slovakia',
		      'Slovenia',
		      'Solomon Islands',
		      'Somalia',
		      'South Africa',
		      'South Georgia and the South Sandwich Islands',
		      'Spain',
		      'Sri Lanka',
		      'Sudan',
		      'Suriname',
		      'Svalbard and Jan Mayen',
		      'Swaziland',
		      'Sweden',
		      'Switzerland',
		      'Syrian Arab Republic',
		      'Taiwan',
		      'Tajikistan',
		      'Tanzania, United Republic of',
		      'Thailand',
		      'Timor-Leste',
		      'Togo',
		      'Tokelau',
		      'Tonga',
		      'Trinidad and Tobago',
		      'Tunisia',
		      'Turkey',
		      'Turkmenistan',
		      'Turks and Caicos Islands',
		      'Tuvalu',
		      'Uganda',
		      'Ukraine',
		      'United Arab Emirates',
		      'United Kingdom',
		      'United States',
		      'United States Minor Outlying Islands',
		      'Uruguay',
		      'Uzbekistan',
		      'Vanuatu',
		      'Venezuela',
		      'Viet Nam',
		      'Virgin Islands, British',
		      'Virgin Islands, U.S.',
		      'Wallis and Futuna',
		      'Western Sahara',
		      'Yemen',
		      'Zambia',
		      'Zimbabwe');
		return $negara;
	}

	public function getNegaraJSON()
	{
		$negara =
		array('Afghanistan',
		      'Åland Islands',
		      'Albania',
		      'Algeria',
		      'American Samoa',
		      'Andorra',
		      'Angola',
		      'Anguilla',
		      'Antarctica',
		      'Antigua and Barbuda',
		      'Argentina',
		      'Armenia',
		      'Aruba',
		      'Australia',
		      'Austria',
		      'Azerbaijan',
		      'Bahamas',
		      'Bahrain',
		      'Bangladesh',
		      'Barbados',
		      'Belarus',
		      'Belgium',
		      'Belize',
		      'Benin',
		      'Bermuda',
		      'Bhutan',
		      'Bolivia',
		      'Bosnia and Herzegovina',
		      'Botswana',
		      'Bouvet Island',
		      'Brazil',
		      'British Indian Ocean Territory',
		      'Brunei Darussalam',
		      'Bulgaria',
		      'Burkina Faso',
		      'Burundi',
		      'Cambodia',
		      'Cameroon',
		      'Canada',
		      'Cape Verde',
		      'Cayman Islands',
		      'Central African Republic',
		      'Chad',
		      'Chile',
		      'China',
		      'Christmas Island',
		      'Cocos (Keeling) Islands',
		      'Colombia',
		      'Comoros',
		      'Congo',
		      'Congo, The Democratic Republic of the',
		      'Cook Islands',
		      'Costa Rica',
		      'Cote D\'Ivoire',
		      'Croatia',
		      'Cuba',
		      'Cyprus',
		      'Czech Republic',
		      'Denmark',
		      'Djibouti',
		      'Dominica',
		      'Dominican Republic',
		      'Ecuador',
		      'Egypt',
		      'El Salvador',
		      'Equatorial Guinea',
		      'Eritrea',
		      'Estonia',
		      'Ethiopia',
		      'Falkland Islands (Malvinas)',
		      'Faroe Islands',
		      'Fiji',
		      'Finland',
		      'France',
		      'French Guiana',
		      'French Polynesia',
		      'French Southern Territories',
		      'Gabon',
		      'Gambia',
		      'Georgia',
		      'Germany',
		      'Ghana',
		      'Gibraltar',
		      'Greece',
		      'Greenland',
		      'Grenada',
		      'Guadeloupe',
		      'Guam',
		      'Guatemala',
		      'Guernsey',
		      'Guinea',
		      'Guinea-Bissau',
		      'Guyana',
		      'Haiti',
		      'Heard Island and Mcdonald Islands',
		      'Holy See (Vatican City State)',
		      'Honduras',
		      'Hong Kong',
		      'Hungary',
		      'Iceland',
		      'India',
		      'Indonesia',
		      'Iran, Islamic Republic Of',
		      'Iraq',
		      'Ireland',
		      'Isle of Man',
		      'Israel',
		      'Italy',
		      'Jamaica',
		      'Japan',
		      'Jersey',
		      'Jordan',
		      'Kazakhstan',
		      'Kenya',
		      'Kiribati',
		      'Democratic People\'s Republic of Korea',
		      'Korea, Republic of',
		      'Kosovo',
		      'Kuwait',
		      'Kyrgyzstan',
		      'Lao People\'s Democratic Republic',
		      'Latvia',
		      'Lebanon',
		      'Lesotho',
		      'Liberia',
		      'Libyan Arab Jamahiriya',
		      'Liechtenstein',
		      'Lithuania',
		      'Luxembourg',
		      'Macao',
		      'Macedonia, The Former Yugoslav Republic of',
		      'Madagascar',
		      'Malawi',
		      'Malaysia',
		      'Maldives',
		      'Mali',
		      'Malta',
		      'Marshall Islands',
		      'Martinique',
		      'Mauritania',
		      'Mauritius',
		      'Mayotte',
		      'Mexico',
		      'Micronesia, Federated States of',
		      'Moldova, Republic of',
		      'Monaco',
		      'Mongolia',
		      'Montenegro',
		      'Montserrat',
		      'Morocco',
		      'Mozambique',
		      'Myanmar',
		      'Namibia',
		      'Nauru',
		      'Nepal',
		      'Netherlands',
		      'Netherlands Antilles',
		      'New Caledonia',
		      'New Zealand',
		      'Nicaragua',
		      'Niger',
		      'Nigeria',
		      'Niue',
		      'Norfolk Island',
		      'Northern Mariana Islands',
		      'Norway',
		      'Oman',
		      'Pakistan',
		      'Palau',
		      'Palestinian Territory, Occupied',
		      'Panama',
		      'Papua New Guinea',
		      'Paraguay',
		      'Peru',
		      'Philippines',
		      'Pitcairn',
		      'Poland',
		      'Portugal',
		      'Puerto Rico',
		      'Qatar',
		      'Reunion',
		      'Romania',
		      'Russian Federation',
		      'Rwanda',
		      'Saint Helena',
		      'Saint Kitts and Nevis',
		      'Saint Lucia',
		      'Saint Pierre and Miquelon',
		      'Saint Vincent and the Grenadines',
		      'Samoa',
		      'San Marino',
		      'Sao Tome and Principe',
		      'Saudi Arabia',
		      'Senegal',
		      'Serbia',
		      'Seychelles',
		      'Sierra Leone',
		      'Singapore',
		      'Slovakia',
		      'Slovenia',
		      'Solomon Islands',
		      'Somalia',
		      'South Africa',
		      'South Georgia and the South Sandwich Islands',
		      'Spain',
		      'Sri Lanka',
		      'Sudan',
		      'Suriname',
		      'Svalbard and Jan Mayen',
		      'Swaziland',
		      'Sweden',
		      'Switzerland',
		      'Syrian Arab Republic',
		      'Taiwan',
		      'Tajikistan',
		      'Tanzania, United Republic of',
		      'Thailand',
		      'Timor-Leste',
		      'Togo',
		      'Tokelau',
		      'Tonga',
		      'Trinidad and Tobago',
		      'Tunisia',
		      'Turkey',
		      'Turkmenistan',
		      'Turks and Caicos Islands',
		      'Tuvalu',
		      'Uganda',
		      'Ukraine',
		      'United Arab Emirates',
		      'United Kingdom',
		      'United States',
		      'United States Minor Outlying Islands',
		      'Uruguay',
		      'Uzbekistan',
		      'Vanuatu',
		      'Venezuela',
		      'Viet Nam',
		      'Virgin Islands, British',
		      'Virgin Islands, U.S.',
		      'Wallis and Futuna',
		      'Western Sahara',
		      'Yemen',
		      'Zambia',
		      'Zimbabwe');
		echo json_encode($negara);
	}
}
