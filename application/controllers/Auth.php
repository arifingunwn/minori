<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Auth extends CI_Controller {

	function __construct()
	{
			parent::__construct();
			$this->load->library('form_validation');
			$this->load->library('session');
			$this->load->helper('my_helper');

			$this->load->model('Auth_model', 'auth');
      $this->load->database();
	}

	//LOGIN
  public function index()
	{
		if(!empty($_SESSION['user']))
		{
			redirect('rcr');
		}

		if (isset($_POST['login'])) {
			$username = $this->input->post('username');
			$password = $this->input->post('password');

			if($this->auth->doLogin($username, $password))
			{
				redirect('rcr');
			}
		}

		$this->load->view('rcr/login');
	}

	public function logout()
	{
		unset($_SESSION['user']);
		unset($_SESSION['authorization']);
		redirect('auth/');
	}
}
