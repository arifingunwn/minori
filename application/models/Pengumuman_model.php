<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * Model Pengumuman
 */

class Pengumuman_model extends CI_Model {
    private $table = 'tb_pengumuman';
    public $judul;
    public $text;
    public $tanggal;
    public $penerbit;

    public function __construct()
    {
        parent::__construct();
        $this->load->database();
    }

    public function save()
    {
      $this->load->library('session');

      $this->judul = $this->input->post('judul');
      $this->text = $this->input->post('text');
      $this->penerbit = $this->session->userdata('user');
      $this->tanggal = Date('Y-m-d');
      return $this->db->insert($this->table, $this);
    }

    public function get()
    {
      $q = $this->db->get($this->table);
      return $q->result_array();
    }

    public function get_row($kd)
    {
      $q = $this->db->get_where($this->table, array(
        'kd' => $kd
      ));
      return $q->row();
    }

    public function update($kd)
    {
      $this->load->library('session');
      $this->judul = $this->input->post('judul');
      $this->text = $this->input->post('text');
      $this->penerbit = $this->session->userdata('user');
      $this->tanggal = Date('Y-m-d');
      $q = $this->db->update($this->table, $this, array(
        'kd' => $kd
      ));
      return $q;
    }

    public function delete($kd)
    {
      return $this->db->delete($this->table, array(
       'kd' => $kd
      ));
    }
}
