<?php

/**
 * Model Magang
 */
class User_model extends CI_Model
{

  function __construct()
  {
    parent::__construct();
    $this->load->database();
  }

  public function get()
  {
    return $this->db->get('ms_user')->result_array();
  }

  public function find($username)
  {
    return $this->db->get_where('ms_user', array(
      'username' => $username
    ))->row();
  }

  public function add_new_user($array)
  {
    $user = array (
      'username' => $array['username'],
      'password' => $this->hash_password($array['password'])
    );
    return $this->db->insert('ms_user', $user);
  }

  public function add_auth()
  {
    $username = $this->input->post('username');
    $module_id = $this->input->post('module_id');

    return $this->db->insert('ms_authorization', array(
      'module_id' => $module_id,
      'username'  => $username
    ));
  }

  public function delete_auth()
  {
    $username = $this->input->post('username');
    $module_id = $this->input->post('module_id');

    return $this->db->delete('ms_authorization', array(
      'module_id' => $module_id,
      'username'  => $username
    ));
  }

  public function set_auth()
  {
    $username = $this->input->post('username');
    $module_id = $this->input->post('module_id');
    $status = $this->input->post('status');
    $auth = $this->input->post('auth');

    $this->db->set(array(
      $auth => $status
    ));
    $this->db->where(array(
      'username' => $username,
      'module_id' => $module_id
    ));
    return $this->db->update('ms_authorization');
  }

  public function update_user()
  {
    $this->db->set(array(
      'password' => $this->hash_password($this->input->post('password'))
    ));
    $this->db->where('username', $this->input->post('username'));
    return $this->db->update('ms_user');
  }

  private function hash_password($password)
  {
    $options = [
    'cost' => 12,
    ];
    return password_hash($password, PASSWORD_BCRYPT, $options);
  }
}


?>
