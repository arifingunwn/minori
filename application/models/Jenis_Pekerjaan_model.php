<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * Model Jenis Pekerjaan
 */

class Jenis_Pekerjaan_model extends CI_Model {
    private $table = 'tb_pekerjaan';
    public $kd_jenis_pekerjaan; // a.k.a parent id
    public $pekerjaan; //nama
    public $pekerjaan_jp; // nama pekerjaan in japanese
    public $publish; //no idea what is it, old programmer create the table

    private $str = ''; //tree

    public function __construct()
    {
        parent::__construct();
        $this->load->database();
    }

    public function save()
    {
      $this->kd_jenis_pekerjaan = $this->input->post('kd_jenis_pekerjaan');
      $this->pekerjaan          = $this->input->post('pekerjaan');
      $this->pekerjaan_jp       = $this->input->post('pekerjaan_jp');
      $this->publish            = 1;
      return $this->db->insert($this->table, $this);
    }

    public function get()
    {
      $this->db->select('P.kd_pekerjaan, JP.kd_jenis_pekerjaan, P.pekerjaan, P.pekerjaan_jp, JP.jenis_pekerjaan');
      $this->db->from('tb_pekerjaan P');
      $this->db->join('tb_jenis_pekerjaan JP', 'P.kd_jenis_pekerjaan = JP.kd_jenis_pekerjaan');
      $this->db->order_by('JP.kd_jenis_pekerjaan', 'asc');
      $q = $this->db->get();
      return $q->result_array();
    }

    public function get_jenis_pekerjaan()
    {
      return $this->db->get('tb_jenis_pekerjaan')->result_array();
    }

    public function get_row($kd)
    {
      $q = $this->db->get_where($this->table, array(
        'kd' => $kd
      ));
      return $q->row();
    }

    public function edit()
    {
      $this->kd_jenis_pekerjaan = $this->input->post('kd_jenis_pekerjaan');
      $this->pekerjaan = $this->input->post('pekerjaan');
      $this->pekerjaan_jp = $this->input->post('pekerjaan_jp');
      return $this->db->update($this->table, $this, array(
        'kd_pekerjaan' => $this->input->post('kd_pekerjaan')
      ));
    }

    // private $branch = array();
    // public function generateTree($data, $parent_id = 1)
    // {
    //   //$branch = array();
    //   foreach ($data as $row) {
    //     if ($row['kd_jenis_pekerjaan'] == $parent_id) {
    //       $children = $this->generateTree($data, $row['kd_pekerjaan']);
    //
    //       if($children)
    //       {
    //         $row['children'] = $children;
    //       }
    //       $this->branch[] = $row;
    //     }
    //   }
    //   return $this->branch;
    // }
    //
    // public function getTree($data)
    // {
    //   $tree = $this->generateTree($data, 1);
    // }
}
