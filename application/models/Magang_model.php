<?php

/**
 * Model Magang
 */
class Magang_model extends CI_Model
{

  function __construct()
  {
    parent::__construct();
  }

  public function save_register($arr)
  {
    $tahun = substr(Date('Y'), 2, 2);
    $q = $this->db->query("SELECT COALESCE(MAX(kd_pemagang), 0) as kd_pemagang FROM tb_pemagangan WHERE kd_pemagang LIKE 'CM{$tahun}%';");
    $last_id = $q->row();
    echo substr($last_id->kd_pemagang, 4, 5);
    $kd_pemagang = "CM{$tahun}" . str_pad('0000' . intval(substr($last_id->kd_pemagang, 4, 5)) + 1, 5, STR_PAD_LEFT);

    //MOVE FOTO FROM TMP TO MEDIA/{MD5}
    $foto = $arr['register_1']['photo'];
    $temp_path = $_SERVER['DOCUMENT_ROOT'].'/minori/tmp/foto/';
    $image_path = $_SERVER['DOCUMENT_ROOT'].'/media/'. md5($kd_pemagang.'manfikar') . '/';
    if (!is_dir($image_path)) {
      mkdir($image_path, 0777, true);
    }
    rename($temp_path.$foto, $image_path.$foto);

    $this->db->trans_start();

    //INSERT tb_pemagangan
    $this->db->insert('tb_pemagangan', array(
      'kd_pemagang' => $kd_pemagang,
      'email'       => $arr['register_0']['email'],
      'kode'        => $arr['register_0']['password'],
      'no_ktp'      => $arr['register_0']['no_ktp'],
      'nama_pem' => $arr['register_1']['nama_lengkap'],
      'foto' => $arr['register_1']['photo'],
      'tempat_lahir' => $arr['register_1']['tempat_lahir'],
      'tanggal_lahir' => $arr['register_1']['tanggal_lahir'],
      'jk' => $arr['register_1']['jenis_kelamin'],
      'status' => $arr['register_1']['status'],
      'gol_darah' => $arr['register_1']['golongan_darah'],
      'kd_provinsi' => $arr['register_1']['provinsi_sekarang'],
      'id_agama' => $arr['register_1']['agama'],
      'tempat_asal' => $arr['register_1']['tempat_asal'],
      'warga_negara' => $arr['register_1']['kewarganegaraan'],
      'alamat_ktp' => $arr['register_1']['alamat_ktp'],
      'tempat_tinggal' => $arr['register_1']['alamat_sekarang'],
      'no_tel' => $arr['register_1']['nomor_telepon'],
      'no_hp' => $arr['register_1']['nomor_handphone'],
      'no_hp1' => $arr['register_1']['nomor_handphone_tambahan'],
      'no_sepatu' => $arr['register_1']['ukuran_sepatu'],
      'lingkar_pinggang' => $arr['register_1']['lingkar_pinggang'],
      'bb' => $arr['register_1']['berat_badan'],
      'tb' => $arr['register_1']['tinggi_badan'],
      'suka_pelajaran' => $arr['register_1']['pelajaran_favorit'],
      'hobi' => $arr['register_1']['hobi'],
      'skil' => $arr['register_1']['keterampilan'],
      'peng_keluarnegri' => empty($arr['register_1']['riwayat_keluar_negri']) ? "b" : "p",
      'pasport'          => $arr['register_1']['passport'],
      'tgldaftar' => Date('Y-m-d')
    ));

    //INSERT KELUAR NEGRI
    if($arr['register_1']['pernah_keluar_negri'] == 'p')
    {
      if(!empty($arr['register_1']['negara_pernah_dikunjungi']))
      {
        foreach ($arr['register_1']['negara_pernah_dikunjungi'] as $v) {
          $this->db->insert('tb_riwayat_keluarnegri', array(
            'kd_pemagang' => $kd_pemagang,
            'Negara'      => $v['negara'],
            'tahun'       => $v['tahun'],
            'bulan'       => $v['bulan'],
            'tujuan'      => $v['tujuan'],
          ));
        }
      }
    }

    //INSERT PASSPORT
    if($arr['register_1']['passport'] == 'Memiliki')
    {
      $this->db->insert('tb_pasport', array(
        'kd_pemagang'   => $kd_pemagang,
        'no_pass'       => $arr['register_1']['no_passport'],
        'masa_berlaku'  => $arr['register_1']['tanggal_berlaku_passport'],
        'terbit'        => $arr['register_1']['tanggal_penerbitan_passport'],
      ));
    }

    $this->db->insert('tb_riwayat_penyakit', array(
      'nik' => $kd_pemagang,
      'nama_penyakit' => $arr['register_1']['nama_penyakit'],
      'bulan' => $arr['register_1']['bulan_penyakit'],
      'tahun' => $arr['register_1']['tahun_penyakit']
    ));

    $merokok = 0;
    if($arr['register_1']['merokok'] == 'YA')
    {
      $merokok = 2;
    }
    else if($arr['register_1']['merokok'] == 'BERHENTI')
    {
      $merokok = 1;
    }
    else
    {
      $merokok = 0;
    }

    if($merokok == 2) //JIKA MEROKOK
    {
      $this->db->insert('tb_detail_kebiasaan', array(
        'kd_pemagang' => $kd_pemagang,
        'kd_kebiasaan'=> 1, //tb_kebiasaan 1 = merokok
        'status'      => $merokok,
        'hari'        => $arr['register_1']['batang_rokok_per_hari']
      ));
    }

    $minum_alkohol = 0;
    if($arr['register_1']['minum_alkohol'] == 'YA')
    {
      $minum_alkohol = 2;
    }
    else if($arr['register_1']['minum_alkohol'] == 'BERHENTI')
    {
      $minum_alkohol = 1;
    }
    else
    {
      $minum_alkohol = 0;
    }

    if($minum_alkohol == 2) //JIKA MINUM ALKOHOL
    {
      $this->db->insert('tb_detail_kebiasaan', array(
        'kd_pemagang' => $kd_pemagang,
        'kd_kebiasaan'=> 2, //tb_kebiasaan 2 = MINUM ALKOHOL
        'status'      => $minum_alkohol,
        'bulan'       => $arr['register_1']['frekuensi_minum_alkohol_perbulan']
      ));
    }

    //REGISTER 2 = Bahasa
    if(!empty($arr['register_2']['bahasa']))
    {
      foreach ($arr['register_2']['bahasa'] as $bahasa) {
        $this->db->insert('tb_kemahiran', array(
          'kd_pengajar' => $kd_pemagang,
          'kemahiran'   => $bahasa
        ));
      }
    }

    //REGISTER 3 = RIWAYAT PEKERJAAN
    if(!empty($arr['register_3']))
    {
      foreach ($arr['register_3'] as $riwayat) {
        $this->db->insert('tb_riwayat_pekerjaan', array(
          'nik'         => $kd_pemagang,
          'perusahaan'  => $riwayat['nama_perusahaan'],
          'posisi'      => $riwayat['kd_pekerjaan'],
          'bulan_masuk' => $riwayat['bulan_mulai'],
          'tahun_masuk' => $riwayat['tahun_mulai'],
          'bulan_keluar' => $riwayat['bulan_selesai'],
          'tahun_keluar' => $riwayat['tahun_selesai']
        ));
      }
    }

    //REGISTER 4 = RIWAYAT PENDIDIKAN
    if(!empty($arr['register_4']))
    {
      foreach ($arr['register_4'] as $key => $pendidikan)
      {
        if($key != 'sekolah_lanjutan' && $key != 'belajar_bahasa_jepang')
        {
          $this->db->insert('dt_riwayat_pendidikan', array(
            'kd_pemagang'   => $kd_pemagang,
            'instansi'      => $pendidikan['nama_sekolah'],
            'id_pendidikan' => $key,
            'mulai'         => $pendidikan['tahun_masuk'],
            'selesai'       => $pendidikan['tahun_selesai'],
            'id_jurusan'       => empty($pendidikan['jurusan']) ? "" : $pendidikan['jurusan']
          ));
        }
      }

      if(!empty($arr['register_4']['sekolah_lanjutan']))
      {
        foreach ($arr['register_4']['sekolah_lanjutan'] as $key => $pendidikan)
        {
          $this->db->insert('dt_riwayat_pendidikan', array(
            'kd_pemagang'   => $kd_pemagang,
            'instansi'      => $pendidikan['nama_instansi'],
            'id_pendidikan' => $pendidikan['tingkatan'],
            'mulai'   => $pendidikan['tahun_masuk'],
            'selesai' => $pendidikan['tahun_selesai'],
            'id_jurusan'       => $pendidikan['jurusan']
          ));
        }
      }
      // BELAJAR BAHASA JEPANG
      if(!empty($arr['register_4']['belajar_bahasa_jepang']))
      {
        foreach ($arr['register_4']['belajar_bahasa_jepang'] as $jepang)
        {
          $this->db->insert('dt_riwayat_pendidikan', array(
            'kd_pemagang'   => $kd_pemagang,
            'instansi'      => $jepang['nama_instansi'],
            'mulai'   => $jepang['tahun_masuk'],
            'selesai' => $jepang['tahun_selesai'],
            'id_jurusan' => 'JEPANG'
          ));
        }
      }
    }

    //REGISTER 5 = KELUARGA
    if(!empty($arr['register_5']))
    {
      //INSERT AYAH ID_HUBUNGAN = 2
      $this->db->insert('tb_keluarga_pem', array(
        'kd_pemagang'   => $kd_pemagang,
        'nama'          => $arr['register_5']['ayah']['nama'],
        'hubungan'      => 2,
        'tempat_lahir'  => $arr['register_5']['ayah']['tempat_lahir'],
        'pekerjaan'     => $arr['register_5']['ayah']['pekerjaan'],
        'tanggal_lahir' => $arr['register_5']['ayah']['tanggal_lahir'],
        'status'        => '',
        'hp'            => $arr['register_5']['ayah']['no_telp']
      ));

      //INSERT IBU ID_HUBUNGAN = 1
      $this->db->insert('tb_keluarga_pem', array(
        'kd_pemagang'   => $kd_pemagang,
        'nama'          => $arr['register_5']['ibu']['nama'],
        'hubungan'      => 1,
        'tempat_lahir'  => $arr['register_5']['ibu']['tempat_lahir'],
        'pekerjaan'     => $arr['register_5']['ibu']['pekerjaan'],
        'tanggal_lahir' => $arr['register_5']['ibu']['tanggal_lahir'],
        'status'        => '',
        'hp'            => $arr['register_5']['ibu']['no_telp']
      ));

      //SAUDARA KANDUNG
      if(!empty($arr['register_5']['saudara_kandung']))
      {
        foreach ($arr['register_5']['saudara_kandung'] as $saudara) {
          $this->db->insert('tb_keluarga_pem', array(
            'kd_pemagang'   => $kd_pemagang,
            'nama'          => $saudara['nama'],
            'hubungan'      => $saudara['hubungan'],
            'tempat_lahir'  => $saudara['tempat_lahir'],
            'pekerjaan'     => $saudara['pekerjaan'],
            'tanggal_lahir' => $saudara['tanggal_lahir'],
            'status'        => '',
            'hp'            => $saudara['no_telp']
          ));
        }
      }

      //KELUARGA PRIBADI
      if(!empty($arr['register_5']['keluarga_pribadi']))
      {
        foreach ($arr['register_5']['keluarga_pribadi'] as $kp) {
          $this->db->insert('tb_keluarga', array(
            'nik'           => $kd_pemagang,
            'nama_kel'      => $kp['nama'],
            'kd_stat_kel'   => $kp['hubungan'],
            'tempat'        => $kp['tempat_lahir'],
            'pekerjaan'     => $kp['pekerjaan'],
            'tgl'           => $kp['tanggal_lahir'],
            'no_telp'       => $kp['no_telp']
          ));
        }
      }

      //KELUARGA DI JEPANG
      if(!empty($arr['register_5']['keluarga_dijepang']))
      {
        foreach ($arr['register_5']['keluarga_dijepang'] as $k) {
          $this->db->insert('keluarga_pemagang_dijepang', array(
            'kd_pemagang'     => $kd_pemagang,
            'nama'            => $k['nama'],
            'kd_hubungan'     => $k['hubungan'],
            'kewarganegaraan' => $k['kewarganegaraan'],
            'instansi'        => $k['perusahaan'],
            'tangal_lahir'    => $k['tanggal_lahir'],
            'visa'            => $k['status_visa']
          ));
        }
      }
    }

    return $this->db->trans_complete();
  }

  public function doLogin($email, $password)
  {
    $this->db->select('kode');
    $this->db->from('tb_pemagangan');
    $this->db->where('email', $email);
    $q = $this->db->get();

    $pass = $q->row();
    if(md5($password) == $pass->kode)
    {
      $this->load->library('session');
      $this->session->set_userdata('member', $email);
      redirect('member/');
    }

    redirect('magang');
  }

  public function forget_password()
  {
    $email = htmlspecialchars($this->input->post('email'));
    $key1 = md5(Date('Ymd').$email);
    $key2 = urlencode(base64_encode(urlencode($email)));

    echo $key1;
    echo "\n";
    echo $key2;

    $q = $this->db->get_where('tb_pemagangan', array(
      'email' => $email
    ));
    $row = $q->row();

    if (!empty($row)) {
      $config = array(
        'protocol' => 'mail',
        'mailpath' => '/usr/sbin/sendmail',
        'smtp_host' => 'mail.minori.co.id',
        'smtp_port' => 465,
        'smtp_user' => 'noreply@minori.co.id', // change it to yours
        'smtp_pass' => 'Minor2017', // change it to yours
        'mailtype' => 'html',
        'charset' => 'iso-8859-1',
        'wordwrap' => TRUE,
        'validate' => TRUE
      );
      $this->load->library('email');
      $this->email->initialize($config);
      $this->email->set_newline("\r\n");
      $this->email->from('noreply@minori.co.id', 'MINORI');
      $this->email->to($email);
      $this->email->subject('Reset Password');
      $this->email->message("Click this link to reset password <a href=" . site_url('magang/reset_password/'). $key1 . '/' . $key2 . ">Reset Password</a>");
      $flag = $this->email->send();

      if($flag)
      {
        return 'E-mail reset password telah dikirim ke alamat email ' . $email;
      }
      else
      {
        return 'Terjadi kesalahan saat mengirim email ke ';//. $this->email->print_debugger();
      }
    }
    else
    {
      return 'Email tersebut tidak terdaftar';
    }
  }

  public function reset_password()
  {
    $salt = Date('Ymd');
    $key1 = $this->input->post('key1');
    $key2 = $this->input->post('key2');

    $email = urldecode(base64_decode(urldecode($key2)));

    $q = $this->db->get_where('tb_pemagangan', array(
      'email' => $email
    ));
    $pemagang = $q->row();
    if(empty($pemagang))
    {
      return 'Email tersebut tidak terdaftar';
    }
    else
    {
      $hashed_email = md5($salt.$pemagang->email);
      if($key1 == $hashed_email)
      {
        $this->db->set('kode', md5($this->input->post('new_password')));
        return "sukses";
      }
      else
      {
        return 'Gagal';
      }
    }
  }
}


?>
