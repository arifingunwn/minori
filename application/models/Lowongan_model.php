<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * Model Jenis Pekerjaan
 */

class Lowongan_model extends CI_Model {

    public function __construct()
    {
        parent::__construct();
        $this->load->database();
    }

    public function add_lowongan($array)
    {
      $query = "
        INSERT INTO tb_lowongan
            SELECT CONCAT( 'LM',LPAD(CAST(SUBSTRING(MAX(kd), 3, 10) AS UNSIGNED) + 1, 10, '0')),
            '{$array['kategori_lowongan']}',
            '{$array['judul_lowongan']}',
            '{$array['descripsi_lowongan']}',
            '{$array['awal_lowongan']}',
            '{$array['akhir_lowongan']}',
            'T' FROM tb_lowongan
      ";

      return $this->db->query($query);
    }

    public function update_lowongan($array)
    {
      $this->db->where(array('kd' => $array['kd_lowongan']));
      return $this->db->update('tb_lowongan', array(
        'kd_kategori' => $array['kategori_lowongan'],
        'judul' => $array['judul_lowongan'],
        'text' => $array['descripsi_lowongan'],
        'awal' => $array['awal_lowongan'],
        'akhir' => $array['akhir_lowongan']
      ));
    }
}
