<?php

/**
 * Model Magang
 */
class Rcr_model extends CI_Model
{

  function __construct()
  {
    parent::__construct();
    $this->load->database();
  }

  public function add_new_user($array)
  {
    $user = array (
      'username' => $array['username'],
      'password' => $this->hash_password($array['password'])
    );
    return $this->db->insert('ms_user', $user);
  }

  public function update_user()
  {
    $this->db->set(array(
      'password' => $this->hash_password($this->input->post('password'))
    ));
    $this->db->where('username', $this->input->post('username'));
    return $this->db->update('ms_user');
  }

  public function add_recruit($array)
  {
    $this->db->trans_start();
    $this->db->delete('tb_pendaftar', array('id_lowongan' => $array['kd_lowongan']));

    foreach ($array['kd_pemagang'] as $val) {
      $this->db->insert('tb_pendaftar', array(
        'id_pemagang' => $val,
        'id_lowongan' => $array['kd_lowongan']
      ));
    }
    $this->db->trans_complete();
  }

  public function add_test($array)
  {
    $this->db->trans_start();
    $this->db->delete('tb_test_pendaftar', array('kd_lowongan' => $array['kd_lowongan']));

    foreach ($array['kd_pemagang'] as $val) {
      $this->db->insert('tb_test_pendaftar', array(
        'kd_pemagang' => $val,
        'kd_lowongan' => $array['kd_lowongan']
      ));
    }
    $this->db->trans_complete();
  }

  public function set_nilai_test_pendaftar($array)
  {
    $this->db->set('nilai', $array['nilai']);
    $this->db->set('keterangan', $array['keterangan']);
    $this->db->where(array(
      'kd_pemagang' => $array['kd_pemagang'],
      'kd_lowongan' => $array['kd_lowongan']
    ));
    $this->db->update('tb_test_pendaftar');
  }

  public function add_test_internal($array)
  {
    $this->db->trans_start();

    if(empty($array['kd_detail_nilai']))
    {
      $this->db->insert('tb_detail_nilai', array(
        'kd_pemagang' => $array['kd_pemagang'],
        'tgl_test' => $array['tgl_test'],
        'kd_asosiasi' => 0,
        'kd_perusahaan' => 0
      ));

      $id_detail_nilai = $this->db->insert_id();

      if(!empty($array['test']))
      {
        foreach ($array['test'] as $key => $value) {
          $this->db->insert('tb_nilai', array(
            'kd_detail_nilai' => $id_detail_nilai,
            'kd_tes' => key($value),
            'nilai' => $value[key($value)]
          ));
        }
      }
    }
    else {
      $this->db->set('tgl_test', $array['tgl_test']);
      $this->db->where('kd_detail_nilai', $array['kd_detail_nilai']);
      $this->db->update('tb_detail_nilai');

      $this->db->delete('tb_nilai', array(
        'kd_detail_nilai' => $array['kd_detail_nilai']
      ));

      foreach ($array['test'] as $key => $value) {
        $this->db->insert('tb_nilai', array(
          'kd_detail_nilai' => $array['kd_detail_nilai'],
          'kd_tes' => key($value),
          'nilai' => $value[key($value)]
        ));
      }
    }


    return $this->db->trans_complete();
  }

  public function save_import_test_internal($arr)
  {
    $this->db->trans_start();
    foreach ($arr as $post_key => $post_value) {

      $this->db->query("
        DELETE N FROM tb_nilai N
        INNER JOIN tb_detail_nilai DN ON N.kd_detail_nilai = DN.kd_detail_nilai
        WHERE DN.tgl_test = ? AND DN.kd_pemagang = ?
      ", array($post_value['tgl_test'], $post_value['kd_pemagang']));

      $this->db->delete('tb_detail_nilai', array(
      	'tgl_test'      => $post_value['tgl_test'],
        'kd_pemagang'   => $post_value['kd_pemagang']
      ));

      //INSERT DETAIL NILAI (Header)
      $this->db->insert('tb_detail_nilai', array(
      	'kd_pemagang'    => $post_value['kd_pemagang'],
      	'tgl_test'       => $post_value['tgl_test'],
      	'kd_asosiasi'    => 0,
      	'kd_perusahaan'  => 0
      ));

      $id_detail_nilai = $this->db->insert_id();

      foreach ($post_value['test'] as $k => $v) {
    		$this->db->insert('tb_nilai', array(
    			'kd_detail_nilai' => $id_detail_nilai,
    			'kd_tes' => $k,
    			'nilai' => $v
    		));
      }

      $data_pemagang = array(
        'bb' => $post_value['berat_badan'],
        'tb' => $post_value['tinggi_badan'],
        'lingkar_pinggang' => $post_value['lingkar_pinggang'],
        'no_sepatu' => $post_value['no_sepatu']
      );
      $this->db->set($data_pemagang);
      $this->db->where('kd_pemagang', $post_value['kd_pemagang']);
      $this->db->update('tb_pemagangan');
    }
    $trans = $this->db->trans_complete();
    return $trans;
  }

  public function get_pemagang_monthly_statistic()
  {
    $q = $this->db->query("SELECT COUNT(kd_pemagang) as jumlah FROM tb_pemagangan WHERE YEAR(tgldaftar) = YEAR(NOW()) AND MONTH(tgldaftar) = MONTH(NOW()) GROUP BY MONTH(tgldaftar), YEAR(tgldaftar)");
    return $q->row();
  }

  public function get_pemagang_daily_statistic()
  {
    $q = $this->db->query("SELECT COUNT(kd_pemagang) as jumlah FROM tb_pemagangan WHERE YEAR(tgldaftar) = YEAR(NOW()) AND MONTH(tgldaftar) = MONTH(NOW()) AND DAY(tgldaftar) = DAY(NOW()) GROUP BY CAST(tgldaftar as DATE)");
  }

  private function hash_password($password)
  {
    $options = [
    'cost' => 12,
    ];
    return password_hash($password, PASSWORD_BCRYPT, $options);
  }
}


?>
