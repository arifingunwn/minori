<?php
defined('BASEPATH') OR exit('No direct script access allowed');


class Auth_model extends CI_Model {
	function __construct()
  {
    parent::__construct();
    $this->load->database();
  }

	public function doLogin($username, $password)
	{
		$query = $this->db->get_where('ms_user', array('username' => $username));
		$data = $query->row();

		if(!empty($data))
		{
			$hash = $data->password;
			$valid = password_verify($password, $hash);

			if($valid)
			{
				$this->db->select('R.nama_role');
				$this->db->from('ms_user U');
				$this->db->join('dt_roles DR', 'U.username = DR.username');
				$this->db->join('ms_roles R', 'DR.id_role = R.id_role');
				$q = $this->db->get();

				$roles = $q->result_array();

				$this->session->set_userdata('user', $data->username);
				$this->session->set_userdata('roles', $roles);
				return true;
			}
		}

		else{
			return false;
		}
	}
}
