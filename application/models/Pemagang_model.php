<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * Model Pemagang
 */

class Pemagang_model extends CI_Model {
    private $table = 'tb_pemagangan';
    private $column_order = array('kd_pemagang', 'nama_pem', 'provinsi', 'umur', 'tinggi_badan', 'berat_badan', 'email', 'pendidikan_terakhir', 'jk', 'pengalaman_kerja', 'pernah_ke_jepang');
    private $column_search = array('P.kd_pemagang', 'nama_pem', 'provinsi', 'email', 'no_hp');
    private $order = array('tgldaftar', 'asc');

    public function __construct()
    {
        parent::__construct();
        $this->load->database();
    }

    public function upload_berkas()
    {
      $config['upload_path']          = $_SERVER['DOCUMENT_ROOT'].'/minori_beta/data/berkas';
      $config['allowed_types']        = 'gif|jpg|jpeg|png|pdf|xls|xlsx|doc|docx';
      $config['max_size']             = 5120;
      $config['max_width']            = 0;
      $config['max_height']           = 0;

      $this->load->library('upload', $config);

      if ( ! $this->upload->do_upload('berkas_file'))
      {
        echo $this->upload->display_errors();
      }
      else
      {
        $q = $this->db->insert('tb_berkas', array(
          'kd_pemagang' => $this->input->post('kd_pemagang'),
          'nama_berkas' => $this->input->post('nama_berkas'),
          'berkas' => $this->upload->file_name,
          'ket' => $this->input->post('keterangan'),
          'tgl_upload' => Date('Y-m-d')
        ));
        if($q)
        {
          return "sukses";
        }
        else
        {
            return "Gagal insert database";
        }
      }
    }

    public function upload_test_external()
    {
      $config['upload_path']          = $_SERVER['DOCUMENT_ROOT'].'/minori_beta/data/test_external';
      $config['allowed_types']        = 'gif|jpg|jpeg|png|pdf|xls|xlsx|doc|docx';
      $config['max_size']             = 5120;
      $config['max_width']            = 0;
      $config['max_height']           = 0;

      $this->load->library('upload', $config);

      if(!empty($_FILES['file_test_external']['name']))
      {
        if ( ! $this->upload->do_upload('file_test_external'))
        {
          echo 'Gagal Upload' . $this->upload->display_errors();
        }
        else
        {
          $q = $this->db->insert('test_external', array(
            'kd_pemagang' => $this->input->post('kd_pemagang'),
            'tgl_seleksi' => $this->input->post('tgl_test'),
            'kd_asosiasi' => $this->input->post('kd_asosiasi'),
            'kd_perusahaan' => $this->input->post('kd_perusahaan'),
            'kd_bidang_usaha' => $this->input->post('kd_bidang_usaha'),
            'file' => $this->upload->file_name,
            'hasil' => $this->input->post('hasil_test')
          ));
        }
      }

      else
      {
        $q = $this->db->insert('test_external', array(
          'kd_pemagang' => $this->input->post('kd_pemagang'),
          'tgl_seleksi' => $this->input->post('tgl_test'),
          'kd_asosiasi' => $this->input->post('kd_asosiasi'),
          'kd_perusahaan' => $this->input->post('kd_perusahaan'),
          'kd_bidang_usaha' => $this->input->post('kd_bidang_usaha'),
          'hasil' => $this->input->post('hasil_test')
        ));
      }

      if($q)
      {
        return "sukses";
      }
      else
      {
          return "Gagal insert database";
      }
    }

    public function add_blacklist($kd_pemagang)
    {
      var_dump($_POST);
      $tanggal = $this->input->post('tanggal');
      $alasan = $this->input->post('alasan');
      $this->db->insert('blacklist', array(
        'kd_pemagang' => $kd_pemagang,
        'alasan' => $alasan,
        'tgl' => $tanggal
      ));
    }

    public function update_member_biodata()
    {
      $this->db->set(array(
        'nama_pem' => $this->input->post('nama_pem'),
        'email' => $this->input->post('email'),
        'tanggal_lahir' => $this->input->post('tahun_lahir') . '-' . $this->input->post('bulan_lahir') . '-' .$this->input->post('tanggal_lahir'),
        'tempat_lahir' => $this->input->post('tempat_lahir'),
        'tempat_tinggal' => $this->input->post('tempat_tinggal'),
        'kd_provinsi' => $this->input->post('kd_provinsi'),
        'no_tel' => $this->input->post('no_tel'),
        'no_hp' => $this->input->post('no_hp'),
        'no_hp1' => $this->input->post('no_hp1'),
        'no_hp2' => $this->input->post('no_hp2'),
        'suka_pelajaran' => $this->input->post('suka_pelajaran'),
        'hobi' => $this->input->post('hobi'),
        'skil' => $this->input->post('skil'),
        'warga_negara' => $this->input->post('warga_negara'),
        'tgl_rev' => Date('Y-m-d')
      ));
      $this->db->where('kd_pemagang', $this->input->post('kd_pemagang'));
      $this->db->update('tb_pemagangan');
    }

    public function update_member_passport()
    {
      $kd_pemagang = $this->input->post('kd_pemagang');
      $this->db->where('kd_pemagang', $kd_pemagang);
      $this->db->from('tb_pasport');
      $count = $this->db->count_all_results();

      if($count > 0)
      {
        $this->db->set(array(
          'no_pass'       => $this->input->post('no_pass'),
          'masa_berlaku'  => $this->input->post('masa_berlaku'),
          'terbit'        => $this->input->post('terbit')
        ));
        $this->db->where('kd_pemagang', $kd_pemagang);
        $this->db->update('tb_pasport');
      }
      else
      {
        $this->db->insert('tb_pasport', array(
          'kd_pemagang'   => $kd_pemagang,
          'no_pass'       => $this->input->post('no_pass'),
          'masa_berlaku'  => $this->input->post('masa_berlaku'),
          'terbit'        => $this->input->post('terbit')
        ));
      }
    }

    public function update_member_password()
    {
      $this->db->set(array(
        'kode' => md5($this->input->post('password'))
      ));
      $this->db->where(array(
        'kd_pemagang' => $this->input->post('kd_pemagang')
      ));
      $this->db->update('tb_pemagangan');
    }

    private function get_datatables_query()
    {
      $column = array(
        'nama_pem',
        'PROV.provinsi',
        'P.kd_pemagang',
        'P.tanggal_lahir',
        'P.tb as tinggi_badan',
        'P.bb as berat_badan',
        'P.email',
        'RPEND.id_pendidikan',
        'JP.nama_jurusan',
        'jk',
        'no_hp as nomor_handphone'  ,
        'IFNULL(RKN.kd, 0) as pernah_ke_jepang'
      );

      if (!empty($_POST['pengalaman_kerja']) && $_POST['pengalaman_kerja'] != 'TIDAK') {
        array_push($column, 'GROUP_CONCAT(DISTINCT CONCAT(\'-\', PEKERJAAN.pekerjaan) SEPARATOR \',<br/>\') as pengalaman_kerja');
      }
      else {
        array_push($column, '\'\' as pengalaman_kerja');
      }

      $this->db->distinct();


      $this->db->select($column);
      $this->db->from($this->table . ' P');

      $this->db->join('tb_provinsi PROV', 'P.kd_provinsi = PROV.kd_provinsi', 'left');

      if($_POST['bisa_bahasa_jepang'] == 'true')
      {
        $this->db->join('tb_kemahiran KEMAHIRAN', 'P.kd_pemagang = KEMAHIRAN.kd_pengajar AND KEMAHIRAN.kemahiran LIKE \'%jepang%\'');
      }

      if($_POST['pernah_ke_jepang'] == 'true')
      {
        $this->db->join('tb_riwayat_keluarnegri RKN', 'P.kd_pemagang = RKN.kd_pemagang AND RKN.negara = \'jepang\'');
      }
      else
      {
        $this->db->join('tb_riwayat_keluarnegri RKN', 'P.kd_pemagang = RKN.kd_pemagang AND RKN.negara = \'jepang\'', 'left');
      }

      // PENDIDIKAN TERAKHIR
      $this->db->join('dt_riwayat_pendidikan RPEND', 'RPEND.kd_pemagang = P.kd_pemagang AND RPEND.urutan = (SELECT MAX(urutan) FROM dt_riwayat_pendidikan WHERE kd_pemagang = P.kd_pemagang)', 'left');
      $this->db->join('ms_jurusan_pendidikan JP', 'RPEND.id_jurusan = JP.id_jurusan', 'left');

      //PENGALAMAN KERJA
      if (!empty($_POST['pengalaman_kerja']) && $_POST['pengalaman_kerja'] != 'TIDAK') {
        $this->db->join('tb_riwayat_pekerjaan RW', 'RW.nik = P.kd_pemagang', 'left');
        $this->db->join('tb_pekerjaan PEKERJAAN', 'RW.posisi = PEKERJAAN.kd_pekerjaan', 'left');
      }

      $this->db->group_by(array(
        'P.kd_pemagang',
      ));

      $i = 0;

      // foreach ($this->column_search as $val) {
      //   if($_POST['search']['value'])
      //   {
      //     if ($i === 0) {
      //       $this->db->group_start();
      //       $this->db->like($val, $_POST['search']['value']);
      //     }
      //     else{
      //       $this->db->or_like($val, $_POST['search']['value']);
      //     }
      //
      //     if(count($this->column_search) - 1 == $i)
      //     {
      //       $this->db->group_end();
      //     }
      //   }
      //
      //   $i++;
      // }

      if($_POST['jenis_kelamin'] != 'ALL')
      {
        $this->db->where(array('P.jk' => $_POST['jenis_kelamin'] == 'L' ? 'L' : 'P'));
      }

      if($_POST['pendidikan'] != 'TIDAK')
      {
        $tingkat = $this->input->post('pendidikan');
        $this->db->where('RPEND.urutan >= ' . $tingkat);
      }

      if($_POST['pengalaman_kerja'] != 'TIDAK')
      {
        $pengalaman_kerja = $this->input->post('pengalaman_kerja');
        $this->db->where('RW.posisi', $pengalaman_kerja);
      }

      // if(!empty($_POST['pengalaman_kerja_custom']))
      // {
      //   $pengalaman_kerja_custom = $this->input->post('pengalaman_kerja_custom');
      //   $this->db->like('RW.posisi', $pengalaman_kerja_custom);
      // }

      if($_POST['kode_pemagang'] != '')
      {
        $this->db->like('P.kd_pemagang', $this->input->post('kode_pemagang'));
      }

      if($_POST['nama'] != '')
      {
        $this->db->like('P.nama_pem', $this->input->post('nama'));
      }

      if($_POST['provinsi'] != 'TIDAK')
      {
        $this->db->where('P.kd_provinsi', $this->input->post('provinsi'));
      }

      if($_POST['umur'] != '')
      {
        $this->db->where('(YEAR(NOW()) - YEAR(P.tanggal_lahir)) = ', intval($this->input->post('umur')));
      }

      if($_POST['email'] != '')
      {
        $this->db->where('P.email', $this->input->post('email'));
      }

      if($_POST['no_handphone'] != '')
      {
        $this->db->where('P.no_hp', $this->input->post('no_handphone'));
      }

      //UNTUK recruit
      if($_POST['recruit'] == '1')
      {
        $this->db->where_not_in('P.kd_pemagang', '(SELECT id_pemagang FROM tb_pendaftar)', FALSE);
      }

      //UNTUK TEST
      if(!empty($_POST['lowongan']))
      {
        $this->db->where_in('P.kd_pemagang', '(SELECT id_pemagang FROM tb_pendaftar WHERE id_lowongan = \'' . $_POST['lowongan'] . '\')', FALSE);
      }

      //EXCLUDE pemagang blacklist
      $this->db->where_not_in('P.kd_pemagang', '(SELECT kd_pemagang FROM blacklist)', FALSE);

      //EXCLUDE pemagang lulus
      $this->db->where_not_in('P.kd_pemagang', '(SELECT kd_pemagang FROM lulus)', FALSE);

      //Rentang daftar
      if(isset($_POST['rentang']))
      {
        if($_POST['rentang'] == 'BULAN_INI')
        {
          $this->db->where('YEAR(tgldaftar) = YEAR(NOW()) AND MONTH(tgldaftar) = MONTH(NOW())', NULL, FALSE);
        }

        if($_POST['rentang'] == 'HARI_INI')
        {
          $this->db->where('WHERE YEAR(tgldaftar) = YEAR(NOW()) AND MONTH(tgldaftar) = MONTH(NOW()) AND DAY(tgldaftar) = DAY(NOW())', NULL, FALSE);
        }
      }

      //ORDERING PROCESSING
      if(isset($_POST['order']))
      {
        //JIKA KOLOM KEEMPAT (UMUR)
        if($_POST['order']['0']['column'] == 4)
        {
          $this->db->order_by('YEAR(NOW()) - YEAR(P.tanggal_lahir)', $_POST['order']['0']['dir']);
        }
        else {
            $this->db->order_by($this->column_order[($_POST['order']['0']['column'] - 1)], $_POST['order']['0']['dir']);
        }
      }
      else if (isset($this->order))
      {
        $order = $this->order;
        $this->db->order_by(key($order), $order[key($order)]);
      }
    }

    public function get_datatables()
    {
      $this->get_datatables_query();
      if($_POST['length'] != -1)
      {
        $data_with_pengalaman_kerja = array();
        $this->db->limit($_POST['length'], $_POST['start']);
        $query = $this->db->get()->result_array();
        if(!empty($query) && !empty($_POST['pengalaman_kerja']) && $_POST['pengalaman_kerja'] != 'TIDAK')
        // if(true)
        {
          foreach ($query as $val) {
            $this->db->select('GROUP_CONCAT(DISTINCT CONCAT(\'-\', PEKERJAAN.pekerjaan) SEPARATOR \',<br/>\') as pengalaman_kerja');
            $this->db->from('tb_riwayat_pekerjaan RW');
            $this->db->join('tb_pekerjaan PEKERJAAN', 'RW.posisi = PEKERJAAN.kd_pekerjaan', 'left');
            $this->db->where('RW.nik', $val['kd_pemagang']);
            $this->db->group_by('RW.nik');
            $rw = $this->db->get()->row();
            if(!empty($rw))
            {
              $val['pengalaman_kerja'] = $rw->pengalaman_kerja;
            }
            array_push($data_with_pengalaman_kerja, $val);
          }
          return $data_with_pengalaman_kerja;
        }
        else
        {
          return $query;
        }
      }
    }

    function count_filtered()
    {
        $this->get_datatables_query();
        $query = $this->db->get();
        return $query->num_rows();
    }

    public function count_all()
    {
        $this->db->from($this->table);
        return $this->db->count_all_results();
    }



    public function export_to_excel($kd_pemagang)
    {
      $query = "
        SELECT  P.kd_pemagang as kode_pemagang,
              P.nama_pem as nama,
              P.tb as tinggi_badan,
              P.bb as berat_badan,
              YEAR(NOW()) - YEAR(tanggal_lahir) as usia,
              status,
              alamat_ktp,
              tempat_tinggal,
              PROV.provinsi,
              RP_SMA.instansi as nama_sma,
              JP_SMA.nama_jurusan as jurusan_sma,
              RP_PT.instansi as nama_sekolah_tinggi,
              JP_PT.nama_jurusan as jurusan_sekolah_tinggi,
              P.pasport as passport,
              peng_keluarnegri as pernah_keluar_negri,
              GROUP_CONCAT(DISTINCT RW.pekerjaan SEPARATOR ', ') as pengalaman_kerja,
              CONCAT(no_hp, ', ', no_hp1, ', ', no_hp2) as nomor_handphone,
              no_tel as nomor_telepone,
              tgldaftar as tanggal_daftar,
              tanggal_lahir,
              email,
              AGAMA.agama,
              GROUP_CONCAT(DISTINCT CONCAT(T.nama_tes, '=', N.nilai) SEPARATOR ', ') as test_internal
        FROM tb_pemagangan as P
        LEFT JOIN tb_provinsi PROV ON PROV.kd_provinsi = P.kd_provinsi
        LEFT JOIN dt_riwayat_pendidikan RP_SMA ON RP_SMA.kd_pemagang = P.kd_pemagang AND RP_SMA.id_pendidikan = 'SMA'
        LEFT JOIN ms_jurusan_pendidikan JP_SMA ON JP_SMA.id_jurusan = RP_SMA.id_jurusan

        LEFT JOIN dt_riwayat_pendidikan RP_PT ON RP_PT.kd_pemagang = P.kd_pemagang AND (RP_PT.id_pendidikan = 'S1' OR RP_PT.id_pendidikan = 'D3')
        LEFT JOIN ms_jurusan_pendidikan JP_PT ON JP_PT.id_jurusan = RP_PT.id_jurusan

        LEFT JOIN (
          SELECT nik as kd_pemagang, X.pekerjaan FROM tb_pekerjaan X
          INNER JOIN tb_riwayat_pekerjaan Y ON X.kd_pekerjaan = Y.posisi
        ) RW ON RW.kd_pemagang = P.kd_pemagang

        LEFT JOIN tb_agama AGAMA ON AGAMA.kd_agama = P.id_agama
        LEFT JOIN tb_detail_nilai DN ON DN.kd_pemagang = P.kd_pemagang
        LEFT JOIN tb_nilai N ON DN.kd_detail_nilai = N.kd_detail_nilai
        LEFT JOIN tb_tes T ON N.kd_tes = T.kd_tes
        WHERE P.kd_pemagang IN ?
        GROUP BY P.kd_pemagang
      ";
      $q = $this->db->query($query, array($kd_pemagang));
      return $q->result_array();
    }

    public function get_recruited_pemagang($id_lowongan)
    {
      $query = "
        SELECT P.kd_pemagang as kode_pemagang,
              P.nama_pem as nama,
              P.tb as tinggi_badan,
              P.bb as berat_badan,
              YEAR(NOW()) - YEAR(tanggal_lahir) as usia,
              status,
              alamat_ktp,
              tempat_tinggal,
              PROV.provinsi,
              RP_SMA.instansi as nama_sma,
              JP_SMA.nama_jurusan as jurusan_sma,
              RP_PT.instansi as nama_sekolah_tinggi,
              JP_PT.nama_jurusan as jurusan_sekolah_tinggi,
              P.pasport as passport,
              peng_keluarnegri as pernah_keluar_negri,
              GROUP_CONCAT(DISTINCT RW.pekerjaan SEPARATOR ', ') as pengalaman_kerja,
              CONCAT(no_hp, ', ', no_hp1, ', ', no_hp2) as nomor_handphone,
              no_tel as nomor_telepone,
              tgldaftar as tanggal_daftar,
              tanggal_lahir,
              email,
              AGAMA.agama,
              GROUP_CONCAT(DISTINCT CONCAT(T.nama_tes, '=', N.nilai) SEPARATOR ', ') as test_internal,
              foto
        FROM tb_pemagangan as P
        LEFT JOIN tb_provinsi PROV ON PROV.kd_provinsi = P.kd_provinsi
        LEFT JOIN dt_riwayat_pendidikan RP_SMA ON RP_SMA.kd_pemagang = P.kd_pemagang AND RP_SMA.id_pendidikan = 'SMA'
        LEFT JOIN ms_jurusan_pendidikan JP_SMA ON JP_SMA.id_jurusan = RP_SMA.id_jurusan

        LEFT JOIN dt_riwayat_pendidikan RP_PT ON RP_PT.kd_pemagang = P.kd_pemagang AND (RP_PT.id_pendidikan = 'S1' OR RP_PT.id_pendidikan = 'D3')
        LEFT JOIN ms_jurusan_pendidikan JP_PT ON JP_PT.id_jurusan = RP_PT.id_jurusan

        LEFT JOIN (
          SELECT nik as kd_pemagang, X.pekerjaan FROM tb_pekerjaan X
          INNER JOIN tb_riwayat_pekerjaan Y ON X.kd_pekerjaan = Y.posisi
        ) RW ON RW.kd_pemagang = P.kd_pemagang

        LEFT JOIN tb_agama AGAMA ON AGAMA.kd_agama = P.id_agama
        LEFT JOIN tb_detail_nilai DN ON DN.kd_pemagang = P.kd_pemagang
        LEFT JOIN tb_nilai N ON DN.kd_detail_nilai = N.kd_detail_nilai
        LEFT JOIN tb_tes T ON N.kd_tes = T.kd_tes
        WHERE P.kd_pemagang IN (SELECT id_pemagang FROM tb_pendaftar WHERE id_lowongan = ?)
        GROUP BY P.kd_pemagang
      ";
      $q = $this->db->query($query, htmlspecialchars($id_lowongan));
      return $q->result_array();
    }

    public function get_to_be_tested_pemagang($id_lowongan)
    {
      $query = "
        SELECT P.kd_pemagang as kode_pemagang,
              P.nama_pem as nama,
              P.tb as tinggi_badan,
              P.bb as berat_badan,
              YEAR(NOW()) - YEAR(tanggal_lahir) as usia,
              status,
              alamat_ktp,
              tempat_tinggal,
              PROV.provinsi,
              RP_SMA.instansi as nama_sma,
              JP_SMA.nama_jurusan as jurusan_sma,
              RP_PT.instansi as nama_sekolah_tinggi,
              JP_PT.nama_jurusan as jurusan_sekolah_tinggi,
              P.pasport as passport,
              peng_keluarnegri as pernah_keluar_negri,
              GROUP_CONCAT(DISTINCT RW.pekerjaan SEPARATOR ', ') as pengalaman_kerja,
              CONCAT(no_hp, ', ', no_hp1, ', ', no_hp2) as nomor_handphone,
              no_tel as nomor_telepone,
              tgldaftar as tanggal_daftar,
              tanggal_lahir,
              email,
              AGAMA.agama,
              GROUP_CONCAT(DISTINCT CONCAT(T.nama_tes, '=', N.nilai) SEPARATOR ', ') as test_internal,
              foto
        FROM tb_pemagangan as P
        LEFT JOIN tb_provinsi PROV ON PROV.kd_provinsi = P.kd_provinsi
        LEFT JOIN dt_riwayat_pendidikan RP_SMA ON RP_SMA.kd_pemagang = P.kd_pemagang AND RP_SMA.id_pendidikan = 'SMA'
        LEFT JOIN ms_jurusan_pendidikan JP_SMA ON JP_SMA.id_jurusan = RP_SMA.id_jurusan

        LEFT JOIN dt_riwayat_pendidikan RP_PT ON RP_PT.kd_pemagang = P.kd_pemagang AND (RP_PT.id_pendidikan = 'S1' OR RP_PT.id_pendidikan = 'D3')
        LEFT JOIN ms_jurusan_pendidikan JP_PT ON JP_PT.id_jurusan = RP_PT.id_jurusan

        LEFT JOIN (
          SELECT nik as kd_pemagang, X.pekerjaan FROM tb_pekerjaan X
          INNER JOIN tb_riwayat_pekerjaan Y ON X.kd_pekerjaan = Y.posisi
        ) RW ON RW.kd_pemagang = P.kd_pemagang

        LEFT JOIN tb_agama AGAMA ON AGAMA.kd_agama = P.id_agama
        LEFT JOIN tb_detail_nilai DN ON DN.kd_pemagang = P.kd_pemagang
        LEFT JOIN tb_nilai N ON DN.kd_detail_nilai = N.kd_detail_nilai
        LEFT JOIN tb_tes T ON N.kd_tes = T.kd_tes
        WHERE P.kd_pemagang IN (SELECT kd_pemagang FROM tb_test_pendaftar WHERE kd_lowongan = ?)
        GROUP BY P.kd_pemagang
      ";
      $q = $this->db->query($query, htmlspecialchars($id_lowongan));
      return $q->result_array();
    }

    public function get_hasil_test_pemagang($id_lowongan)
    {
      $query = "
        SELECT P.kd_pemagang as kode_pemagang,
              P.nama_pem as nama,
              P.tb as tinggi_badan,
              P.bb as berat_badan,
              YEAR(NOW()) - YEAR(tanggal_lahir) as usia,
              status,
              alamat_ktp,
              tempat_tinggal,
              PROV.provinsi,
              RP_SMA.instansi as nama_sma,
              JP_SMA.nama_jurusan as jurusan_sma,
              RP_PT.instansi as nama_sekolah_tinggi,
              JP_PT.nama_jurusan as jurusan_sekolah_tinggi,
              P.pasport as passport,
              peng_keluarnegri as pernah_keluar_negri,
              GROUP_CONCAT(DISTINCT RW.pekerjaan SEPARATOR ', ') as pengalaman_kerja,
              CONCAT(no_hp, ', ', no_hp1, ', ', no_hp2) as nomor_handphone,
              no_tel as nomor_telepone,
              tgldaftar as tanggal_daftar,
              tanggal_lahir,
              email,
              AGAMA.agama,
              GROUP_CONCAT(DISTINCT CONCAT(T.nama_tes, '=', N.nilai) SEPARATOR ', ') as test_internal,
              foto
        FROM tb_pemagangan as P
        LEFT JOIN tb_provinsi PROV ON PROV.kd_provinsi = P.kd_provinsi
        LEFT JOIN dt_riwayat_pendidikan RP_SMA ON RP_SMA.kd_pemagang = P.kd_pemagang AND RP_SMA.id_pendidikan = 'SMA'
        LEFT JOIN ms_jurusan_pendidikan JP_SMA ON JP_SMA.id_jurusan = RP_SMA.id_jurusan

        LEFT JOIN dt_riwayat_pendidikan RP_PT ON RP_PT.kd_pemagang = P.kd_pemagang AND (RP_PT.id_pendidikan = 'S1' OR RP_PT.id_pendidikan = 'D3')
        LEFT JOIN ms_jurusan_pendidikan JP_PT ON JP_PT.id_jurusan = RP_PT.id_jurusan

        LEFT JOIN (
          SELECT nik as kd_pemagang, X.pekerjaan FROM tb_pekerjaan X
          INNER JOIN tb_riwayat_pekerjaan Y ON X.kd_pekerjaan = Y.posisi
        ) RW ON RW.kd_pemagang = P.kd_pemagang

        LEFT JOIN tb_agama AGAMA ON AGAMA.kd_agama = P.id_agama
        LEFT JOIN tb_detail_nilai DN ON DN.kd_pemagang = P.kd_pemagang
        LEFT JOIN tb_nilai N ON DN.kd_detail_nilai = N.kd_detail_nilai
        LEFT JOIN tb_tes T ON N.kd_tes = T.kd_tes
        WHERE P.kd_pemagang IN (SELECT kd_pemagang FROM tb_test_pendaftar WHERE kd_lowongan = ?)
        GROUP BY P.kd_pemagang
      ";
      $q = $this->db->query($query, htmlspecialchars($id_lowongan));
      return $q->result_array();
    }
}

?>
