<div class="ui segment very padded container" style="margin-top: 8em">
  <h1 class="ui dividing header"><?php echo $pengumuman->Judul ?></h1>
  <b><i><?php echo $pengumuman->tanggal ?><i></b><br/>
  <div class="ui teal tag label">
    <?php echo $pengumuman->penerbit ?>
  </div>
  <div class="ui padded segment">
    <p>
      <?php echo nl2br($pengumuman->text) ?>
    </p>
  </div>
</div>
