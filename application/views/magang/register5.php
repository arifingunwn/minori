<div class="ui segment very padded container">
  <?php echo form_open('', 'class="ui form"') ?>
    <div class="ui dividing header blue">
      DATA KELUARGA / KERABAT DEKAT
    </div>
    <h3 class="ui horizontal divider header">KELUARGA</h3>
    <h3 class="ui dividing header">DATA AYAH</h3>
    <div class="fields">
      <div class="four wide field">
        <label>NAMA</label>
        <input type="text" name="nama_ayah" placeholder="Nama Ayah" value="<?php echo isset($_SESSION['register_5']['ayah']['nama']) ? $_SESSION['register_5']['ayah']['nama'] : '' ?>">
      </div>
      <div class="three wide field">
        <label>TEMPAT LAHIR</label>
        <input type="text" name="tempat_lahir_ayah" value="<?php echo isset($_SESSION['register_5']['ayah']['tempat_lahir']) ? $_SESSION['register_5']['ayah']['tempat_lahir'] : '' ?>">
      </div>
      <div class="five wide field">
        <label>TANGGAL LAHIR</label>
        <div class="fields">
          <div class="five wide field">
            <select class="ui dropdown" name="tanggal_lahir_ayah">
              <?php
              if(isset($_SESSION['register_5']['ayah']['tanggal_lahir'])){
                $tanggal = strtotime($_SESSION['register_5']['ayah']['tanggal_lahir']);
                echo option_tanggal(intval(Date('d', $tanggal)));
              }
              else{
                echo option_tanggal();
              }
              ?>
            </select>
          </div>
          <div class="seven wide field">
            <select class="ui dropdown" name="bulan_lahir_ayah">
              <?php
              if(isset($_SESSION['register_5']['ayah']['tanggal_lahir'])){
                $tanggal = strtotime($_SESSION['register_5']['ayah']['tanggal_lahir']);
                echo option_bulan(intval(Date('m', $tanggal)));
              }
              else{
                echo option_bulan();
              }
              ?>
            </select>
          </div>
          <div class="seven wide field">
            <select class="ui dropdown" name="tahun_lahir_ayah">
              <?php
              if(isset($_SESSION['register_5']['ayah']['tanggal_lahir'])){
                $tanggal = strtotime($_SESSION['register_5']['ayah']['tanggal_lahir']);
                echo option_tahun(100, intval(Date('Y', $tanggal)));
              }
              else{
                echo option_tahun(100, 1970);
              }
              ?>
            </select>
          </div>
        </div>
      </div>
    </div>
    <div class="fields">
      <div class="four wide field">
        <label>PEKERJAAN</label>
        <select class="ui dropdown search" name="pekerjaan_ayah">
        <?php $temp = ''; $pekerjaan_counter = 0; ?>
        <?php foreach ($pekerjaan as $p): ?>
          <?php echo $pekerjaan_counter == 0 ? "<optgroup label={$p['jenis_pekerjaan']}>" : "" ?>
          <?php if ($p['kd_jenis_pekerjaan'] != $temp): ?>
            </optgroup>
            <optgroup label="<?php echo $p['jenis_pekerjaan'] ?>">
          <?php endif; ?>
              <option <?php echo !empty($_SESSION['register_5']['ayah']['pekerjaan']) && $_SESSION['register_5']['ayah']['pekerjaan'] == $p['kd_pekerjaan'] ? " selected " : "" ?>
                      value="<?php echo $p['kd_pekerjaan'] ?>"><?php echo $p['pekerjaan'] ?></option>
          <?php $temp = $p['kd_jenis_pekerjaan'] ?>
          <?php $pekerjaan_counter++ ?>
        <?php endforeach; ?>
            </optgroup>
        </select>
      </div>
      <div class="field">
        <label>No Telepon / Handphone</label>
        <input type="text" name="no_telp_ayah" placeholder="No. Telp / Handphone Ayah" value="<?php echo isset($_SESSION['register_5']['ayah']['no_telp']) ? $_SESSION['register_5']['ayah']['no_telp'] : '' ?>">
      </div>
    </div>

    <h3 class="ui dividing header">DATA IBU</h3>
    <div class="fields">
      <div class="four wide field">
        <label>NAMA</label>
        <input type="text" name="nama_ibu" placeholder="Nama Ibu" value="<?php echo isset($_SESSION['register_5']['ibu']['nama']) ? $_SESSION['register_5']['ibu']['nama'] : '' ?>">
      </div>
      <div class="three wide field">
        <label>TEMPAT LAHIR</label>
        <input type="text" name="tempat_lahir_ibu" value="<?php echo isset($_SESSION['register_5']['ibu']['tempat_lahir']) ? $_SESSION['register_5']['ibu']['tempat_lahir'] : '' ?>">
      </div>
      <div class="five wide field">
        <label>TANGGAL LAHIR</label>
        <div class="fields">
          <div class="five wide field">
            <select class="ui dropdown" name="tanggal_lahir_ibu">
              <?php
              if(isset($_SESSION['register_5']['ibu']['tanggal_lahir'])){
                $tanggal = strtotime($_SESSION['register_5']['ibu']['tanggal_lahir']);
                echo option_tanggal(intval(Date('d', $tanggal)));
              }
              else{
                echo option_tanggal();
              }
              ?>
            </select>
          </div>
          <div class="seven wide field">
            <select class="ui dropdown" name="bulan_lahir_ibu">
              <option value="" disabled="disabled" >Bulan</option>
              <?php
              if(isset($_SESSION['register_5']['ibu']['tanggal_lahir'])){
                $tanggal = strtotime($_SESSION['register_5']['ibu']['tanggal_lahir']);
                echo option_bulan(intval(Date('m', $tanggal)));
              }
              else{
                echo option_bulan();
              }
              ?>
            </select>
          </div>
          <div class="seven wide field">
            <select class="ui dropdown" name="tahun_lahir_ibu">
              <?php
              if(isset($_SESSION['register_5']['ibu']['tanggal_lahir'])){
                $tanggal = strtotime($_SESSION['register_5']['ibu']['tanggal_lahir']);
                echo option_tahun(100, intval(Date('Y', $tanggal)));
              }
              else{
                echo option_tahun(100, 1969);
              }
              ?>
            </select>
          </div>
        </div>
      </div>
    </div>
    <div class="fields">
      <div class="four wide field">
        <label>PEKERJAAN</label>
        <select class="ui dropdown search" name="pekerjaan_ibu">
        <?php $temp = ''; $pekerjaan_counter = 0; ?>
        <?php foreach ($pekerjaan as $p): ?>
          <?php echo $pekerjaan_counter == 0 ? "<optgroup label={$p['jenis_pekerjaan']}>" : "" ?>
          <?php if ($p['kd_jenis_pekerjaan'] != $temp): ?>
            </optgroup>
            <optgroup label="<?php echo $p['jenis_pekerjaan'] ?>">
          <?php endif; ?>
              <option <?php echo !empty($_SESSION['register_5']['ibu']['pekerjaan']) && $_SESSION['register_5']['ibu']['pekerjaan'] == $p['kd_pekerjaan'] ? " selected " : "" ?>
                      value="<?php echo $p['kd_pekerjaan'] ?>"><?php echo $p['pekerjaan'] ?></option>
          <?php $temp = $p['kd_jenis_pekerjaan'] ?>
          <?php $pekerjaan_counter++ ?>
        <?php endforeach; ?>
            </optgroup>
        </select>
      </div>
      <div class="field">
        <label>No Telepon / Handphone</label>
        <input type="text" name="no_telp_ibu" placeholder="No. Telp / Handphone Ibu" value="<?php echo isset($_SESSION['register_5']['ibu']['no_telp']) ? $_SESSION['register_5']['ibu']['no_telp'] : '' ?>">
      </div>
    </div>

    <div class="dynamic-field-parent">
    <?php if (isset($_SESSION['register_5']['saudara_kandung'])): ?>
    <?php $counter = 0; ?>
    <?php foreach ($_SESSION['register_5']['saudara_kandung'] as $v): ?>
      <div class="dynamic-field-child">
        <h3 class="ui dividing header">SAUDARA KANDUNG <?php echo $counter + 1 ?></h3>
        <div class="fields">
          <div class="four wide field">
            <label>NAMA</label>
            <input type="text" name="saudara_kandung[<?php echo $counter; ?>][nama]" placeholder="Nama Saudara Kandung" value="<?php echo $v['nama'] ?>">
          </div>
          <div class="four wide field">
            <label>TEMPAT LAHIR</label>
            <input type="text" name="saudara_kandung[<?php echo $counter; ?>][tempat_lahir]" placeholder="Kota Lahir" value="<?php echo $v['tempat_lahir'] ?>">
          </div>
          <div class="five wide field">
            <label>TANGGAL LAHIR</label>
            <div class="fields">
              <div class="five wide field">
                <select class="ui dropdown" name="saudara_kandung[<?php echo $counter; ?>][tanggal_lahir]">
                  <?php
                  if(isset($v['tanggal_lahir'])){
                    $tanggal = strtotime($v['tanggal_lahir']);
                    echo option_tanggal(intval(Date('d', $tanggal)));
                  }
                  else{
                    echo option_tanggal();
                  }
                  ?>
                </select>
              </div>
              <div class="seven wide field">
                <select class="ui dropdown" name="saudara_kandung[<?php echo $counter; ?>][bulan_lahir]">
                  <?php
                  if(isset($v['tanggal_lahir'])){
                    $tanggal = strtotime($v['tanggal_lahir']);
                    echo option_bulan(intval(Date('m', $tanggal)));
                  }
                  else{
                    echo option_bulan();
                  }
                  ?>
                </select>
              </div>
              <div class="seven wide field">
                <select class="ui dropdown" name="saudara_kandung[<?php echo $counter; ?>][tahun_lahir]">
                  <?php
                  if(isset($v['tanggal_lahir'])){
                    $tanggal = strtotime($v['tanggal_lahir']);
                    echo option_tahun(100, intval(Date('Y', $tanggal)));
                  }
                  else{
                    echo option_tahun();
                  }
                  ?>
                </select>
              </div>
            </div>
          </div>
        </div>
        <div class="fields">
          <div class="four wide field">
            <label>HUBUNGAN</label>
            <select class="ui dropdown" name="saudara_kandung[<?php echo $counter; ?>][hubungan]">
            <?php foreach ($hubungan as $value): ?>
              <option <?php echo ($value['kd_hubungan'] == $v['hubungan'] ? 'selected' : '') ?> value="<?php echo $value['kd_hubungan'] ?>"><?php echo $value['hubungan'] ?></option>
            <?php endforeach; ?>
            </select>
          </div>
          <div class="four wide field">
            <label>PEKERJAAN</label>
            <select class="ui dropdown search" name="saudara_kandung[<?php echo $counter; ?>][pekerjaan]">
            <?php $temp = ''; $pekerjaan_counter = 0; ?>
            <?php foreach ($pekerjaan as $p): ?>
              <?php echo $pekerjaan_counter == 0 ? "<optgroup label={$p['jenis_pekerjaan']}>" : "" ?>
              <?php if ($p['kd_jenis_pekerjaan'] != $temp): ?>
                </optgroup>
                <optgroup label="<?php echo $p['jenis_pekerjaan'] ?>">
              <?php endif; ?>
                  <option <?php echo $v['pekerjaan'] == $p['kd_pekerjaan'] ? " selected " : "" ?>
                          value="<?php echo $p['kd_pekerjaan'] ?>"><?php echo $p['pekerjaan'] ?></option>
              <?php $temp = $p['kd_jenis_pekerjaan'] ?>
              <?php $pekerjaan_counter++ ?>
            <?php endforeach; ?>
                </optgroup>
            </select>
          </div>
          <div class="four wide field">
            <label>No Telepon / Handphone</label>
            <input type="text" name="saudara_kandung[<?php echo $counter; ?>][no_telepon]" palceholder="No Hp / Telepon" value="<?php echo $v['no_telp']?>">
          </div>
        </div>
      </div>
    <?php $counter++; ?>
    <?php endforeach; ?>
    <?php else: ?>
      <div class="dynamic-field-child">
        <h3 class="ui dividing header">SAUDARA KANDUNG 1</h3>
        <div class="fields">
          <div class="four wide field">
            <label>NAMA</label>
            <input type="text" name="saudara_kandung[0][nama]" placeholder="Nama Saudara Kandung">
          </div>
          <div class="four wide field">
            <label>TEMPAT LAHIR</label>
            <input type="text" name="saudara_kandung[0][tempat_lahir]" placeholder="Kota Lahir">
          </div>
          <div class="five wide field">
            <label>TANGGAL LAHIR</label>
            <div class="fields">
              <div class="five wide field">
                <select class="ui dropdown" name="saudara_kandung[0][tanggal_lahir]">
                <?php for($i = 1; $i <= 31; $i++) {?>
                  <option value="<?php echo $i ?>"><?php echo $i ?></option>
                <?php } ?>
                </select>
              </div>
              <div class="seven wide field">
                <select class="ui dropdown" name="saudara_kandung[0][bulan_lahir]">
                  <option value="" disabled="disabled" >Bulan</option>
                  <option value="1">Januari</option>
                  <option value="2">Februari</option>
                  <option value="3">Maret</option>
                  <option value="4">April</option>
                  <option value="5">Mei</option>
                  <option value="6">Juni</option>
                  <option value="7">Juli</option>
                  <option value="8">Agustus</option>
                  <option value="9">September</option>
                  <option value="10">Oktober</option>
                  <option value="11">November</option>
                  <option value="12">Desember</option>
                </select>
              </div>
              <div class="seven wide field">
                <select class="ui dropdown" name="saudara_kandung[0][tahun_lahir]">
                  <?php
                    $tahun = Date("Y");
                    for($i = $tahun; $i>=$tahun - 100; $i--)
                    {
                      echo "<option value='{$i}'>{$i}</option>";
                    }
                  ?>
                </select>
              </div>
            </div>
          </div>
        </div>
        <div class="fields">
          <div class="four wide field">
            <label>HUBUNGAN</label>
            <select class="ui dropdown" name="saudara_kandung[0][hubungan]">
              <?php foreach ($hubungan as $value): ?>
                <option value="<?php echo $value['kd_hubungan'] ?>"><?php echo $value['hubungan'] ?></option>
              <?php endforeach; ?>
            </select>
          </div>
          <div class="four wide field">
            <label>PEKERJAAN</label>
            <select class="ui dropdown search" name="saudara_kandung[0][pekerjaan]">
            <?php $temp = ''; $pekerjaan_counter = 0; ?>
            <?php foreach ($pekerjaan as $p): ?>
              <?php echo $pekerjaan_counter == 0 ? "<optgroup label={$p['jenis_pekerjaan']}>" : "" ?>
              <?php if ($p['kd_jenis_pekerjaan'] != $temp): ?>
                </optgroup>
                <optgroup label="<?php echo $p['jenis_pekerjaan'] ?>">
              <?php endif; ?>
                  <option value="<?php echo $p['kd_pekerjaan'] ?>"><?php echo $p['pekerjaan'] ?></option>
              <?php $temp = $p['kd_jenis_pekerjaan'] ?>
              <?php $pekerjaan_counter++ ?>
            <?php endforeach; ?>
                </optgroup>
            </select>
          </div>
          <div class="four wide field">
            <label>No Telepon / Handphone</label>
            <input type="text" name="saudara_kandung[0][no_telepon]" palceholder="No Hp / Telepon">
          </div>
        </div>
      </div>
    <?php endif; ?>
    </div>
    <button type="button" class="ui button icon green" id="btn_tambah_saudara_kandung"><i class="ui icon plus"></i> TAMBAH</button>

    <h3 class="ui horizontal divider header">KELUARGA PRIBADI (Istri / Suami / Anak)</h3>
    <div class="keluarga-pribadi-parent">
    <?php if (isset($_SESSION['register_5']['keluarga_pribadi'])): ?>
    <?php $counter = 0; ?>
    <?php foreach ($_SESSION['register_5']['keluarga_pribadi'] as $v): ?>
      <div class="keluarga-pribadi-child">
        <h3 class="ui dividing header">KELUARGA PRIBADI</h3>
        <div class="fields">
          <div class="four wide field">
            <label>NAMA</label>
            <input type="text" name="keluarga_pribadi[<?php echo $counter ?>][nama]" placeholder="Nama Keluarga Pribadi" value="<?php echo $v['nama']?>">
          </div>
          <div class="four wide field">
            <label>TEMPAT LAHIR</label>
            <input type="text" name="keluarga_pribadi[<?php echo $counter ?>][tempat_lahir]" placeholder="Kota Lahir" value="<?php echo $v['tempat_lahir']?>">
          </div>
          <div class="five wide field">
            <label>TANGGAL LAHIR</label>
            <div class="fields">
              <div class="five wide field">
                <select class="ui dropdown" name="keluarga_pribadi[<?php echo $counter ?>][tanggal_lahir]">
                  <?php
                  if(isset($v['tanggal_lahir'])){
                    $tanggal = strtotime($v['tanggal_lahir']);
                    echo option_tanggal(intval(Date('d', $tanggal)));
                  }
                  else{
                    echo option_tanggal();
                  }
                  ?>
                </select>
              </div>
              <div class="seven wide field">
                <select class="ui dropdown" name="keluarga_pribadi[<?php echo $counter ?>][bulan_lahir]">
                  <?php
                  if(isset($v['tanggal_lahir'])){
                    $tanggal = strtotime($v['tanggal_lahir']);
                    echo option_bulan(intval(Date('m', $tanggal)));
                  }
                  else{
                    echo option_bulan();
                  }
                  ?>
                </select>
              </div>
              <div class="seven wide field">
                <select class="ui dropdown" name="keluarga_pribadi[<?php echo $counter ?>][tahun_lahir]">
                  <?php
                  if(isset($v['tanggal_lahir'])){
                    $tanggal = strtotime($v['tanggal_lahir']);
                    echo option_tahun(100, intval(Date('Y', $tanggal)));
                  }
                  else{
                    echo option_tahun();
                  }
                  ?>
                </select>
              </div>
            </div>
          </div>
        </div>
        <div class="fields">
          <div class="four wide field">
            <label>HUBUNGAN</label>
            <select class="ui dropdown" name="keluarga_pribadi[<?php echo $counter ?>][hubungan]">
              <?php foreach ($hubungan as $value): ?>
                <option <?php echo ($value['kd_hubungan'] == $v['hubungan'] ? 'selected' : '') ?> value="<?php echo $value['kd_hubungan'] ?>"><?php echo $value['hubungan'] ?></option>
              <?php endforeach; ?>
            </select>
          </div>
          <div class="four wide field">
            <label>PEKERJAAN</label>
            <select class="ui dropdown search" name="keluarga_pribadi[<?php echo $counter ?>][pekerjaan]">
            <?php $temp = ''; $pekerjaan_counter = 0; ?>
            <?php foreach ($pekerjaan as $p): ?>
              <?php echo $pekerjaan_counter == 0 ? "<optgroup label={$p['jenis_pekerjaan']}>" : "" ?>
              <?php if ($p['kd_jenis_pekerjaan'] != $temp): ?>
                </optgroup>
                <optgroup label="<?php echo $p['jenis_pekerjaan'] ?>">
              <?php endif; ?>
                  <option <?php echo $v['pekerjaan'] == $p['kd_pekerjaan'] ? " selected " : "" ?>
                          value="<?php echo $p['kd_pekerjaan'] ?>"><?php echo $p['pekerjaan'] ?></option>
              <?php $temp = $p['kd_jenis_pekerjaan'] ?>
              <?php $pekerjaan_counter++ ?>
            <?php endforeach; ?>
                </optgroup>
            </select>
          </div>
          <div class="four wide field">
            <label>No Telepon / Handphone</label>
            <input type="text" name="keluarga_pribadi[<?php echo $counter ?>][no_telepon]" palceholder="No Hp / Telepon" value="<?php echo $v['no_telp']?>">
          </div>
        </div>
      </div>
    <?php $counter++; ?>
    <?php endforeach; ?>
    <?php else: ?>
      <div class="keluarga-pribadi-child">
        <h3 class="ui dividing header">KELUARGA PRIBADI</h3>
        <div class="fields">
          <div class="four wide field">
            <label>NAMA</label>
            <input type="text" name="keluarga_pribadi[0][nama]" placeholder="Nama Keluarga Pribadi">
          </div>
          <div class="four wide field">
            <label>TEMPAT LAHIR</label>
            <input type="text" name="keluarga_pribadi[0][tempat_lahir]" placeholder="Kota Lahir">
          </div>
          <div class="five wide field">
            <label>TANGGAL LAHIR</label>
            <div class="fields">
              <div class="five wide field">
                <select class="ui dropdown" name="keluarga_pribadi[0][tanggal_lahir]">
                <?php for($i = 1; $i <= 31; $i++) {?>
                  <option value="<?php echo $i ?>"><?php echo $i ?></option>
                <?php } ?>
                </select>
              </div>
              <div class="seven wide field">
                <select class="ui dropdown" name="keluarga_pribadi[0][bulan_lahir]">
                  <option value="" disabled="disabled" >Bulan</option>
                  <option value="1">Januari</option>
                  <option value="2">Februari</option>
                  <option value="3">Maret</option>
                  <option value="4">April</option>
                  <option value="5">Mei</option>
                  <option value="6">Juni</option>
                  <option value="7">Juli</option>
                  <option value="8">Agustus</option>
                  <option value="9">September</option>
                  <option value="10">Oktober</option>
                  <option value="11">November</option>
                  <option value="12">Desember</option>
                </select>
              </div>
              <div class="seven wide field">
                <select class="ui dropdown" name="keluarga_pribadi[0][tahun_lahir]">
                  <?php
                    $tahun = Date("Y");
                    for($i = $tahun; $i>=$tahun - 100; $i--)
                    {
                      echo "<option value='{$i}'>{$i}</option>";
                    }
                  ?>
                </select>
              </div>
            </div>
          </div>
        </div>
        <div class="fields">
          <div class="four wide field">
            <label>HUBUNGAN</label>
            <select class="ui dropdown" name="keluarga_pribadi[0][hubungan]">
              <?php foreach ($hubungan as $value): ?>
                <option value="<?php echo $value['kd_hubungan'] ?>"><?php echo $value['hubungan'] ?></option>
              <?php endforeach; ?>
            </select>
          </div>
          <div class="four wide field">
            <label>PEKERJAAN</label>
            <select class="ui dropdown search" name="keluarga_pribadi[0][pekerjaan]">
            <?php $temp = ''; $pekerjaan_counter = 0; ?>
            <?php foreach ($pekerjaan as $p): ?>
              <?php echo $pekerjaan_counter == 0 ? "<optgroup label={$p['jenis_pekerjaan']}>" : "" ?>
              <?php if ($p['kd_jenis_pekerjaan'] != $temp): ?>
                </optgroup>
                <optgroup label="<?php echo $p['jenis_pekerjaan'] ?>">
              <?php endif; ?>
                  <option value="<?php echo $p['kd_pekerjaan'] ?>"><?php echo $p['pekerjaan'] ?></option>
              <?php $temp = $p['kd_jenis_pekerjaan'] ?>
              <?php $pekerjaan_counter++ ?>
            <?php endforeach; ?>
                </optgroup>
            </select>
          </div>
          <div class="four wide field">
            <label>No Telepon / Handphone</label>
            <input type="text" name="keluarga_pribadi[0][no_telepon]" palceholder="No Hp / Telepon">
          </div>
        </div>
      </div>
    <?php endif; ?>
    </div>
    <button type="button" class="ui button icon green" id="btn_tambah_keluarga_pribadi"><i class="ui icon plus"></i> TAMBAH</button>

    <h4 class="ui top attached inverted header">Keluarga yang berada di Jepang</h4>
    <div class="ui attached segment">
      <div class="keluarga-dijepang-parent">
      <?php if (isset($_SESSION['register_5']['keluarga_dijepang'])): ?>
      <?php $counter = 0; ?>
      <?php foreach ($_SESSION['register_5']['keluarga_dijepang'] as $v): ?>
        <div class="keluarga-dijepang-child">
          <h3 class="ui dividing header">KELUARGA PRIBADI</h3>
          <div class="fields">
            <div class="four wide field">
              <label>NAMA</label>
              <input type="text" name="keluarga_dijepang[<?php echo $counter ?>][nama]" placeholder="Nama Keluarga Pribadi" value="<?php echo $v['nama']?>">
            </div>
            <div class="four wide field">
              <label>NAMA PERUSAHAAN / SEKOLAH</label>
              <input type="text" name="keluarga_dijepang[<?php echo $counter ?>][perusahaan]" placeholder="Perusahaan / Sekolah" value="<?php echo $v['perusahaan']?>">
            </div>
            <div class="five wide field">
              <label>TANGGAL LAHIR</label>
              <div class="fields">
                <div class="five wide field">
                  <select class="ui dropdown" name="keluarga_dijepang[<?php echo $counter ?>][tanggal_lahir]">
                    <?php
                    if(isset($v['tanggal_lahir'])){
                      $tanggal = strtotime($v['tanggal_lahir']);
                      echo option_tanggal(intval(Date('d', $tanggal)));
                    }
                    else{
                      echo option_tanggal();
                    }
                    ?>
                  </select>
                </div>
                <div class="seven wide field">
                  <select class="ui dropdown" name="keluarga_dijepang[<?php echo $counter ?>][bulan_lahir]">
                    <?php
                    if(isset($v['tanggal_lahir'])){
                      $tanggal = strtotime($v['tanggal_lahir']);
                      echo option_bulan(intval(Date('m', $tanggal)));
                    }
                    else{
                      echo option_bulan();
                    }
                    ?>
                  </select>
                </div>
                <div class="seven wide field">
                  <select class="ui dropdown" name="keluarga_dijepang[<?php echo $counter ?>][tahun_lahir]">
                    <?php
                    if(isset($v['tanggal_lahir'])){
                      $tanggal = strtotime($v['tanggal_lahir']);
                      echo option_tahun(100, intval(Date('Y', $tanggal)));
                    }
                    else{
                      echo option_tahun();
                    }
                    ?>
                  </select>
                </div>
              </div>
            </div>
          </div>
          <div class="fields">
            <div class="four wide field">
              <label>HUBUNGAN</label>
              <select class="ui dropdown" name="keluarga_dijepang[<?php echo $counter ?>][hubungan]">
                <?php foreach ($hubungan as $value): ?>
                  <option <?php echo ($value['kd_hubungan'] == $v['hubungan'] ? 'selected' : '') ?> value="<?php echo $value['kd_hubungan'] ?>"><?php echo $value['hubungan'] ?></option>
                <?php endforeach; ?>
              </select>
            </div>
            <div class="four wide field">
              <label>KEWARGANEGARAAN</label>
              <input type="text" name="keluarga_dijepang[<?php echo $counter ?>][kewarganegaraan]" placeholder="Indonesia / Jepang" value="<?php echo $v['kewarganegaraan'] ?>">
            </div>
            <div class="four wide field">
              <label>STATUS VISA</label>
              <input type="text" name="keluarga_dijepang[<?php echo $counter ?>][status_visa]" palceholder="Status Visa" value="<?php echo $v['status_visa']?>">
            </div>
          </div>
        </div>
      <?php $counter++; ?>
      <?php endforeach; ?>
      <?php else: ?>
        <div class="keluarga-dijepang-child">
          <div class="fields">
            <div class="four wide field">
              <label>NAMA</label>
              <input type="text" name="keluarga_dijepang[0][nama]" placeholder="Nama Keluarga Di Jepang">
            </div>
            <div class="four wide field">
              <label>PERUSAHAAN / SEKOLAH</label>
              <input type="text" name="keluarga_dijepang[0][perusahaan]" placeholder="Perusahaan / Sekolah">
            </div>
            <div class="five wide field">
              <label>TANGGAL LAHIR</label>
              <div class="fields">
                <div class="five wide field">
                  <select class="ui dropdown" name="keluarga_dijepang[0][tanggal_lahir]">
                  <?php for($i = 1; $i <= 31; $i++) {?>
                    <option value="<?php echo $i ?>"><?php echo $i ?></option>
                  <?php } ?>
                  </select>
                </div>
                <div class="seven wide field">
                  <select class="ui dropdown" name="keluarga_dijepang[0][bulan_lahir]">
                    <option value="" disabled="disabled" >Bulan</option>
                    <option value="1">Januari</option>
                    <option value="2">Februari</option>
                    <option value="3">Maret</option>
                    <option value="4">April</option>
                    <option value="5">Mei</option>
                    <option value="6">Juni</option>
                    <option value="7">Juli</option>
                    <option value="8">Agustus</option>
                    <option value="9">September</option>
                    <option value="10">Oktober</option>
                    <option value="11">November</option>
                    <option value="12">Desember</option>
                  </select>
                </div>
                <div class="seven wide field">
                  <select class="ui dropdown" name="keluarga_dijepang[0][tahun_lahir]">
                    <?php
                      $tahun = Date("Y");
                      for($i = $tahun; $i>=$tahun - 100; $i--)
                      {
                        echo "<option value='{$i}'>{$i}</option>";
                      }
                    ?>
                  </select>
                </div>
              </div>
            </div>
          </div>
          <div class="fields">
            <div class="four wide field">
              <label>HUBUNGAN</label>
              <select class="ui dropdown" name="keluarga_dijepang[0][hubungan]">
                <?php foreach ($hubungan as $value): ?>
                  <option value="<?php echo $value['kd_hubungan'] ?>"><?php echo $value['hubungan'] ?></option>
                <?php endforeach; ?>
              </select>
            </div>
            <div class="four wide field">
              <label>KEWARGANEGARAAN</label>
              <input type="text" name="keluarga_dijepang[0][kewarganegaraan]" placeholder="Indonesia / Jepang">
            </div>
            <div class="four wide field">
              <label>STATUS VISA</label>
              <input type="text" name="keluarga_dijepang[0][status_visa]" palceholder="Status Visa">
            </div>
          </div>
        </div>
      <?php endif; ?>
      </div>
      <button type="button" class="ui button icon green" id="btn_tambah_keluarga_dijepang"><i class="ui icon plus"></i> TAMBAH</button>
    </div>

    <button type="submit" name="submit" class="ui button icon primary" style="display: block; margin-top: 2em;"><i class="ui icon arrow right"></i> SELANJUTNYA</button>
  </form>
</div>
