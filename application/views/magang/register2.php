<div class="ui segment very padded text container">

<?php echo form_open('', 'class="ui form" id="register_form_2"') ?>
  <h2 class="ui dividing header">KEMAHIRAN BERBAHASA ASING</h2>
  <div class="field" id="field_parent">
    <label>Menguasai Bahasa</label>
    <?php if (!empty($_SESSION['register_2']['bahasa'])): ?>
      <?php $counter = 0; ?>
      <?php foreach ($_SESSION['register_2']['bahasa'] as $v): ?>
        <div class="field dynamic-field">
          <div class="inline fields">
              <div class="field">
                <div class="ui radio checkbox">
                  <input type="radio" data-input="bahasa<?php echo $counter; ?>" name="bahasa[<?php echo $counter; ?>]" value="jepang" <?php echo $v == 'jepang' ? 'checked' : '' ?>>
                  <label>JEPANG</label>
                </div>
              </div>

              <div class="field">
                <div class="ui radio checkbox">
                  <input type="radio" data-input="bahasa<?php echo $counter; ?>" name="bahasa[<?php echo $counter; ?>]" value="inggris" <?php echo $v == 'inggris' ? 'checked' : '' ?>>
                  <label>INGGRIS</label>
                </div>
              </div>

              <div class="field">
                <div class="ui radio checkbox">
                  <input type="radio" class="lainnya" data-input="bahasa<?php echo $counter; ?>" name="bahasa[<?php echo $counter; ?>]" <?php echo $v != 'inggris' && $v != 'jepang' ? 'checked' : '' ?>/>
                  <label>LAINNYA</label>
                </div>
                <input type="text" id="bahasa<?php echo $counter; ?>" <?php echo $v != 'inggris' && $v != 'jepang' ? '' : 'disabled="disabled"' ?> name="bahasa[<?php echo $counter; ?>]" value="<?php echo $v != 'inggris' && $v != 'jepang' ? $v : '' ?>">
              </div>
              <?php if($counter != 0): ?>
              <button type="button" onclick="$(this).parent().remove();" class="ui icon button red"><i class="ui icon x"></i></button>
            <?php endif; ?>
          </div>
        </div>
        <?php $counter++; ?>
      <?php endforeach; ?>
    <?php else: ?>
    <div class="field dynamic-field">
      <div class="inline fields">
          <div class="field">
            <div class="ui radio checkbox">
              <input type="radio" data-input="bahasa0" name="bahasa[0]" value="jepang">
              <label>JEPANG</label>
            </div>
          </div>

          <div class="field">
            <div class="ui radio checkbox">
              <input type="radio" data-input="bahasa0" name="bahasa[0]" value="inggris">
              <label>INGGRIS</label>
            </div>
          </div>

          <div class="field">
            <div class="ui radio checkbox">
              <input type="radio" data-input="bahasa0" class="lainnya" name="bahasa[0]"/>
              <label>LAINNYA</label>
            </div>
            <input type="text" id="bahasa0" disabled="disabled" name="bahasa[0]">
          </div>
      </div>
    </div>
    <?php endif; ?>
  </div>
  <button type="button" class="ui icon button green" id="btn_tambah_bahasa"><i class="ui icon plus"></i> TAMBAH</button>
  <button type="submit" class="ui icon button primary" style="display: block; margin-top: 2em;"><i class="ui icon arrow right"></i> SELANJUTNYA</button>
</form>
</div>
