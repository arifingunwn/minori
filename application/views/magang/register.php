    <div class="ui segment very padded container">
    <?php if (validation_errors() != false) { ?>
    <div class="ui negative message">
      <?php echo validation_errors(); ?>
    </div>
    <?php } ?>
    <?php echo form_open('', 'class="ui form" id="register_form_1"') ?>
      <h2 class="ui dividing header">DATA PRIBADI</h2>
      <div class="ui grid">
        <div class="ten wide column">
          <div class="required field">
            <label>NAMA LENGKAP</label>
            <input type="text" name="nama_lengkap" placeholder="NAMA LENGKAP PEMAGANG" value="<?php echo isset($_SESSION['register_1']['nama_lengkap']) ? $_SESSION['register_1']['nama_lengkap'] : "" ?>">
          </div>

          <div class="field">
            <div class="two fields">
              <div class="required field">
                <label>TEMPAT LAHIR</label>
                <input type="text" name="tempat_lahir" placeholder="KOTA" value="<?php echo isset($_SESSION['register_1']['tempat_lahir']) ? $_SESSION['register_1']['tempat_lahir'] : "" ?>">
              </div>

              <div class="required field">
                <label>TANGGAL LAHIR</label>
                <div class="inline fields">
                  <div class="four wide field">
                    <select class="ui search dropdown" name="tanggal_lahir">
                      <?php
                      if(isset($_SESSION['register_1']['tanggal_lahir'])){
                        echo option_tanggal(substr($_SESSION['register_1']['tanggal_lahir'], strripos($_SESSION['register_1']['tanggal_lahir'], '-') + 1, 2));
                      }
                      else{
                        echo option_tanggal();
                      }
                      ?>
                    </select>
                  </div>

                  <div class="six wide field">
                    <select class="ui search dropdown" name="bulan_lahir">
                      <option value="" disabled="disabled" >Bulan</option>
                      <?php
                      if(isset($_SESSION['register_1']['tanggal_lahir'])){
                        $tanggal = strtotime($_SESSION['register_1']['tanggal_lahir']);
                        echo option_bulan(intval(Date('m', $tanggal)));
                      }
                      else{
                        echo option_bulan();
                      }
                      ?>
                    </select>
                  </div>

                  <div class="six wide field">
                    <select class="ui search dropdown" name="tahun_lahir">
                      <?php
                      if(isset($_SESSION['register_1']['tanggal_lahir'])){
                        $tanggal = strtotime($_SESSION['register_1']['tanggal_lahir']);
                        echo option_tahun(40, intval(Date('Y', $tanggal)));
                      }
                      else{
                        echo option_tahun();
                      }
                      ?>
                    </select>
                  </div>
                </div>
              </div>

            </div>
          </div>

          <div class="required field">
            <label>JENIS KELAMIN</label>
            <div class="inline fields">
              <div class="field">
                <div class="ui radio checkbox">
                  <input type="radio" name="jenis_kelamin" value="L" <?php echo isset($_SESSION['register_1']['jenis_kelamin']) && $_SESSION['register_1']['jenis_kelamin'] == 'L' ? 'checked' : '' ?> >
                  <label>PRIA</label>
                </div>
              </div>

              <div class="field">
                <div class="ui radio checkbox">
                  <input type="radio" name="jenis_kelamin" value="P" <?php echo isset($_SESSION['register_1']['jenis_kelamin']) && $_SESSION['register_1']['jenis_kelamin'] == 'P' ? 'checked' : '' ?>>
                  <label>WANITA</label>
                </div>
              </div>
            </div>
          </div>

          <div class="requried field">
            <label for="status">STATUS</label>
            <div class="inline fields">
              <div class="field">
                <div class="ui radio checkbox">
                  <input type="radio" name="status" value="b" <?php echo isset($_SESSION['register_1']['status']) && $_SESSION['register_1']['status'] == 'b' ? 'checked' : '' ?>>
                  <label>BELUM MENIKAH</label>
                </div>
              </div>
              <div class="field">
                <div class="ui radio checkbox">
                  <input type="radio" name="status" value="n" <?php echo isset($_SESSION['register_1']['status']) && $_SESSION['register_1']['status'] == 'n' ? 'checked' : '' ?>>
                  <label>MENIKAH</label>
                </div>
              </div>
              <div class="field">
                <div class="ui radio checkbox">
                  <input type="radio" name="status" value="jh" <?php echo isset($_SESSION['register_1']['status']) && $_SESSION['register_1']['status'] == 'jh' ? 'checked' : '' ?>>
                  <label>CERAI HIDUP</label>
                </div>
              </div>
              <div class="field">
                <div class="ui radio checkbox">
                  <input type="radio" name="status" value="jm" <?php echo isset($_SESSION['register_1']['status']) && $_SESSION['register_1']['status'] == 'jm' ? 'checked' : '' ?>>
                  <label>CERAI MATI</label>
                </div>
              </div>
              <div class="field">
                <div class="ui radio checkbox">
                  <input type="radio" name="status" value="ns" <?php echo isset($_SESSION['register_1']['status']) && $_SESSION['register_1']['status'] == 'ns' ? 'checked' : '' ?>>
                  <label>NIKAH SIRIH</label>
                </div>
              </div>
            </div>
          </div>

          <div class="required field">
            <label for="golongan_darah">GOLONGAN DARAH</label>
            <div class="inline fields">
              <div class="field">
                <div class="ui radio checkbox">
                  <input type="radio" name="golongan_darah" value="O" <?php echo isset($_SESSION['register_1']['golongan_darah']) && $_SESSION['register_1']['golongan_darah'] == 'O' ? 'checked' : '' ?>>
                  <label>O</label>
                </div>
              </div>

              <div class="field">
                <div class="ui radio checkbox">
                  <input type="radio" name="golongan_darah" value="A" <?php echo isset($_SESSION['register_1']['golongan_darah']) && $_SESSION['register_1']['golongan_darah'] == 'A' ? 'checked' : '' ?>>
                  <label>A</label>
                </div>
              </div>

              <div class="field">
                <div class="ui radio checkbox">
                  <input type="radio" name="golongan_darah" value="B" <?php echo isset($_SESSION['register_1']['golongan_darah']) && $_SESSION['register_1']['golongan_darah'] == 'B' ? 'checked' : '' ?>>
                  <label>B</label>
                </div>
              </div>

              <div class="field">
                <div class="ui radio checkbox">
                  <input type="radio" name="golongan_darah" value="AB" <?php echo isset($_SESSION['register_1']['golongan_darah']) && $_SESSION['register_1']['golongan_darah'] == 'AB' ? 'checked' : '' ?>>
                  <label>AB</label>
                </div>
              </div>

              <div class="field">
                <div class="ui radio checkbox">
                  <input type="radio" name="golongan_darah" value="TIDAK_TAHU">
                  <label>TIDAK TAHU</label>
                </div>
              </div>
            </div>
          </div>

          <div class="required field">
            <label>ALAMAT SEKARANG</label>
            <textarea form="register_form_1" rows="2" name="alamat_sekarang"><?php echo isset($_SESSION['register_1']['alamat_sekarang']) ? $_SESSION['register_1']['alamat_sekarang'] : "" ?></textarea>
          </div>

          <div class="required field">
            <label>PROVINSI</label>
            <select class="ui fluid search dropdown" name="provinsi_sekarang">
              <option value="" disabled="disabled" >--PILIH PROVINSI--</option>
              <?php foreach ($provinsi as $k => $v): ?>
              <option <?php echo isset($_SESSION['register_1']['provinsi_sekarang']) && $_SESSION['register_1']['provinsi_sekarang'] == $v['kd_provinsi'] ? 'selected' : '' ?> value="<?php echo $v['kd_provinsi'] ?>"><?php echo $v['provinsi'] ?></option>
              <?php endforeach; ?>
            </select>
          </div>

          <div class="required field">
            <label>AGAMA</label>
            <select class="ui search dropdown" name="agama">
              <?php foreach ($agama as $k => $v): ?>
              <option <?php echo isset($_SESSION['register_1']['agama']) && $_SESSION['register_1']['agama'] == $v['kd_agama'] ? 'selected' : '' ?> value="<?php echo $v['kd_agama']?>"><?php echo $v['agama']?></option>
              <?php endforeach; ?>
            </select>
          </div>

          <div class="six wide required field">
            <label>KOTA ASAL</label>
            <input type="text" name="tempat_asal" palceholder="Kota" value="<?php echo isset($_SESSION['register_1']['tempat_asal']) ? $_SESSION['register_1']['tempat_asal'] : "" ?>">
          </div>

          <div class="six wide required field">
            <label>KEWARGANEGARAAN</label>
            <input type="text" name="kewarganegaraan" placeholder="Indonesia, Jepang, dll" value="<?php echo isset($_SESSION['register_1']['kewarganegaraan']) ? $_SESSION['register_1']['kewarganegaraan'] : "" ?>">
          </div>

          <div class="six wide required field">
            <label>ALAMAT PADA KTP</label>
            <textarea form="register_form_1" rows='2' name="alamat_ktp"><?php echo isset($_SESSION['register_1']['alamat_ktp']) ? $_SESSION['register_1']['alamat_ktp'] : "" ?></textarea>
          </div>

          <h4 class="ui dividing header">NOMOR TELEPON / HANDPHONE</h4>
          <div class="field">
            <div class="inline fields">
              <div class="field">
                <label>TELEPON</label>
                <input type="text" name="nomor_telepon" placeholder="(021) 559911" value="<?php echo isset($_SESSION['register_1']['nomor_telepon']) ? $_SESSION['register_1']['nomor_telepon'] : "" ?>">
              </div>

              <div class="required field">
                <label>HANDPHONE</label>
                <input type="text" name="nomor_handphone" placeholder="+628121212" value="<?php echo isset($_SESSION['register_1']['nomor_handphone']) ? $_SESSION['register_1']['nomor_handphone'] : "" ?>">
              </div>

              <div class="field">
                <label>HANDPHONE Tambahan</label>
                <input type="text" name="nomor_handphone_tambahan" placeholder="+6281212122" value="<?php echo isset($_SESSION['register_1']['nomor_handphone_tambahan']) ? $_SESSION['register_1']['nomor_handphone_tambahan'] : "" ?>">
              </div>
            </div>
          </div>

          <h4 class="ui dividing header">INFORMASI FISIK</h4>

          <div class="field">
            <div class="fields">
              <div class="four wide required field">
                <label>UKURAN SEPATU</label>
                <input type="text" name="ukuran_sepatu" placeholder="Cth. 41 / 42 / 43" value="<?php echo isset($_SESSION['register_1']['ukuran_sepatu']) ? $_SESSION['register_1']['ukuran_sepatu'] : "" ?>">
              </div>

              <div class="four wide required field">
                <label>LINGKAR PINGGANG</label>
                <input type="text" name="lingkar_pinggang" placeholder="centimeter" value="<?php echo isset($_SESSION['register_1']['lingkar_pinggang']) ? $_SESSION['register_1']['lingkar_pinggang'] : "" ?>"/>
              </div>

              <div class="four wide required field">
                <label>BERAT BADAN</label>
                <input type="text" name="berat_badan" placeholder="Berat Badan Kilogram" value="<?php echo isset($_SESSION['register_1']['berat_badan']) ? $_SESSION['register_1']['berat_badan'] : "" ?>">
              </div>

              <div class="four wide required field">
                <label>TINGGI BADAN</label>
                <input type="text" name="tinggi_badan" placeholder="Tinggi Centimeter" value="<?php echo isset($_SESSION['register_1']['tinggi_badan']) ? $_SESSION['register_1']['tinggi_badan'] : "" ?>">
              </div>
            </div>
          </div>

          <h4 class="ui dividing header">INFORMASI TAMBAHAN</h4>
          <div class="six wide required field">
            <label>PELAJARAN YANG DISUKAI</label>
            <input type="text" name="pelajaran_favorit" placeholder="Pelajaran Favorit" value="<?php echo isset($_SESSION['register_1']['pelajaran_favorit']) ? $_SESSION['register_1']['pelajaran_favorit'] : "" ?>">
          </div>

          <div class="six wide required field">
            <label>HOBI</label>
            <input type="text" name="hobi" placeholder="Hobi yang disukai" value="<?php echo isset($_SESSION['register_1']['hobi']) ? $_SESSION['register_1']['hobi'] : "" ?>">
          </div>

          <div class="six wide required field">
            <label>KETERAMPILAN / SKILL</label>
            <input type="text" name="keterampilan" placeholder="Keahlian" value="<?php echo isset($_SESSION['register_1']['keterampilan']) ? $_SESSION['register_1']['keterampilan'] : "" ?>">
          </div>

          <h4 class="ui dividing header">KESEHATAN</h4>
          <div class="inline field">
            <input type="checkbox" tabindex=0 class="hidden" onChange="toggleInput('penyakit_kronis')" <?php echo empty($_SESSION['register_1']['nama_penyakit']) ? '' : 'checked' ?>/>
            <label>Pernah menderita penyakit akut</label>
          </div>

          <div class="fields" id="penyakit_kronis" <?php echo !empty($_SESSION['register_1']['nama_penyakit']) ? "" : "style='display: none'" ?>>
            <div class="six wide field">
              <label>Nama Penyakit</label>
              <input type="text" name="nama_penyakit" placeholder="Penyakit" value="<?php echo isset($_SESSION['register_1']['nama_penyakit']) ? $_SESSION['register_1']['nama_penyakit'] : "" ?>">
            </div>

            <div class="six wide field">
              <label>TAHUN</label>
              <input type="text" name="tahun_penyakit" placeholder="tahun" value="<?php echo isset($_SESSION['register_1']['tahun_penyakit']) ? $_SESSION['register_1']['tahun_penyakit'] : "" ?>">
            </div>

            <div class="six wide field">
              <label>BULAN</label>
              <input type="text" name="bulan_penyakit" placeholder="bulan" value="<?php echo isset($_SESSION['register_1']['bulan_penyakit']) ? $_SESSION['register_1']['bulan_penyakit'] : "" ?>">
            </div>
          </div>

          <div class="field">
            <label>MEROKOK</label>
            <div class="field">
              <div class="ui radio checkbox">
                <input type="radio" name="merokok" value="TIDAK" <?php echo isset($_SESSION['register_1']['merokok']) && $_SESSION['register_1']['merokok'] == 'TIDAK' ? 'checked' : '' ?>>
                <label>TIDAK</label>
              </div>
            </div>

            <div class="field">
              <div class="ui radio checkbox">
                <input type="radio" name="merokok" value="BERHENTI" <?php echo isset($_SESSION['register_1']['merokok']) && $_SESSION['register_1']['merokok'] == 'BERHENTI' ? 'checked' : '' ?> >
                <label>BERHENTI</label>
              </div>
            </div>

            <div class="inline fields">
              <div class="field">
                <div class="ui radio checkbox">
                  <input type="radio" name="merokok" value="YA" <?php echo isset($_SESSION['register_1']['merokok']) && $_SESSION['register_1']['merokok'] == 'YA' ? 'checked' : '' ?>>
                  <label>YA</label>
                </div>
              </div>

              <div class="four wide field">
                <input type="text" name="batang_rokok_per_hari" value="<?php echo isset($_SESSION['register_1']['batang_rokok_per_hari']) ? $_SESSION['register_1']['batang_rokok_per_hari'] : "" ?>">
                <label>Batang / Hari</label>
              </div>
            </div>
          </div>

          <div class="field">
            <label>MINUM ALKOHOL</label>
            <div class="field">
              <div class="ui radio checkbox">
                <input type="radio" name="minum_alkohol" value="TIDAK" <?php echo isset($_SESSION['register_1']['minum_alkohol']) && $_SESSION['register_1']['minum_alkohol'] == 'TIDAK' ? 'checked' : '' ?> >
                <label>TIDAK</label>
              </div>
            </div>

            <div class="field">
              <div class="ui radio checkbox">
                <input type="radio" name="minum_alkohol" value="BERHENTI" <?php echo isset($_SESSION['register_1']['minum_alkohol']) && $_SESSION['register_1']['minum_alkohol'] == 'BERHENTI' ? 'checked' : '' ?>>
                <label>BERHENTI</label>
              </div>
            </div>

            <div class="inline fields">
              <div class="field">
                <div class="ui radio checkbox">
                  <input type="radio" name="minum_alkohol" value="YA" <?php echo isset($_SESSION['register_1']['minum_alkohol']) && $_SESSION['register_1']['minum_alkohol'] == 'YA' ? 'checked' : '' ?> >
                  <label>YA</label>
                </div>
              </div>

              <div class="four wide field">
                <input type="text" name="frekuensi_minum_alkohol_perbulan" value="<?php echo isset($_SESSION['register_1']['frekuensi_minum_alkohol_perbulan']) ? $_SESSION['register_1']['frekuensi_minum_alkohol_perbulan'] : "" ?>">
                <label>kali / Bulan</label>
              </div>
            </div>
          </div>

          <div class="field">
            <label>PERNAH KE LUAR NEGRI</label>
            <div class="inline fields">
              <div class="field">
                <div class="ui radio checkbox">
                  <input type="radio" name="pernah_keluar_negri" value="p" <?php echo isset($_SESSION['register_1']['pernah_keluar_negri']) && $_SESSION['register_1']['pernah_keluar_negri'] == 'p' ? 'checked' : '' ?> >
                  <label>YA</label>
                </div>
              </div>

              <div class="field">
                <div class="ui radio checkbox">
                  <input type="radio" name="pernah_keluar_negri" value="b" <?php echo isset($_SESSION['register_1']['pernah_keluar_negri']) && $_SESSION['register_1']['pernah_keluar_negri'] == 'b' ? 'checked' : '' ?>>
                  <label>TIDAK</label>
                </div>
              </div>
            </div>
          </div>

          <div class="pernah_keluar_negri" <?php echo isset($_SESSION['register_1']['pernah_keluar_negri']) && $_SESSION['register_1']['pernah_keluar_negri'] == 'p' ? "" : "style='display: none;'" ?>>
            <?php if (!empty($_SESSION['register_1']['negara_pernah_dikunjungi'])): ?>
              <?php $counter = 0; ?>
              <?php foreach ($_SESSION['register_1']['negara_pernah_dikunjungi'] as $k => $v): ?>
                <div class="fields pernah_keluar_negri_child">
                  <div class="four wide field">
                    <label>Negara</label>
                    <select class="ui dropdown" name="negara_pernah_dikunjungi[<?php echo $counter ?>][negara]">
                    <?php foreach ($negara as $val): ?>
                      <option <?php echo ($val == $v['negara'] ? 'selected' : '') ?> value=<?php echo $val ?>><?php echo $val; ?></option>
                    <?php endforeach; ?>
                    </select>
                  </div>

                  <div class="six wide field">
                    <label>Selama</label>
                    <div class="inline fields">
                      <input type="text" name="negara_pernah_dikunjungi[<?php echo $counter ?>][tahun]" value="<?php echo $v['tahun'] ?>" placeholder="1">
                      <label>Tahun</label>

                      <input type="text" name="negara_pernah_dikunjungi[<?php echo $counter ?>][bulan]" value="<?php echo $v['bulan'] ?>" placeholder="1">
                      <label>Bulan</label>
                    </div>
                    <div class="field">
                      <label>Tujuan</label>
                      <input type="text" name="negara_pernah_dikunjungi[<?php echo $counter ?>][tujuan]" value="<?php echo $v['tujuan'] ?>" placeholder="Tujuan : Liburan / Sekolah">
                    </div>
                  </div>
                </div>
                <?php $counter++; ?>
              <?php endforeach; ?>
            <?php else: ?>
            <div class="fields pernah_keluar_negri_child">
              <div class="four wide field">
                <label>Negara</label>
                <select class="ui dropdown" name="negara_pernah_dikunjungi[0][negara]">
                <?php foreach ($negara as $val): ?>
                  <option value=<?php echo $val ?>><?php echo $val; ?></option>
                <?php endforeach; ?>
                </select>
              </div>

              <div class="six wide field">
                <label>Selama</label>
                <div class="inline fields">
                  <input type="text" name="negara_pernah_dikunjungi[0][tahun]" value="" placeholder="1">
                  <label>Tahun</label>

                  <input type="text" name="negara_pernah_dikunjungi[0][bulan]" value="" placeholder="1">
                  <label>Bulan</label>
                </div>
                <div class="field">
                  <label>Tujuan</label>
                  <input type="text" name="negara_pernah_dikunjungi[0][tujuan]" value="" placeholder="Tujuan : Liburan / Sekolah">
                </div>
              </div>
            </div>
          <?php endif; ?>
          </div>
          <button type="button" style="margin-bottom: 2em;" class="ui icon mini button green" id="btn_tambah_negara_pernah_dikunjungi"><i class="ui icon plus"></i> TAMBAH</button>

          <div class="field">
            <label>MEMILIKI PASSPORT</label>
            <div class="inline fields">
              <div class="field">
                <div class="ui radio checkbox">
                  <input type="radio" name="passport" value="Memiliki" <?php echo isset($_SESSION['register_1']['passport']) && $_SESSION['register_1']['passport'] == 'Memiliki' ? 'checked' : '' ?>>
                  <label>YA</label>
                </div>
              </div>

              <div class="field">
                <div class="ui radio checkbox">
                  <input type="radio" name="passport" value="Belum" <?php echo isset($_SESSION['register_1']['passport']) && $_SESSION['register_1']['passport'] == 'Belum' ? 'checked' : '' ?>>
                  <label>TIDAK</label>
                </div>
              </div>
            </div>
          </div>

          <div class="memiliki_passport">
            <div class="fields">
              <div class="six field">
                <label>No. Passport</label>
                <input type="text" name="no_passport" value="<?php echo isset($_SESSION['register_1']['no_passport']) ? $_SESSION['register_1']['no_passport'] : "" ?>" placeholder="Nomor Passport">
              </div>

            </div>

            <div class="fields">
              <div class="eight field">
                <label>Berlaku Sampai</label>
                <div class="inline fields">
                  <select class="ui dropdown" name="tanggal_berlaku_passport">
                    <?php
                    if(isset($_SESSION['register_1']['tanggal_berlaku_passport'])){
                      echo option_tanggal(substr($_SESSION['register_1']['tanggal_berlaku_passport'], strripos($_SESSION['register_1']['tanggal_berlaku_passport'], '-') + 1, 2));
                    }
                    else{
                      echo option_tanggal();
                    }
                    ?>
                  </select>

                  <select class="ui dropdown" name="bulan_berlaku_passport">
                    <?php
                    if(isset($_SESSION['register_1']['tanggal_berlaku_passport'])){
                      $tanggal = strtotime($_SESSION['register_1']['tanggal_berlaku_passport']);
                      echo option_bulan(intval(Date('m', $tanggal)));
                    }
                    else{
                      echo option_bulan();
                    }
                    ?>
                  </select>

                  <select class="ui dropdown" name="tahun_berlaku_passport">
                    <?php
                    if(isset($_SESSION['register_1']['tanggal_berlaku_passport'])){
                      $tanggal = strtotime($_SESSION['register_1']['tanggal_berlaku_passport']);
                      echo option_tahun_passport(40, intval(Date('Y', $tanggal)));
                    }
                    else{
                      echo option_tahun_passport();
                    }
                    ?>
                  </select>
                </div>
              </div>

              <div class="eight field">
                <label>Tanggal Penerbitan</label>
                <div class="inline fields">
                  <select class="ui dropdown" name="tanggal_penerbitan_passport">
                    <?php
                    if(isset($_SESSION['register_1']['tanggal_penerbitan_passport'])){
                      echo option_tanggal(substr($_SESSION['register_1']['tanggal_penerbitan_passport'], strripos($_SESSION['register_1']['tanggal_penerbitan_passport'], '-') + 1, 2));
                    }
                    else{
                      echo option_tanggal();
                    }
                    ?>
                  </select>

                  <select class="ui dropdown" name="bulan_penerbitan_passport">
                    <?php
                    if(isset($_SESSION['register_1']['tanggal_penerbitan_passport'])){
                      $tanggal = strtotime($_SESSION['register_1']['tanggal_penerbitan_passport']);
                      echo option_bulan(intval(Date('m', $tanggal)));
                    }
                    else{
                      echo option_bulan();
                    }
                    ?>
                  </select>

                  <select class="ui dropdown" name="tahun_penerbitan_passport">
                    <?php
                    if(isset($_SESSION['register_1']['tanggal_penerbitan_passport'])){
                      $tanggal = strtotime($_SESSION['register_1']['tanggal_penerbitan_passport']);
                      echo option_tahun(40, intval(Date('Y', $tanggal)));
                    }
                    else{
                      echo option_tahun();
                    }
                    ?>
                  </select>
                </div>
              </div>
            </div>
          </div>

        </div>   <!-- END BIODATA COL LEFT -->

        <div class="ui six wide column">
          <label>FOTO</label>
            <div class="ui segment">
              <img id="upload-container" class="segment" src="<?php echo isset($_SESSION['register_1']['photo']) ? site_url('/tmp/foto/') . $_SESSION['register_1']['photo'] : '' ?>"/>
            <div class="field">
              <label class="ui button bottom">
                Pilih Foto
                <input type="file" name="file_foto" style="position: absolute; top: -9999px" id="upload" accept="image/*">
              </label>
            </div>
          </div>
        </div>

        <button type="button" name="register_1" id="btn_submit" class="ui primary submit button"><i class="icon arrow right"></i> SELANJUTNYA</button>
      </div>
      <input type="hidden" name="photo" id="hidden_photo" value="<?php echo empty($_SESSION['register_1']['photo']) ? "" : $_SESSION['register_1']['photo'] ?>">
    </form>
  </div>
