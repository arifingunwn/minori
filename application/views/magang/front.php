<div class="ui grid container fluid" style="margin-top: 4em">
  <!-- <div class="five wide column">
    <div class="ui segment inverted grey">
      <h2 class="ui header">Pengumuman</h2>
      <div class="content">
        Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.
      </div>
    </div>
  </div> -->

  <div class="ten wide column">
    <div class="ui padded segment">
      <h2 class="ui blue header">LOWONGAN MAGANG</h2>
        <table class="ui very basic compact table">
          <tbody>
          <?php $counter = 0; ?>
          <?php foreach ($lowongan as $val): ?>
            <tr>
              <td><?php echo ++$counter; ?>. </td>
              <td><?php echo $val['judul'] ?></td>
              <td><a target="_blank" href="<?php echo site_url('magang/show_lowongan/') . $val['kd'] ?>">Lihat Detail</a></td>
            </tr>
          <?php endforeach; ?>
          </tbody>
        </table>
      </div>
  </div>

  <div class="six wide column">
    <?php echo validation_errors(); ?>
    <?php echo form_open('', 'class="ui form" id="front_form"') ?>
      <div class="ui segment container grey">
        <h2 class="ui header">PENDAFTARAN</h2>
        <div class="field">
          <div class="ui corner fluid  labeled input">
            <div class="ui corner label">
              <i class="asterisk icon"></i>
            </div>
            <input type="text" id="ktp" placeholder="NO. KTP" name="ktp" value="">

          </div>
        </div>

        <div class="field">
          <div class="ui corner fluid  labeled input">
            <div class="ui corner label">
              <i class="asterisk icon"></i>
            </div>
            <input type="email" placeholder="EMAIL" name="email" value="">
          </div>
        </div>

        <div class="field">
          <div class="ui corner fluid  labeled input">
            <div class="ui corner label">
              <i class="asterisk icon"></i>
            </div>
            <input type="password" placeholder="PASSWORD" name="password" data-validate="password" value="">
          </div>
        </div>

        <div class="field">
          <div class="ui corner fluid  labeled input">
            <div class="ui corner label">
              <i class="asterisk icon"></i>
            </div>
            <input type="password" placeholder="CONFIRM PASSWORD" name="password_confirmation" value="">
          </div>
        </div>

        <button type="submit" name="daftar" class="ui primary icon submit button"><i class="icon user"></i> DAFTAR</button>
      </div>
    </form>
  </div>
</div>
