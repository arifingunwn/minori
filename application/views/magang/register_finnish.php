<div class="ui very padded segment container">
  <h3 class="ui horizontal divider header">BIODATA</h3>
  <div class="ui segment">
    <p>
      Silahkan periksa lagi biodata anda.
      Lalu <a href="#konfirmasi">KONFIRMASI</a>
    </p>
  </div>
  <div class="ui segment">
    <table class="ui celled structured table">
      <tr>
        <td rowspan="3"><b>Nama Lengkap</b> <span class="isian"><?php echo $_SESSION['register_1']['nama_lengkap']; ?></span></td>
        <td><b>Kewarga Negaraan</b> <span class="isian"><?php echo $_SESSION['register_1']['kewarganegaraan']; ?></span></td>
        <td><b>Umur</b> <span class="isian"><?php echo (Date('Y') - substr($_SESSION['register_1']['tanggal_lahir'], 0, 4)); ?></span></td>
        <td rowspan="6" width="20%"><img src="<?php echo site_url('/tmp/foto/') . $_SESSION['register_1']['photo'] ?>" alt=""></td>
      </tr>

      <tr>
        <td><b>Jenis Kelamin</b> <span class="isian"><?php echo $_SESSION['register_1']['jenis_kelamin']; ?></span></td>
        <td><b>Status Kawin</b>  <span class="isian"><?php echo $_SESSION['register_1']['status']; ?></span></td>
      </tr>

      <tr>
        <td colspan="2"><b>Agama</b> <span class="isian"><?php echo $_SESSION['register_1']['agama']; ?></span></td>
      </tr>

      <tr>
        <td colspan="2">Tanggal Lahir <span class="isian"><?php echo $_SESSION['register_1']['tanggal_lahir']; ?></span></td>
        <td>Tempat Asal <span class="isian"><?php echo $_SESSION['register_1']['tempat_asal']; ?></span></td>
      </tr>

      <tr>
        <td colspan="2">Alamat KTP <span class="isian"><?php echo $_SESSION['register_1']['alamat_ktp']; ?></span></td>
        <td>Nomor HP <span class="isian"><?php echo $_SESSION['register_1']['nomor_handphone']; ?></span></td>
      </tr>

      <tr>
        <td colspan="2">Alamat Sekarang <span class="isian"><?php echo $_SESSION['register_1']['alamat_sekarang']; ?></span></td>
        <td>No Telepon Lainnya <span class="isian"><?php echo $_SESSION['register_1']['nomor_telepon']; ?></span></td>
      </tr>

      <tr>
        <td colspan="4">Email <span class="isian"></span></td>
      </tr>

      <!-- PASSPORT -->
      <tr>
        <td colspan="4" class="collapsing" style="padding: 0">
          <table class="ui basic table subtable">
            <thead>
              <tr>
                <th>PASPOR</th>
                <th>No. Paspor</th>
                <th>Tanggal Penerbitan</th>
                <th>Berlaku Hingga</th>
              </tr>
            </thead>
            <tbody>
            <?php if(empty($_SESSION['register_1']['passport']) || $_SESSION['register_1']['passport'] == 'TIDAK'): ?>
              <tr>
                <td>Tidak Ada</td>
                <td></td>
                <td></td>
                <td></td>
              </tr>
            <?php else: ?>
              <tr>
                <td>Ada</td>
                <td><?php echo $_SESSION['register_1']['no_passport']; ?></td>
                <td><?php echo $_SESSION['register_1']['tanggal_penerbitan_passport'] ?></td>
                <td><?php echo $_SESSION['register_1']['tanggal_berlaku_passport'] ?></td>
              </tr>
            <?php endif; ?>
            </tbody>
          </table>
        </td>
      </tr>

      <!-- Riwayat Ke Luar Negri -->
      <tr>
        <td colspan="4"><h4>Riwayat Ke Luar Negri</h4></td>
      </tr>
      <tr>
        <td colspan="4" class="collapsing" style="padding: 0">
          <table class="ui celled table subtable">
            <!-- <thead>
              <tr>
                <th>Tahun</th>
                <th>Bulan</th>
                <th>Negara Tujuan</th>
                <th>Tujuan</th>
              </tr>
            </thead> -->
            <tbody>
            <?php if(!empty($_SESSION['register_1']['negara_pernah_dikunjungi']) || $_SESSION['register_1']['pernah_keluar_negri'] == 'p'): ?>
              <?php foreach ($_SESSION['register_1']['negara_pernah_dikunjungi'] as $val): ?>
                <tr>
                  <td><?php echo $val['tahun'] ?> Tahun</td>
                  <td><?php echo $val['bulan'] ?> Bulan</td>
                  <td><?php echo $val['negara'] ?></td>
                  <td></td>
                </tr>
              <?php endforeach; ?>
            <?php else: ?>
              <tr>
                <td>Tidak Pernah</td>
                <td></td>
                <td></td>
              </tr>
            <?php endif; ?>
            </tbody>
          </table>
        </td>
      </tr>

      <tr>
        <td colspan="4"><h4>Riwayat Belajar Bahasa Jepang</h4></td>
      </tr>
      <tr>
        <td colspan="4" class="collapsing" style="padding: 0">
          <table class="ui celled table subtable">
            <thead>
              <tr>
                <th>Instansi</th>
                <th>Mulai</th>
                <th>Selesai</th>
              </tr>
            </thead>
            <tbody>
            <?php if (isset($_SESSION['register_4']['belajar_bahasa_jepang'])): ?>
            <?php foreach ($_SESSION['register_4']['belajar_bahasa_jepang'] as $val): ?>
              <tr>
                <td><?php echo $val['nama_instansi'] ?></td>
                <td><?php echo $val['tahun_masuk'] ?></td>
                <td><?php echo $val['tahun_selesai'] ?></td>
              </tr>
            <?php endforeach; ?>
            <?php else: ?>
              <tr>
                <td></td>
                <td></td>
                <td></td>
              </tr>
            <?php endif; ?>
            </tbody>
          </table>
        </td>
      </tr>
      <tr>
        <td colspan="4"><h4>Keluarga yang sedang berada di Jepang</h4></td>
      </tr>

      <tr>
        <td colspan="4" class="collapsing" style="padding: 0">
          <table class="ui celled table subtable">
            <thead>
              <tr>
                <th>Nama</th>
                <th>Hubungan</th>
                <th>Tgl Lahir</th>
                <th>kewarganegaraan</th>
                <th>Nama Sekolah / Perusahaan</th>
                <th>Status Visa</th>
              </tr>
            </thead>
            <tbody>
            <?php if (!empty($_SESSION['register_5']['keluarga_dijepang'])): ?>
            <?php foreach ($_SESSION['register_5']['keluarga_dijepang'] as $key => $value): ?>
              <tr>
                <td><?php echo $value['nama'] ?></td>
                <td><?php echo searchArray($value['hubungan'], $hubungan, 'kd_hubungan', 'hubungan') ?></td>
                <td><?php echo $value['tanggal_lahir'] ?></td>
                <td><?php echo $value['kewarganegaraan'] ?></td>
                <td><?php echo $value['perusahaan'] ?></td>
                <td><?php echo $value['status_visa'] ?></td>
              </tr>
            <?php endforeach; ?>
            <?php else: ?>
              <tr>
                <tr>
                  <td></td>
                  <td></td>
                  <td></td>
                  <td></td>
                  <td></td>
                  <td></td>
                </tr>
              </tr>
            <?php endif; ?>
            </tbody>
          </table>
        </td>
      </tr>

      <!-- RIWAYAT PENDIDIKAN -->
      <tr>
        <td colspan="4"><h4>Riwayat Pendidikan</h4></td>
      </tr>

      <tr>
        <td colspan="4" class="collapsing" style="padding: 0">
          <table class="ui celled table subtable">
            <thead>
              <tr>
                <th>Tahun Masuk</th>
                <th></th>
                <th>Tahun Selesai</th>
                <th>Nama Sekolah</th>
                <th>Jurusan</th>
              </tr>
            </thead>
            <tbody>
            <?php if (isset($_SESSION['register_4']['SD'])): ?>
            <tr>
              <td><?php echo $_SESSION['register_4']['SD']['tahun_masuk'] ?></td>
              <td>~</td>
              <td><?php echo $_SESSION['register_4']['SD']['tahun_selesai'] ?></td>
              <td><?php echo $_SESSION['register_4']['SD']['nama_sekolah'] ?></td>
              <td></td>
            </tr>
            <?php endif; ?>

            <?php if (isset($_SESSION['register_4']['SMP'])): ?>
            <tr>
              <td><?php echo $_SESSION['register_4']['SMP']['tahun_masuk'] ?></td>
              <td>~</td>
              <td><?php echo $_SESSION['register_4']['SMP']['tahun_selesai'] ?></td>
              <td><?php echo $_SESSION['register_4']['SMP']['nama_sekolah'] ?></td>
              <td></td>
            </tr>
            <?php endif; ?>

            <?php if (isset($_SESSION['register_4']['SMA'])): ?>
            <tr>
              <td><?php echo $_SESSION['register_4']['SMA']['tahun_masuk'] ?></td>
              <td>~</td>
              <td><?php echo $_SESSION['register_4']['SMA']['tahun_selesai'] ?></td>
              <td><?php echo $_SESSION['register_4']['SMA']['nama_sekolah'] ?></td>
              <td><?php echo $_SESSION['register_4']['SMA']['jurusan'] ?></td>

            </tr>
            <?php endif; ?>

            <?php if (isset($_SESSION['register_4']['sekolah_lanjutan'])): ?>
            <?php foreach ($_SESSION['register_4']['sekolah_lanjutan'] as $v): ?>
              <tr>
                <td><?php echo $v['tahun_masuk'] ?></td>
                <td>~</td>
                <td><?php echo $v['tahun_selesai'] ?></td>
                <td><?php echo $v['nama_instansi'] ?></td>
                <td><?php echo $v['jurusan'] ?></td>
              </tr>
            <?php endforeach; ?>
            <?php endif; ?>
            </tbody>
          </table>
        </td>
      </tr>

      <!-- RIWAYAT PEKERJAAN -->
      <tr>
        <td colspan="2"><h4>Riwayat Pekerjaan</h4></td>
        <td colspan="2"><h4>Jenis Kerja</h4></td>
      </tr>

      <tr>
        <td colspan="2"></td>
        <td colspan="2"></td>
      </tr>

      <!-- KELUARGA -->
      <tr>
        <td colspan="4"><h4>SUSUNAN KELUARGA</h4></td>
      </tr>

      <tr>
        <td colspan="4" class="collapsing" style="padding: 0">
          <table class="ui celled table">
            <thead>
              <tr>
                <th>Hubungan</th>
                <th>Nama</th>
                <th>Tgl Lahir</th>
                <th>Usia</th>
                <th>Pekerjaan</th>
                <th>Kontak</th>
              </tr>
            </thead>
            <tbody>
            <?php if(isset($_SESSION['register_5'])): ?>
              <tr>
                <td>AYAH</td>
                <td><?php echo $_SESSION['register_5']['ayah']['nama'] ?></td>
                <td><?php echo $_SESSION['register_5']['ayah']['tanggal_lahir'] ?></td>
                <td><?php echo (Date('Y') - substr($_SESSION['register_5']['ayah']['tanggal_lahir'], 0, 4)) ?></td>
                <td><?php echo $_SESSION['register_5']['ayah']['pekerjaan'] ?></td>
                <td><?php echo $_SESSION['register_5']['ayah']['no_telp'] ?></td>
              </tr>
              <tr>
                <td>IBU</td>
                <td><?php echo $_SESSION['register_5']['ibu']['nama'] ?></td>
                <td><?php echo $_SESSION['register_5']['ibu']['tanggal_lahir'] ?></td>
                <td><?php echo (Date('Y') - substr($_SESSION['register_5']['ibu']['tanggal_lahir'], 0, 4)) ?></td>
                <td><?php echo $_SESSION['register_5']['ibu']['pekerjaan'] ?></td>
                <td><?php echo $_SESSION['register_5']['ibu']['no_telp'] ?></td>
              </tr>
              <?php foreach ($_SESSION['register_5']['saudara_kandung'] as $key => $val): ?>
                <tr>
                  <td><?php echo searchArray($val['hubungan'], $hubungan, 'kd_hubungan', 'hubungan') ?></td>
                  <td><?php echo $val['nama'] ?></td>
                  <td><?php echo $val['tanggal_lahir'] ?></td>
                  <td><?php echo (Date('Y') - substr($val['tanggal_lahir'], 0, 4)) ?></td>
                  <td><?php echo $val['pekerjaan'] ?></td>
                  <td><?php echo $val['no_telp'] ?></td>
                </tr>
              <?php endforeach; ?>
              <?php if(isset($_SESSION['register_5']['keluarga_pribadi'])): ?>
                <?php foreach ($_SESSION['register_5']['keluarga_pribadi'] as $key => $val): ?>
                  <tr>
                    <td><?php echo searchArray($val['hubungan'], $hubungan, 'kd_hubungan', 'hubungan') ?></td>
                    <td><?php echo $val['nama'] ?></td>
                    <td><?php echo $val['tanggal_lahir'] ?></td>
                    <td><?php echo (Date('Y') - substr($val['tanggal_lahir'], 0, 4)) ?></td>
                    <td><?php echo $val['pekerjaan'] ?></td>
                    <td><?php echo $val['no_telp'] ?></td>
                  </tr>
                <?php endforeach; ?>
              <?php endif; ?>
            <?php endif; ?>
              <tr>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
              </tr>
            </tbody>
          </table>
        </td>
      </tr>

      <tr>
        <td><b>Tinggi Badan</b>  <span class="isian"><?php echo $_SESSION['register_1']['tinggi_badan']; ?></span></td>
        <td><b>Berat Badan</b> <span class="isian"><?php echo $_SESSION['register_1']['berat_badan']; ?></span></td>
        <td><b>Lingkar Pinggang</b> <span class="isian"><?php echo $_SESSION['register_1']['lingkar_pinggang']; ?></span></td>
        <td><b>Ukuran Sepatu</b> <span class="isian"><?php echo $_SESSION['register_1']['ukuran_sepatu']; ?></span></td>
      </tr>

      <tr>
        <td><b>Penglihatan (kiri/kanan)</b> <span class="isian">belum diperiksa</span></td>
        <td colspan="2"><b>Penglihatan dengan Kacamata (kiri/kanan)</b> <span class="isian">belum diperiksa</span></td>
        <td><b>Golongan Darah</b> <span class="isian"><?php echo $_SESSION['register_1']['golongan_darah']; ?></span></td>
      </tr>

      <tr>
        <td colspan="2"><b>Pelajaran yang disukai</b> <span class="isian"><?php echo $_SESSION['register_1']['pelajaran_favorit']; ?></span></td>
        <td>Hobi <span class="isian"><?php echo $_SESSION['register_1']['hobi']; ?></span></td>
        <td><b>Keterampilan / Skill</b> <span class="isian"><?php echo $_SESSION['register_1']['keterampilan']; ?></span></td>
      </tr>

      <tr>
        <td><b>Riwayat Penyakit</b> tidak ada</td>
        <td colspan="2"><b>Nama Penyakit</b></td>
        <td></td>
      </tr>

      <tr>
        <td><b>Merokok</b></td>
        <?php if($_SESSION['register_1']['merokok'] == 'YA'): ?>
          <td colspan="3">YA, <?php echo $_SESSION['register_1']['batang_rokok_per_hari'] ?> batang per hari.</td>
        <?php else: ?>
          <td colspan="3">Tidak</td>
        <?php endif; ?>
      </tr>

      <tr>
        <td><b>Minum Alkohol</b></td>
        <?php if($_SESSION['register_1']['minum_alkohol'] == 'YA'): ?>
          <td colspan="3">YA, <?php echo $_SESSION['register_1']['frekuensi_minum_alkohol_perbulan'] ?>kali per bulan.</td>
        <?php else: ?>
          <td colspan="3">Tidak</td>
        <?php endif; ?>
      </tr>
    </table>

    <button type="button" id="konfirmasi" class="ui icon button primary"><i class="ui icon save"></i> KONFIRMASI</button>
  </div>
</div>
