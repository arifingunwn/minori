<div class="ui segment very padded container">
  <?php echo form_open('', 'class="ui form"') ?>
    <h2 class="ui dividing header blue">RIWAYAT PENDIDIKAN</h2>
    <h4 class="ui horizontal divider header">FORMAL</h4>
    <h3 class="ui dividing header">SEKOLAH DASAR / SEDERAJAT</h3>

    <div class="fields">
      <div class="six wide field">
        <label>NAMA SEKOLAH</label>
        <input type="text" name="nama_sekolah_sd" value="<?php echo !empty($_SESSION['register_4']['SD']['nama_sekolah']) ? $_SESSION['register_4']['SD']['nama_sekolah'] : '' ?>" placeholder="Nama Sekolah">
      </div>

      <div class="two wide field">
        <label>BULAN MASUK</label>
        <select class="ui dropdown" name="bulan_masuk_sd">
        <?php
          if (isset($_SESSION['register_4']['SD']['bulan_masuk'])) {
            echo option_bulan($_SESSION['register_4']['SD']['bulan_masuk']);
          }
          else
          {
            echo option_bulan();
          }
        ?>
        </select>
      </div>

      <div class="two wide field">
        <label>TAHUN MASUK</label>
        <select class="ui dropdown" name="tahun_masuk_sd">
        <?php
          if(isset($_SESSION['register_4']['SD']['tahun_masuk']))
          {
            echo option_tahun(50, $_SESSION['register_4']['SD']['tahun_masuk']);
          }
          else
          {
            echo option_tahun();
          }
        ?>
        </select>
      </div>

      <div class="two wide field">
        <label>BULAN SELESAI</label>
        <select class="ui dropdown" name="bulan_selesai_sd">
        <?php
          if (isset($_SESSION['register_4']['SD']['bulan_selesai']))
          {
            echo option_bulan($_SESSION['register_4']['SD']['bulan_selesai']);
          }
          else
          {
              echo option_bulan();
          }
        ?>
        </select>
      </div>

      <div class="two wide field">
        <label>TAHUN SELESAI</label>
        <select class="ui dropdown" name="tahun_selesai_sd">
          <?php
            if(isset($_SESSION['register_4']['SD']['tahun_selesai']))
            {
              echo option_tahun(50, $_SESSION['register_4']['SD']['tahun_selesai']);
            }
            else
            {
              echo option_tahun();
            }
          ?>
        </select>
      </div>
    </div>

    <h3 class="ui dividing header">SEKOLAH MENENGAH PERTAMA / SLTP / SEDERAJAT</h3>
    <div class="fields">
      <div class="six wide field">
        <label>NAMA SEKOLAH</label>
        <input type="text" name="nama_sekolah_smp" value="<?php echo !empty($_SESSION['register_4']['SMP']['nama_sekolah']) ? $_SESSION['register_4']['SMP']['nama_sekolah'] : '' ?>" placeholder="Nama Sekolah SMP">
      </div>

      <div class="two wide field">
        <label>BULAN MASUK</label>
        <select class="ui dropdown" name="bulan_masuk_smp">
        <?php
          if (isset($_SESSION['register_4']['SMP']['bulan_masuk'])) {
            echo option_bulan($_SESSION['register_4']['SMP']['bulan_masuk']);
          }
          else
          {
            echo option_bulan();
          }
        ?>
        </select>
      </div>

      <div class="two wide field">
        <label>TAHUN MASUK</label>
        <select class="ui dropdown" name="tahun_masuk_smp">
          <?php
            if(isset($_SESSION['register_4']['SMP']['tahun_masuk']))
            {
              echo option_tahun(50, $_SESSION['register_4']['SMP']['tahun_masuk']);
            }
            else
            {
              echo option_tahun();
            }
          ?>
        </select>
      </div>

      <div class="two wide field">
        <label>BULAN SELESAI</label>
        <select class="ui dropdown" name="bulan_selesai_smp">
        <?php
          if (isset($_SESSION['register_4']['SMP']['bulan_selesai']))
          {
            echo option_bulan($_SESSION['register_4']['SMP']['bulan_selesai']);
          }
          else
          {
            echo option_bulan();
          }
        ?>
        </select>
      </div>

      <div class="two wide field">
        <label>TAHUN SELESAI</label>
        <select class="ui dropdown" name="tahun_selesai_smp">
          <?php
            if(isset($_SESSION['register_4']['SMP']['tahun_selesai']))
            {
              echo option_tahun(50, $_SESSION['register_4']['SMP']['tahun_selesai']);
            }
            else
            {
              echo option_tahun();
            }
          ?>
        </select>
      </div>
    </div>

    <h4 class="ui dividing header">SEKOLAH MENENGAH ATAS / SLTA / SEDERAJAT</h4>
    <div class="fields">
      <div class="eight wide field">
        <label>NAMA SEKOLAH</label>
        <input type="text" name="nama_sekolah_sma" value="<?php echo !empty($_SESSION['register_4']['SMA']['nama_sekolah']) ? $_SESSION['register_4']['SMA']['nama_sekolah'] : '' ?>" placeholder="Nama Sekolah SMA / SMK / Sederajat">
      </div>
      <div class="six wide field">
        <label>JURUSAN</label>
        <select class="ui dropdown" name="jurusan_sma">
        <?php foreach ($jurusan as $j): ?>
          <?php if($j['id_tingkatan_pendidikan'] == 'SMA'): ?>
          <option  <?php echo !empty($_SESSION['register_4']['SMA']['jurusan']) && $_SESSION['register_4']['SMA']['jurusan'] == $j['id_jurusan'] ? " selected " : "" ?>
            value="<?php echo $j['id_jurusan'] ?>"><?php echo $j['nama_jurusan'] ?>
          </option>
          <?php endif; ?>
        <?php endforeach; ?>
        </select>
      </div>
    </div>
    <div class="fields">
      <div class="two wide field">
        <label>BULAN MASUK</label>
        <select class="ui dropdown" name="bulan_masuk_sma">
        <?php
          if (isset($_SESSION['register_4']['SMA']['bulan_masuk'])) {
            echo option_bulan($_SESSION['register_4']['SMA']['bulan_masuk']);
          }
          else
          {
            echo option_bulan();
          }
        ?>
        </select>
      </div>

      <div class="two wide field">
        <label>TAHUN MASUK</label>
        <select class="ui dropdown" name="tahun_masuk_sma">
          <?php
            if(isset($_SESSION['register_4']['SMA']['tahun_masuk']))
            {
              echo option_tahun(50, $_SESSION['register_4']['SMA']['tahun_masuk']);
            }
            else
            {
              echo option_tahun();
            }
          ?>
        </select>
      </div>

      <div class="two wide field">
        <label>BULAN SELESAI</label>
        <select class="ui dropdown" name="bulan_selesai_sma">
        <?php
          if (isset($_SESSION['register_4']['SMA']['bulan_selesai']))
          {
            echo option_bulan($_SESSION['register_4']['SMA']['bulan_selesai']);
          }
          else
          {
            echo option_bulan();
          }
        ?>
        </select>
      </div>

      <div class="two wide field">
        <label>TAHUN SELESAI</label>
        <select class="ui dropdown" name="tahun_selesai_sma">
          <?php
            if(isset($_SESSION['register_4']['SMA']['tahun_selesai']))
            {
              echo option_tahun(50, $_SESSION['register_4']['SMA']['tahun_selesai']);
            }
            else
            {
              echo option_tahun();
            }
          ?>
        </select>
      </div>
    </div>

    <h4 class="ui dividing header">SEKOLAH LANJUTAN</h4>
    <div class="sekolah_lanjutan_parent">
    <?php if (!empty($_SESSION['register_4']['sekolah_lanjutan'])): ?>
    <?php $counter = 0; ?>
    <?php foreach ($_SESSION['register_4']['sekolah_lanjutan'] as $v): ?>
      <div class="sekolah_lanjutan_child">
        <div class="fields">
          <div class="six wide field">
            <label>NAMA INSTANSI</label>
            <input type="text" name="sekolah_lanjutan[<?php echo $counter; ?>][nama_instansi]" value="<?php echo $v['nama_instansi'] ?>" placeholder="Nama Instansi">
          </div>

          <div class="six wide field">
            <label>JURUSAN</label>
            <select class="ui dropdown" name="sekolah_lanjutan[<?php echo $counter; ?>][jurusan]">
            <?php foreach ($jurusan as $j): ?>
              <?php if($j['id_tingkatan_pendidikan'] == 'S1' || $j['id_tingkatan_pendidikan'] == 'S2'): ?>
              <option <?php echo $v['jurusan'] == $j['id_jurusan'] ? " selected " : "" ?>
                value="<?php echo $j['id_jurusan'] ?>"><?php echo $j['nama_jurusan'] ?>
              </option>
              <?php endif; ?>
            <?php endforeach; ?>
            </select>
          </div>
        </div>
        <div class="fields">
          <div class="two wide field">
            <label>BULAN MASUK</label>
            <select class="ui dropdown" name="sekolah_lanjutan[<?php echo $counter; ?>][bulan_masuk]">
              <?php
                if(isset($v['bulan_masuk']))
                {
                  echo option_bulan(50, $v['bulan_masuk']);
                }
                else
                {
                  echo option_tahun();
                }
              ?>
            </select>
          </div>

          <div class="two wide field">
            <label>TAHUN MASUK</label>
            <select class="ui dropdown" name="sekolah_lanjutan[<?php echo $counter; ?>][tahun_masuk]">
              <?php
                if(isset($v['tahun_masuk']))
                {
                  echo option_tahun(50, $v['tahun_masuk']);
                }
                else
                {
                  echo option_tahun();
                }
              ?>
            </select>
          </div>

          <div class="two wide field">
            <label>BULAN SELESAI</label>
            <select class="ui dropdown" name="sekolah_lanjutan[<?php echo $counter; ?>][bulan_selesai]">
              <?php
                if(isset($v['bulan_selesai']))
                {
                  echo option_bulan(50, $v['bulan_selesai']);
                }
                else
                {
                  echo option_tahun();
                }
              ?>
            </select>
          </div>

          <div class="two wide field">
            <label>TAHUN SELESAI</label>
            <select class="ui dropdown" name="sekolah_lanjutan[<?php echo $counter; ?>][tahun_selesai]">
              <?php
                if(isset($v['tahun_selesai']))
                {
                  echo option_tahun(50, $v['tahun_selesai']);
                }
                else
                {
                  echo option_tahun();
                }
              ?>
            </select>
          </div>

          <div class="four wide field">
            <label>TINGKATAN</label>
            <select class="ui dropdown" name="sekolah_lanjutan[<?php echo $counter; ?>][tingkatan]">
              <option value="D1" <?php echo ($v['tingkatan'] == 'D1' ? 'selected' : '') ?> >D1</option>
              <option value="D2" <?php echo ($v['tingkatan'] == 'D2' ? 'selected' : '') ?> >D2</option>
              <option value="D3" <?php echo ($v['tingkatan'] == 'D3' ? 'selected' : '') ?> >D3</option>
              <option value="S1" <?php echo ($v['tingkatan'] == 'S1' ? 'selected' : '') ?> >S1</option>
              <option value="S2" <?php echo ($v['tingkatan'] == 'S2' ? 'selected' : '') ?> >S2</option>
              <option value="S3" <?php echo ($v['tingkatan'] == 'S3' ? 'selected' : '') ?> >S3</option>
            </select>
          </div>
        </div>
      </div>
    <?php $counter++; ?>
    <?php endforeach; ?>
    <?php else: ?>
      <div class="sekolah_lanjutan_child">
        <div class="fields">
          <div class="six wide field">
            <label>NAMA INSTANSI</label>
            <input type="text" name="sekolah_lanjutan[0][nama_instansi]" value="" placeholder="Nama Instansi">
          </div>

          <div class="four wide field">
            <label>JURUSAN</label>
            <select class="ui dropdown" name="sekolah_lanjutan[0][jurusan]">
            <?php foreach ($jurusan as $j): ?>
              <?php if($j['id_tingkatan_pendidikan'] == 'S1' || $j['id_tingkatan_pendidikan'] == 'S2'): ?>
              <option
                value="<?php echo $j['id_jurusan'] ?>"><?php echo $j['nama_jurusan'] ?>
              </option>
              <?php endif; ?>
            <?php endforeach; ?>
            </select>
          </div>

          <div class="four wide field">
            <label>TINGKATAN</label>
            <select class="ui dropdown" name="sekolah_lanjutan[0][tingkatan]">
              <option value="D1">D1</option>
              <option value="D2">D2</option>
              <option value="D3">D3</option>
              <option value="S1">S1</option>
              <option value="S2">S2</option>
              <option value="S3">S3</option>
            </select>
          </div>
        </div>
        <div class="fields">
          <div class="two wide field">
            <label>BULAN MASUK</label>
            <select class="ui dropdown" name="sekolah_lanjutan[0][bulan_masuk]">
              <?php
                  echo option_bulan();
              ?>
            </select>
          </div>

          <div class="two wide field">
            <label>TAHUN MASUK</label>
            <select class="ui dropdown" name="sekolah_lanjutan[0][tahun_masuk]">
              <?php
                $tahun = Date("Y");
                for($i = $tahun; $i>=$tahun - 40; $i--)
                {
                  echo "<option value='{$i}'>{$i}</option>";
                }
              ?>
            </select>
          </div>

          <div class="two wide field">
            <label>BULAN SELESAI</label>
            <select class="ui dropdown" name="sekolah_lanjutan[0][bulan_selesai]">
              <?php
                  echo option_bulan();
              ?>
            </select>
          </div>

          <div class="two wide field">
            <label>TAHUN SELESAI</label>
            <select class="ui dropdown" name="sekolah_lanjutan[0][tahun_selesai]">
              <?php
                $tahun = Date("Y");
                for($i = $tahun; $i>=$tahun - 40; $i--)
                {
                  echo "<option value='{$i}'>{$i}</option>";
                }
              ?>
            </select>
          </div>
        </div>
      </div>
    <?php endif; ?>
    </div>
    <button type="button" id="btn_tambah_sekolah_lanjutan" class="ui icon button green" style="display: block; margin-bottom: 1.5em;"><i class="ui icon plus"></i> TAMBAH</button>

    <h4 class="ui horizontal divider header">RIWAYAT BELAJAR BAHASA JEPANG</h4>

    <div class="grouped fields riwayat_bahasa_jepang_parent">
    <?php if (!empty($_SESSION['register_4']['belajar_bahasa_jepang'])): ?>
    <?php $counter = 0; ?>
    <?php foreach ($_SESSION['register_4']['belajar_bahasa_jepang'] as $v): ?>
      <div class="fields belajar_bahasa_jepang_child">
        <div class="six wide field">
          <label>NAMA INSTANSI</label>
          <input type="text" name="belajar_bahasa_jepang[<?php echo $counter ?>][nama_instansi]" value="<?php echo $v['nama_instansi']?>" placeholder="Nama Instansi">
        </div>

        <div class="two wide field">
          <label>BULAN MASUK</label>
          <select class="ui dropdown" name="belajar_bahasa_jepang[<?php echo $counter ?>][bulan_masuk]">
            <?php
              if(isset($v['bulan_masuk']))
              {
                echo option_tahun(50, $v['bulan_masuk']);
              }
              else
              {
                echo option_tahun();
              }
            ?>
          </select>
        </div>

        <div class="two wide field">
          <label>TAHUN MASUK</label>
          <select class="ui dropdown" name="belajar_bahasa_jepang[<?php echo $counter ?>][tahun_masuk]">
            <?php
              if(isset($v['tahun_masuk']))
              {
                echo option_tahun(50, $v['tahun_masuk']);
              }
              else
              {
                echo option_tahun();
              }
            ?>
          </select>
        </div>

        <div class="two wide field">
          <label>BULAN SELESAI</label>
          <select class="ui dropdown" name="belajar_bahasa_jepang[<?php echo $counter ?>][bulan_selesai]">
            <?php
              if(isset($v['bulan_selesai']))
              {
                echo option_tahun(50, $v['bulan_selesai']);
              }
              else
              {
                echo option_tahun();
              }
            ?>
          </select>
        </div>

        <div class="two wide field">
          <label>TAHUN SELESAI</label>
          <select class="ui dropdown" name="belajar_bahasa_jepang[<?php echo $counter ?>][tahun_selesai]">
            <?php
              if(isset($v['tahun_selesai']))
              {
                echo option_tahun(50, $v['tahun_selesai']);
              }
              else
              {
                echo option_tahun();
              }
            ?>
          </select>
        </div>
      </div>
    <?php $counter++; ?>
    <?php endforeach; ?>
    <?php else: ?>
      <div class="fields belajar_bahasa_jepang_child">
        <div class="six wide field">
          <label>NAMA INSTANSI</label>
          <input type="text" name="belajar_bahasa_jepang[0][nama_instansi]" value="" placeholder="Nama Instansi">
        </div>

        <div class="two wide field">
          <label>BULAN MASUK</label>
          <select class="ui dropdown" name="belajar_bahasa_jepang[0][bulan_masuk]">
            <?php
                echo option_tahun();
            ?>
          </select>
        </div>

        <div class="two wide field">
          <label>TAHUN MASUK</label>
          <select class="ui dropdown" name="belajar_bahasa_jepang[0][tahun_masuk]">
            <?php
              $tahun = Date("Y");
              for($i = $tahun; $i>=$tahun - 40; $i--)
              {
                echo "<option value='{$i}'>{$i}</option>";
              }
            ?>
          </select>
        </div>

        <div class="two wide field">
          <label>BULAN SELESAI</label>
          <select class="ui dropdown" name="belajar_bahasa_jepang[0][bulan_selesai]">
            <?php
                echo option_tahun();
            ?>
          </select>
        </div>

        <div class="two wide field">
          <label>TAHUN SELESAI</label>
          <select class="ui dropdown" name="belajar_bahasa_jepang[0][tahun_selesai]">
            <?php
              $tahun = Date("Y");
              for($i = $tahun; $i>=$tahun - 40; $i--)
              {
                echo "<option value='{$i}'>{$i}</option>";
              }
            ?>
          </select>
        </div>
      </div>
    <?php endif; ?>
    </div>
    <button type="button" class="ui icon button green" id="btn_tambah_belajar_bahasa_jepang" style="display: block; margin-bottom: 1.5em;"><i class="ui icon plus"></i> TAMBAH</button>
    <button type="submit" name="pendidikan" class="ui icon button primary"><i class="ui icon arrow right"></i> SELANJUTNYA</button>
  </form>
</div>
