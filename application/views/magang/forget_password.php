<div class="ui very padded segment fluid container" style="margin-top: 7em">
  <div class="ui segment text container">
    <?php if(!empty($message)): ?>
    <div class="message green">
      <?php echo $message ?>
    </div>
    <?php endif; ?>
    <h2 class="ui dividing blue header">RESET PASSWORD</h2>

    <?php echo form_open('', 'class="ui form"') ?>
      <div class="four field">
        <label>E-mail</label>
        <input type="email" name="email" placeholder="Email">
      </div>
      <button type="submit" name="reset" class="ui icon primary button">Reset</button>
    </form>
  </div>
</div>
