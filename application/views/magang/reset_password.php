<div class="ui very padded segment fluid container" style="margin-top: 7em">
  <?php if (!empty($error)): ?>
  <div class="ui message red">
    <?php echo $error; ?>
  </div>
  <?php endif; ?>
  <div class="ui segment text container">
    <h2 class="ui dividing blue header">RESET PASSWORD</h2>

    <?php echo form_open('', 'class="ui form" id="reset_password"') ?>
      <input type="hidden" name="key1" value="<?php echo $key1; ?>">
      <input type="hidden" name="key2" value="<?php echo $key2; ?>">
      <div class="four field">
        <label>New Password</label>
        <input type="password" name="new_password">
      </div>

      <div class="four field">
        <label>New Password</label>
        <input type="password" name="new_password_confirmation">
      </div>

      <button type="submit" name="reset" class="ui icon submit primary button"><i class="ui icon save"></i> SAVE</button>
    </form>
  </div>
</div>
