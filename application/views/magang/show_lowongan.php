<div class="ui segment very padded container" style="margin-top: 8em">
  <h1 class="ui dividing header"><?php echo $lowongan[0]['judul'] ?></h1>
  <?php if(empty($sudah_pernah_melamar->kd_pemagang)): ?>
    <?php echo form_open('') ?>
  <a href="#" class="ui icon button orange" id="btn_lamar"
    data-lowongan="<?php echo $lowongan[0]['kd'] ?>"
    data-token="<?php echo $this->security->get_csrf_hash()?>"
    >LAMAR SEKARANG</a>
  <?php else: ?>
  <a href="#" class="ui icon button">SUDAH TERDAFTAR</a>
  <?php endif; ?>
  <div class="ui padded segment">
    <p>
      <?php echo nl2br($lowongan[0]['text']) ?>
    </p>
  </div>
</div>
