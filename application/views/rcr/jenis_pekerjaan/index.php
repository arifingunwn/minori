<h2 class="ui dividing blue header">PEKERJAAN</h2>
<?php echo show_flash_data() ?>
<button type="button" class="ui icon green button" id="btn_tambah_pekerjaan"><i class="ui icon plus"></i> TAMBAH</button>

<div class="pekerjaan-tree">
  <ul>
    <?php foreach ($jenis_pekerjaan as $jp): ?>
      <li>
        <?php echo $jp['jenis_pekerjaan'] ?>
        <ul>
        <?php foreach ($pekerjaan as $p): ?>
          <?php if ($p['kd_jenis_pekerjaan'] == $jp['kd_jenis_pekerjaan']): ?>
            <li data-pekerjaan="<?php echo $p['kd_pekerjaan'] ?>"
                data-jp="<?php echo $p['kd_jenis_pekerjaan'] ?>"
                data-nama_pekerjaan="<?php echo $p['pekerjaan'] ?>"
                data-nama_pekerjaan_jepang="<?php echo $p['pekerjaan_jp'] ?>"
                class="btn_edit_pekerjaan"><?php echo $p['pekerjaan'] . ' / ' . $p['pekerjaan_jp'] ?> </li>
          <?php endif; ?>
        <?php endforeach; ?>
        </ul>
      </li>
    <?php endforeach; ?>
  </ul>
</div>

<!-- <table class="ui compact table celled data-table">
  <thead>
    <tr>
      <th>Jenis Pekerjaan</th>
      <th>Pekerjaan</th>
      <th>Pekerjaan (Jepang)</th>
      <th></th>
    </tr>
  </thead>
  <tbody>
    <?php foreach ($pekerjaan as $p): ?>
    <tr>
      <td><?php echo $p['jenis_pekerjaan'] ?></td>
      <td><?php echo $p['pekerjaan'] ?></td>
      <td><?php echo $p['pekerjaan_jp'] ?></td>
      <td>
        <a href="#" class="ui icon orange mini button btn_edit_pekerjaan" data-pekerjaan="<?php echo $p['kd_pekerjaan'] ?>" data-jp="<?php echo $p['kd_jenis_pekerjaan'] ?>"><i class="ui icon pencil"></i> Edit</a>
      </td>
    </tr>
    <?php endforeach; ?>
  </tbody>
</table> -->

<div class="ui small modal" id="pekerjaan">
  <div class="header">

  </div>

  <div class="content">
    <?php echo form_open('Jenis_Pekerjaan/add', 'class="ui form"') ?>
      <input type="hidden" name="kd_pekerjaan">
      <div class="field">
        <label>Nama Pekerjaan</label>
        <input type="text" name="pekerjaan" placeholder="Nama Pekerjaan">
      </div>

      <div class="field">
        <label>Nama Pekerjaan (Jepang)</label>
        <input type="text" name="pekerjaan_jp" placeholder="Nama Pekerjaan Bahasa Jepang">
      </div>

      <div class="field">
        <label>Jenis Pekerjaan</label>
        <select class="ui dropdown search" name="kd_jenis_pekerjaan">
        <?php foreach ($jenis_pekerjaan as $jp): ?>
          <option value="<?php echo $jp['kd_jenis_pekerjaan'] ?>"><?php echo $jp['jenis_pekerjaan'] ?></option>
        <?php endforeach; ?>
        </select>
      </div>

      <button type="submit" class="ui icon button primary"><i class="ui icon save"></i> SIMPAN</button>
    </form>
  </div>
</div>
