<h1 class="ui header">DASHBOARD</h1>

<div class="ui statistic">
  <div class="value">
    <a target="_blank" href="<?php echo site_url('rcr/pemagang/index/bulan_ini') ?>"><?php echo $new_pemagang == NULL ? "0" : $new_pemagang->jumlah ?></a>
  </div>
  <div class="label">
    Pendaftar baru bulan ini
  </div>
</div>

<div class="ui statistic">
  <div class="value">
    <a target="_blank" href="<?php echo site_url('rcr/pemagang/index/bulan_ini') ?>"><?php echo $new_pemagang_per_day == NULL ? "0" : $new_pemagang_per_day->jumlah ?></a>
  </div>
  <div class="label">
    Pendaftar baru hari ini
  </div>
</div>
