<h1 class="ui dividing header">Pilih Pemagang untuk Lowongan :</h1>
<h3>
  <?php echo $lowongan->judul ?>
</h3>

<h4 class="ui top attached header">Pemagang yang terdaftar :</h4>
<div class="ui bottom attached segment" style="height: 250px; overflow: scroll">

<?php $inline_kd_pemagang = ''; ?>
  <?php echo form_open('lowongan/add_recruit', 'class="ui form" id="recruit"') ?>
    <input type="hidden" name="kd_lowongan" value="<?php echo isset($lowongan->kd) ? $lowongan->kd : '' ?>">
    <table class="ui very basic compact table list_pemagang_terpilih">
    <?php if (!empty($list_pemagang_terpilih)): ?>
    <?php $counter = 0; ?>
    <?php foreach ($list_pemagang_terpilih as $v): ?>
    <?php $inline_kd_pemagang .= $v['kd_pemagang'].',' ?>
      <tr>
        <td><?php echo ++$counter; ?>
          <input type="hidden" name="kd_pemagang[]" value="<?php echo $v['kd_pemagang'] ?>">
        </td>
        <td><?php echo $v['kd_pemagang'] ?></td>
        <td><?php echo $v['nama_pem'] ?></td>
        <td>
          <a href="<?php echo site_url('rcr/pemagang/show/').$v['kd_pemagang'] ?>" target="_blank" class="ui mini button icon blue">Detail</a>
          <a href="#" class="ui mini button icon red" onclick="$(this).parent().parent().remove()">Hapus</a>
        </td>
      </tr>
    <?php endforeach; ?>
    <?php endif; ?>
    </table>
  </form>
</div>
<button type="button" class="ui icon button mini primary" id="btn_recruit"><i class="ui icon save"></i> SIMPAN</button>
<a class="ui icon button mini yellow" href="<?php echo site_url('rcr/lowongan/test/'.$lowongan->kd) ?>"><i class="ui icon arrow right"></i> TEST</a>
<?php echo form_open('pemagang/export_to_excel/', 'style="display:inline"') ?>
  <input type="hidden" name="kd_pemagang" id="print" value="<?php echo rtrim($inline_kd_pemagang, ',') ?>">
  <a target="_blank" class="ui icon button mini green" href="<?php echo base_url('report/recruited_pemagang/' . $lowongan->kd) ?>">REPORT</a>
  <button type="submit" class="ui icon green mini button" name="button"><i class="ui icon download"></i> EXPORT TERDAFTAR TO EXCEL</button>
</form>

<h4 class="ui header">FILTER :</h4>
<div class="ui segment">
  <div class="ui mini form">
    <div class="six fields">
      <div class="field">
        <label>Kode Pemagang</label>
        <input type="text" id="filter_kd_pemagang" placeholder="Kode Pemagang">
      </div>
      <div class="field">
        <label>Nama</label>
        <input type="text" id="filter_nama" placeholder="Nama">
      </div>
      <div class="field">
        <label>Provinsi</label>
        <select class="ui dropdown search" id="filter_provinsi">
          <option value="TIDAK">--PROVINSI--</option>
          <?php foreach ($provinsi as $prov): ?>
          <option value="<?php echo $prov['kd_provinsi'] ?>"><?php echo $prov['provinsi'] ?></option>
          <?php endforeach; ?>
        </select>
      </div>
      <div class="field">
        <label>Umur</label>
        <input type="text" id="filter_umur" placeholder="Umur">
      </div>
      <div class="field">
        <label>E-Mail</label>
        <input type="text" id="filter_email" placeholder="E-mail">
      </div>
      <div class="field">
        <label>Nomor Handphone</label>
        <input type="text" id="filter_no_hp" placeholder="No. Handphone">
      </div>
    </div>

    <div class="six fields">
      <div class="field">
        <label>Pengalaman Kerja</label>
        <select class="ui dropdown search" id="select_pengalaman_kerja" name="pengalaman_kerja">
          <option value="TIDAK">--TIDAK--</option>
          <?php
          $jenis_pekerjaan = '';
          foreach ($pekerjaan as $val):
            if($val == 1)
            {
              echo "<optgroup label={$val['jenis_pekerjaan']}>";
            }

            if($jenis_pekerjaan != $val['jenis_pekerjaan'] && $val > 0)
            {
              echo "</optgroup><optgroup label={$val['jenis_pekerjaan']}>";
              $jenis_pekerjaan = $val['jenis_pekerjaan'];
            }
            else {
              echo "<option value=\"{$val['kd_pekerjaan']}\">" . $val['pekerjaan'] . "</option>";
            }
          ?>
          <?php endforeach; ?>
            </optgroup>
        </select>
      </div>
      <div class="field">
        <label>Pendidikan</label>
        <select class="ui dropdown search" id="pendidikan">
          <option value="TIDAK">--TIDAK--</option>
          <?php foreach ($tingkat_pendidikan as $val): ?>
          <option value="<?php echo $val['tingkat'] ?>"><?php echo $val['nama'] ?></option>
          <?php endforeach; ?>
        </select>
      </div>
      <div class="field">
        <label>Jurusan Pendidikan</label>
        <select class="ui dropdown search" id="filter_jurusan_pendidikan">
          <option value="TIDAK">--TIDAK--</option>
          <?php foreach ($jurusan as $jurus): ?>
          <option value="<?php echo $jurus['id_jurusan'] ?>"><?php echo $jurus['nama_jurusan'] ?></option>
          <?php endforeach; ?>
        </select>
      </div>

      <div class="field">
        <label>Pengalaman Kerja Custom</label>
        <input type="text" id="filter_pengalaman_kerja_custom">
      </div>
    </div>

    <div class="fields">
      <div class="inline field">
        <div class="ui checkbox">
          <input type="checkbox" id="bisa_bahasa_jepang" value="YA">
          <label for="bisa_bahasa_jepang">Bisa Bahasa Jepang</label>
        </div>
      </div>
      <div class="inline field">
        <div class="ui checkbox">
          <input type="checkbox" id="pernah_ke_jepang" value="YA">
          <label for="pernah_ke_jepang">PERNAH KE JEPANG</label>
        </div>
      </div>
    </div>

    <div class="fields">
      <div class="inline field">
        <div class="ui checkbox">
          <input type="checkbox" id="checkbox_pria">
          <label>PRIA</label>
        </div>
        <div class="ui checkbox">
          <input type="checkbox" id="checkbox_wanita">
          <label>WANITA</label>
        </div>
      </div>
    </div>
  </div>
  <button type="button" class="ui icon mini button primary" id="btn_search_filter"><i class="ui icon filter"></i> SEARCH</button>
  <button type="button" class="ui icon mini button red"><i class="ui icon x"></i> CLEAR FILTER</button>
  <button type="button" id="print_pemagang" class="ui icon mini button green"><i class="ui icon print"></i> EXPORT PEMAGANG</button>
  <?php echo form_open('pemagang/export_to_excel', 'id="export_to_excel"') ?>
    <input type="hidden" name="limit"/>
    <input type="hidden" name="page"/>
    <input type="hidden" name="kd_pemagang" value="">
  </form>
</div>

<h3 class="ui blue header">PEMAGANG</h3>
<table class="data-table-ssp ui celled compact selectable table" id="pilih_pemagang">
  <thead>
    <th></th>
    <th>Kode</th>
    <th>Nama</th>
    <th>Provinsi</th>
    <th>Umur</th>
    <th>Tinggi Badan</th>
    <th>Berat Badan</th>
    <th>E-Mail</th>
    <th>Pendidikan Terakhir</th>
    <th>Jenis Kelamin</th>
    <th>Nomor Handphone</th>
    <th>Pengalaman Kerja</th>
    <th></th>
  </thead>
</table>

<script type="text/javascript">
  var table;
  var jenis_kelamin = 'ALL';
  var pernah_ke_jepang = [];
  $(document).ready(function(){
    table = $('.data-table-ssp').DataTable({
      "processing": true,
      "serverSide": true,
      "order": [],
      "searching": false,
      "ajax": {
        "url": "<?php echo site_url('pemagang/server_side_ajax') ?>",
        "type": "POST",
        "data": function (d){
          d.bisa_bahasa_jepang = $('input#bisa_bahasa_jepang').is(':checked'),
          d.pernah_ke_jepang = $('input#pernah_ke_jepang').is(':checked'),
          d.pendidikan = $('select#pendidikan').val(),
          d.jenis_kelamin = jenis_kelamin,
          d.pengalaman_kerja = $('#select_pengalaman_kerja').val(),
          d.pengalaman_kerja_custom = $('input#pengalaman_kerja_custom').val(),
          d.kode_pemagang = $('input#filter_kd_pemagang').val(),
          d.nama = $('input#filter_nama').val(),
          d.provinsi = $('select#filter_provinsi').val(),
          d.umur = $('input#filter_umur').val(),
          d.email = $('input#filter_email').val(),
          d.no_handphone = $('input#filter_no_hp').val(),
          d.jurusan_pendidikan = $('input#filter_jurusan_pendidikan').val(),
          d.recruit = 1
        },
        "dataSrc": function(json)
        {
          $.each(json.pernah_ke_jepang, function(k, v){
            pernah_ke_jepang.push(v);
          });
          return json.data;
        }
      },

      "columnDefs": [
      {
        "targets": [0],
        "orderable": false
        }
      ],
      "fnRowCallback": function(row, data, dataIndex)
      {
        if(pernah_ke_jepang.indexOf(data[1]) >= 0)
        {
          $(row).css("background-color", "pink");
          $('td:eq(2)', row).html(data[2] + "<i class='jp flag'></i>");
        }
      }
    });
  });
</script>
