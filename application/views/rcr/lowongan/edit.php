<h1 class="ui dividing header">EDIT LOWONGAN</h1>
<div class="ui container segment">
  <?php echo form_open('lowongan/edit', 'class="ui form"') ?>
    <input type="hidden" name="kd_lowongan" value="<?php echo $lowongan->kd; ?>">
    <div class="eight wide field required">
      <label>Judul Lowongan</label>
      <input type="text" name="judul_lowongan" value="<?php echo $lowongan->judul; ?>" placeholder="Judul Lowongan">
    </div>

    <div class="eight wide field required">
      <label>Kategori</label>
      <select class="ui search" name="kd_kategori">
        <?php
        $jenis_pekerjaan = '';
        foreach ($category as $val):
          if($val == 1)
          {
            echo "<optgroup label={$val['jenis_pekerjaan']}>";
          }

          if($jenis_pekerjaan != $val['jenis_pekerjaan'] && $val > 0)
          {
            echo "</optgroup><optgroup label={$val['jenis_pekerjaan']}>";
            $jenis_pekerjaan = $val['jenis_pekerjaan'];
          }
          else {
            echo "<option " . ($val['kd_pekerjaan'] == $lowongan->kd_kategori ? " selected " : " ") . " value=\"{$val['kd_pekerjaan']}\">" . $val['pekerjaan'] . "</option>";
          }
        ?>
        <?php endforeach; ?>
          </optgroup>
      </select>
    </div>

    <div class="wide field">
      <label>Deskripsi</label>
      <textarea name="descripsi_lowongan" rows="8" cols="80"><?php echo $lowongan->text ?></textarea>
    </div>

    <div class="eight wide field required">
      <label>Awal</label>
      <div class="ui calendar" id="datepicker1" class="datepicker">
        <div class="ui input left icon">
          <i class="calendar icon"></i>
          <input type="text" name="awal_lowongan" placeholder="Date" value="<?php echo $lowongan->awal ?>">
        </div>
      </div>
    </div>

    <div class="eight wide field required">
      <label>Akhir</label>
      <div class="ui calendar" id="datepicker2" class="datepicker">
        <div class="ui input left icon">
          <i class="calendar icon"></i>
          <input type="text" name="akhir_lowongan" placeholder="Date" value="<?php echo $lowongan->akhir ?>">
        </div>
      </div>
    </div>

    <button type="submit" class="ui button icon primary" name="submit_lowongan"> <i class="ui icon save"></i> SIMPAN</button>
  </form>
</div>



<script type="text/javascript">
  $(document).ready(function(){
    tinymce.init({
      selector: 'textarea',
      height: 200,
      menubar: false,
      plugins: [
        'advlist autolink lists link image charmap print preview anchor',
        'searchreplace visualblocks code fullscreen',
        'insertdatetime media table contextmenu paste code'
      ],
      toolbar: 'undo redo | insert | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image',
      content_css: '//www.tinymce.com/css/codepen.min.css'
    });
  });
</script>
