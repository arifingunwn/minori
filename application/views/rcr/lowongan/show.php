<h2 class="ui dividing blue header">DETAIL LOWONGAN</h2>
<div class="ui grid">
  <div class="eight wide column">
    <div class="ui segment container">
      <table class="ui very basic table">
        <tr>
          <th>Judul</th>
          <td><?php echo $data[0]['judul'] ?></td>
        </tr>
        <tr>
          <th>Kategori</th>
          <td><?php echo $data[0]['pekerjaan'] ?></td>
        </tr>
      </table>
      <a target="_blank" href="<?php echo site_url('rcr/lowongan/edit/').$data[0]['kd'] ?>" class="ui icon orange mini button"><i class="ui icon pencil"></i> EDIT</a>
      <a target="_blank" href="" class="ui icon black mini button"><i class="ui icon stop"></i> NONAKTIFKAN</a>
      <a target="_blank" class="ui icon mini button green" style="float: right" href="<?php echo site_url('rcr/lowongan/recruit/').$data[0]['kd'] ?>"><i class="ui icon plus"></i> RECRUIT</a>
    </div>
    <div class="ui segment container">
      <?php echo $data[0]['text']; ?>
    </div>
  </div>
  <div class="eight wide column">
    <div class="ui segment">
      <a href="<?php echo site_url('rcr/lowongan/nilai/'.$data[0]['kd']) ?>" class="ui icon yellow mini button">NILAI</a>
        <a target="_blank" class="ui icon button mini green" href="<?php echo base_url('report/hasil_test_pemagang/' . $data[0]['kd']) ?>">REPORT</a>
      <!-- <button type="button" name="button" id="btn_edit_nilai" class="ui icon button orange mini"><i class="ui icon pencil"></i> EDIT</button>
      <button type="button" name="button" id="btn_save_nilai" style="display: none" class="ui icon button orange mini"><i class="ui icon save"></i> SIMPAN</button> -->
      <table class="ui sortable compact table">
        <thead>
          <tr>
            <th>No.</th>
            <th>Kode</th>
            <th>Nama</th>
            <th>Nilai</th>
            <th>Keterangan</th>
          </tr>
        </thead>
        <tbody>
        <?php if (!empty($data)): ?>
        <?php $counter = 0; ?>
        <?php foreach ($data as $val): ?>
          <tr>
            <td><?php echo ++$counter; ?></td>
            <td><?php echo $val['kd_pemagang'] ?></td>
            <td><?php echo $val['nama_pem'] ?></td>
            <td>
              <?php echo $val['nilai']; ?>
            </td>
            <td>
              <?php echo $val['keterangan'] ?>
              <!-- <a target="_blank" href="<?php echo site_url('rcr/pemagang/show/').$val['kd_pemagang'] ?>" class="ui icon mini button primary"><i class="ui icon search"></i> Lihat</a>
              <button type="button" class="ui icon mini red button btn_hapus_recruit" data-url="<?php echo site_url('rcr/lowongan/remove_recruit') ?>" data-pemagang="<?php echo $val['kd_pemagang'] ?>" data-lowongan="<?php echo $val['kd'] ?>" ><i class="ui icon x"></i> HAPUS</button> -->
            </td>
          </tr>
        <?php endforeach; ?>
        </tbody>
      </table>
    </div>
  </div>
</div>
<?php endif; ?>
