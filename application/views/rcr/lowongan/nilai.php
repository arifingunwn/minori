<h2 class="ui dividing blue header">NILAI TEST PENDAFTAR</h2>

<div class="ui segment">
  <table class="ui border table data-table">
    <thead>
      <tr>
        <th>KODE</th>
        <th>NAMA</th>
        <th>NILAI</th>
        <th>KETERANGAN</th>
        <th></th>
      </tr>
    </thead>
    <tbody>
    <?php foreach ($nilai as $n): ?>
      <tr>
        <td><?php echo $n['kd_pemagang'] ?></td>
        <td><?php echo $n['nama_pem'] ?></td>
        <td><?php echo $n['nilai'] ?></td>
        <td><?php echo $n['keterangan'] ?></td>
        <td><button type="button" data-kd="<?php echo $n['kd_pemagang'] ?>" data-nilai="<?php echo $n['nilai'] ?>" class="ui icon primary mini button edit_nilai">EDIT</button></td>
      </tr>
    <?php endforeach; ?>
    </tbody>
  </table>
</div>

<div class="ui small modal">
  <div class="header"></div>
  <div class="content">
    <?php echo form_open('', 'class="ui form" id="form_nilai"') ?>
      <input type="hidden" name="kd_pemagang" value="">
      <div class="field">
        <label>NILAI</label>
        <select class="ui dropdown" name="nilai">
          <option value="S">S</option>
          <option value="A">A</option>
          <option value="B">B</option>
          <option value="K">K</option>
        </select>
      </div>
      <div class="field">
        <label>KETERANGAN</label>
        <textarea name="keterangan" rows="3" form="form_nilai"></textarea>
      </div>
      <button type="submit" class="ui icon primary button"><i class="ui icon save"></i> SAVE</button>
    </form>
  </div>
</div>
