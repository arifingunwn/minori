<h1 class="ui dividing header">LOWONGAN</h1>
<?php echo show_flash_data() ?>
<a style="margin-bottom: 2em" href="<?php echo site_url('rcr/lowongan/add'); ?>" class="ui icon button"><i class="ui icon plus"></i> TAMBAH</a>
<table class="data-table ui celled compact table">
  <thead>
    <th>Kode</th>
    <th class="four wide">Judul</th>
    <th>start</th>
    <th>end</th>
    <th class="six wide"></th>
  </thead>
  <tbody>
  <?php foreach ($lowongan as $val): ?>
    <tr>
      <td><?php echo $val['kd'] ?></td>
      <td><?php echo $val['judul'] ?></td>
      <td><?php echo $val['awal'] ?></td>
      <td><?php echo $val['akhir'] ?></td>
      <td>
        <?php if(!empty($_SESSION['authorization']['LOWONGAN'][0]['can_read'])): ?>
        <a target="_blank" href="<?php echo site_url('rcr/lowongan/show/').$val['kd'] ?>" class="ui icon mini button blue"><i class="ui icon search"></i> Lihat</a>
        <?php endif; ?>
        <a target="_blank" href="<?php echo site_url('rcr/lowongan/recruit/').$val['kd'] ?>" class="ui icon mini button green"><i class="ui icon add user"></i> Recruit</a>
        <a target="_blank" href="<?php echo site_url('rcr/lowongan/test/').$val['kd'] ?>" class="ui icon mini button brown"><i class="ui icon add user"></i> Test</a>
        <a target="_blank" href="<?php echo site_url('rcr/lowongan/nilai/').$val['kd'] ?>" class="ui icon mini button yellow"><i class="ui icon add user"></i> Nilai</a>
        <?php if(!empty($_SESSION['authorization']['LOWONGAN'][0]['can_update'])): ?>
        <a target="_blank" href="<?php echo site_url('rcr/lowongan/edit/').$val['kd'] ?>" class="ui icon mini button orange"><i class="ui icon pencil"></i></a>
        <?php endif; ?>
        <?php if(!empty($_SESSION['authorization']['LOWONGAN'][0]['can_delete'])): ?>
          <?php if($val['status'] == 'T'): ?>
            <a target="_blank" href="<?php echo site_url('lowongan/nonaktif/').$val['kd'] ?>" class="ui icon mini button black link_confirmation" onclick='confirmDelete()'><i class="ui icon delete"></i> Nonaktifkan</a>
          <?php else: ?>
            <a target="_blank" href="<?php echo site_url('lowongan/aktif/').$val['kd'] ?>" class="ui icon mini button green link_confirmation" onclick='confirmDelete()'><i class="ui icon delete"></i> Aktifkan</a>
          <?php endif; ?>
        <?php endif; ?>
      </td>
    </tr>
  <?php endforeach; ?>
  </tbody>
</table>
