<h1 class="ui dividing header">JURUSAN PENDIDIKAN</h1>

<?php echo show_flash_data() ?>

<table class="ui celled table data-table">
  <thead>
    <th>ID</th>
    <th>Nama Jurusan</th>
    <th>Nama Jurusan Jepang</th>
    <th>Tingkatan</th>
    <th></th>
  </thead>

  <tbody>
  <?php foreach ($jurusan as $val): ?>
    <tr>
      <td><?php echo $val['id_jurusan'] ?></td>
      <td><?php echo $val['nama_jurusan'] ?></td>
      <td><?php echo $val['nama_jurusan_jp'] ?></td>
      <td><?php echo $val['nama'] ?></td>
      <td>
        <?php if(!empty($_SESSION['authorization']['JURUSPEND'][0]['can_update'])): ?>
        <button type="button" class="ui icon button mini orange btn_edit_jurusan"
          data-id="<?php echo $val['id_jurusan'] ?>"
          data-nama="<?php echo $val['nama_jurusan'] ?>"
          data-tp="<?php echo $val['id_tingkatan_pendidikan'] ?>"
          data-jp="<?php echo $val['nama_jurusan_jp'] ?>"
          ><i class="ui icon pencil"></i> EDIT</button>
          <?php endif; ?>
      </td>
    </tr>
  <?php endforeach; ?>
  </tbody>
</table>

<?php if(!empty($_SESSION['authorization']['JURUSPEND'][0]['can_create'])): ?>
<button type="button" class="ui icon button green" id="btn_tambah_jurusan"><i class="ui icon plus"></i> TAMBAH</button>
<?php endif; ?>
<div class="ui modal">
  <div class="header">

  </div>

  <div class="content">
    <?php echo form_open('jurusan/process', 'class="ui form" id="jurusan_pendidikan"') ?>
      <div class="four wide field">
        <label>Nama Jurusan</label>
        <input type="text" name="nama_jurusan">
      </div>

      <div class="four wide field">
        <label>Nama Jurusan Jepang</label>
        <input type="text" name="nama_jurusan_jp">
      </div>

      <div class="four wide field">
        <label>Tingkat</label>
        <select class="ui search dropdown" name="tingkat_pendidikan">
        <?php foreach ($tingkat_pendidikan as $val): ?>
          <option value="<?php echo $val['id'] ?>"><?php echo $val['nama'] ?></option>
        <?php endforeach; ?>
        </select>
      </div>
      <input type="hidden" name="type" value="ADD">
      <input type="hidden" name="id_jurusan" value="">
      <button type="submit" class="ui icon button primary submit"><i class="ui icon save"></i> SIMPAN</button>
    </form>
  </div>
</div>
