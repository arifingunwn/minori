<h2 class="ui dividing header"><?php echo $pengumuman->Judul ?></h2>
<b><i><?php echo $pengumuman->tanggal ?><i></b><br/>
<div class="ui teal tag label">
  <?php echo $pengumuman->penerbit ?>
</div>
<br/>
<a style="margin-top: 2em;" href="<?php echo site_url('rcr/pengumuman/edit/').$pengumuman->kd ?>" class="ui icon mini button orange"><i class="ui icon pencil"></i> Edit</a>
<div class="ui text segment">
  <?php echo nl2br($pengumuman->text); ?>
</div>
