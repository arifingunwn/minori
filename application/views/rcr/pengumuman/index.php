<h2 class="ui dividing blue header">PENGUMUMAN</h2>

<?php echo show_flash_data() ?>
<table class="ui celled compact table">
  <thead>
    <tr>
      <th>ID</th>
      <th>JUDUL</th>
      <th>TANGGAL</th>
      <th></th>
    </tr>
  </thead>
  <tbody>
  <?php foreach ($pengumuman as $p): ?>
    <tr>
      <td><?php echo $p['kd'] ?></td>
      <td><?php echo $p['Judul'] ?></td>
      <td><?php echo $p['tanggal'] ?></td>
      <td>
        <?php if(!empty($_SESSION['authorization']['PENGUMUMAN'][0]['can_read'])): ?>
        <a href="<?php echo site_url('rcr/pengumuman/show/').$p['kd'] ?>" class="ui blue mini button icon " ><i class="ui icon search"></i> Lihat</a>
        <?php endif ?>
        <?php if(!empty($_SESSION['authorization']['PENGUMUMAN'][0]['can_update'])): ?>
        <a href="<?php echo site_url('rcr/pengumuman/edit/').$p['kd'] ?>" class="ui orange mini button icon " ><i class="ui icon pencil"></i> Edit</a>
        <?php endif; ?>
        <?php if(!empty($_SESSION['authorization']['PENGUMUMAN'][0]['can_delete'])): ?>
        <a href="<?php echo site_url('rcr/pengumuman/delete/'.$p['kd']) ?>" class="ui red mini button icon link_confirmation" ><i class="ui icon x"></i> Hapus</a>
        <?php endif; ?>
      </td>
    </tr>
  <?php endforeach; ?>
  </tbody>
</table>

<?php if(!empty($_SESSION['authorization'][0]['PENGUMUMAN']['can_create'])): ?>
<a class="ui button icon green" href="<?php echo site_url('rcr/pengumuman/add') ?>"><i class="ui icon plus"></i> TAMBAH</a>
<?php endif; ?>
