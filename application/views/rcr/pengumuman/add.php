<h2 class="header">TAMBAH PENGUMUMAN</h2>
<?php echo validation_errors(); ?>
<div class="content">
  <?php echo form_open('pengumuman/add', 'class="ui form" id="pengumuman"')?>
    <div class="eight wide field required">
      <label>Judul Pengumuman</label>
      <input type="text" name="judul" placeholder="Judul Pengumuman">
    </div>

    <div class="ten wide field required">
      <label>Text</label>
      <textarea id="text" name="text" form="pengumuman"></textarea>
    </div>

    <button type="submit" class="ui button icon primary" name="submit_pengumuman"> <i class="ui icon save"></i> SIMPAN</button>
  </form>
</div>

<script type="text/javascript">
  $(document).ready(function(){
    tinymce.init({
      selector: 'textarea',
      height: 200,
      menubar: false,
      plugins: [
        'advlist autolink lists link image charmap print preview anchor',
        'searchreplace visualblocks code',
        'insertdatetime media table contextmenu paste code'
      ],
      toolbar: 'undo redo | insert | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image'
    });
  });
</script>
