<h2>Translation Indonesia - Jepang</h2>
<table class="ui table compact border datatable">
  <thead>
    <tr>
      <th>Indonesia</th>
      <th>Jepang</th>
    </tr>
  </thead>
  <tbody>
  <?php foreach ($bahasa as $b): ?>
    <tr>
      <td><?php echo $b['indonesia'] ?></td>
      <td><?php echo $b['jepang'] ?></td>
    </tr>
  <?php endforeach; ?>
  </tbody>
</table>

<button type="button" class="ui icon green button" onclick="$('.ui.modal').modal('show')"><i class="ui icon plus"></i> TAMBAH</button>

<div class="ui small modal">
  <div class="header">
    TAMBAH TERJEMAHAN
  </div>
  <div class="content">
    <?php echo form_open('bahasa/add', 'class="ui form"') ?>
      <div class="field">
        <label>Indonesia</label>
        <input type="text" name="indonesia">
      </div>

      <div class="field">
        <label>Jepang</label>
        <input type="text" name="jepang">
      </div>

      <button type="submit" class="ui icon primary button"><i class="ui icon save"></i> SIMPAN</button>
    </form>
  </div>
</div>
