<h1 class="ui header">List Pemagang Berdasarkan Riwayat Pekerjaan</h1>
<table class="data-table ui celled table">
  <thead>
    <th>Nama</th>
    <th>Perusahaan</th>
    <th>Pekerjaan</th>
    <th>Durasi</th>
  </thead>
  <tbody>
    <?php foreach ($pemagang as $val): ?>
    <tr>
      <td><?php echo $val['nama_pem'] ?></td>
      <td><?php echo $val['nama_perusahaan'] ?></td>
      <td><?php echo $val['pekerjaan'] ?></td>
      <td><?php echo $val['lama_kerja'] ?> bulan</td>
    </tr>
    <?php endforeach; ?>
  </tbody>
</table>
