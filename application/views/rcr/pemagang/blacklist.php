<h2 class="ui dividing header">PEMAGANG BLACKLIST</h2>

<table class="ui compact table border data-table">
  <thead>
    <tr>
      <th>KODE</th>
      <th>NAMA</th>
      <th>ALASAN</th>
      <th>TANGAL</th>
      <th></th>
    </tr>
  </thead>
  <tbody>
  <?php foreach ($blacklist as $b): ?>
    <tr>
      <td><?php echo $b['kd_pemagang'] ?></td>
      <td><?php echo $b['nama_pem'] ?></td>
      <td><?php echo $b['alasan'] ?></td>
      <td><?php echo $b['tgl'] ?></td>
      <td>
        <a class="ui mini blue button" href="<?php echo site_url('rcr/pemagang/show/'.$b['kd_pemagang']) ?>">lihat</a>
        <a class="ui mini green button link_confirmation" href="<?php echo site_url('rcr/pemagang/hapus_dari_blacklist/'.$b['kd_pemagang']) ?>">Hapus dari Blacklist</a>
      </td>
    </tr>
  <?php endforeach; ?>
  </tbody>
</table>
