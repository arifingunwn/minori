<h2 class="ui orange header">TEST INTERNAL</h2>
<h3><?php //echo $pemagang->nama_pem; ?></h3>

<?php echo show_flash_data() ?>

<table class="ui blue celled very compact table">
  <thead>
    <tr>
      <?php foreach ($test as $key => $val): ?>
      <th><?php echo $val['nama_tes']; ?></th>
    <?php endforeach; ?>
      <th></th>
    </tr>
  </thead>
  <tbody>
    <tr>
    <?php foreach ($test as $key => $val): ?>
      <td><?php echo $val['nilai'] ?></td>
    <?php endforeach; ?>
      <td></td>
    </tr>
  </tbody>
</table>

<button type="button" class="ui icon green button" id="tambah_tes_internal"><i class="ui icon plus"></i> EDIT</button>

<div class="ui modal">
  <div class="header">
    <h2>TAMBAH TES INTERNAL</h2>
  </div>
  <div class="content">
    <?php echo form_open('pemagang/test_internal/'.$kd_pemagang, 'class="ui form"') ?>
      <div class="four wide field required">
        <label>Akhir</label>
        <div class="ui calendar" id="datepicker_test_internal" class="datepicker">
          <div class="ui input left icon">
            <i class="calendar icon"></i>
            <input type="text" name="tgl_test" placeholder="Date" value="<?php echo isset($test[0]['tgl_test']) ? $test[0]['tgl_test'] : '' ?>">
          </div>
        </div>
      </div>
      <input type="hidden" name="kd_detail_nilai" value="<?php echo isset($test[0]['kd_detail_nilai']) ? $test[0]['kd_detail_nilai'] : '' ?>">
      <input type="hidden" name="kd_pemagang" value="<?php echo $kd_pemagang; ?>">
      <div class="fields">
      <?php
      $counter = 0;
      $i = 0;
      foreach ($test_column as $key => $val): ?>
      <?php $counter++; ?>
        <div class="four wide field">
          <label><?php echo $val['nama_tes'] ?></label>
          <input type="text" name="test[<?php echo $i ?>][<?php echo $val['kd_tes'] ?>]" value="">
        </div>
      <?php if ($counter == 4): ?>
      </div>
      <div class="fields">
      <?php $counter = 0; ?>
      <?php endif; ?>
      <?php $i++; ?>
      <?php endforeach; ?>
      </div>
      <button type="submit" name="submit" class="ui icon primary button"><i class="ui icon save"></i> SIMPAN</button>
    </form>
  </div>
</div>
