<div class="ui container">
  <?php echo form_open('pemagang/ajax_form_riwayat_pekerjaan/'.$kd_pemagang, 'id="register_form_3" class="ui form"') ?>
    <h2 class="ui dividing header">RIWAYAT KERJA</h2>
    <div class="parent_pekerjaan">
    <?php if (!empty($riwayat_pekerjaan)): ?>
    <?php $counter = 0; ?>
    <?php foreach ($riwayat_pekerjaan as $v): ?>
      <div class="fields dynamic-field" id="row<?php echo $counter; ?>">
        <div class="four wide field">
          <label>Nama Perusahaan</label>
          <input type="text" name="riwayat[<?php echo $counter; ?>][perusahaan]" value="<?php echo $v['perusahaan'] ?>">
        </div>
        <div class="three wide field">
          <label>Bidang</label>
          <select class="ui search dropdown" name="riwayat[<?php echo $counter; ?>][kd_pekerjaan]">
          <?php
          $jenis_pekerjaan = '';
          foreach ($pekerjaan as $val):
            if($val == 1)
            {
              echo "<optgroup label={$val['jenis_pekerjaan']}>";
            }

            if($jenis_pekerjaan != $val['jenis_pekerjaan'] && $val > 0)
            {
              echo "</optgroup><optgroup label={$val['jenis_pekerjaan']}>";
              $jenis_pekerjaan = $val['jenis_pekerjaan'];
            }
            elseif ($val['kd_pekerjaan'] == $v['posisi']) {
              echo "<option selected value=\"{$val['kd_pekerjaan']}\">" . $val['pekerjaan'] . "</option>";
            }
            else {
              echo "<option value=\"{$val['kd_pekerjaan']}\">" . $val['pekerjaan'] . "</option>";
            }
          ?>
          <?php endforeach; ?>
            </optgroup>
          </select>
        </div>

        <div class="field">
          <label>Mulai</label>
          <div class="inline fields">
            <div class="field">
              <select class="ui search dropdown" name="riwayat[<?php echo $counter; ?>][bulan_mulai]">
                <option value="" disabled="disabled" >Bulan</option>
                <?php
                if(isset($v['bulan_mulai'])){
                  echo option_bulan($v['bulan_mulai']);
                }
                else{
                  echo option_bulan();
                }
                ?>
              </select>

            </div>

            <div class="field">
              <select class="ui search dropdown" name="riwayat[<?php echo $counter; ?>][tahun_mulai]">
                <?php
                if(isset($v['tahun_mulai'])){
                  echo option_tahun(40, $v['tahun_mulai']);
                }
                else{
                  echo option_tahun();
                }
                ?>
              </select>
            </div>
          </div>
        </div>

        <div class="field">
          <label>Selesai</label>
          <div class="inline fields">
            <div class="field">
              <select class="ui search dropdown" name="riwayat[<?php echo $counter; ?>][bulan_selesai]">
                <option value="" disabled="disabled" >Bulan</option>
                <?php
                if(isset($v['bulan_selesai'])){
                  echo option_bulan($v['bulan_selesai']);
                }
                else{
                  echo option_bulan();
                }
                ?>
              </select>
            </div>

            <div class="field">
              <select class="ui search dropdown" name="riwayat[<?php echo $counter; ?>][tahun_selesai]">
                <?php
                if(isset($v['tahun_selesai'])){
                  echo option_tahun(40, $v['tahun_selesai']);
                }
                else{
                  echo option_tahun();
                }
                ?>
              </select>
              <span onclick="$(this).parent().parent().parent().parent().remove()"><i class="ui icon x"></i></span>
            </div>
          </div>
        </div>
      </div>
    <?php $counter++; ?>
    <?php endforeach; ?>
    <?php else: ?>
      <div class="fields dynamic-field" id="row1">
        <div class="four wide field">
          <label>Nama Perusahaan</label>
          <input type="text" name="riwayat[0][perusahaan]" value="">
        </div>
        <div class="three wide field">
          <label>Bidang</label>
          <select class="ui search dropdown" name="riwayat[0][kd_pekerjaan]">
          <?php
          $jenis_pekerjaan = '';
          foreach ($pekerjaan as $val):
            if($val == 1)
            {
              echo "<optgroup label={$val['jenis_pekerjaan']}>";
            }

            if($jenis_pekerjaan != $val['jenis_pekerjaan'] && $val > 0)
            {
              echo "</optgroup><optgroup label={$val['jenis_pekerjaan']}>";
              $jenis_pekerjaan = $val['jenis_pekerjaan'];
            }
            else {
              echo "<option value=\"{$val['kd_pekerjaan']}\">" . $val['pekerjaan'] . "</option>";
            }
          ?>
          <?php endforeach; ?>
            </optgroup>
          </select>
        </div>

        <div class="field">
          <label>Mulai</label>
          <div class="inline fields">
            <div class="field">
              <select class="ui search dropdown" name="riwayat[0][bulan_mulai]">
                <option value="" disabled="disabled" >Bulan</option>
                <option value="1">Januari</option>
                <option value="2">Februari</option>
                <option value="3">Maret</option>
                <option value="4">April</option>
                <option value="5">Mei</option>
                <option value="6">Juni</option>
                <option value="7">Juli</option>
                <option value="8">Agustus</option>
                <option value="9">September</option>
                <option value="10">Oktober</option>
                <option value="11">November</option>
                <option value="12">Desember</option>
              </select>

            </div>

            <div class="field">
              <select class="ui search dropdown" name="riwayat[0][tahun_mulai]">
                <?php
                  $tahun = Date("Y");
                  for($i = $tahun; $i>=$tahun - 40; $i--)
                  {
                    echo "<option value='{$i}'>{$i}</option>";
                  }
                ?>
              </select>
            </div>
          </div>
        </div>

        <div class="field">
          <label>Selesai</label>
          <div class="inline fields">
            <div class="field">
              <select class="ui search dropdown" name="riwayat[0][bulan_selesai]">
                <option value="" disabled="disabled" >Bulan</option>
                <option value="1">Januari</option>
                <option value="2">Februari</option>
                <option value="3">Maret</option>
                <option value="4">April</option>
                <option value="5">Mei</option>
                <option value="6">Juni</option>
                <option value="7">Juli</option>
                <option value="8">Agustus</option>
                <option value="9">September</option>
                <option value="10">Oktober</option>
                <option value="11">November</option>
                <option value="12">Desember</option>
              </select>

            </div>

            <div class="field">
              <select class="ui search dropdown" name="riwayat[0][tahun_selesai]">
                <?php
                  $tahun = Date("Y");
                  for($i = $tahun; $i>=$tahun - 40; $i--)
                  {
                    echo "<option value='{$i}'>{$i}</option>";
                  }
                ?>
              </select>
              <button type="button" class="ui button icon red" onclick="$('#row1').remove()"><i class="ui icon x"></i></button>
            </div>
          </div>
        </div>
      </div>
    <?php endif; ?>
    </div>
    <button type="button" id="btn_tambah_pekerjaan" onclick="" class="ui button icon green" style="display: block; margin-bottom: 2em;"><i class="ui icon plus"></i> TAMBAH</button>
    <button type="submit" class="ui button icon primary"><i class="ui icon arrow right"></i> SELANJUTNYA</button>
  </form>
</div>
