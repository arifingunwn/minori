<h2 class="ui dividing green header">IMPORT TEST INTERNAL DARI EXCEL</h2>
<?php if(!empty($error)): ?>
<div class="ui message red">
  <?php echo $error ?>
</div>
<?php endif; ?>
<div class="ui segment">
  <p>Format file harus <b>.csv (Comma)</b> Template bisa di-unduh <a href="<?php echo site_url('assets/bin/example.csv') ?>">disini</a></p>
  <?php echo form_open('pemagang/import_test_internal', 'class="ui form" enctype="multipart/form-data"') ?>
    <input type="file" name="file" >
    <input type="submit" name="import" class="ui icon orange button" value="import">
  </form>
</div>

<?php echo form_open('pemagang/save_import_test_internal', 'class="ui form"') ?>
  <?php $counter = 0; ?>
  <?php foreach ($temp_array as $key => $value): ?>
    <?php foreach ($value as $k => $v): ?>
      <?php if (is_int($k)): ?>
      <input type="hidden" name="<?php echo 'import' . '[' . $counter . '][test][' . $k . ']' ?>" value="<?php echo $v; ?>">
      <?php else: ?>
      <input type="hidden" name="<?php echo 'import' . '[' . $counter . '][' . $k . ']' ?>" value="<?php echo $v; ?>">
      <?php endif; ?>
    <?php endforeach; ?>
    <?php $counter++; ?>
  <?php endforeach; ?>
  <button type="submit" class="ui icon primary button" onclick="return(confirm('Apakah data sudah benar? Data dengan tanggal dan nama pemagang yang sama akan dihapus di data lama! '))"><i class="ui icon save"></i> SIMPAN</button>
</form>

<?php if(!empty($temp_array)): ?>
<table class="ui celled table celled">
  <thead>
    <tr>
      <th></th>
    <?php foreach ($temp_array as $value): ?>
      <th><?php echo $value['nama'] ?></th>
    <?php endforeach; ?>
    </tr>
  </thead>

  <tbody>
  <?php foreach ($test_column as $value): ?>
  <tr>
    <td class="four wide"><strong><?php echo $value['nama_tes']; ?></strong></td>
    <?php foreach ($temp_array as $val): ?>
    <td><?php echo $val[$value['kd_tes']] ?></td>
    <?php endforeach; ?>
  </tr>
  <?php endforeach; ?>
  <tr>
    <td><strong>Berat Badan</strong></td>
    <?php foreach ($temp_array as $v): ?>
    <td><?php echo $v['berat_badan'] ?></td>
    <?php endforeach; ?>
  </tr>

  <tr>
    <td><strong>Tinggi Badan</strong></td>
    <?php foreach ($temp_array as $v): ?>
    <td><?php echo $v['tinggi_badan'] ?></td>
    <?php endforeach; ?>
  </tr>

  <tr>
    <td><strong>Lingkar Pinggang</strong></td>
    <?php foreach ($temp_array as $v): ?>
    <td><?php echo $v['lingkar_pinggang'] ?></td>
    <?php endforeach; ?>
  </tr>

  <tr>
    <td><strong>Nomor Sepatu</strong></td>
    <?php foreach ($temp_array as $v): ?>
    <td><?php echo $v['no_sepatu'] ?></td>
    <?php endforeach; ?>
  </tr>
  </tbody>
</table>
<?php endif; ?>
