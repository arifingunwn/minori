<h1 class="ui dividing header">
  BIODATA PEMAGANG
</h1>

<?php echo show_flash_data() ?>

<div class="ui three column grid padded segment">
    <div class="two wide column">
      <img src="<?php echo 'https://www.minori.co.id/media/'. md5($pemagang->kd_pemagang.'manfikar') .'/'.$pemagang->foto ?>" class="ui small circular image" />
    </div>
    <div class="eight wide column">
      <h2><?php echo strtoupper($pemagang->nama_pem); ?> <?php echo !empty($lulus) ? "(LULUS)" : "" ?> <?php echo !empty($blacklist) ? "(BLACKLIST)" : "" ?> </h2>
      <?php if(!empty($blacklist)): ?>
      <p style="font-size: 8pt">
        Tanggal : <?php echo $blacklist->tgl ?> <br/>
        Alasan  :<?php echo $blacklist->alasan ?>
      </p>
      <?php endif; ?>

      <?php if(!empty($lulus)): ?>
      <p style="font-size: 8pt">
        Tanggal lulus : <?php echo $lulus->tgl ?> <br/>
        Alasan lulus  : <?php echo $lulus->alasan ?> <br/>
        Proyek        : <?php echo $lulus->judul ?>
      </p>
      <?php endif; ?>

      <strong><?php echo Date('Y') - Date('Y', strtotime($pemagang->tanggal_lahir)); ?> Tahun</strong> <i class="ui icon gender <?php echo $pemagang->jk == 'L' ? 'blue male' : 'pink female' ?>"></i>
      <br/>
      <a href="<?php echo site_url('rcr/pemagang/print_biodata_pemagang/').$pemagang->kd_pemagang ?>" class="ui button mini icon primary"><i class="ui icon print"></i> EXPORT</button>
      <a href="<?php echo site_url('pemagang/print_biodata_pemagang_jepang/').$pemagang->kd_pemagang ?>" class="ui button mini icon primary"><i class="ui icon print"></i> EXPORT JEPANG</button>
      <a href="#" id="btn_edit_biodata" class="ui button mini icon orange"><i class="ui icon pencil"></i> EDIT</a>
      <a target="_blank" href="<?php echo site_url('rcr/pemagang/test_internal/'). $pemagang->kd_pemagang ?>" class="ui mini button green">TEST INTERNAL</a>

      <?php if(empty($blacklist) && empty($lulus)): ?>
      <div style="margin-top: 1em">
        <a href="#" class="ui mini button blue" id="btn_lulus">LULUS</a>
        <a href="#" class="ui mini button black" id="btn_blacklist">BLACKLIST</a>
      </div>
      <?php endif; ?>

    </div>
    <div class="right aligned six wide column">
      <i class="ui icon mobile"></i> <strong><?php echo $pemagang->no_hp.', '.$pemagang->no_hp1.', '.$pemagang->no_hp2 ?></strong> <br/>
      <i class="ui icon phone"></i> <strong><?php echo $pemagang->no_tel ?></strong> <br/>
      <i class="ui icon flag"></i> <strong><?php echo $pemagang->warga_negara ?></strong> <br/>
      <i class="ui icon mail"></i> <strong><?php echo $pemagang->email ?></strong> <br/>
      <i class="ui icon marker"></i> <strong>Alamat Sekarang</strong> <br/>
      <?php echo $pemagang->tempat_tinggal ?> <br/>
      <?php echo $pemagang->provinsi ?>
    </div>
    <i><small>Tanggal Daftar : <?php echo $pemagang->tgldaftar ?></small></i>
    <i><small>Terakhir revisi : <?php echo $pemagang->tgl_rev ?></small></i>
</div>

<div class="ui two column grid">
  <div class="twelve wide column">
    <div class="ui segment">
      <div class="ui top attached tabular menu">
        <a href="#" class="item active" data-tab="riwayat_kerja">Riwayat Pekerjaan</a>
        <a href="#" class="item" data-tab="pendidikan">Pendidikan</a>
        <a href="#" class="item" data-tab="keluarga">Keluarga</a>
        <a href="#" class="item" data-tab="passport">Passport</a>
        <a href="#" class="item" data-tab="keluar_negri">Riwayat Keluar Negri</a>
        <a href="#" class="item" data-tab="riwayat_penyakit">Riwayat Penyakit</a>
      </div>

      <div class="ui bottom attached tab active" data-tab="riwayat_kerja">
        <table class="ui celled compact blue table">
          <thead>
            <th>
              <div class="ui ribbon orange label" id="edit_riwayat_pekerjaan" data-kd="<?php echo $pemagang->kd_pemagang ?>">EDIT</div>
              Perusahaan</th>
            <th>Posisi</th>
            <th>Mulai</th>
            <th>Selesai</th>
          </thead>
          <tbody>
          <?php foreach ($riwayat_pekerjaan as $rp): ?>
            <tr>
              <td><?php echo $rp['perusahaan'] ?></td>
              <td><?php echo $rp['pekerjaan'] ?></td>
              <td><?php echo $rp['bulan_masuk'] . '/' . $rp['tahun_masuk'] ?></td>
              <td><?php echo $rp['bulan_keluar'] . '/' . $rp['tahun_keluar'] ?></td>
            </tr>
          <?php endforeach; ?>
          </tbody>
        </table>
      </div>

      <div class="ui bottom attached tab" data-tab="pendidikan">
        <table class="ui celled compact table">
          <thead>
            <th>
              <!-- <div class="ui ribbon orange label" id="edit_riwayat_pendidikan" data-kd="<?php echo $pemagang->kd_pemagang ?>">EDIT</div> -->
              Nama Sekolah</th>
            <th>Tingkatan</th>
            <th>Jurusan</th>
            <th>Mulai</th>
            <th>Selesai</th>
          </thead>
          <tbody>
          <?php foreach ($riwayat_pendidikan as $rp): ?>
            <tr>
              <td contenteditable="true"><?php echo $rp['instansi'] ?></td>
              <td><?php echo $rp['tingkat_pendidikan'] ?></td>
              <td><?php echo $rp['nama_jurusan'] ?></td>
              <td><?php echo $rp['bulan_masuk'] . '/' . $rp['mulai'] ?></td>
              <td><?php echo $rp['bulan_selesai'] . '/' . $rp['selesai'] ?></td>
            </tr>
          <?php endforeach; ?>
          </tbody>
        </table>
      </div>

      <div class="ui bottom attached tab" data-tab="keluarga">
        <table class="ui celled compact green table">
          <thead>
            <tr>
              <th>
                Hubungan</th>
              <th>Nama</th>
              <th>Tgl Lahir</th>
              <th>Usia</th>
              <th>Pekerjaan</th>
              <th>Kontak</th>
            </tr>
          </thead>
          <tbody>
          <?php if(!empty($keluarga_pem)): ?>
            <?php foreach ($keluarga_pem as $val): ?>
              <tr>
                <td><?php echo $val['hubungan'] ?></td>
                <td><?php echo $val['nama'] ?></td>
                <td><?php echo $val['tanggal_lahir'] ?></td>
                <td><?php echo (Date('Y') - substr($val['tanggal_lahir'], 0, 4)) ?></td>
                <td><?php echo $val['pekerjaan'] ?></td>
                <td><?php echo $val['hp'] ?></td>
              </tr>
            <?php endforeach; ?>
            <?php foreach ($keluarga as $val): ?>
              <tr>
                <td><?php echo $val['hubungan'] ?></td>
                <td><?php echo $val['nama_kel'] ?></td>
                <td><?php echo $val['tgl'] ?></td>
                <td><?php echo (Date('Y') - substr($val['tgl'], 0, 4)) ?></td>
                <td><?php echo $val['pekerjaan'] ?></td>
                <td></td>
              </tr>
            <?php endforeach; ?>
          <?php endif; ?>
            <tr>
              <td></td>
              <td></td>
              <td></td>
              <td></td>
              <td></td>
              <td></td>
            </tr>
          </tbody>
        </table>

        <h3>Sedang di Jepang</h3>
        <table class="ui celled compact red table">
          <thead>
            <th>Hubungan</th>
            <th>Nama</th>
            <th>Umur</th>
            <th>kewarganegaraan</th>
            <th>Status Visa</th>
            <th>Instansi</th>
          </thead>
          <tbody>
          <?php foreach ($keluarga_pemagang_dijepang as $kjp): ?>
            <tr>
              <td><?php echo $kjp['hubungan'] ?></td>
              <td><?php echo $kjp['nama'] ?></td>
              <td><?php echo $kjp['umur'] ?></td>
              <td><?php echo $kjp['kewarganegaraan'] ?></td>
              <td><?php echo $kjp['visa'] ?></td>
              <td><?php echo $kjp['instansi'] ?></td>
            </tr>
          <?php endforeach; ?>
          </tbody>
        </table>
      </div>

      <div class="ui bottom attached tab" data-tab="passport">
        <table class="ui celled compact brown table">
          <thead>
            <tr>
              <th>
                <div class="ui ribbon orange label" id="edit_passport" data-kd="<?php echo $pemagang->kd_pemagang ?>">EDIT</div>
                Passport</th>
              <th>No. Paspor</th>
              <th>Tanggal Penerbitan</th>
              <th>Berlaku Hingga</th>
            </tr>
          </thead>
          <tbody>
          <?php if($pemagang->pasport == 'Memiliki'): ?>
            <?php foreach ($passport as $val) { ?>
              <tr>
                <td>Ada</td>
                <td><?php echo $val['no_pass'] ?></td>
                <td><?php echo $val['masa_berlaku'] ?></td>
                <td><?php echo $val['terbit'] ?></td>
              </tr>
            <?php } ?>
          <?php else: ?>
            <tr>
              <td>Tidak Ada</td>
              <td></td>
              <td></td>
              <td></td>
            </tr>
          <?php endif; ?>
          </tbody>
        </table>
      </div>

      <div class="ui bottom attached tab" data-tab="keluar_negri">
        <table class="ui celled compact yellow table">
          <thead>
            <tr>
              <th>
                Tahun</th>
              <th>Bulan</th>
              <th>Negara Tujuan</th>
              <th>Tujuan</th>
            </tr>
          </thead>
          <tbody>
          <?php if(!empty($pemagang->peng_keluarnegri) || $pemagang->peng_keluarnegri == 'p'): ?>
            <?php foreach ($riwayat_keluar_negri as $val): ?>
              <tr>
                <td><?php echo $val['tahun'] ?> Tahun</td>
                <td><?php echo $val['bulan'] ?> Bulan</td>
                <td><?php echo $val['Negara'] ?></td>
                <td><?php echo $val['tujuan'] ?></td>
              </tr>
            <?php endforeach; ?>
          <?php else: ?>
            <tr>
              <td>Tidak Pernah</td>
              <td></td>
              <td></td>
            </tr>
          <?php endif; ?>
          </tbody>
        </table>
      </div>

      <!-- RIWAYAT PENYAKIT -->
      <div class="ui bottom attached tab" data-tab="riwayat_penyakit">
        <table class="ui celled compact pink table">
          <thead>
            <tr>
              <th>Nama Penyakit</th>
              <th>Bulan</th>
              <th>Tahun</th>
            </tr>
          </thead>
          <tbody>
          <?php if(!empty($riwayat_penyakit)): ?>
            <?php foreach ($riwayat_penyakit as $val): ?>
              <tr>
                <td><?php echo $val['nama_penyakit'] ?></td>
                <td><?php echo $val['bulan'] ?></td>
                <td><?php echo $val['tahun'] ?></td>
              </tr>
            <?php endforeach; ?>
          <?php else: ?>
            <tr>
              <td>Tidak Pernah</td>
              <td></td>
              <td></td>
            </tr>
          <?php endif; ?>
          </tbody>
        </table>
      </div>

    </div>
  </div>

  <div class="four wide column">
    <div class="ui segment">
      <table class="ui very basic table">
        <tr>
          <td><strong>No. KTP</strong></td>
          <td><?php echo $pemagang->no_ktp ?></td>
        </tr>
        <tr>
          <td><strong>Tanggal Lahir</strong></td>
          <td><?php echo $pemagang->tanggal_lahir ?></td>
        </tr>
        <tr>
          <td><strong>Agama</strong></td>
          <td><?php echo $pemagang->agama ?></td>
        </tr>
        <tr>
          <td><strong>Golongan Darah</strong></td>
          <td><?php echo $pemagang->gol_darah ?></td>
        </tr>
        <tr>
          <td><strong>Tinggi Badan</strong></td>
          <td><?php echo $pemagang->tb ?></td>
        </tr>
        <tr>
          <td><strong>Berat Badan</strong></td>
          <td><?php echo $pemagang->bb ?></td>
        </tr>
        <tr>
          <td><strong>Lingkar Pinggang</strong></td>
          <td><?php echo $pemagang->lingkar_pinggang ?></td>
        </tr>
        <tr>
          <td><strong>Ukuran Sepatu</strong></td>
          <td><?php echo $pemagang->no_sepatu ?></td>
        </tr>
        <tr>
          <td><strong>Suka Pelajaran</strong></td>
          <td><?php echo $pemagang->suka_pelajaran ?></td>
        </tr>
        <tr>
          <td><strong>Hobi</strong></td>
          <td><?php echo $pemagang->hobi ?></td>
        </tr>
        <tr>
          <td><strong>Skill</strong></td>
          <td><?php echo $pemagang->skil ?></td>
        </tr>
        <tr>
          <td><strong>Merokok</strong></td>
          <td>
            <?php if(!empty($merokok)):  ?>
            <?php echo $merokok->status == 2 ? 'YA' . ', ' . $merokok->hari . ' batang per-hari' : $merokok->status ?>
            <?php endif; ?>
          </td>
        </tr>
        <tr>
          <td><strong>Minum Alkohol</strong></td>
          <td>
            <?php if(!empty($minum_alkohol)):  ?>
            <?php echo $minum_alkohol->status == 2 ? 'YA' . ', ' . $minum_alkohol->bulan . ' kali per-bulan' : $minum_alkohol->status ?>
            <?php endif; ?>
          </td>
        </tr>
      </table>
    </div>
  </div>
</div>

<div class="ui grid">
  <div class="eight wide column">
    <h3 class="ui top attached brown header">BERKAS</h3>
    <div class="ui bottom attached segment" id="sisipkan_berkas">
      <table class="ui celled brown compact table">
        <thead>
          <tr>
            <th>No.</th>
            <th>Berkas</th>
            <th>Rincian</th>
            <th></th>
          </tr>
        </thead>
        <tbody>
        <?php $counter = 0; ?>
        <?php foreach ($berkas as $b): ?>
          <tr>
            <td><?php echo ++$counter; ?></td>
            <td><?php echo $b['nama_berkas'] ?></td>
            <td><?php echo $b['ket'] ?></td>
            <td>
              <a href="<?php echo site_url('rcr/download_berkas/'. $b['kd']) ?>" class="ui icon link_confirmation mini blue button"><i class="ui icon download"></i> Download</a>
              <a href="<?php echo site_url('rcr/delete_berkas/'. $b['kd']) ?>" class="ui icon link_confirmation mini red button"><i class="ui icon x"></i> Hapus</a>
            </td>
          </tr>
        <?php endforeach; ?>
        </tbody>
      </table>

      <button type="button" id="btn_tambah_berkas" class="ui icon mini green button" onclick="$('.ui.modal#berkas').modal('show')"><i class="ui icon plus"></i> TAMBAH</button>
    </div>
  </div>

  <div class="eight wide column">
    <h3 class="ui top attached orange header">
      TEST EXTERNAL
    </h3>
    <div class="ui bottom attached segment">
      <table class="ui celled orange compact table">
        <thead>
          <th>No.</th>
          <th>tanggal</th>
          <th>Asosiasi</th>
          <th>Perusahaan</th>
          <th>Jenis Usaha</th>
          <th>Hasil</th>
          <th>File</th>
        </thead>
        <tbody>
        <?php $counter = 0; ?>
        <?php foreach ($test_external as $te): ?>
          <tr>
            <td><?php echo ++$counter; ?></td>
            <td><?php echo $te['tgl_seleksi'] ?></td>
            <td><?php echo $te['asosiasi'] ?></td>
            <td><?php echo $te['perusahaan'] ?></td>
            <td><?php echo $te['jenis_usaha'] ?></td>
            <td><?php echo $te['hasil'] ?></td>
            <td>
              <a href="<?php echo site_url('rcr/download_test_external/'. $te['kd_test_external']) ?>" class="ui icon mini green button"><i class="ui icon download"></i></a>
              <a href="<?php echo site_url('rcr/delete_test_external/'. $te['kd_test_external']) ?>" class="ui icon mini red link_confirmation button"><i class="ui icon x"></i></a>
            </td>
          </tr>
        <?php endforeach; ?>
        </tbody>
      </table>

      <button type="button" class="ui icon green mini button" id="test_external"><i class="ui icon plus"></i> TAMBAH</button>
    </div>
  </div>
</div>

<!-- MODAL FORM -->
<div class="ui modal" id="berkas">
  <div class="header">
    UPLOAD BERKAS
  </div>

  <div class="content">
    <?php echo form_open('pemagang/upload_berkas', 'class="ui form" enctype="multipart/form-data"') ?>
      <input type="hidden" name="kd_pemagang" value="<?php echo $pemagang->kd_pemagang ?>">
      <div class="ui fields">
        <div class="eight wide field">
          <label>KETERANGAN</label>
          <input type="text" name="keterangan" value="">
        </div>

        <div class="four wide field">
          <label>TIPE</label>
          <select class="ui dropdown" name="nama_berkas">
            <option value="Izasah Terakhir">Izasah Terakhir</option>
            <option value="KK Orang Tua">KK Orang Tua</option>
            <option value="Akte">Akte</option>
            <option value="KTP">KTP</option>
            <option value="Foto">Foto</option>
            <option value="Surat Pengalaman Kerja">Surat Pengalaman Kerja</option>
            <option value="Hasil Test">Hasil Test</option>
            <option value="Hasil Interview">Hasil Interview</option>
            <option value="Hasil MCU 1">Hasil MCU 1</option>
            <option value="Data Diri">Data Diri</option>
            <option value="CV">CV</option>
            <option value="Rapid Test">Rapid Test</option>
          </select>
        </div>
      </div>
      <div class="ui field">
        <label>Pilih Berkas</label>
        <input type="file" name="berkas_file" >
      </div>
      <button type="submit" name="submit_berkas" class="ui icon button brown" ><i class="ui icon upload"></i> UPLOAD</button>
    </form>
  </div>
</div>

<div class="ui modal" id="test_external">
  <div class="header">
    UPLOAD TEST EXTERNAL
  </div>
  <div class="content">
    <?php echo form_open('pemagang/upload_test_external', 'class="ui form" enctype="multipart/form-data"') ?>
      <input type="hidden" name="kd_pemagang" value="<?php echo $pemagang->kd_pemagang ?>">

      <div class="eight wide field required">
        <label>TANGGAL TEST</label>
        <div class="ui calendar" id="datepicker_test_external" class="datepicker">
          <div class="ui input left icon">
            <i class="calendar icon"></i>
            <input type="text" name="tgl_test" placeholder="Date">
          </div>
        </div>
      </div>

      <div class="eight wide field">
        <label>ASOSIASI</label>
        <select class="ui dropdown" name="kd_asosiasi">
        <?php foreach ($asosiasi as $as): ?>
          <option value="<?php echo $as['kd_pt']?>"><?php echo $as['nama_pt'] ?></option>
        <?php endforeach; ?>
        </select>
      </div>

      <div class="eight wide field">
        <label>PERUSAHAAN</label>
        <select class="ui dropdown" name="kd_perusahaan">
        <?php foreach ($perusahaan as $p): ?>
          <option value="<?php echo $p['kd_pt']?>"><?php echo $p['nama_pt'] ?></option>
        <?php endforeach; ?>
        </select>
      </div>

      <div class="eight wide field">
        <label>JENIS USAHA</label>
        <select class="ui dropdown" name="kd_bidang_usaha">
        <?php foreach ($jenis_usaha as $js): ?>
          <option value="<?php echo $js['kd_jenis_usaha']?>"><?php echo $js['nama'] ?></option>
        <?php endforeach; ?>
        </select>
      </div>

      <div class="eight wide field">
        <label>HASIL TEST</label>
        <input type="text" name="hasil_test" value="">
      </div>

      <div class="eight wide field">
        <label>FILE</label>
        <input type="file" name="file_test_external">
      </div>

      <button type="submit" class="ui icon primary button"><i class="ui icon upload"></i> UPLOAD</button>
    </form>
  </div>
</div>

<div class="ui modal" id="biodata_pemagang">
  <div class="header">
    <h2>UBAH DATA PEMAGANG</h2>
  </div>

  <div class="content">
    <?php echo form_open('pemagang/edit/'.$pemagang->kd_pemagang, 'class="ui form"') ?>
      <div class="ui grid">
        <div class="five wide column">
          <div class="field">
            <label>Nama</label>
            <input type="text" name="nama_pem" value="<?php echo $pemagang->nama_pem ?>">
          </div>

          <div class="field">
            <label>Tanggal Lahir</label>
            <div class="ui calendar" id="datepicker_tanggal_lahir" class="datepicker">
              <div class="ui input left icon">
                <i class="calendar icon"></i>
                <input type="text" name="tanggal_lahir" placeholder="Date" value="<?php echo $pemagang->tanggal_lahir ?>">
              </div>
            </div>
          </div>

          <div class="field">
            <label>Tempat Kelahiran</label>
            <input type="text" name="tempat_lahir" value="<?php echo $pemagang->tempat_lahir ?>">
          </div>

          <div class="field">
            <label>Alamat KTP</label>
            <textarea name="alamat_ktp" rows="8" cols="80"><?php echo $pemagang->alamat_ktp ?></textarea>
          </div>

          <div class="field">
            <label>Alamat Sekarang</label>
            <textarea name="tempat_tinggal" rows="8" cols="80"><?php echo $pemagang->tempat_tinggal ?></textarea>
          </div>

          <div class="field">
            <label>Tempat Asal</label>
            <input type="text" name="tempat_asal" value="<?php echo $pemagang->tempat_asal ?>">
          </div>

          <div class="field">
            <label>Provinsi</label>
            <select class="ui search dropdown" name="kd_provinsi">
            <?php foreach ($provinsi as $p): ?>
              <option <?php echo $pemagang->kd_provinsi == $p['kd_provinsi'] ? "selected" : "" ?> value="<?php echo $p['kd_provinsi'] ?>"><?php echo $p['provinsi'] ?></option>
            <?php endforeach; ?>
            </select>
          </div>
        </div>

        <div class="five wide column">
          <div class="field">
            <label>Nomor Handphone</label>
            <input type="text" name="no_hp" value="<?php echo $pemagang->no_hp ?>">
          </div>

          <div class="field">
            <label>Nomor Handphone 2</label>
            <input type="text" name="no_hp1" value="<?php echo $pemagang->no_hp1 ?>">
          </div>

          <div class="field">
            <label>Nomor Handphone 3</label>
            <input type="text" name="no_hp2" value="<?php echo $pemagang->no_hp2 ?>">
          </div>

          <div class="field">
            <label>Nomor Telepon</label>
            <input type="text" name="no_tel" value="<?php echo $pemagang->no_tel ?>">
          </div>

          <div class="field">
            <label>Golongan Darah</label>
            <input type="text" name="gol_darah" value="<?php echo $pemagang->gol_darah ?>">
          </div>

          <div class="field">
            <label>Tinggi Badan</label>
            <input type="text" name="tb" value="<?php echo $pemagang->tb ?>">
          </div>

          <div class="field">
            <label>Berat Badan</label>
            <input type="text" name="bb" value="<?php echo $pemagang->bb ?>">
          </div>

          <div class="field">
            <label>Lingkar Pinggang</label>
            <input type="text" name="lingkar_pinggang" value="<?php echo $pemagang->lingkar_pinggang ?>">
          </div>

          <div class="field">
            <label>Ukuran Sepatu</label>
            <input type="text" name="no_sepatu" value="<?php echo $pemagang->no_sepatu ?>">
          </div>

          <div class="field">
            <label>Suka Pelajaran</label>
            <input type="text" name="suka_pelajaran" value="<?php echo $pemagang->suka_pelajaran ?>">
          </div>
        </div>

      </div>

      <button type="submit" class="ui icon button submit primary"><i class="ui icon save"></i> SIMPAN</button>
    </form>
  </div>
</div>

<!-- FORM BLACKLIST -->
<div class="ui small modal" id="blacklist">
  <div class="header">
    <h2>Blacklist</h2>
  </div>

  <div class="content">
    <?php echo form_open('pemagang/blacklist_pemagang/'.$pemagang->kd_pemagang, 'class="ui form" id="blacklist_form"') ?>
      <input type="hidden" name="kd_pemagang" value="<?php echo $pemagang->kd_pemagang ?>">
      <div class="eight wide field">
        <label>Tanggal</label>
        <div class="ui calendar" id="datepicker_blacklist" class="datepicker">
          <div class="ui input left icon">
            <i class="calendar icon"></i>
            <input type="text" name="tanggal" placeholder="Tanggal ditetapkan">
          </div>
        </div>
      </div>
      <div class="eight wide field">
        <label>Alasan</label>
        <textarea id="alasan" name="alasan" form="blacklist_form" rows="8" cols="80"></textarea>
      </div>
      <button type="submit" class="ui icon button black"><i class="ui icon save"></i> BLACKLIST</button>
    </form>
  </div>
</div>

<!-- FORM LULUS -->
<div class="ui small modal" id="lulus">
  <div class="header">
    <h2>LULUS</h2>
  </div>

  <div class="content">
    <?php echo form_open('pemagang/lulus_pemagang/'.$pemagang->kd_pemagang, 'class="ui form" id="lulus_form"') ?>
      <input type="hidden" name="kd_pemagang" value="<?php echo $pemagang->kd_pemagang ?>">
      <div class="eight wide field">
        <label>Tanggal</label>
        <div class="ui calendar" id="lulus_datepicker" class="datepicker">
          <div class="ui input left icon">
            <i class="calendar icon"></i>
            <input type="text" name="tgl" placeholder="Tanggal ditetapkan">
          </div>
        </div>
      </div>
      <div class="eight wide field">
        <label>Alasan</label>
        <textarea name="alasan_lulus" id="alasan_lulus" form="lulus_form" rows="8" cols="80"></textarea>
      </div>
      <button type="submit" class="ui icon button blue"><i class="ui icon save"></i> LULUS</button>
    </form>
  </div>
</div>

<!-- AJAX FORM -->
<div class="ui long modal" id="ajax_form">
  <div class="header"></div>

  <div class="content"></div>
</div>
<!-- END MODAL FORM -->
