<h2 class="ui dividing green header">PEMAGANG LULUS</h2>

<table class="ui table compact celled data-table">
  <thead>
    <tr>
      <th>Kode Pemagang</th>
      <th>Nama</th>
      <th>Kode Lowongan</th>
      <th>Nilai</th>
      <th>Keterangan</th>
    </tr>
  </thead>
  <tbody>
    <?php foreach ($lulus as $l): ?>
    <tr>
      <td><?php echo $l['kd_pemagang'] ?></td>
      <td><?php echo $l['nama_pem'] ?></td>
      <td><?php echo $l['kd_lowongan'] ?></td>
      <td><?php echo $l['nilai'] ?></td>
      <td><?php echo $l['keterangan'] ?></td>
    </tr>
    <?php endforeach; ?>
    <tr>
      <td></td>
      <td></td>
      <td></td>
      <td></td>
      <td></td>
    </tr>
  </tbody>
</table>
