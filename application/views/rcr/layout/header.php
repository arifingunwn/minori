<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <title>Magang</title>
    <link rel="stylesheet" href="<?php echo base_url('bower_components/semantic/dist/semantic.min.css') ?>" media="screen" title="no title" charset="utf-8">
    <link rel="stylesheet" href="<?php echo base_url('bower_components/Croppie/croppie.css') ?>" media="screen" title="no title" charset="utf-8">
    <link rel="stylesheet" href="<?php echo base_url('bower_components/sweetalert/dist/sweetalert.css')?>" media="screen" title="no title" charset="utf-8">
    <link rel="stylesheet" href="<?php echo base_url('bower_components/datatables.net-dt/css/jquery.dataTables.min.css')?>">
    <link rel="stylesheet" href="<?php echo base_url('bower_components/semantic-ui-calendar/dist/calendar.min.css')?>">
    <link rel="stylesheet" href="<?php echo base_url('bower_components/jstree/dist/themes/default/style.min.css')?>">
    <link rel="stylesheet" href="<?php echo base_url('assets/stylesheet/style.css') ?>">
    <style media="screen">
      .input {
        margin-bottom: 2em;
      }

      .main {
        margin-top: 7em;
      }

      body{
        /*background: #eaeaea;*/
      }
    </style>
    <script type="text/javascript" src="<?php echo base_url('bower_components/jquery/dist/jquery.min.js') ?>"></script>
  </head>
<body>
  <!-- <div class="ui bottom attached segment pushable"> -->
    <div class="ui left fixed inverted small vertical menu">
      <a class="item" href="<?php echo base_url('rcr'); ?>">
        <i class="home icon"></i>
        Home
      </a>

      <?php if(in_array('LOWONGAN', array_keys($_SESSION['authorization']))): ?>
      <div class="ui dropdown item">
        <i class="dropdown icon"></i>
        Project / Lowongan
        <div class="menu">
          <a class="item" href="<?php echo base_url('rcr/lowongan') ?>">Lihat Lowongan</a>
          <?php if(!empty($_SESSION['authorization']['LOWONGAN'][0]['can_create'])): ?>
          <a class="item" href="<?php echo base_url('rcr/lowongan/add') ?>">Tambah Lowongan</a>
          <?php endif; ?>
        </div>
      </div>
      <?php endif; ?>

      <div class="ui dropdown item">
        <i class="dropdown icon"></i>
        Master
        <div class="menu">
          <?php if(in_array('JURUSPEND', array_keys($_SESSION['authorization']))): ?>
          <a class="item" href="<?php echo base_url('rcr/jurusan') ?>">Jurusan Pendidikan</a>
          <?php endif; ?>
          <?php if(in_array('PEKERJAAN', array_keys($_SESSION['authorization']))): ?>
          <a class="item" href="<?php echo base_url('rcr/pekerjaan') ?>">Pekerjaan</a>
          <?php endif; ?>
          <?php if(in_array('TRANSLATE', array_keys($_SESSION['authorization']))): ?>
          <a class="item" href="<?php echo base_url('rcr/bahasa') ?>">Terjemahan Bahasa</a>
        <?php endif; ?>
        </div>
      </div>

      <?php if(in_array('PEMAGANG', array_keys($_SESSION['authorization']))): ?>
      <div class="ui dropdown item">
        <i class="dropdown icon"></i>
        Pemagang
        <div class="menu">
          <a class="item" href="<?php echo base_url('rcr/pemagang'); ?>">Lihat Pemagang</a>
          <?php if(in_array('PMGLULUS', array_keys($_SESSION['authorization']))): ?>
          <a class="item" href="<?php echo base_url('rcr/pemagang/pemagang_lulus'); ?>">Pemagang Lulus</a>
          <?php endif; ?>
          <?php if(in_array('PMGBL', array_keys($_SESSION['authorization']))): ?>
          <a class="item" href="<?php echo base_url('rcr/pemagang/blacklist'); ?>">Lihat Blacklist</a>
          <?php endif; ?>
          <?php if(in_array('TESTINT', array_keys($_SESSION['authorization']))): ?>
          <a class="item" href="<?php echo base_url('rcr/pemagang/import_test_internal') ?>">Import Test Internal</a>
          <?php endif; ?>
        </div>
      </div>
      <?php endif; ?>

      <?php if(in_array('PENGUMUMAN', array_keys($_SESSION['authorization']))): ?>
      <div class="ui dropdown item">
        <i class="dropdown icon"></i>
        Pengumuman
        <div class="menu">
          <a class="item" href="<?php echo base_url('rcr/pengumuman'); ?>">Lihat Pengumuman</a>
        </div>
      </div>
    <?php endif; ?>

    <?php if(in_array('USER', array_keys($_SESSION['authorization']))): ?>
      <div class="ui dropdown item">
        <i class="dropdown icon"></i>
        User / Admin
        <div class="menu">
          <a class="item" href="<?php echo base_url('rcr/user') ?>">Lihat User</a>
          <a class="item" href="<?php echo base_url('rcr/add_user') ?>">Tambah User</a>
        </div>
      </div>
    <?php endif; ?>

      <a class="item">
        <i class="setting icon"></i>
        Pengaturan
      </a>

      <a class="item" href="<?php echo base_url('auth/logout') ?>">
        <i class="icon power"></i>
        Logout
      </a>
    </div>
    <!-- <div class="pusher"> -->
      <div class="ui main rcr basic segment">
