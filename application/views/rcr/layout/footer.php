    </div>
  <!-- </div>
</div> -->

<script type="text/javascript" src="<?php echo base_url('bower_components/semantic/dist/semantic.min.js') ?>"></script>
<script type="text/javascript" src="<?php echo base_url('bower_components/datatables.net/js/jquery.dataTables.min.js') ?>"></script>
<script type="text/javascript" src="<?php echo base_url('bower_components/Croppie/croppie.min.js')?>"></script>
<script type="text/javascript" src="<?php echo base_url('bower_components/sweetalert/dist/sweetalert.min.js') ?>"></script>
<script type="text/javascript" src="<?php echo base_url('bower_components/tinymce/tinymce.min.js') ?>"></script>
<script type="text/javascript" src="<?php echo base_url('bower_components/semantic-ui-calendar/dist/calendar.min.js') ?>"></script>
<script type="text/javascript" src="<?php echo base_url('bower_components/jstree/dist/jstree.min.js') ?>"></script>
<script type="text/javascript" src="<?php echo base_url('assets/js/script.js') ?>"></script>
<script type="text/javascript" src="<?php echo base_url('assets/js/tablesort.js') ?>"></script>
<script type="text/javascript">
  $(document).ready(function(){
    Rcr.init();
    $('.ui.dropdown').dropdown();
    $('select.dropdown').dropdown();
  });
</script>
</body>
</html>
