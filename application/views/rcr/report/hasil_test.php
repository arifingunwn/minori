<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <title><?php echo $title ?></title>
    <link rel="stylesheet" href="<?php echo base_url('bower_components/semantic/dist/semantic.min.css') ?>" media="screen" title="no title" charset="utf-8">
  </head>
  <body>
    <div style="overflow: scroll;">
      <h2><?php echo $report_type ?></h2>
      <table class="ui celled table">
        <thead>
          <tr>
            <th>No</th>
            <th>Foto</th>
            <th>kode pemagang</th>
        		<th>nama</th>
        		<th>usia</th>
        		<th>status</th>
        		<th>provinsi</th>
        		<th>nama sma</th>
        		<th>jurusan sma</th>
        		<th>nama sekolah tinggi</th>
        		<th>jurusan sekolah tinggi</th>
        		<th>pengalaman kerja</th>
        		<th>nomor handphone</th>
        		<th>tanggal daftar</th>
        		<th>email</th>
          </tr>
        </thead>
        <tbody>
          <?php $no = 1; ?>
          <?php foreach ($pemagang as $key => $pmg): ?>
            <tr>
              <?php
              $status = '';
                if($pmg['status'] == 'b')
                {
                  $status = 'Belum Menikah';
                }
                else if($pmg['status'] == 'jh'){
                  $status="Cerai Hidup";
                }else if($pmg['status'] == 'jm'){
                  $status="Cerai Mati";
                }else if($pmg['status'] == 'ns'){
                  $status="Nikah Sirih";
                }else{
                  $status= "Menikah";
                }
              ?>
              <td><?php echo $no++; ?></td>
              <td>
                <img width="100" src="<?php echo 'https://www.minori.co.id/media/'. md5($pmg['kode_pemagang'].'manfikar') .'/'.$pmg['foto'] ?>"/>
              </td>
              <td><?php echo $pmg['kode_pemagang'] ?></td>
          		<td><?php echo $pmg['nama'] ?></td>
          		<td><?php echo $pmg['usia'] ?></td>
          		<td><?php echo $status ?></td>
          		<td><?php echo $pmg['provinsi'] ?></td>
          		<td><?php echo $pmg['nama_sma'] ?></td>
          		<td><?php echo $pmg['jurusan_sma'] ?></td>
          		<td><?php echo $pmg['nama_sekolah_tinggi'] ?></td>
          		<td><?php echo $pmg['jurusan_sekolah_tinggi'] ?></td>
          		<td><?php echo $pmg['pengalaman_kerja'] ?></td>
          		<td><?php echo $pmg['nomor_handphone'] ?></td>
          		<td><?php echo $pmg['tanggal_daftar'] ?></td>
          		<td><?php echo $pmg['email'] ?></td>
            </tr>
          <?php endforeach; ?>
        </tbody>
      </table>
    </div>
  </body>
</html>
