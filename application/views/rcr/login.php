<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <title>Recruitment Login</title>
    <link rel="stylesheet" href="<?php echo base_url('bower_components/semantic/dist/semantic.min.css') ?>" media="screen" title="no title" charset="utf-8">
    <style media="screen">
    body {
     background: url('<?php echo base_url("assets/img/pexels-photo-129922.jpeg") ?>') fixed;
     background-size: cover;
     padding: 0;
     margin: 0;
    }

    .form-holder {
     background: rgba(255,255,255,0.2);
     margin-top: 10%;
     border-radius: 3px;
    }

    .form-head {
     font-size: 30px;
     letter-spacing: 2px;
     text-transform: uppercase;
     color: #fff;
     text-shadow: 0 0 30px #000;
     margin: 15px auto 30px auto;
    }

    .remember-me {
     text-align: left;
    }
    .ui.checkbox label {
     color: #ddd;
    }
    </style>
  </head>
  <body>
    <div class="ui one column center aligned grid">
      <div class="column five wide form-holder">
        <h2 class="center aligned header form-head">MINORI RCR</h2>
        <?php echo form_open('', 'class="ui form"') ?>
          <input type="hidden" name="<?php echo $this->security->get_csrf_token_name(); ?>" value="<?php echo $this->security->get_csrf_hash(); ?>">
          <div class="field">
            <input type="text" name="username" placeholder="Username">
          </div>
          <div class="field">
            <input type="password" name="password" password="password" placeholder="Password">
          </div>
          <div class="field">
            <input type="submit" name="login" value="login" class="ui button large fluid blue">
          </div>
        </form>
      </div>
    </div>
  </body>
</html>
