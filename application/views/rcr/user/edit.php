<h2 class="ui dividing blue header">EDIT USER</h2>

<div class="ui grid">
  <div class="eight wide column">
    <div class="ui segment">
      <?php echo form_open('user/edit_user', 'class="ui form" id="user"') ?>
        <div class="field required">
          <label>Username</label>
          <input type="text" name="username" value="<?php echo $user->username ?>" readonly placeholder="Min. 5 huruf">
        </div>

        <div class="field required">
          <label>Password</label>
          <input type="password" name="password" placeholder="Password">
        </div>

        <div class="field required">
          <label>Password Confirmation</label>
          <input type="password" name="passconf" placeholder="Password Confirmation">
        </div>

        <button type="submit" class="ui button submit icon primary" name="submit"><i class="ui icon save"></i> SIMPAN</button>
      </form>
    </div>
  </div>

  <div class="eight wide column">
    <h4 class="ui top attached header">ROLES</h4>
    <div class="ui bottom attached segment">
      <table class="ui table celled compact">
        <thead>
          <tr>
            <th>Modul</th>
            <th>BUAT</th>
            <th>LIHAT</th>
            <th>EDIT</th>
            <th>HAPUS</th>
            <th>SPECIAL</th>
          </tr>
        </thead>
        <tbody>
          <?php foreach ($roles as $role): ?>
          <tr data-module="<?php echo $role['module_id'] ?>" data-username="<?php echo $username ?>" data-token="<?php echo $this->security->get_csrf_hash() ?>">
            <td>
              <label>
                <input type="checkbox" class="add_module_checkbox" value="<?php echo $role['module_id'] ?>" <?php echo empty($role['username']) ? "" : "checked" ?>>
                <?php echo $role['module_name'] ?>
              </label>
            </td>
            <td><input type="checkbox" class="set_authorization" data-auth="can_create" <?php echo empty($role['can_create']) ? "" : "checked" ?>></td>
            <td><input type="checkbox" class="set_authorization" data-auth="can_read" <?php echo empty($role['can_read']) ? "" : "checked" ?>></td>
            <td><input type="checkbox" class="set_authorization" data-auth="can_update" <?php echo empty($role['can_update']) ? "" : "checked" ?>></td>
            <td><input type="checkbox" class="set_authorization" data-auth="can_delete" <?php echo empty($role['can_delete']) ? "" : "checked" ?>></td>
            <td><input type="checkbox" class="set_authorization" data-auth="special" <?php echo empty($role['special']) ? "" : "checked" ?>></td>
          </tr>
          <?php endforeach; ?>
        </tbody>
      </table>
    </div>
  </div>
</div>
