<h1 class="ui dividing header">USER / ADMIN</h1>
<?php echo show_flash_data() ?>
<table class="ui celled table">
  <thead>
    <th>USERNAME</th>
    <th></th>
  </thead>
  <tbody>
  <?php foreach ($user as $val): ?>
    <tr>
      <td><?php echo $val['username'] ?></td>
      <td><a href="user/edit/<?php echo $val['username'] ?>">Edit</a></td>
    </tr>
  <?php endforeach; ?>
  </tbody>
</table>

<button type="button" class="ui icon green button" onclick="$('.ui.modal').modal('show')"><i class='ui icon plus'></i> TAMBAH</button>

<div class="ui modal">
  <div class="header">
    <h1 class="ui header">USER</h1>
  </div>
  <div class="content">
    <?php echo form_open('user/add_user', 'class="ui form" id="user"') ?>
      <div class="six wide field required">
        <label>Username</label>
        <input type="text" name="username" placeholder="Min. 5 huruf">
      </div>

      <div class="six wide field required">
        <label>Password</label>
        <input type="password" name="password" placeholder="Password">
      </div>

      <div class="six wide field required">
        <label>Password Confirmation</label>
        <input type="password" name="passconf" placeholder="Password Confirmation">
      </div>

      <button type="submit" class="ui button submit icon primary" name="submit"><i class="ui icon save"></i> SIMPAN</button>
    </form>
  </div>
</div>
