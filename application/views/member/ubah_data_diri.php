<div class="ui fluid main container">
    <div class="ui grid" style="margin-top: 4em;">
      <div class="three wide column">
        <div class="ui vertical fluid pointing menu">
          <a class="item" href="<?php echo site_url('member/') ?>"><i class="ui icon home"></i> Home</a>
          <a class="item" href="<?php echo site_url('member/ubah_data_diri') ?>"><i class="ui icon user"></i> Ubah Data Diri</a>
          <a class="item" href="<?php echo site_url('magang/logout') ?>"><i class="ui icon open"></i> Logout</a>
        </div>
      </div>

      <div class="thirteen wide column">
        <div class="ui segment">
          <div class="ui top attached tabular menu">
            <a class="item active" data-tab="biodata">BIODATA</a>
            <a class="item" data-tab="passport">PASSPORT</a>
            <a class="item" data-tab="keluarga">KELUARGA</a>
            <a class="item" data-tab="riwayat_pekerjaan">RIWAYAT PEKERJAAN</a>
            <a class="item" data-tab="riwayat_pendidikan">RIWAYAT PENDIDIKAN</a>
            <a class="item" data-tab="password">PASSWORD</a>
          </div>

          <!-- EDIT BIODATA -->
          <div class="ui bottom attached segment tab active" data-tab="biodata">
              <div class="ui grid">
                <div class="ten wide column">
                  <?php echo form_open('member/update_biodata', 'class="ui equal width form" id="form_biodata"') ?>
                    <input type="hidden" name="kd_pemagang" value="<?php echo $pemagang->kd_pemagang ?>">
                    <div class="two fields">
                      <div class="four wide field">
                        <label>Nama Lengkap</label>
                      </div>
                      <div class="twelve wide field">
                        <input type="text" name="nama_pem" value="<?php echo $pemagang->nama_pem ?>">
                      </div>
                    </div>

                    <div class="two fields">
                      <div class="four wide field">
                        <label>E-Mail</label>
                      </div>

                      <div class="twelve wide field">
                        <input type="text" name="email" value="<?php echo $pemagang->email ?>">
                      </div>
                    </div>

                    <div class="two fields">
                      <div class="four wide field">
                        <label>Tanggal Lahir</label>
                      </div>

                      <div class="twelve wide field">
                        <div class="three fields">
                          <div class="field">
                            <select class="ui dropdown" name="tanggal_lahir">
                            <?php echo option_tanggal(Date('d', strtotime($pemagang->tanggal_lahir))) ?>
                            </select>
                          </div>
                          <div class="field">
                            <select class="ui dropdown" name="bulan_lahir">
                              <?php echo option_bulan(Date('m', strtotime($pemagang->tanggal_lahir))) ?>
                            </select>
                          </div>
                          <div class="field">
                            <select class="ui dropdown" name="tahun_lahir">
                              <?php echo option_tahun(50, Date('Y', strtotime($pemagang->tanggal_lahir))) ?>
                            </select>
                          </div>
                        </div>
                      </div>
                    </div>

                    <div class="two fields">
                      <div class="four wide field">
                        <label>Tempat Lahir</label>
                      </div>

                      <div class="twelve wide field">
                        <input type="text" name="tempat_lahir" value="<?php echo $pemagang->tempat_lahir ?>">
                      </div>
                    </div>

                    <div class="two fields">
                      <div class="four wide field">
                        <label>Alamat Sekarang</label>
                      </div>

                      <div class="twelve wide field">
                        <textarea name="tempat_tinggal" form="form_biodata" rows="3"><?php echo $pemagang->tempat_tinggal ?></textarea>
                      </div>
                    </div>

                    <div class="two fields">
                      <div class="four wide field">
                        <label>Provinsi</label>
                      </div>

                      <div class="twelve wide field">
                        <select class="ui dropdown search" name="kd_provinsi">
                          <?php foreach ($provinsi as $prov): ?>
                          <option value="<?php echo $prov['kd_provinsi'] ?>"><?php echo $prov['provinsi'] ?></option>
                          <?php endforeach; ?>
                        </select>
                      </div>
                    </div>

                    <div class="two fields">
                      <div class="four wide field">
                        <label>Nomor Telepon</label>
                      </div>

                      <div class="twelve wide field">
                        <input type="text" name="no_tel" value="<?php echo $pemagang->no_tel ?>">
                      </div>
                    </div>

                    <div class="two fields">
                      <div class="four wide field">
                        <label>Nomor Handphone</label>
                      </div>

                      <div class="twelve wide field">
                        <input type="text" name="no_hp" value="<?php echo $pemagang->no_hp ?>">
                      </div>
                    </div>

                    <div class="two fields">
                      <div class="four wide field">
                        <label>Nomor Handphone Tambahan #2</label>
                      </div>

                      <div class="twelve wide field">
                        <input type="text" name="no_hp1" value="<?php echo $pemagang->no_hp1 ?>">
                      </div>
                    </div>

                    <div class="two fields">
                      <div class="four wide field">
                        <label>Nomor Handphone Tambahan #3</label>
                      </div>

                      <div class="twelve wide field">
                        <input type="text" name="no_hp2" value="<?php echo $pemagang->no_hp2 ?>">
                      </div>
                    </div>

                    <div class="two fields">
                      <div class="four wide field">
                        <label>Suka Pelajaran</label>
                      </div>

                      <div class="twelve wide field">
                        <input type="text" name="suka_pelajaran" value="<?php echo $pemagang->suka_pelajaran ?>">
                      </div>
                    </div>

                    <div class="two fields">
                      <div class="four wide field">
                        <label>Hobi</label>
                      </div>

                      <div class="twelve wide field">
                        <input type="text" name="hobi" value="<?php echo $pemagang->hobi ?>">
                      </div>
                    </div>

                    <div class="two fields">
                      <div class="four wide field">
                        <label>Keahlian</label>
                      </div>

                      <div class="twelve wide field">
                        <input type="text" name="skil" value="<?php echo $pemagang->skil ?>">
                      </div>
                    </div>

                    <div class="two fields">
                      <div class="four wide field">
                        <label>Kewarganegaraan</label>
                      </div>

                      <div class="twelve wide field">
                        <input type="text" name="warga_negara" value="<?php echo $pemagang->warga_negara ?>">
                      </div>
                    </div>
                    <button type="submit" class="ui icon button primary"><i class="ui icon save"></i> SIMPAN</button>
                  </form>
                </div>

                <div class="six wide column">

                  <?php echo form_open('', 'class="ui form"') ?>
                    <div class="ui segment">
                      <img id="upload-container" class="segment" src="<?php echo 'https://www.minori.co.id/media/'. md5($pemagang->kd_pemagang.'manfikar') .'/'.$pemagang->foto ?>"/>
                        <div class="field">
                          <label class="ui button bottom">
                            Ganti Foto
                            <input type="file" name="file_foto" style="position: absolute; top: -9999px" id="upload" accept="image/*">
                          </label>
                        </div>
                    </div>
                    <button type="button" name="button" class="ui icon button primary"><i class="ui icon upload"></i> UPLOAD</button>
                  </form>
                </div>
              </div>
          </div>

          <div class="ui bottom attached segment tab" data-tab="passport">
            <?php echo form_open('member/update_passport', 'class="ui form"') ?>
              <input type="hidden" name="kd_pemagang" value="<?php echo !empty($pemagang->kd_pemagang) ? $pemagang->kd_pemagang : "" ?>">
              <div class="two fields">
                <div class="four wide field">
                  <label>Nomor Passport</label>
                </div>

                <div class="six wide field">
                  <input type="text" name="no_pass" value="<?php echo !empty($passport->no_pass) ? $passport->no_pass : "" ?>">
                </div>
              </div>

              <div class="two fields">
                <div class="four wide field">
                  <label>Masa Berlaku</label>
                </div>

                <div class="six wide field">
                  <input type="text" name="masa_berlaku" value="<?php echo !empty($passport->masa_berlaku) ? $passport->masa_berlaku : "" ?>">
                </div>
              </div>

              <div class="two fields">
                <div class="four wide field">
                  <label>Tanggal Terbit</label>
                </div>

                <div class="six wide field">
                  <input type="text" name="terbit" value="<?php echo !empty($passport->terbit) ? $passport->terbit : "" ?>">
                </div>
              </div>
              <button type="submit" class="ui icon button brown"><i class="ui icon save"></i> SIMPAN</button>
            </form>
          </div>

          <div class="ui bottom attached segment tab" data-tab="keluarga">
            <?php echo form_open('', 'class="ui form"') ?>
              <table class="ui table celled">
                <thead>
                  <tr>
                    <th>Hubungan</th>
                    <th>Nama</th>
                    <th>Tanggal Lahir</th>
                    <th>Pekerjaan</th>
                    <th>Kontak</th>
                  </tr>
                </thead>
                <tbody>
                  <?php foreach ($keluarga as $k): ?>
                  <tr>
                    <td><?php echo $k['hubungan'] ?></td>
                    <td>
                      <div class="ui input">
                        <input type="text" name="" value="<?php echo $k['nama_kel'] ?>">
                      </div>
                    </td>
                    <td><?php echo $k['tgl'] ?></td>
                    <td><?php echo $k['pekerjaan'] ?></td>
                    <td>
                      <div class="ui input">
                        <input type="text" name="" value="<?php echo $k['no_telp'] ?>">
                      </div>
                    </td>
                  </tr>
                  <?php endforeach; ?>

                  <?php foreach ($keluarga_pem as $k): ?>
                  <tr>
                    <td><?php echo $k['hubungan'] ?></td>
                    <td>
                      <div class="ui input">
                        <input type="text" name="" value="<?php echo $k['nama'] ?>">
                      </div>
                    </td>
                    <td><?php echo $k['tanggal_lahir'] ?></td>
                    <td><?php echo $k['pekerjaan'] ?></td>
                    <td>
                      <div class="ui input">
                        <input type="text" name="" value="<?php echo $k['hp'] ?>">
                      </div>
                    </td>
                  </tr>
                  <?php endforeach; ?>
                </tbody>
              </table>
              <button type="submit" class="ui icon button green"><i class="ui icon save"></i> SIMPAN</button>
            </form>
          </div>

          <div class="ui bottom attached segment tab" data-tab="riwayat_pekerjaan">
            <table class="ui table celled">
              <thead>
                <tr>
                  <th>Perusahaan</th>
                  <th>Posisi</th>
                  <th>Mulai</th>
                  <th>Selesai</th>
                </tr>
              </thead>
              <tbody>
                <?php foreach ($riwayat_pekerjaan as $rp): ?>
                <tr>
                  <td><?php echo $rp['perusahaan'] ?></td>
                  <td><?php echo $rp['pekerjaan'] ?></td>
                  <td><?php echo $rp['bulan_masuk'] . '-' . $rp['tahun_masuk'] ?></td>
                  <td><?php echo $rp['bulan_keluar'] . '-' . $rp['tahun_keluar'] ?></td>
                </tr>
                <?php endforeach; ?>
              </tbody>
            </table>
          </div>

          <div class="ui bottom attached segment tab" data-tab="riwayat_pendidikan">
            <table class="ui table celled">
              <thead>
                <tr>
                  <th>Nama Sekolah</th>
                  <th>Tingkatan</th>
                  <th>Jurusan</th>
                  <th>Mulai</th>
                  <th>Selesai</th>
                </tr>
              </thead>
              <tbody>
                <?php foreach ($riwayat_pendidikan as $rp): ?>
                <tr>
                  <td><?php echo $rp['instansi'] ?></td>
                  <td><?php echo $rp['id_pendidikan'] ?></td>
                  <td><?php echo $rp['nama_jurusan'] ?></td>
                  <td><?php echo $rp['mulai'] ?></td>
                  <td><?php echo $rp['selesai'] ?></td>
                </tr>
                <?php endforeach; ?>
              </tbody>
            </table>
          </div>

          <div class="ui bottom attached segment tab" data-tab="password">
            <?php echo form_open('member/update_password', 'class="ui form"') ?>
              <input type="hidden" name="kd_pemagang" value="<?php echo $pemagang->kd_pemagang ?>">
              <div class="two fields">
                <div class="four wide field">
                  <label>Password Baru</label>
                </div>

                <div class="six wide field">
                  <input type="text" name="password" value="">
                </div>
              </div>

              <div class="two fields">
                <div class="four wide field">
                  <label>Konfirmasi Password Baru</label>
                </div>
                <div class="six wide field">
                  <input type="text" name="password_confirmation" value="">
                </div>
              </div>
              <button type="submit" class="ui icon button orange"><i class="ui icon save"></i> SIMPAN</button>
            </form>
          </div>
        </div>
      </div>s
    </div>
</div>
