<div class="ui fluid main container">
    <div class="ui grid" style="margin-top: 4em;">
      <div class="three wide column">
        <div class="ui vertical fluid pointing menu">

          <a class="item active" href="<?php echo site_url('member/') ?>"><i class="ui icon home"></i> Home</a>
          <a class="item" href="<?php echo site_url('member/ubah_data_diri') ?>"><i class="ui icon user"></i> Ubah Data Diri</a>
          <a class="item" href="<?php echo site_url('magang/logout') ?>"><i class="ui icon open"></i> Logout</a>
        </div>
      </div>

      <div class="nine wide column">
        <div class="ui segment">
          <h2 class="ui dividing blue header">PENGUMUMAN</h2>

          <table class="ui very basic compact table">
            <thead>
              <th></th>
              <th></th>
              <th></th>
            </thead>
            <tbody>
            <?php foreach ($pengumuman as $p): ?>
              <tr>
                <td><?php echo $p['tanggal'] ?></td>
                <td><?php echo $p['Judul'] ?></td>

                <td><a href="<?php echo site_url('magang/show_pengumuman/') . $p['kd']?>">Lihat</a></td>
              </tr>
            <?php endforeach; ?>
            </tbody>
          </table>
        </div>

        <div class="ui segment">
          <h2 class="ui dividing blue header">LOWONGAN</h2>
          <table class="ui very basic compact table">
            <thead>
              <th></th>
              <th></th>
              <th></th>
            </thead>
            <tbody>
            <?php foreach ($lowongan as $p): ?>
              <tr>
                <td><?php echo $p['awal'] ?></td>
                <td><?php echo $p['judul'] ?></td>
                <td><a href="<?php echo site_url('magang/show_lowongan/') . $p['kd']?>">Lihat</a></td>
              </tr>
            <?php endforeach; ?>
            </tbody>
          </table>
        </div>
      </div>

      <div class="four wide column">
        <div class="ui segment">
          <h3 class="ui blue header">FAQ</h3>
          <p>
            Pertanyaan : </br>
            Kenapa Saya Harus Mendaftar online? sedangkan saya sudah mengirim surat lamaran.</br>
            Jawaban :</br>
            Karna kami membutuhkan data anda secara soft copy (bukan kertas).</br>
            Kenapa ?</br>
            Ada beberapa keuntungan</br>
            1. data anda tidak akan pernah hilang dalam database kami, jika dalam bentuk kertas, bisa saja data anda terselip. atau rusak</br>
            2. Data anda mudah di temukan oleh Kami.</br>
            3. Data anda bisa anda rubah kapanpun dan di manapun asalkan ada sambungan internet, tanpa harus datang langsung ke kantor kami</br>
            Dan banyak lagi keuntunganya</br>
          </p>
        </div>
      </div>
    </div>
</div>
