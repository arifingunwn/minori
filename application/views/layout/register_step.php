<div class="ui main container">
  <div class="ui mini steps">
    <a href="register" class="step <?php echo isset($biodata) ? "active" : ""; ?>">
      <i class="icon user"></i>
      <div class="content">
        <div class="title">
          BIODATA
        </div>
        <div class="description">
          Silahkan isi data diri
        </div>
      </div>
    </a>

    <a href="register2" class="step <?php echo isset($bahasa) ? "active" : ""; ?>">
      <i class="icon talk"></i>
      <div class="content">
        <div class="title">
          KEAHLIAN BAHASA
        </div>
      </div>
    </a>

    <a href="register3" class="step <?php echo isset($active_pekerjaan) ? "active" : ""; ?>">
      <i class="icon suitcase"></i>
      <div class="content">
        <div class="title">
          RIWAYAT PEKERJAAN
        </div>
        <div class="description">
          Silahkan isi data diri
        </div>
      </div>
    </a>

    <a href="register4" class="step <?php echo isset($pendidikan) ? "active" : ""; ?>">
      <i class="icon university"></i>
      <div class="content">
        <div class="title">
          PENDIDIKAN
        </div>
        <div class="description">
          Silahkan isi data diri
        </div>
      </div>
    </a>

    <a href="register5" class="step <?php echo isset($kerabat) ? "active" : ""; ?>">
      <i class="icon users"></i>
      <div class="content">
        <div class="title">
          DATA KERABAT
        </div>
        <div class="description">
          Silahkan isi data diri
        </div>
      </div>
    </a>

    <a href="register_finnish" class="step <?php echo isset($selesai) ? "active" : ""; ?>">
      <i class="icon checkmark"></i>
      <div class="content">
        <div class="title">
          SELESAI
        </div>
        <div class="description">
          Silahkan isi data diri
        </div>
      </div>
    </a>
  </div>
</div>
