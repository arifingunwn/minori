<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <title>Magang</title>
    <link rel="stylesheet" href="<?php echo base_url('bower_components/semantic/dist/semantic.min.css') ?>" media="screen" title="no title" charset="utf-8">
    <link rel="stylesheet" href="<?php echo base_url('bower_components/Croppie/croppie.css') ?>" media="screen" title="no title" charset="utf-8">
    <link rel="stylesheet" href="<?php echo base_url('bower_components/sweetalert/dist/sweetalert.css')?>" media="screen" title="no title" charset="utf-8">
    <link rel="stylesheet" href="<?php echo base_url('assets/stylesheet/style.css') ?>">
    <style media="screen">
      .input {
        margin-bottom: 2em;
      }

      .main {
        margin-top: 7em;
      }

      body{
        background: #eaeaea;
      }
    </style>
  </head>
<body>

  <div class="ui small modal">
    <div class="header">
      <h2 class="ui blue header">LOGIN</h2>
    </div>
    <div class="content">
      <?php echo form_open('magang/login', 'class="ui form" id="member_login"') ?>
        <div class="field">
          <label>E-mail</label>
          <input type="email" name="email" placeholder="email">
        </div>

        <div class="field">
          <label>Password</label>
          <input type="password" name="password">
        </div>
        <button type="submit" name="login" class="ui icon submit primary button">Login</button>
        <a href="<?php echo site_url('magang/forget_password') ?>">Lupa password.</a>
      <?php echo form_close(); ?>
    </div>
  </div>

  <div class="ui top fixed menu">
    <div class="ui container">
      <div class="ui text menu">
        <a href="<?php echo site_url() ?>" class="item">
          <img src="<?php echo base_url('assets/img/logo.png') ?>" alt="Minori's logo" class="ui logo" />
        </a>
      </div>
      <?php if (isset($_SESSION['member'])): ?>

      <?php else: ?>
      <div class="right menu">
        <a href="#" class="item" onclick="$('.ui.modal').modal('show')"><i class="icon users"></i>LOGIN</a>
        <a href="<?php echo site_url() ?>" class="item"><i class="icon user"></i>DAFTAR</a>
      </div>
      <?php endif; ?>
    </div>
  </div>
