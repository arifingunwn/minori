<div class="ui inverted footer bottom fixed segment" style="margin-bottom: 0;">
  <div class="ui left aligned container">
      <div class="ui stackable inverted divided grid">
        <div class="nine wide column">
          <h4 class="ui inverted header">Web Lainnya</h4>
          <div class="ui inverted link list">
            <div class="item">
              <h4>Kursus Bahasa Jepang</h4>
              <a href="http://ayumi.co.id">http://ayumi.co.id</a>
            </div>
            <div class="item">
              <h4>Jasa Translator dan Interpreter</h4>
              <a href="http://ayumi.co.id">http://ayumi.co.id</a>
            </div>
          </div>
        </div>
        <div class="seven wide column">
          <h4 class="ui inverted header">AFILIASI</h4>
              <img src="<?php echo site_url('assets/img/newlogo.jpg') ?>" alt="affiliate">
              <img src="<?php echo site_url('assets/img/RenCreativeslogonew-web.jpg') ?>" alt="affiliate">
              <img src="<?php echo site_url('assets/img/ayumilogonew-web.jpg') ?>" alt="affiliate">
              <img src="<?php echo site_url('assets/img/milestonelogonew-web.jpg') ?>" alt="affiliate">
        </div>
      </div>
      <div class="ui inverted section divider"></div>
      <div class="ui horizontal inverted small divided link list">
        <a class="item" href="#">Site Map</a>
        <a class="item" href="#">Contact Us</a>
        <a class="item" href="#">Terms and Conditions</a>
        <a class="item" href="#">Privacy Policy</a>
      </div>
    </div>
</div>

<script type="text/javascript" src="<?php echo base_url('bower_components/jquery/dist/jquery.min.js') ?>"></script>
<script type="text/javascript" src="<?php echo base_url('bower_components/semantic/dist/semantic.min.js') ?>"></script>
<script type="text/javascript" src="<?php echo base_url('bower_components/Croppie/croppie.min.js')?>"></script>
<script type="text/javascript" src="<?php echo base_url('bower_components/sweetalert/dist/sweetalert.min.js') ?>"></script>
<script type="text/javascript" src="<?php echo base_url('assets/js/script.js') ?>"></script>
<script type="text/javascript">
  $(document).ready(function(){
    Register.init();
    $('.menu.tabular .item').tab();
  });
</script>
</body>
</html>
