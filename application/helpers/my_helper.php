<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

function searchArray($needle, $haystack, $key, $value)
{
  foreach ($haystack as $k => $v) {
    if ($v[$key] == $needle) {
      return $v[$value];
    }
  }
  return '';
}

function checkAuth($array, $module_id, $tipe)
{
  foreach ($array as $value) {
    if($value[$tipe] == 1 && $value['module_id'] == $module_id)
    {
      return true;
    }
  }
  return false;
}

function cetak_nullable($val)
{
  if(isset($val) && !empty($val))
  {
    return $val;
  }
  else{
    return '';
  }
}

function option_tanggal($selected = 0)
{
  $str = '';
  for($i = 1; $i<=31; $i++) {
    if($selected == $i)
    {
      $str .= "<option selected value={$i}>{$i}</option> \n";
    }

    else{
      $str .= "<option value={$i}>{$i}</option> \n";
    }
  }

  return $str;
}

function option_bulan($selected = 0)
{
    $str = '';
    $str .= '<option value="1" ' . ($selected == 1 ? 'selected' : '' ). ' >Januari</option>';
    $str .= '<option value="2" ' . ($selected == 2 ? 'selected' : '' ). ' >Februari</option>';
    $str .= '<option value="3" ' . ($selected == 3 ? 'selected' : '' ). ' >Maret</option>';
    $str .= '<option value="4" ' . ($selected == 4 ? 'selected' : '' ). ' >April</option>';
    $str .= '<option value="5" ' . ($selected == 5 ? 'selected' : '' ). ' >Mei</option>';
    $str .= '<option value="6" ' . ($selected == 6 ? 'selected' : '' ). ' >Juni</option>';
    $str .= '<option value="7" ' . ($selected == 7 ? 'selected' : '' ). ' >Juli</option>';
    $str .= '<option value="8" ' . ($selected == 8 ? 'selected' : '' ). ' >Agustus</option>';
    $str .= '<option value="9" ' . ($selected == 9 ? 'selected' : '' ). ' >September</option>';
    $str .= '<option value="10" ' . ($selected == 10 ? 'selected' : '') . ' >Oktober</option>';
    $str .= '<option value="11" ' . ($selected == 11 ? 'selected' : '') . ' >November</option>';
    $str .= '<option value="12" ' . ($selected == 12 ? 'selected' : '') . ' >Desember</option>';

    return $str;
}

function option_tahun($range = 40, $selected = 0)
{
  $tahun = Date("Y");
  $str = '';
  for($i = $tahun; $i>=$tahun - $range; $i--)
  {
    if($selected == $i)
    {
      $str .= "<option selected value={$i}>{$i}</option> \n";
    }

    else{
      $str .= "<option value={$i}>{$i}</option> \n";
    }
  }

  return $str;
}

function option_tahun_passport($range = 40, $selected = 0)
{
  $tahun = Date("Y");
  $str = '';
  for($i = $tahun + 10; $i>=$tahun - $range; $i--)
  {
    if($selected == $i)
    {
      $str .= "<option selected value={$i}>{$i}</option> \n";
    }

    else{
      $str .= "<option value={$i}>{$i}</option> \n";
    }
  }

  return $str;
}

function show_flash_data()
{
  $str = "";
  if(!empty($_SESSION['flag']))
  {
    $str .= '<div class="ui message">';
    $str .= $_SESSION['flag'];
    $str .= '</div>';
  }
  return $str;
}
