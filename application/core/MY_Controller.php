<?php

class MY_Controller extends CI_Controller
{
  protected $access = '*';
  protected $user_auth = array();

  public function __construct()
  {
    parent::__construct();
    $this->load->library('session');
    $this->login_check();
    $this->get_auth();

  }

  private function get_auth()
  {
    if(!empty($_SESSION['user']))
    {
      if(empty($_SESSION['authorization']))
      {
        $this->load->database();
        $this->db->select('module_id');
        $this->db->from('ms_authorization');
        $this->db->where(array(
          'username' => $_SESSION['user']
        ));
        $module = $this->db->get()->result_array();

        unset($_SESSION['authorization']);

        foreach ($module as $val) {
          $this->db->select('can_create, can_read, can_update, can_delete, special');
          $this->db->from('ms_authorization');
          $this->db->where(array(
            'module_id' => $val['module_id'],
            'username'  => $_SESSION['user']
          ));
          $_SESSION['authorization'][$val['module_id']] = $this->db->get()->result_array();
        }
      }
    }
  }

  private function login_check()
  {
    if($this->access != "*")
    {
      if (empty($_SESSION['user'])) {
        redirect('auth');
      }
      // if(!$this->check_roles())
      // {
      //   die("Permissions denied!");
      // }
    }
  }

  private function check_roles()
  {
    if($this->access == "@")
    {
      return true;
    }

    else {
      $access = is_array($this->access) ?
        $this->access :
        explode(',', $this->access);

        $roles = $this->session->userdata('roles');
        if(is_array($roles)){
          foreach ($roles as $r) {
            if(in_array($r['nama_role'], $access))
            {
              return true;
            }
          }
        }
      return false;
    }
  }
}

 ?>
