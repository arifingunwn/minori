var Register = (function(){
  var init = {};
  function Front()
  {
    $('input#ktp').on('blur', function(){
      var no_ktp = $(this).val();
      $.ajax({
        url: 'magang/check_ktp/' + no_ktp,
        contentType: 'application/json; utf-8',
        dataType: 'json',
        success: function(data){
          if(data != 0)
          {
            swal('Anda sudah pernah mendaftar');
          }
        }
      });
    });

    $.fn.form.settings.rules.checkEmail = function(value, checkEmail) {
      var valid = false;
      $.ajax({
        url: 'magang/check_email',
        type: 'POST',
        async: false,
        data: { email: value },
        success: function(data){
          if(data == 0)
          {
            valid = true;
          }
          else
          {
            valid = false;
          }
        }
      });

      return valid;
    };

    $('#front_form').form({
      inline: true,
      on: 'blur',
      fields: {
        ktp: {
            identifier: 'ktp',
            rules: [
              {
                type: 'empty',
                prompt: 'Nomor KTP harus diisi'
              },
              {
                type: 'number',
                prompt: 'Nomor KTP hanya angka'
              }
            ]
        },

        email: {
          identifier: 'email',
          rules: [
            {
              type: 'empty',
              prompt: 'Email harus diisi'
            },
            {
              type: 'email',
              prompt: 'Email tidak valid'
            },
            {
              type: 'checkEmail',
              prompt: 'Email tersebut sudah terdaftar'
            }
          ]
        },

        password: {
          identifier: 'password',
          rules: [
            {
              type   : 'empty',
              prompt : 'Password harus diisi'
            },
            {
              type   : 'minLength[6]',
              prompt : 'Password harus melebihi {ruleValue} karakter'
            }
          ]
        },

        password_confirmation: {
          identifier: 'password_confirmation',
          rules: [
            {
              type: 'match[password]',
              prompt: 'Password harus sama'
            },
            {
              type   : 'empty',
              prompt : 'Password harus diisi'
            }
          ]
        }
      }
    });

    $('form#member_login').form({
      inline: true,
      on: 'blur',
      fields: {
        email: {
          identifier: 'email',
          rules: [
            {
              type: 'empty',
              prompt: 'email tidak boleh kosong'
            },
            {
              type: 'email',
              prompt: 'Harap masukkan email yang valid'
            }
          ]
        },

        password: {
          identifier: 'password',
          rules: [
            {
              type   : 'empty',
              prompt : 'Password harus diisi'
            }
          ]
        },
      }
    });
  }

  function DataPribadi()
  {
    // $('.memiliki_passport').hide();
    // $('.pernah_keluar_negri').hide();
    $('#btn_tambah_negara_pernah_dikunjungi').hide();

    $('input[name=passport]').on('change', function(e){
      if(e.target.value == 'Memiliki')
      {
        $('.memiliki_passport').show();
      }
      else {
        $('.memiliki_passport').hide();
      }
    });

    $('input[name=pernah_keluar_negri]').on('change', function(e){
      if(e.target.value == 'p')
      {
        $('.pernah_keluar_negri').show();
        $('#btn_tambah_negara_pernah_dikunjungi').show();
      }
      else {
        $('.pernah_keluar_negri').hide();
        $('#btn_tambah_negara_pernah_dikunjungi').hide();
      }
    });

    $('#btn_tambah_negara_pernah_dikunjungi').on('click', function(){
      $.ajax({
        url: 'getNegaraJSON',
        contentType: 'application/json; utf-8',
        type: 'POST',
        dataType: 'json',
        success: function(data){
          var number = parseInt($('.pernah_keluar_negri_child').length);
          var str =
          '<div class="fields pernah_keluar_negri_child">' +
            '<div class="four wide field">' +
              '<label>Negara</label>' +
              '<select class="ui dropdown" name="negara_pernah_dikunjungi[' + number + '][negara]">';

          $.each(data, function(k, v){
            str += "<option value=\'" + v + "\'" + ">" + v + "</option> \n";
          });
          str +=
              '</select>' +
            '</div>' +

            '<div class="six wide field">' +
              '<label>Selama</label>' +
              '<div class="inline fields">' +
                '<input type="text" name="negara_pernah_dikunjungi[' + number + '][tahun]" placeholder="1">' +
                '<label>Tahun</label>' +
                '<input type="text" name="negara_pernah_dikunjungi[' + number + '][bulan]" placeholder="1">' +
                '<label>Bulan</label>' +
              '</div>' +
              '<div class="field">' +
                '<label>Tujuan</label>' +
                '<input type="text" name="negara_pernah_dikunjungi[' + number + '][tujuan]" placeholder="Tujuan: Liburan / Sekolah">' +
                '<button type="button" class="ui mini icon button red" onclick="$(this).parent().parent().parent().remove();"><i class="ui icon x"></i></button>' +
              '</div>' +
            '</div>' +
          '</div>';

          $('.pernah_keluar_negri').append(str);
        }
      });
    });

    $('#register_form_1').form({
      inline: true,
      on: 'blur',
      fields: {
        nama_lengkap: {
          identifier: 'nama_lengkap',
          rules: [
            {
              type: 'empty',
              prompt: 'nama lengkap tidak boleh kosong'
            },
          ]
        },
        tempat_lahir: {
          identifier: 'tempat_lahir',
          rules: [
            {
              type: 'empty',
              prompt: 'Tempat lahir harus diisi'
            },
          ]
        },
        alamat_sekarang: {
          identifier: 'alamat_sekarang',
          rules: [
            {
              type: 'empty',
              prompt: 'Alamat sekarang harus diisi'
            }
          ]
        },
        kota_asal: {
          identifier: 'kota_asal',
          rules: [
            {
              type: 'empty',
              prompt: 'Kota Asal harus diisi'
            }
          ]
        },
        kewarganegaraan: {
          identifier: 'kewarganegaraan',
          rules: [{
            type: 'empty',
            prompt: 'kewarganegaraan harus diisi'
          }]

        },
        alamat_ktp: {
          identifier: 'alamat_ktp',
          rules: [{
            type: 'empty',
            prompt: 'alamat ktp harus diisi'
          }]

        },
        nomor_handphone: {
          identifier: 'nomor_handphone',
          rules: [{
            type: 'empty',
            prompt: 'Nomor Hp harus diisi'
          }]

        },
        ukuran_sepatu: {
          identifier: 'ukuran_sepatu',
          rules: [{
            type: 'empty',
            prompt: 'Ukuran sepatu harus diisi'
          }]

        },
        lingkar_pinggang: {
          identifier: 'lingkar_pinggang',
          rules: [{
            type: 'empty',
            prompt: 'Lingkar pinggang harus diisi'
          }]

        },
        berat_badan: {
          identifier: 'berat_badan',
          rules: [{
            type: 'empty',
            prompt: 'Berat badan harus diisi'
          }]

        },
        tinggi_badan: {
          identifier: 'tinggi_badan',
          rules: [{
            type: 'empty',
            prompt: 'Tinggi badan harus diisi'
          }]

        },
        pelajaran_favorit: {
          identifier: 'pelajaran_favorit',
          rules: [{
            type: 'empty',
            prompt: 'Pelajaran favorit harus diisi'
          }]

        },
        hobi: {
          identifier: 'hobi',
          rules: [{
            type: 'empty',
            prompt: 'Hobi harus harus diisi'
          }]

        },
        keterampilan: {
          identifier: 'keterampilan',
          rules: [{
            type: 'empty',
            prompt: 'Keterampilan harus diisi'
          }]
        }
      }
    });
  }

  function PreparePhotoUpload() {
    var $uploadCrop;
		function readFile(input) {
 			if (input.files && input.files[0]) {
         var reader = new FileReader();
         reader.onload = function (e)
         {
           $('.upload-container').addClass('ready');
	            	$uploadCrop.croppie('bind', {
	            		url: e.target.result
	            	}).then(function(){
	            		console.log('jQuery bind complete');
	            	});
              }
        reader.readAsDataURL(input.files[0]);
        }
        else
        {
          swal("Sorry - you're browser doesn't support the FileReader API");
		    }
		}

		$uploadCrop = $('#upload-container').croppie({
      viewport: {
        width: 150,
        height: 200,
        type: 'square'
      },

      boundary: {
        width: 300,
        height: 200
      }
		});

    $('#btn_submit').on('click', function(){
        $uploadCrop.croppie('result', {
          type: 'canvas',
          size: 'viewport'
        }).then(function (resp){
          $.ajax({
            url: 'uploadPhoto',
            type: 'POST',
            data: { image: resp },
            success: function(data)
            {
              if(data.length > 0)
              {
                $('#hidden_photo').val(data);
                $('#register_form_1').submit();
              }
            },
            error: function (request, status, error) {
              swal('Upload Foto error, Silahkan Coba lagi.');
            }
          });
        });
    });

		$('input#upload').on('change', function () { readFile(this); });
	}

  function BahasaAsing()
  {
    $('#btn_tambah_bahasa').on('click', function(){
      var number = parseInt($('.dynamic-field').length);
      $('#field_parent')
        .append('<div class="field dynamic-field">' +
                  '<div class="inline fields">' +
                    '<div class="field">' +
                      '<div class="ui radio checkbox">' +
                        '<input type="radio" data-input="bahasa' + number + '" name="bahasa[' + number + ']" value="JEPANG">' +
                        '<label>JEPANG</label>' +
                        '</div>' +
                      '</div>' +
                      '<div class="field">' +
                        '<div class="ui radio checkbox">' +
                          '<input type="radio" data-input="bahasa' + number + '" name="bahasa[' + number + ']" value="INGGRIS">' +
                          '<label>INGGRIS</label>' +
                        '</div>' +
                      '</div>' +
                      '<div class="field">' +
                        '<div class="ui radio checkbox">' +
                          '<input type="radio" class="lainnya" name="bahasa[' + number + ']" data-input="bahasa' + number + '">' +
                            '<label>LAINNYA</label>' +
                        '</div>' +
                        '<input type="text" id="bahasa' + number + '" disabled name="bahasa[' + number + ']">' +
                      '</div>' +
                      '<button type="button" onclick="$(this).parent().remove();" class="ui icon button danger"><i class="ui icon x"></i></button>' +
                    '</div>' +
                  '</div>'
        );
    });

    $(document).on('change', 'input[type=radio].lainnya', function(e){
      $('#' + $(this).data('input')).prop('disabled', false);
    });

    $(document).on('change', 'input[type=radio]:not(.lainnya)', function(){
      $('#' + $(this).data('input')).prop('disabled', true);
    });
  }

  function FormPekerjaan()
  {
    $('#btn_tambah_pekerjaan').on('click', function(){
      $.ajax({
        url: 'getAllPekerjaan',
        contentType: 'application/json; utf-8',
        dataType: 'JSON',
        type: 'POST',
        success: function(data)
        {
          if(!$.isEmptyObject(data))
          {
            var number = $('.dynamic-field').length + 1;
            var temp = '';
            var pekerjaan_select_input = '';
            var tahun_option = getYearSelectOption();

            $.each(data, function(k, v){
              if(k == 0)
              {
                pekerjaan_select_input += "<optgroup>"
              }
              else if (v.jenis_pekerjaan != temp && k != 0)
              {
                pekerjaan_select_input += "</optgroup><optgroup label=\'" + v.jenis_pekerjaan +"\'>"
                temp = v.jenis_pekerjaan;
              }
              else
              {
                pekerjaan_select_input += "<option value=\'" + v.kd_pekerjaan + "\'>" + v.pekerjaan + "</option>";
              }
            });
            pekerjaan_select_input += "</optgroup>";

            $('.parent_pekerjaan').append(
              '<div class="fields dynamic-field" id="row' + number +'">' +
                '<div class="four wide field">' +
                  '<label>Nama Perusahaan</label>' +
                  '<input type="text" name="riwayat[' + (number - 1) + '][nama_perusahaan]" value="">' +
                '</div>' +
                '<div class="three wide field">' +
                  '<label>Bidang</label>' +
                  '<select class="ui search dropdown" name="riwayat[' + (number - 1) + '][kd_pekerjaan]">' +
                    pekerjaan_select_input +
                  '</select>' +
                '</div>' +

                '<div class="field">' +
                  '<label>Mulai</label>' +
                  '<div class="inline fields">' +
                    '<div class="field">' +
                      '<select class="ui search dropdown" name="riwayat[' + (number - 1) + '][bulan_mulai]">' +
                        '<option value="" disabled="disabled" >Bulan</option>' +
                        '<option value="1">Januari</option>' +
                        '<option value="2">Februari</option>' +
                        '<option value="3">Maret</option>' +
                        '<option value="4">April</option>' +
                        '<option value="5">Mei</option>' +
                        '<option value="6">Juni</option>' +
                        '<option value="7">Juli</option>' +
                        '<option value="8">Agustus</option>' +
                        '<option value="9">September</option>' +
                        '<option value="10">Oktober</option>' +
                        '<option value="11">November</option>' +
                        '<option value="12">Desember</option>' +
                      '</select>' +
                    '</div>' +
                    '<div class="field">' +
                      '<select class="ui search dropdown" name="riwayat[' + (number - 1) + '][tahun_mulai]">' +
                        tahun_option +
                      '</select>' +
                    '</div>' +
                  '</div>' +
                '</div>' +

                '<div class="field">' +
                  '<label>Selesai</label>' +
                  '<div class="inline fields">' +
                    '<div class="field">' +
                      '<select class="ui search dropdown" name="riwayat[' + (number - 1) + '][bulan_selesai]">' +
                        '<option value="" disabled="disabled" >Bulan</option>' +
                        '<option value="1">Januari</option>' +
                        '<option value="2">Februari</option>' +
                        '<option value="3">Maret</option>' +
                        '<option value="4">April</option>' +
                        '<option value="5">Mei</option>' +
                        '<option value="6">Juni</option>' +
                        '<option value="7">Juli</option>' +
                        '<option value="8">Agustus</option>' +
                        '<option value="9">September</option>' +
                        '<option value="10">Oktober</option>' +
                        '<option value="11">November</option>' +
                        '<option value="12">Desember</option>' +
                      '</select>' +
                    '</div>' +
                    '<div class="field">' +
                      '<select class="ui search dropdown" name="riwayat[' + (number - 1) + '][tahun_selesai]">' +
                        tahun_option +
                      '</select>' +
                      '<button type="button" class="ui button icon red" onclick="$(\'#row' + number + '\').remove()"><i class="ui icon x"></i></button>' +
                    '</div>' +
                  '</div>' +
                '</div>'+
              '</div>'
            );
          }
        }
      });
    });
  }

  function Pendidikan()
  {
    var tahun_option = getYearSelectOption();
    $('#btn_tambah_sekolah_lanjutan').on('click', function(){
      var number = $('.sekolah_lanjutan_child').length;
      var str =
      '<div class="sekolah_lanjutan_child">' +
        '<div class="fields">' +
          '<div class="eight wide field">' +
            '<label>NAMA INSTANSI</label>' +
            '<input type="text" name="sekolah_lanjutan[' + number + '][nama_instansi]" value="" placeholder="Nama Instansi">' +
          '</div>' +
          '<div class="eight wide field">' +
            '<label>JURUSAN</label>' +
            '<input type="text" name="sekolah_lanjutan[' + number + '][jurusan]" value="" placeholder="Jurusan">' +
          '</div>' +
        '</div>' +
        '<div class="fields">' +
          '<div class="four wide field">' +
            '<label>TAHUN MASUK</label>' +
            '<select class="ui dropdown" name="sekolah_lanjutan[' + number + '][tahun_masuk]">' +
              tahun_option +
            '</select>' +
          '</div>' +
          '<div class="four wide field">' +
            '<label>TAHUN SELESAI</label>' +
            '<select class="ui dropdown" name="sekolah_lanjutan[' + number + '][tahun_selesai]">' +
              tahun_option +
            '</select>' +
          '</div>' +
          '<div class="four wide field">' +
            '<label>TINGKATAN</label>' +
            '<select class="ui dropdown" name="sekolah_lanjutan[' + number + '][tingkatan]">' +
              '<option value="D1">D1</option>' +
              '<option value="D2">D2</option>' +
              '<option value="D3">D3</option>' +
              '<option value="S1">S1</option>' +
              '<option value="S2">S2</option>' +
              '<option value="S3">S3</option>' +
            '</select>' +
          '</div>' +
          '<div class="four wide field">' +
            '<button class="ui button icon red" type="button" onclick="$(this).parent().parent().parent().remove()"><i class="ui icon x"></i></button>' +
          '</div>' +
        '</div>' +
      '</div>';
      $('.sekolah_lanjutan_parent').append(str);
    });

    $('#btn_tambah_belajar_bahasa_jepang').on('click', function(){
      var number = parseInt($('.belajar_bahasa_jepang_child').length);
      var str =
      '<div class="fields belajar_bahasa_jepang_child">' +
        '<div class="eight wide field">' +
          '<label>NAMA INSTANSI</label>' +
          '<input type="text" name="belajar_bahasa_jepang[' + number + '][nama_instansi]" value="" placeholder="Nama Instansi">' +
        '</div>' +
        '<div class="four wide field">' +
          '<label>TAHUN MASUK</label>' +
          '<select class="ui dropdown" name="belajar_bahasa_jepang[' + number + '][tahun_masuk]">' +
            tahun_option +
          '</select>' +
        '</div>' +
        '<div class="four wide field">' +
          '<label>TAHUN SELESAI</label>' +
          '<select class="ui dropdown" name="belajar_bahasa_jepang[' + number + '][tahun_selesai]">' +
            tahun_option +
          '</select>' +
        '</div>' +
        '<div class="field">' +
          '<button type="button" class="ui button icon red" onclick="$(this).parent().parent().remove()"><i class="ui icon x"></i></button>' +
        '</div>' +
      '</div>'

      $('.riwayat_bahasa_jepang_parent').append(str);
    });
  }

  function Keluarga()
  {
    var tahun_option = getYearSelectOption(100);
    var tanggal_option = "";
    var hubungan = "";
    var pekerjaan_select_input = "";
    var temp = '';
    $.ajax({
      async: false,
      url: 'getHubungan',
      type: 'POST',
      dataType: 'json',
      success: function(data)
      {
        $.each(data, function(k, v)
        {
          hubungan += "<option value='" + v.kd_hubungan + "'>" + v.hubungan + "</option>";
        });
      }
    });

    $.ajax({
      async: false,
      url: 'getAllPekerjaan',
      type: 'POST',
      contentType: 'application/json; utf-8',
      dataType: 'json',
      success: function(data)
      {
        $.each(data, function(k, v){
          if(k == 0)
          {
            pekerjaan_select_input += "<optgroup>"
          }
          else if (v.jenis_pekerjaan != temp && k != 0)
          {
            pekerjaan_select_input += "</optgroup><optgroup label=\'" + v.jenis_pekerjaan +"\'>"
            temp = v.jenis_pekerjaan;
          }
          else
          {
            pekerjaan_select_input += "<option value=\'" + v.kd_pekerjaan + "\'>" + v.pekerjaan + "</option>";
          }
        });
        pekerjaan_select_input += "</optgroup>";
      }
    });

    for(var i = 1; i <= 31; i++)
    {
      tanggal_option += '<option value = "' + i + '">' + i + '</option>';
    }

    $('#btn_tambah_saudara_kandung').on('click', function(){
      var number = parseInt($('.dynamic-field-child').length);
      var str = "";
      str +=
      '<div class="dynamic-field-child">' +
        '<h3 class="ui dividing header">SAUDARA KANDUNG ' + (number + 1) + '</h3>' +
        '<div class="fields">' +
          '<div class="four wide field">' +
            '<label>NAMA</label>' +
            '<input type="text" name="saudara_kandung[' + number + '][nama]" placeholder="Nama Saudara Kandung">' +
          '</div>' +
          '<div class="four wide field">' +
            '<label>TEMPAT LAHIR</label>' +
            '<input type="text" name="saudara_kandung[' + number + '][tempat_lahir]" placeholder="Kota Lahir">' +
          '</div>' +
          '<div class="five wide field">' +
            '<label>TANGGAL LAHIR</label>' +
            '<div class="fields">' +
              '<div class="five wide field">' +
                '<select class="ui dropdown" name="saudara_kandung[' + number + '][tanggal_lahir]">' +
                tanggal_option +
                '</select>' +
              '</div>' +
              '<div class="seven wide field">' +
                '<select class="ui dropdown" name="saudara_kandung[' + number + '][bulan_lahir]">' +
                  '<option value="" disabled="disabled" >Bulan</option>' +
                  '<option value="1">Januari</option>' +
                  '<option value="2">Februari</option>' +
                  '<option value="3">Maret</option>' +
                  '<option value="4">April</option>' +
                  '<option value="5">Mei</option>' +
                  '<option value="6">Juni</option>' +
                  '<option value="7">Juli</option>' +
                  '<option value="8">Agustus</option>' +
                  '<option value="9">September</option>' +
                  '<option value="10">Oktober</option>' +
                  '<option value="11">November</option>' +
                  '<option value="12">Desember</option>' +
                '</select>' +
              '</div>' +
              '<div class="seven wide field">' +
                '<select class="ui dropdown" name="saudara_kandung[' + number + '][tahun_lahir]">' +
                  tahun_option +
                '</select>' +
              '</div>' +
            '</div>' +
          '</div>' +
        '</div>' +
        '<div class="fields">' +
          '<div class="four wide field">' +
            '<label>HUBUNGAN</label>' +
            '<select class="ui dropdown" name="saudara_kandung[' + number + '][hubungan]">' +
            hubungan +
            '</select>' +
          '</div>' +
          '<div class="four wide field">' +
            '<label>PEKERJAAN</label>' +
            '<select class="ui dropdown search" name="saudara_kandung[' + number + '][pekerjaan]">' +
            pekerjaan_select_input +
            '</select>' +
          '</div>' +
          '<div class="four wide field">' +
            '<label>No Telepon / Handphone</label>' +
            '<div class="inline fields">' +
              '<input type="text" name="saudara_kandung[' + number + '][no_telepon]" palceholder="No Hp / Telepon">' +
              '<button class="ui button icon red" style="margin-left: 2em;" onclick="$(this).parent().parent().parent().parent().remove();"><i class="ui icon x"></i></button>' +
            '</div>' +
          '</div>' +
        '</div>' +
      '</div>';

      $('.dynamic-field-parent').append(str);
    });


    $('#btn_tambah_keluarga_pribadi').on('click', function(){
      var str_keluarga_pribadi = "";
      var number_keluarga_pribadi = 0;
      number_keluarga_pribadi = parseInt($('.keluarga-pribadi-child').length);
      str_keluarga_pribadi +=
      '<div class="keluarga-pribadi-child">' +
        '<h3 class="ui dividing header">KELUARGA PRIBADI ' + (number_keluarga_pribadi + 1) + '</h3>' +
        '<div class="fields">' +
          '<div class="four wide field">' +
            '<label>NAMA</label>' +
            '<input type="text" name="keluarga_pribadi[' + number_keluarga_pribadi + '][nama]" placeholder="Nama Keluarga Pribadi">' +
          '</div>' +
          '<div class="four wide field">' +
            '<label>TEMPAT LAHIR</label>' +
            '<input type="text" name="keluarga_pribadi[' + number_keluarga_pribadi + '][tempat_lahir]" placeholder="Kota Lahir">' +
          '</div>' +
          '<div class="five wide field">' +
            '<label>TANGGAL LAHIR</label>' +
            '<div class="fields">' +
              '<div class="five wide field">' +
                '<select class="ui dropdown" name="keluarga_pribadi[' + number_keluarga_pribadi + '][tanggal_lahir]">' +
                tanggal_option +
                '</select>' +
              '</div>' +
              '<div class="seven wide field">' +
                '<select class="ui dropdown" name="keluarga_pribadi[' + number_keluarga_pribadi + '][bulan_lahir]">' +
                  '<option value="" disabled="disabled" >Bulan</option>' +
                  '<option value="1">Januari</option>' +
                  '<option value="2">Februari</option>' +
                  '<option value="3">Maret</option>' +
                  '<option value="4">April</option>' +
                  '<option value="5">Mei</option>' +
                  '<option value="6">Juni</option>' +
                  '<option value="7">Juli</option>' +
                  '<option value="8">Agustus</option>' +
                  '<option value="9">September</option>' +
                  '<option value="10">Oktober</option>' +
                  '<option value="11">November</option>' +
                  '<option value="12">Desember</option>' +
                '</select>' +
              '</div>' +
              '<div class="seven wide field">' +
                '<select class="ui dropdown" name="keluarga_pribadi[' + number_keluarga_pribadi + '][tahun_lahir]">' +
                tahun_option +
                '</select>' +
              '</div>' +
            '</div>' +
          '</div>' +
        '</div>' +
        '<div class="fields">' +
          '<div class="four wide field">' +
            '<label>HUBUNGAN</label>' +
            '<select class="ui dropdown" name="keluarga_pribadi[' + number_keluarga_pribadi + '][hubungan]">' +
              hubungan +
            '</select>' +
          '</div>' +
          '<div class="four wide field">' +
            '<label>PEKERJAAN</label>' +
            '<select class="ui dropdown search" name="keluarga_pribadi[' + number_keluarga_pribadi + '][pekerjaan]">' +
            pekerjaan_select_input +
            '</select>' +
          '</div>' +
          '<div class="four wide field">' +
            '<label>No Telepon / Handphone</label>' +
            '<div class="inline fields">' +
              '<input type="text" name="keluarga_pribadi[' + number_keluarga_pribadi + '][no_telepon]" palceholder="No Hp / Telepon">' +
              '<button class="ui button icon red" style="margin-left: 2em;" onclick="$(this).parent().parent().parent().parent().remove();"><i class="ui icon x"></i></button>' +
            '</div>' +
          '</div>' +
        '</div>' +
      '</div>';

      $('.keluarga-pribadi-parent').append(str_keluarga_pribadi);
    });

    $('#btn_tambah_keluarga_dijepang').on('click', function(){
      var str_keluarga_dijepang = "";
      var number_keluarga_dijepang = 0;
      number_keluarga_dijepang = parseInt($('.keluarga-dijepang-child').length);
      str_keluarga_dijepang +=
      '<div class="keluarga-dijepang-child">' +
        '<h3 class="ui dividing header">KELUARGA DIJEPANG ' + (number_keluarga_dijepang + 1) + '</h3>' +
        '<div class="fields">' +
          '<div class="four wide field">' +
            '<label>NAMA</label>' +
            '<input type="text" name="keluarga_dijepang[' + number_keluarga_dijepang + '][nama]" placeholder="Nama Keluarga Pribadi">' +
          '</div>' +
          '<div class="four wide field">' +
            '<label>PERUSAHAAN / SEKOLAH</label>' +
            '<input type="text" name="keluarga_dijepang[' + number_keluarga_dijepang + '][perusahaan]" placeholder="Nama Perusahaan / Sekolah">' +
          '</div>' +
          '<div class="five wide field">' +
            '<label>TANGGAL LAHIR</label>' +
            '<div class="fields">' +
              '<div class="five wide field">' +
                '<select class="ui dropdown" name="keluarga_dijepang[' + number_keluarga_dijepang + '][tanggal_lahir]">' +
                tanggal_option +
                '</select>' +
              '</div>' +
              '<div class="seven wide field">' +
                '<select class="ui dropdown" name="keluarga_dijepang[' + number_keluarga_dijepang + '][bulan_lahir]">' +
                  '<option value="" disabled="disabled" >Bulan</option>' +
                  '<option value="1">Januari</option>' +
                  '<option value="2">Februari</option>' +
                  '<option value="3">Maret</option>' +
                  '<option value="4">April</option>' +
                  '<option value="5">Mei</option>' +
                  '<option value="6">Juni</option>' +
                  '<option value="7">Juli</option>' +
                  '<option value="8">Agustus</option>' +
                  '<option value="9">September</option>' +
                  '<option value="10">Oktober</option>' +
                  '<option value="11">November</option>' +
                  '<option value="12">Desember</option>' +
                '</select>' +
              '</div>' +
              '<div class="seven wide field">' +
                '<select class="ui dropdown" name="keluarga_dijepang[' + number_keluarga_dijepang + '][tahun_lahir]">' +
                tahun_option +
                '</select>' +
              '</div>' +
            '</div>' +
          '</div>' +
        '</div>' +
        '<div class="fields">' +
          '<div class="four wide field">' +
            '<label>HUBUNGAN</label>' +
            '<select class="ui dropdown" name="keluarga_dijepang[' + number_keluarga_dijepang + '][hubungan]">' +
            hubungan +
            '</select>' +
          '</div>' +
          '<div class="four wide field">' +
            '<label>KEWARGANEGARAAN</label>' +
            '<input type="text" name="keluarga_dijepang[' + number_keluarga_dijepang + '][kewarganegaraan]" placeholder="Pekerjaan">' +
          '</div>' +
          '<div class="four wide field">' +
            '<label>STATUS VISA</label>' +
            '<div class="inline fields">' +
              '<input type="text" name="keluarga_dijepang[' + number_keluarga_dijepang + '][status_visa]" palceholder="No Hp / Telepon">' +
              '<button class="ui button icon red" style="margin-left: 2em;" onclick="$(this).parent().parent().parent().parent().remove();"><i class="ui icon x"></i></button>' +
            '</div>'
          '</div>' +
        '</div>' +
      '</div>';

      $('.keluarga-dijepang-parent').append(str_keluarga_dijepang);
    });
  }

  function Finnish()
  {
    $('button#konfirmasi').on('click', function(e){
      swal({
        title: "Apakah data sudah benar?",
        type: "warning",
        showCancelButton: true,
        confirmButtonColor: "#DD6B55",
        confirmButtonText: "Ya",
        closeOnConfirm: false }, function()
        {
          window.location.href = 'register_confirmation';
        });
    });
  }

  function Member()
  {
    $('form#reset_password').form({
      inline: true,
      on: 'blur',
      fields: {
        new_password: {
          identifier: 'new_password',
          rules: [
            {
              type: 'empty',
              prompt: 'Password tidak boleh kosong'
            },
            {
              type: 'minLength[6]',
              prompt: 'Password minimal 6 huruf'
            },
          ]
        },
        new_password_confirmation: {
          identifier: 'new_password_confirmation',
          rules: [
            {
              type: 'empty',
              prompt: 'Konfirmasi password harap diisi'
            },
            {
              type: 'match[new_password]',
              prompt: 'Password harus sama'
            },
          ]
        },
      }
    });

    $('#btn_lamar').on('click', function(){
      var email = "";
      var kd_lowongan = $(this).data('lowongan');
      var token = $(this).data('token');
      $.ajax({
        url: '../checkIfLoggedIn',
        success: function(data)
        {
          if(data != 'not_logged_in')
          {
            email = data;
            $.ajax({
              url: '../../member/apply_lowongan',
              dataType: 'json',
              data: { email: email, kd_lowongan: kd_lowongan, csrf_token_name: token },
              type: 'POST',
              success: function(e){
                if(e == 'success')
                {
                  swal("Sukses", "Sukses mendaftar.", "success");
                }
                else
                {
                  swal("Error", "Terjadi kesalahan", "error");
                }
              }
            });
          }
          else
          {
            swal("", "Anda Belum terdaftar / login");
            window.location.href="../";
          }
        }
      });
    });
  }

  init.init = function (){
    Front();
    DataPribadi();
    PreparePhotoUpload();
    BahasaAsing();
    FormPekerjaan();
    Pendidikan();
    Keluarga();
    Finnish();
    Member();
  }

  return init;
}());


/////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////


var Rcr = (function(){
  var init = {};

  function Pemagang()
  {
    $('.data-table').DataTable();

    $('button#btn_search_filter').on('click', function(){
      $('.data-table-ssp').dataTable().fnDraw();
    });

    $('input#bisa_bahasa_jepang').on('change', function(){
      // $('.data-table-ssp').dataTable().fnDraw();
    });

    $('input#pernah_ke_jepang').on('change', function(){
      // $('.data-table-ssp').dataTable().fnDraw();
    });

    $('select#pendidikan').on('change', function(){
      // $('.data-table-ssp').dataTable().fnDraw();
    });

    $('input#checkbox_pria').on('change', function(e){
      if($(this).is(':checked') && $('input#checkbox_wanita').is(':checked'))
      {
        jenis_kelamin = 'ALL';
      }

      else if($(this).is(':checked') && !($('input#checkbox_wanita').is(':checked')))
      {
        jenis_kelamin = 'L'
      }
      else if(!$(this).is(':checked') && $('input#checkbox_wanita').is(':checked'))
      {
        jenis_kelamin = 'P';
      }

      else {
        jenis_kelamin = 'ALL';
      }
      // $('.data-table-ssp').dataTable().fnDraw();

    });

    $('input#checkbox_wanita').on('change', function(e){
      if($(this).is(':checked') && $('input#checkbox_pria').is(':checked'))
      {
        jenis_kelamin = 'ALL';
      }

      else if($(this).is(':checked') && !($('input#checkbox_pria').is(':checked')))
      {
        jenis_kelamin = 'P'
      }

      else {
        jenis_kelamin = 'ALL';
      }
      // $('.data-table-ssp').dataTable().fnDraw();
    });

    $('input#checkbox_pengalaman_kerja').on('change', function(e){
        if($(this).is(':checked'))
        {
          $('select#select_pengalaman_kerja').prop('disabled', false);
          // $('.data-table-ssp').dataTable().fnDraw();
        }
        else{
          $('select#select_pengalaman_kerja').prop('disabled', true);
          // $('.data-table-ssp').dataTable().fnDraw();
        }
    });

    $('select#select_pengalaman_kerja').on('change', function(e){
      // $('.data-table-ssp').dataTable().fnDraw();
    });


    $('button#test_external').on('click', function(){
      $('.ui.modal#test_external').modal('show');
      $('#datepicker_test_external').calendar({
        type: 'date',
        monthFirst: false,
        formatter: {
          date: function (date, settings) {
            if (!date) return '';
            var day = date.getDate();
            var month = date.getMonth() + 1;
            var year = date.getFullYear();
            return year + '-' + month + '-' + day;
          }
        }
      });
    });

    $('button#print_pemagang').on('click', function(){
      // $('input[name=page]').val($('a.paginate_button.current').data('dt-idx'));
      // $('input[name=limit]').val($('select[name=pilih_pemagang_length]').val());
      var tb = $('.data-table-ssp').DataTable();
      var kode_pemagang = [];
      tb.rows().every(function(rowIdx, tableLoop, rowLoop){
        var data = this.data();
        kode_pemagang.push(data[1]);
      });
      $('input[name=kd_pemagang]').val(kode_pemagang.join());
      $('form#export_to_excel').submit();
    });

    $('#btn_lulus').on('click', function(){
      $('.ui.modal#lulus').modal('show');
      $('#lulus_datepicker').calendar({
        type: 'date',
        monthFirst: false,
        formatter: {
          date: function (date, settings) {
            if (!date) return '';
            var day = date.getDate();
            var month = date.getMonth() + 1;
            var year = date.getFullYear();
            return year + '-' + month + '-' + day;
          }
        }
      });
    });

    $('#btn_blacklist').on('click', function(){
      $('.ui.modal#blacklist').modal('show');
      $('#datepicker_blacklist').calendar({
        type: 'date',
        monthFirst: false,
        formatter: {
          date: function (date, settings) {
            if (!date) return '';
            var day = date.getDate();
            var month = date.getMonth() + 1;
            var year = date.getFullYear();
            return year + '-' + month + '-' + day;
          }
        }
      });
    });

    $('#edit_riwayat_pekerjaan').on('click', function(){
      var kd_pemagang = $(this).data('kd');
      $.ajax({
        url: '../ajax_form_riwayat_pekerjaan/' + kd_pemagang,
        success: function(data)
        {
          $('#ajax_form .content').html(data);
          $('#ajax_form').modal('show');
        }
      });
    });

    $('#edit_riwayat_pendidikan').on('click', function(){
      var kd_pemagang = $(this).data('kd');
      $.ajax({
        url: '../ajax_form_riwayat_pendidikan/' + kd_pemagang,
        success: function(data)
        {
          $('#ajax_form .content').html(data);
          $('#ajax_form').modal('show');
        }
      });
    });

    $(document).on('click', '#btn_tambah_pekerjaan', function(){
      $.ajax({
        url: '../../../magang/getAllPekerjaan',
        contentType: 'application/json; utf-8',
        dataType: 'JSON',
        type: 'POST',
        success: function(data)
        {
          if(!$.isEmptyObject(data))
          {
            var number = $('.dynamic-field').length + 1;
            var temp = '';
            var pekerjaan_select_input = '';
            var tahun_option = getYearSelectOption();

            $.each(data, function(k, v){
              if(k == 0)
              {
                pekerjaan_select_input += "<optgroup>"
              }
              else if (v.jenis_pekerjaan != temp && k != 0)
              {
                pekerjaan_select_input += "</optgroup><optgroup label=\'" + v.jenis_pekerjaan +"\'>"
                temp = v.jenis_pekerjaan;
              }
              else
              {
                pekerjaan_select_input += "<option value=\'" + v.kd_pekerjaan + "\'>" + v.pekerjaan + "</option>";
              }
            });
            pekerjaan_select_input += "</optgroup>";

            $('.parent_pekerjaan').append(
              '<div class="fields dynamic-field" id="row' + number +'">' +
                '<div class="four wide field">' +
                  '<label>Nama Perusahaan</label>' +
                  '<input type="text" name="riwayat[' + (number - 1) + '][perusahaan]" value="">' +
                '</div>' +
                '<div class="three wide field">' +
                  '<label>Bidang</label>' +
                  '<select class="ui search dropdown" name="riwayat[' + (number - 1) + '][kd_pekerjaan]">' +
                    pekerjaan_select_input +
                  '</select>' +
                '</div>' +

                '<div class="field">' +
                  '<label>Mulai</label>' +
                  '<div class="inline fields">' +
                    '<div class="field">' +
                      '<select class="ui search dropdown" name="riwayat[' + (number - 1) + '][bulan_mulai]">' +
                        '<option value="" disabled="disabled" >Bulan</option>' +
                        '<option value="1">Januari</option>' +
                        '<option value="2">Februari</option>' +
                        '<option value="3">Maret</option>' +
                        '<option value="4">April</option>' +
                        '<option value="5">Mei</option>' +
                        '<option value="6">Juni</option>' +
                        '<option value="7">Juli</option>' +
                        '<option value="8">Agustus</option>' +
                        '<option value="9">September</option>' +
                        '<option value="10">Oktober</option>' +
                        '<option value="11">November</option>' +
                        '<option value="12">Desember</option>' +
                      '</select>' +
                    '</div>' +
                    '<div class="field">' +
                      '<select class="ui search dropdown" name="riwayat[' + (number - 1) + '][tahun_mulai]">' +
                        tahun_option +
                      '</select>' +
                    '</div>' +
                  '</div>' +
                '</div>' +

                '<div class="field">' +
                  '<label>Selesai</label>' +
                  '<div class="inline fields">' +
                    '<div class="field">' +
                      '<select class="ui search dropdown" name="riwayat[' + (number - 1) + '][bulan_selesai]">' +
                        '<option value="" disabled="disabled" >Bulan</option>' +
                        '<option value="1">Januari</option>' +
                        '<option value="2">Februari</option>' +
                        '<option value="3">Maret</option>' +
                        '<option value="4">April</option>' +
                        '<option value="5">Mei</option>' +
                        '<option value="6">Juni</option>' +
                        '<option value="7">Juli</option>' +
                        '<option value="8">Agustus</option>' +
                        '<option value="9">September</option>' +
                        '<option value="10">Oktober</option>' +
                        '<option value="11">November</option>' +
                        '<option value="12">Desember</option>' +
                      '</select>' +
                    '</div>' +
                    '<div class="field">' +
                      '<select class="ui search dropdown" name="riwayat[' + (number - 1) + '][tahun_selesai]">' +
                        tahun_option +
                      '</select>' +
                      '<span onclick="$(this).parent().parent().parent().parent().remove()"><i class="ui icon x"></i></span>' +
                    '</div>' +
                  '</div>' +
                '</div>'+
              '</div>'
            );
          }
        }
      });
    });

    $('#btn_edit_biodata').on('click', function(){
      $('.ui.modal#biodata_pemagang').modal('show');
      $('#datepicker_tanggal_lahir').calendar({
        type: 'date',
        monthFirst: false,
        formatter: {
          date: function (date, settings) {
            if (!date) return '';
            var day = date.getDate();
            var month = date.getMonth() + 1;
            var year = date.getFullYear();
            return year + '-' + month + '-' + day;
          }
        }
      });
      $()

    });

    $('#btn_clear_filter').on('click', function(){
      $('#filter_kd_pemagang').val('');
      $('#filter_nama').val('');
      $('#filter_provinsi').val('TIDAK').change();
      $('#filter_umur').val('');
      $('#filter_email').val('');
      $('#filter_no_hp').val('');
      $('#select_pengalaman_kerja').val('TIDAK').change();
      $('select#pendidikan').val('TIDAK').change();
      $('#filter_jurusan_pendidikan').val('TIDAK').change();
      $('#filter_pengalaman_kerja_custom').val('');
    });

    $('.menu.tabular .item').tab();
  }

  function Lowongan()
  {
    $('select.nilai_sabk').on('change', function(){
      var span = $(this).siblings();
      var nilai = $(this).val();
      var kd_pemagang = $(this).data('pemagang');
      var kd_lowongan = $(this).data('lowongan');
      $.ajax({
        url: '../setNilai',
        data: {
          nilai: nilai,
          kd_pemagang: kd_pemagang,
          kd_lowongan: kd_lowongan
        },
        dataType: 'json',
        type: 'POST',
        success: function(d){
          if(d != "failed")
          {
            span[0].innerHTML = d.nilai
          }
        }
      });
    });

    $('#btn_edit_nilai').on('click', function(){
      $('.nilai_sabk').show();
      $('.nilai').hide();
      $('#btn_save_nilai').show();
      $(this).hide();
    });

    $('#btn_save_nilai').on('click', function(){
      $('.nilai_sabk').hide();
      $('.nilai').show();
      $('#btn_edit_nilai').show();
      $(this).hide();
    });

    $('table.sortable').tablesort();
    $('#datepicker1').calendar({
      type: 'date',
      monthFirst: false,
      formatter: {
        date: function (date, settings) {
          if (!date) return '';
          var day = date.getDate();
          var month = date.getMonth() + 1;
          var year = date.getFullYear();
          return year + '-' + month + '-' + day;
        }
      }
    });

    $('#datepicker2').calendar({
      type: 'date',
      monthFirst: false,
      formatter: {
        date: function (date, settings) {
          if (!date) return '';
          var day = date.getDate();
          var month = date.getMonth() + 1;
          var year = date.getFullYear();
          return year + '-' + month + '-' + day;
        }
      }
    });

    $('.link_confirmation').on('click', function(e){
      e.preventDefault();
      confirmLink($(this).attr('href'));
    });

    var nama = '';
    var kode = '';
    $(document).on('click', 'table#pilih_pemagang tbody tr td:not(:last-child)', function(){
      nama = $(this).parent().children(':nth-child(3)').text();
      kode = $(this).parent().children(':nth-child(2)').text();
      if($('.list_pemagang_terpilih td:contains(' + nama + ')').length < 1)
      {
        $('.list_pemagang_terpilih').append(
          '<tr>' +
            '<td>' + ($('table.list_pemagang_terpilih tr').length + 1) + '<input type="hidden" name="kd_pemagang[]" value="' + kode +'" />' + '</td>' +
            '<td>' + nama + '</td>' +
            '<td><a href="#" onclick="$(this).parent().parent().remove()">Hapus</a></td>' +
          '</tr>'
        );
        $(window).scrollTop($('.list_pemagang_terpilih').position().top);
      }
      else{
        swal("", "Pemagang tersebut telah terpilih");
      }
    });

    $('button#btn_recruit').on('click', function(e){
      e.preventDefault();
      swal({
        title: "Konfirmasi",
        text: "Apakah anda yakin?",
        showCancelButton: true,
        confirmButtonColor: "#cc3f44",
        confirmButtonText: "YA",
        closeOnConfirm: false
      }, function(){
        if ($('.list_pemagang_terpilih tr').length > 0) {
          $('form#recruit').submit();
        }
      });
    });

    $('button.btn_hapus_recruit').on('click', function(){
      var row = $(this).parent().parent();
      var url = $(this).data('url');
      var data = {
        kd_pemagang: $(this).data('pemagang'),
        kd_lowongan: $(this).data('lowongan')
      };
      swal({
        title: "Konfirmasi",
        text: "Apakah anda yakin hapus pemagang dari lowongan?",
        showCancelButton: true,
        confirmButtonColor: "#cc3f44",
        confirmButtonText: "YA",
        closeOnConfirm: false
      }, function(){
        $.ajax({
          url: url,
          data: data,
          type: 'POST',
          success: function(data){
            if(data == 'true')
            {
              row.remove();
              swal('Sukses', 'Sukses hapus pemagang dari lowongan.', 'success');
            }
          }
        });
      });
    });

    $('button.edit_nilai').on('click', function(){
      $('.ui.small.modal').modal('show');
      $('.ui.small.modal .header').text($(this).parent().siblings(':nth-child(2)').text());
      $('input[name=kd_pemagang]').val($(this).data('kd'));
      $('select[name=nilai]').val($(this).data('nilai'));
      $('textarea[name=keterangan]').val($(this).parent().siblings(':nth-child(4)').text());
    });
  }

  function Test()
  {
    $('button#tambah_tes_internal').on('click', function(){
      $('.ui.modal').modal('show');
      $('#datepicker_test_internal').calendar({
        type: 'date',
        monthFirst: false,
        formatter: {
          date: function (date, settings) {
            if (!date) return '';
            var day = date.getDate();
            var month = date.getMonth() + 1;
            var year = date.getFullYear();
            return year + '-' + month + '-' + day;
          }
        }
      });
    })
  }

  function Jurusan(){
    $('form#jurusan_pendidikan').form({
      inline: true,
      on: 'blur',
      fields: {
        nama_jurusan: {
          identifier: 'nama_jurusan',
          rules: [
            {
              type: 'empty',
              prompt: 'Nama Jurusan tidak boleh kosong'
            },
          ]
        },
      }
    });

    $(document).on('click', '.btn_edit_jurusan', function(){
      $('.ui.modal').modal('show');
      $('input[name=type]').val('EDIT');
      $('input[name=id_jurusan]').val($(this).data('id'));
      $('input[name=nama_jurusan]').val($(this).data('nama'));
      $('select[name=tingkat_pendidikan]').dropdown('set value', $(this).data('tp'));
    });

    $('button#btn_tambah_jurusan').on('click', function(){
      $('.ui.modal').modal('show');
      $('input[name=type]').val('ADD');
      $('input[name=id_jurusan]').val('');
      $('input[name=nama_jurusan]').val('');
      $('select[name=tingkat_pendidikan]').val('');
    });
  }

  function User()
  {
    $('form#user').form({
      inline: true,
      on: 'blur',
      fields: {
        username: {
          identifier: 'username',
          rules: [
            {
              type: 'empty',
              prompt: 'Username tidak boleh kosong'
            },
            {
              type: 'minLength[5]',
              prompt: 'Username harus lebih dari 5 huruf'
            }
          ]
        },

        password: {
          identifier: 'password',
          rules: [
            {
              type   : 'empty',
              prompt : 'Password harus diisi'
            },
            {
              type   : 'minLength[6]',
              prompt : 'Password harus melebihi {ruleValue} karakter'
            }
          ]
        },

        passconf: {
          identifier: 'passconf',
          rules: [
            {
              type   : 'empty',
              prompt : 'Password harus diisi'
            },
            {
              type: 'match[password]',
              prompt: 'Password harus sama'
            }
          ]
        }
      }
    });


    $('input.add_module_checkbox').on('change', function(){
      var username = $(this).parent().parent().parent().data('username');
      var module_id = $(this).val();
      var csrf = $(this).parent().parent().parent().data('token');
      if ($(this).is(':checked')) {
        $.ajax({
          url: '../add_authorization',
          type: 'post',
          dataType: 'json',
          data: { username: username, module_id: module_id, csrf_test_name: csrf },
          success: function(data){
            if(data != 1)
            {
              swal("Error", "Gagal tambah otorisasi", "error");
            }
          }
        });
      }
      else
      {
        $.ajax({
          url: '../delete_authorization',
          type: 'post',
          dataType: 'json',
          data: {
            username: username,
            module_id: module_id,
            csrf_test_name: csrf
          },
          success: function(data){
            if(data != 1)
            {
              swal("Error", "Gagal tambah otorisasi", "error");
            }
          }
        });
      }
    });

    $('input.set_authorization').on('click', function(){
      var username = $(this).parent().parent().data('username');
      var module_id = $(this).parent().parent().data('module');
      var auth = $(this).data("auth");
      var csrf = $(this).parent().parent().data('token');
      if($(this).is(':checked'))
      {
        $.ajax({
          url: '../set_authorization',
          type: 'post',
          dataType: 'json',
          data: {
            username: username,
            module_id: module_id,
            auth: auth,
            status: 1,
            csrf_test_name: csrf
          },
          success: function(data){
            if(data != 1)
            {
              swal("Error", "Gagal tambah otorisasi", "error");
            }
          }
        });
      }
      else
      {
        $.ajax({
          url: '../set_authorization',
          type: 'post',
          dataType: 'json',
          data: {
            username: username,
            module_id: module_id,
            auth: auth,
            status: 0,
            csrf_test_name: csrf
          },
          success: function(data){
            if(data != 1)
            {
              swal("Error", "Gagal tambah otorisasi", "error");
            }
          }
        });
      }

    });
  }

  function JenisPekerjaan()
  {
    $('.pekerjaan-tree').jstree();

    $(document).on('click', '.btn_edit_pekerjaan', function(){
      var kd_pekerjaan = $(this).data('pekerjaan');
      var kd_jenis_pekerjaan = $(this).data('jp');
      var nama_pekerjaan = $(this).data('nama_pekerjaan');
      var nama_pekerjaan_jepang = $(this).data('nama_pekerjaan_jepang');

      $('.modal#pekerjaan').modal('show');
      $('.modal#pekerjaan .header').html('<h2>EDIT PEKERJAAN</h2>');
      $('.modal#pekerjaan .content form').attr('action', '../Jenis_Pekerjaan/edit');
      $('input[name=kd_pekerjaan').val(kd_pekerjaan);
      $('select[name=kd_jenis_pekerjaan]').val(kd_jenis_pekerjaan);
      $('input[name=pekerjaan]').val(nama_pekerjaan);
      $('input[name=pekerjaan_jp').val(nama_pekerjaan_jepang);
    });

    $('#btn_tambah_pekerjaan').on('click', function(){
      $('.modal#pekerjaan').modal('show');
      $('.modal#pekerjaan .header').html('<h2>TAMBAH PEKERJAAN</h2>');
      $('.modal#pekerjaan .content form').attr('action', '../Jenis_Pekerjaan/add');
      $('input[name=kd_pekerjaan').val('');
      $('select[name=kd_jenis_pekerjaan]').val(kd_jenis_pekerjaan);
      $('input[name=pekerjaan]').val('');
      $('input[name=pekerjaan_jp').val('');
    });
  }

  init.init = function(){
    Pemagang();
    Lowongan();
    Test();
    Jurusan();
    User();
    JenisPekerjaan();
  }

  return init;
}());

function confirmLink(link)
{
  var bool = false;
    swal({
      title: "Konfirmasi",
      text: "Apakah anda yakin?",
      showCancelButton: true,
      confirmButtonColor: "#cc3f44",
      confirmButtonText: "YA",
      closeOnConfirm: false
    }, function(){
      window.location.href = link
    });
}

function toggleInput(id)
{
  $('#' + id).toggle();
}

function getYearSelectOption(jumlah_tahun = 40)
{
  var tahun = parseInt(new Date().getFullYear());
  var tahun_option = "";
  for(var i = tahun; i > tahun - jumlah_tahun; i--)
  {
    tahun_option += "<option value=\'" + i + "\'>" + i + "</option>";
  }

  return tahun_option;
}
